<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Carbon\Carbon;

use App\Models\Collection;

class CollectionExport implements FromCollection, WithHeadings
{
	public $date_from;
	public $date_to;

    public function __construct(string $date_from, string $date_to)
    {
        $this->date_from = $date_from;
        $this->date_to = $date_to;
    }
    /**
    * Headings
    */
    public function headings(): array
    {
        return [
            'client_name',
            'order_number',
            'site_name',
            'updated_bill_of_quantities',
            'billing_progress_percent',
            'invoice_date',
            'invoice_number',
            'or_number',
            'invoice_amount',
            'amount_wo_vat',
            'vat_amount',
            'ewt_amount',
            'net_amount',
            'payment_due',
            'insurer',
            'insurance_policy',
            'policy_number',
            'coverage_due',
        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $collections = Collection::select('collections.*', 'clients.client_name', 'job_order_details.site_name')
            ->selectRaw("concat(job_orders.order_number,'(',job_orders.po_number, ')') as order_number")
            ->selectRaw("FORMAT(job_order_details.updated_bill_of_quantities, 2) as updated_bill_of_quantities")            
            ->leftJoin('clients', 'collections.client_id', '=', 'clients.id')
            ->leftJoin('job_orders', 'collections.job_order_id', '=', 'job_orders.id')
            ->leftJoin('job_order_details', 'collections.job_order_details_id', '=', 'job_order_details.id')
            ->whereBetween('collections.invoice_date', [$this->date_from, $this->date_to])
            ->get();

        $responseData = [];
        foreach ($collections as $collection)
        {
            $responseData[] = [
                'client_name' => $collection->client_name,
                'order_number' => $collection->order_number,
                'site_name' => $collection->site_name,
                'updated_bill_of_quantities' => $collection->updated_bill_of_quantities,
                'billing_progress_percent' => $collection->billing_progress_percent,
                'invoice_date' => $collection->invoice_date,
                'invoice_number' => $collection->invoice_number,
                'or_number' => $collection->or_number,
                'invoice_amount' => $collection->invoice_amount,
                'amount_wo_vat' => $collection->amount_wo_vat,
                'vat_amount' => $collection->vat_amount,
                'ewt_amount' => $collection->ewt_amount,
                'net_amount' => $collection->net_amount,
                'payment_due' => $collection->payment_due,
                'insurer' => $collection->insurer_details->name,
                'insurance_policy' => $collection->policy_details->name,
                'policy_number' => $collection->policy_number,
                'coverage_due' => $collection->coverage_due,
            ];
        }

        return collect($responseData);
    }
}
