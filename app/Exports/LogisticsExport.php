<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Carbon\Carbon;

use App\Models\LogisticMaster;

class LogisticsExport implements FromCollection, WithHeadings
{
    /**
    * Headings
    */
    public function headings(): array
    {
        return [
            'sku',
            'item_name',
            'uom',
            'category',
            'stock',
            'unit_price'
        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $Logistic_master = LogisticMaster::select('logistic_masters.id', 'logistic_masters.sku', 'logistic_masters.item_name', 'logistic_masters.unit_of_measurement', 'logistic_masters.category_id', 'categories.name as category_name', 'logistic_masters.in_stock', 'logistic_masters.created_at', 'logistic_masters.in_stock')
            ->join('categories', 'logistic_masters.category_id', '=', 'categories.id')
            ->orderBy('logistic_masters.item_name', 'ASC')
            ->get();

        $responseData = [];
        foreach ($Logistic_master as $Logistic)
        {
            $responseData[] = [
                'sku' => $Logistic->sku, 
                'item_name' => $Logistic->item_name, 
                'uom' => $Logistic->unit_of_measurement, 
                'category' => $Logistic->category_name, 
                'stock' => ($Logistic->in_stock) ? $Logistic->in_stock : 0,
                'unit_price' => $Logistic->unit_price
            ];
        }

        return collect($responseData);
    }
}
