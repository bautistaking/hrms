<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Carbon\Carbon;

use App\Models\EmployeeRequest;

class OfficialBusinessExport implements FromCollection, WithHeadings
{
	public $employee_id;
	public $date_from;
	public $date_to;

    public function __construct(int $id, int $category_id, string $date_from, string $date_to)
    {
        $this->employee_id = $id;
        $this->category_id = $category_id;
        $this->date_from = $date_from;
        $this->date_to = $date_to;
    }
	/**
    * Headings
    */
	public function headings(): array
    {
        return [
            'EMPLOYEE ID',
            'EMPLOYEE NAME',
            'DATE',
            'TIME START',
            'TIME END',
            'TOTAL HOURS',
            'REQUEST_TYPE',
            'STATUS',
            'REMARKS',
        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $overtime_requests = EmployeeRequest::when($this->employee_id, function($query){
            return $query->where('employee_id', $this->employee_id);
        })
        ->when($this->category_id, function($query){
            return $query->where('employee.category_id', $this->category_id);
        })
        ->join('employee', 'requests.employee_id', '=', 'employee.id')
        ->whereBetween('date_from', [$this->date_from, $this->date_to])
        ->where('request_type', 'OB')
        ->get();

        $responseData = [];
        foreach ($overtime_requests as $overtime_request)
        {
            $responseData[] = [
                'employee_id' => $overtime_request->id_number, 
	            'employee_name' => $overtime_request->employee_name, 
	            'date_from' => $overtime_request->date_from, 
	            'time_start' => date("G:i:s", strtotime($overtime_request->time_start)), 
	            'time_end' => date("G:i:s", strtotime($overtime_request->time_end)), 
	            'number_of_hours' => $overtime_request->number_of_hours,
                'request_name' => $overtime_request->request_name,
                'approved' => $overtime_request->approved_status,
                'description' => $overtime_request->description
            ];
        }

        return collect($responseData);
    }
}
