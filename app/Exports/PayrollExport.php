<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

use App\Models\Payroll;
use App\Models\Loan;

class PayrollExport implements FromCollection, WithHeadings
{
    public $payperiod_id;

    public function __construct(int $id)
    {
        $this->payperiod_id = $id;
    }
    
	/**
    * Headings
    */
	public function headings(): array
    {
        return [
            'EMPLOYEE ID',
            'EMPLOYEE NAME',
            'MONTHLY RATE',
            'DAILY WAGE w/o COLA',
            'NO. OF DAYS',
            'NO. REGULAR/SPECIAL HOLIDAYS',     
            'APPROVED LEAVES',
            'COLA',
            'OTHER INCOME',
            'TOTAL BASIC SALARY',
            'OT No of Hrs',
            'OT Rate/Hr',
            'OT Total Amount',
            'ND No of Hrs',
            'ND Rate/Hr',
            'ND Total Amount',
            'SD No of Hrs',
            'SD 130%',
            'SD Total Amount',
            'SD OT No of Hrs',
            'SD OT 169%',
            'SD OT Total Amount',
            'RH No of Hrs',
            'RH Rate/Hr	',
            'RH Total Amount',
            'RH OT No of Hrs',
            'RH OT Rate/Hr	',
            'RH OT Total Amount',
            'TOTAL OVER TIME',
            'LATE No of Hrs',
            'LATE Rate/Hr	',
            'LATE Total Amount',
            'TOTAL GROSS SALARY',
            'W/TAX',
            'SSS PREM',
            'SSS MPF',
            'PHIC PREM',
            'PAG IBIG PREM',
            'TOTAL LOANS',
            'SSS SALARY LOAN',
            'SSS CALAMITY LOAN',
            'PAGIBIG SALARY LOAN',
            'COMPANY SALARY LOAN',
            'PAGIBIG CALAMITY LOAN',
            'OTHER DEDUCTION',
            'TOTAL DEDUCTION',
            'NET SALARY',
            'TAXABLE INCOME',
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $payrolls = Payroll::where('pay_period', $this->payperiod_id)->get();

        $responseData = [];

        foreach ($payrolls as $payroll)
        {
            $loans_data = [];
            $loans = [];

            if($payroll->total_loans > 0) {
                $loans = Loan::where('employee_id',$payroll->employee_id)
                ->wherecolumn('deduction_count', '<', 'total_deductions')
                ->get();
            }

            if(count($loans) > 0) {
                foreach ($loans as $loan) {
                    $loans_data[$loan['loan_type_id']] = $loan['monthly_amortization'];
                }
            }

            $responseData[] = [
                'employee_id' => $payroll->employee['id_number'], 
	            'employee_name' => $payroll->employee['name'], 
	            'monthly_rate' => $payroll->employee['monthly_rate'], 
	            'daily_wage_wo_cola' => $payroll->employee['daily_wage_wo_cola'], 
	            'no_of_days' => $payroll->number_of_days, 
	            'no_regular_special_holidays' => $payroll->regular_holidays, 
	            'approved_leaves' => $payroll->approved_leaves, 
	            'cola' => $payroll->total_cola, 
	            'onther_income' => $payroll->other_income, 
	            'total_basic_salary' => $payroll->total_basic_salary, 
	            'ot_no_of_hours' => $payroll->over_time_no_of_hrs, 
	            'ot_rate_hour' => $payroll->over_time_rate_per_hrs, 
	            'ot_total_amount' => $payroll->over_time_total_amount, 
	            'nd_no_of_hours' => $payroll->night_differential_no_of_hrs, 
	            'nd_rate_hours' => $payroll->night_differential_rate_per_hrs, 
	            'nd_total_amount' => $payroll->night_differential_total_amount, 
	            'sd_no_of_hours' => $payroll->special_day_no_of_hrs, 
	            'sd_130' => $payroll->special_day_rate_per_hrs, 
	            'sd_total_amount' => $payroll->special_day_total_amount, 
	            'sd_ot_no_of_hours' => $payroll->special_day_over_time_no_of_hrs, 
	            'sd_ot_169' => $payroll->special_day_over_time_rate_per_hrs, 
	            'sd_ot_total_amount' => $payroll->special_day_over_time_total_amount, 
	            'rh_no_of_hours' => $payroll->holiday_day_no_of_hrs, 
	            'rh_rate_hours	' => $payroll->holiday_day_rate_per_hrs, 
	            'rh_total_amount' => $payroll->holiday_day_total_amount, 
	            'rh_ot_no_of_hours' => $payroll->holiday_day_over_time_no_of_hrs, 
	            'rh_ot_rate_hours	' => $payroll->holiday_day_over_time_rate_per_hrs, 
	            'rh_ot_total_amount' => $payroll->holiday_day_over_time_total_amount, 
	            'total_over_time' => $payroll->total_over_time, 
	            'late_no_of_hours' => $payroll->late_no_of_minutes, 
	            'late_rate_hours	' => $payroll->late_rate_per_hrs, 
	            'late_total_amount' => $payroll->late_total_amount, 
	            'total_gross_salary' => $payroll->total_gross_salary, 
	            'w_tax' => $payroll->withholding_tax, 
	            'sss_prem' => $payroll->sss_prem, 
	            'sss_mpf' => $payroll->sss_mpf, 
	            'phic_prem' => $payroll->philhealth_prem, 
	            'pag_ibig_prem' => $payroll->pagibig_prem, 
	            'total_loans' => $payroll->total_loans,
	            'sss_salary_loan' => (count($loans_data) && isset($loans_data[1])) ? $loans_data[1] : 0,
	            'sss_calamity_loan' => (count($loans_data) && isset($loans_data[2])) ? $loans_data[2] : 0,
	            'pagibig_salary_loan' => (count($loans_data) && isset($loans_data[3])) ? $loans_data[3] : 0,
	            'company_salary_loan' => (count($loans_data) && isset($loans_data[4])) ? $loans_data[4] : 0,
	            'pagibig_calamity_loans' => (count($loans_data) && isset($loans_data[5])) ? $loans_data[5] : 0,
                'other_deductions' => $payroll->other_deductions, 
	            'total_deductions' => $payroll->total_deductions, 
	            'net_salary' => $payroll->net_salary, 
	            'taxable_income' => $payroll->taxable_income
            ];
        }

        return collect($responseData);
    }
}
