<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

use App\ViewModels\SiteExpensesReportViewModel;

class SiteExpensesReport implements FromCollection, WithHeadings
{
	public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
    * Headings
    */
	public function headings(): array
    {
        return [
            'SITE NAME',
            'SITE DETAILS',
            'ASSIGNED PO NUMBER',
            'CATEGORY',
            'BOQ AMOUNT (VAT INC)',
            'PROJECT COST',
            'RUNNING EXPENSES',
            'RUNNING BALANCE',
            'OTHER EXPENSES',
            'PAYMENT TYPE',
            'AMOUNT'
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $responseData = [];
        foreach ($this->data as $row)
        {
            foreach($row->sites_expenses as $data) {
                $responseData[] = [
                    'site_name' => $row->site_name, 
                    'site_details' => $row->site_details, 
                    'po_number' => $row->po_number, 
                    'category' => $row->category, 
                    'updated_bill_of_quantities' => $row->updated_bill_of_quantities, 
                    'badget' => $row->badget, 
                    'running_expenses' => $row->running_expenses, 
                    'balance_amount' => $row->balance_amount, 
                    'expenses' => $row->expenses, 
                    'request_name' => $data->request_name, 
                    'total' => $data->total
                ];
            }

        }

        return collect($responseData);
    }
}
