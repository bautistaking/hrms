<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Carbon\Carbon;

use App\Models\Supplier;

class SupplierExport implements FromCollection, WithHeadings
{
    /**
    * Headings
    */
    public function headings(): array
    {
        return [
            'name',
            'address',
            'tin',
            'contact person',
            'contact number',
            'email'
        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $suppliers_master = Supplier::get();

        $responseData = [];
        foreach ($suppliers_master as $supplier)
        {
            $responseData[] = [
                'name' => $supplier->name, 
                'address' => $supplier->address, 
                'tin' => $supplier->tin, 
                'contact_person' => $supplier->contact_person, 
                'contact_number' => $supplier->contact_number,
                'email' => $supplier->email
            ];
        }

        return collect($responseData);
    }
}
