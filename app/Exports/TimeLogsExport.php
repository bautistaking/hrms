<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

use App\Models\AttendanceLog;

class TimeLogsExport implements FromCollection, WithHeadings
{
	public $employee_id;
	public $date_from;
	public $date_to;

    public function __construct(int $id, string $date_from, string $date_to)
    {
        $this->employee_id = $id;
        $this->date_from = $date_from;
        $this->date_to = $date_to;
    }
	/**
    * Headings
    */
	public function headings(): array
    {
        return [
            'EMPLOYEE ID',
            'EMPLOYEE NAME',
            'DATE',
            'TIME IN',
            'TIME IN LOCATION',     
            'TIME OUT',
            'TIME OUT LOCATION',
            'DURATION'
        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $attendance_logs = AttendanceLog::when($this->employee_id, function($query){
            return $query->where('employee_id', $this->employee_id);
        })
        ->whereBetween('date', [$this->date_from, $this->date_to])
        ->get();

        $responseData = [];
        foreach ($attendance_logs as $attendance_log)
        {
            $responseData[] = [
                'employee_id' => $attendance_log->id_number, 
	            'employee_name' => $attendance_log->name, 
	            'date' => $attendance_log->date, 
	            'time_in' => $attendance_log->time_in, 
	            'time_in_location' => $attendance_log->time_in_location, 
	            'time_out' => $attendance_log->time_out, 
	            'time_out_location' => $attendance_log->time_out_location, 
	            'duration' => $attendance_log->duration
            ];
        }

        return collect($responseData);
    }
}
