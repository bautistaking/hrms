<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Models\Approver;
use App\Models\Requestor;
use App\ViewModels\ApproverRequestor;

class ApproverController extends ApiBaseController implements ApproverControllerInterface
{
    /************************************
    * 			APPROVER 	 			*
    ************************************/
    public function getDetails($id)
	{
		try
        {
        	$approver = ApproverRequestor::find($id);
            return $this->response($approver, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(Request $request)
    {
        try
        {
            $approver = Approver::updateOrCreate([
                'employee_id' => $request->approver_id['id']
            ]);
            if($approver) {
                foreach ($request->requestor_ids as $requestor) {
                    $requestor_data = [
                        'approver_id' => $approver->employee_id,
                        'requestor_id' => $requestor['employee']['id']
                    ];
                    Requestor::updateOrCreate($requestor_data);
                }                
            }

            return $this->response($approver, 'Successfully Created/Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return $e->getMessage();
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function delete($id)
    {
        try
        {
            $approver = Approver::find($id);
            $approver->delete();
            return $this->response($approver, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $approvers = Approver::when(request('search'), function($query){
                return $query->where('name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('description', 'LIKE', '%' . request('search') . '%');
            })
            ->latest()
            ->paginate(request('perPage'));
            return $this->responsePaginate($approvers, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function deleteRequestor(Request $request)
    {
        try
        {
            $requestor = Requestor::where('requestor_id', $request->requestor['id'])->delete();
            return $this->response($requestor, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

}
