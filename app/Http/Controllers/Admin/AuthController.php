<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Controllers\AppBaseController as ApiBaseController;

use Illuminate\Support\Facades\DB;
use Laravel\Passport\Client as OClient; 
use App\Models\User;

class AuthController extends ApiBaseController implements AuthControllerInterface
{
    /****************************
    * 			AUTH 			*
    ****************************/
	public function login(LoginRequest $request)
	{ 
		try
    	{
            $user = User::whereEmail(request('email'))->first();
            if(!$user)
                return response([
                    'message' => 'Wrong email or password',
                    'status' => false,
                    'status_code' => $this->unauthorizedStatus,
                ], $this->unauthorizedStatus);

            $client = OClient::where('password_client', 1)->first();

            $data = [
                'grant_type' => 'password',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
                'providers' => 'users',
                'username' => request('email'),
                'password' => User::getSalt(request('email')).env("PEPPER_HASH").request('password'),
            ];

            $request = Request::create('/oauth/token', 'POST', $data);
            
            $response = app()->handle($request);

            // Check if the request was successful
            if ($response->getStatusCode() != 200)
                return response([
                    'message' => 'Wrong email or password',
                    'status' => false,
                    'status_code' => $this->unauthorizedStatus,
                ], $this->unauthorizedStatus);

            $token = json_decode($response->getContent());

            $data = [
                'token' => $token->access_token,
                'refresh_token' => $token->refresh_token,
             ];

            return $this->response($data, 'Successfully Loged!', $this->successStatus);
    	}
    	catch (\Exception $e) 
        {
    		 return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
    	}
	}

    public function refresh(Request $request)
    {
        try
        {  
            $client = DB::table('oauth_clients')
                        ->where('password_client', true)
                        ->first();
            $data = [                
                'grant_type' => 'refresh_token',
                'refresh_token' => $request->refresh_token,
                'client_id' => $client->id,
                'client_secret' => $client->secret,
                'scope' => ''
            ];

            $request = Request::create('/oauth/token', 'POST', $data);
            $response = app()->handle($request);

            $token = json_decode($response->getContent());

            $data = [
                'token' => $token->access_token,
                'refresh_token' => $token->refresh_token,
             ];

            return $this->response($data, 'Token successfully refresh!', $this->successStatus);
        }
        catch (\Exception $e) 
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function logout(Request $request)
    {
        try
        {
            $request->user()->token()->revoke();
            $request->user()->tokens->each(function ($token,$key) {
                $token->delete();
            });

            return $this->response(null, 'Successfully logged out!', $this->successStatus);
        }
        catch (\Exception $e) 
        {
             return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
