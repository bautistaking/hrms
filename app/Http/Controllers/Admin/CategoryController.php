<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Imports\CategoryImport;
use App\Exports\CategoryExport;
use App\Models\Category; 

use Storage;

class CategoryController extends ApiBaseController implements CategoryControllerInterface
{
    /********************************
    * 			CATEGORIES 			*
    *********************************/
    public function getCategory($id)
	{
		try
        {
        	$category = Category::find($id);
            return $this->response($category, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(Request $request)
    {
        try
        {
            $category = new Category;
            $category->name = $request->name;
            $category->slug_name = $request->slug_name;
            $category->description = $request->description;
            $category->module = $request->module;
            $category->save();

            return $this->response($category, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(Request $request)
    {
        try
        {
            $category = Category::find($request->id);
            $category->name = $request->name;
            $category->slug_name = $request->slug_name;
            $category->description = $request->description;
            $category->module = $request->module;
            $category->save();

            return $this->response($category, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $category = Category::find($id);
            $category->delete();
            return $this->response($category, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $category = Category::where('module', $request->module)
            ->when(request('search'), function($query){
                return $query->where('name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('description', 'LIKE', '%' . request('search') . '%');
            })
            ->latest()
            ->paginate(request('perPage'));
            return $this->responsePaginate($category, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function all(Request $request)
    {
        try
        {
            $category = Category::where('module', $request->module)->orderBy('name')->get();
            return $this->response($category, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }    

    public function batchUpload(Request $request)
    {
        try
        {
            Excel::import(new CategoryImport, $request->file('file'));
            return $this->response(true, 'Successfully Retreived!', $this->successStatus);  
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function downloadCsv()
    {
        try
        {
            $directory = 'public/export/category/';
            $files = Storage::files($directory);
            foreach ($files as $file) {
                Storage::delete($file);
            }

            $filename = "Categories-".date("Y-m-d").".csv";
            // Store on default disk
            Excel::store(new CategoryExport, $directory.$filename);

            $data = [
                'filepath' => '/Storage/export/category/'.$filename,
                'filename' => $filename
            ];
            
            if(Storage::exists($directory.$filename))
                return $this->response($data, 'Successfully Retreived!', $this->successStatus); 

            return $this->response(false, 'Successfully Retreived!', $this->successStatus); 
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

}
