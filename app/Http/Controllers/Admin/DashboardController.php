<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Models\CatalogRequest;
use App\Models\PaymentRequest;

class DashboardController extends ApiBaseController implements DashboardControllerInterface
{
    /***********************************
    *           DASHBOARD              *
    ***********************************/
    public function getCatalogRequest()
    {
        try{
            $data = [
                'verified' => CatalogRequest::where('status_id', 4)->count(),
                'approved' => CatalogRequest::where('status_id', 7)->count(),
                'for_canvassing' => CatalogRequest::whereIn('status_id', [2,3])->count(),
                'canceled' => CatalogRequest::whereIn('status_id', [8,9])->count(),
            ];
            return $this->response($data, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getPaymentRequest()
    {
        try{
            $data = [
                'endorsed' => PaymentRequest::where('status_id', 6)->count(),
                'approved' => PaymentRequest::where('status_id', 7)->count(),
                'canceled' => PaymentRequest::whereIn('status_id', [8,9])->count(),
            ];
            return $this->response($data, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
