<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

interface DesignationControllerInterface
{
    /**
	 * @param integer $id
	 * @return Response
	 * @SWG\Get(
	 *      path="/designation/{id}",
	 *      summary="Get designation details",
	 *      tags={"Admin Dashboard - Designation"},
	 *      description="Get designation details",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="Designation Id",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function getDetails($id);

	/**
	 * @param string $name
	 * @param string $description
	 * @return Response
	 * @SWG\Post(
	 *      path="/designation/store",
	 *      summary="Store / Save new designation",
	 *      tags={"Admin Dashboard - Designation"},
	 *      description="Store / Save new designation",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="name",
	 *          description="Designation name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="description",
	 *          description="Description",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */  
    public function store(Request $request);

    /**
	 * @param integer $id
	 * @param string $name
	 * @param string $description
	 * @return Response
	 * @SWG\Put(
	 *      path="/designation/update",
	 *      summary="Update / Save Designation",
	 *      tags={"Admin Dashboard - Designation"},
	 *      description="Update / Save Designation",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="Id",
	 *          type="integer",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="name",
	 *          description="Designation name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="description",
	 *          description="Description",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function update(Request $request);

    /**
	 * @param string $moduleId
	 * @return Response
	 * @SWG\Get(
	 *      path="/designation/delete/{id}",
	 *      summary="Delete Designation",
	 *      tags={"Admin Dashboard - Designation"},
	 *      description="Delete Designation",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="Designation Id",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function delete($id);

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/designation/list",
	 *      summary="List of designation with pagination",
	 *      tags={"Admin Dashboard - Designation"},
	 *      description="List of designation with pagination",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function list(Request $request);

	/**
	 * @return Response
	 * @SWG\Get(
	 *      path="/designation/all",
	 *      summary="Get all designation",
	 *      tags={"Admin Dashboard - Designation"},
	 *      description="Get all designation",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function all();
}
