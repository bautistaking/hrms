<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Services\Helpers\PasswordHelper;
use Maatwebsite\Excel\Facades\Excel;

use App\Models\Employee;
use App\ViewModels\EmployeeProfile;

use App\Imports\EmployeeImport;

class EmployeeController extends ApiBaseController implements EmployeeControllerInterface
{
    /********************************
    * 			EMPLOYEE 			*
    ********************************/
    public function getDetails($id)
	{
		try
        {
        	$employee = EmployeeProfile::find($id);
            return $this->response($employee, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(Request $request)
    {
        try
        {
            $salt = bcrypt(PasswordHelper::generateSalt());

            if(Employee::where('email', $request->email)->first())
                return response([
                    'message' => 'Email already exist.',
                    'status' => false,
                    'status_code' => $this->unauthorizedStatus,
                ], $this->unauthorizedStatus);

            $employee = new Employee;
            $employee->id_number = $request->id_number;
            $employee->name = $request->last_name.', '.$request->first_name.' '.$request->middle_name;
            $employee->email = $request->email;
            $employee->salt = $salt;
            $employee->active = 1;
            $employee->password = PasswordHelper::generate($salt, $request->password);
            $employee->designation_id = $request->designation_id;
            $employee->category_id = $request->category_id;
            $employee->department_id = $request->department_id;
                 $employee->save();

            if($request->birth_year && $request->birth_month && $request->birth_day)
                $request['birth_date'] = $request->birth_year.'-'.($request->birth_month+1).'-'.$request->birth_day;
            
            if($request->date_hired_year && $request->date_hired_month && $request->date_hired_day)
                $request['date_hired'] = $request->date_hired_year.'-'.($request->date_hired_month+1).'-'.$request->date_hired_day;
            
            if($request->date_regularization_year && $request->date_regularization_month && $request->date_regularization_day)
                $request['date_regularization'] = $request->date_regularization_year.'-'.($request->date_regularization_month+1).'-'.$request->date_regularization_day;

            unset($request['id'], $request['email'], $request['password'], $request['designation_id'], $request['category_id'], $request['id_number'], $request['birth_year'], $request['birth_month'], $request['birth_day'], $request['date_hired_year'], $request['date_hired_month'], $request['date_hired_day'], $request['date_regularization_year'], $request['date_regularization_month'], $request['date_regularization_day']);

            $employee->addMeta($request->all());
            return $this->response($employee, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(Request $request)
    {
        try
        {
            $employee = Employee::find($request->id);
            $employee->id_number = $request->id_number;
            $employee->name = $request->last_name.', '.$request->first_name.' '.$request->middle_name;
            $employee->email = $request->email;
            $employee->active = ($request->active === false) ? false : true ;
            if($request->password)
                $employee->password = PasswordHelper::generate($employee->salt, $request->password);

            $employee->designation_id = $request->designation_id;
            $employee->category_id = $request->category_id;
            $employee->department_id = $request->department_id;
            $employee->save();

            if($request->birth_year && $request->birth_month && $request->birth_day)
                $request['birth_date'] = $request->birth_year.'-'.($request->birth_month+1).'-'.$request->birth_day;
            
            if($request->date_hired_year && $request->date_hired_month && $request->date_hired_day)
                $request['date_hired'] = $request->date_hired_year.'-'.($request->date_hired_month+1).'-'.$request->date_hired_day;
            
            if($request->date_regularization_year && $request->date_regularization_month && $request->date_regularization_day)
                $request['date_regularization'] = $request->date_regularization_year.'-'.($request->date_regularization_month+1).'-'.$request->date_regularization_day;

            unset($request['id'], $request['email'], $request['password'], $request['designation_id'], $request['category_id'], $request['id_number'], $request['birth_year'], $request['birth_month'], $request['birth_day'], $request['date_hired_year'], $request['date_hired_month'], $request['date_hired_day'], $request['date_regularization_year'], $request['date_regularization_month'], $request['date_regularization_day']);

            $employee->addMeta($request->all());
            return $this->response($employee, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $employee = Employee::find($id);
            $employee->delete();
            return $this->response($employee, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $employee = EmployeeProfile::when(request('search'), function($query){
                return $query->where('name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('email', 'LIKE', '%' . request('search') . '%');
            })
            ->latest()
            ->paginate(request('perPage'));
            return $this->responsePaginate($employee, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function batchUpload(Request $request)
    {
        try
        {
            Excel::import(new EmployeeImport, $request->file('file'));
            return $this->response(true, 'Successfully Uploaded!', $this->successStatus);  
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function all()
    {
        try
        {
            $employee = EmployeeProfile::where('active', 1)->get();
            return $this->response($employee, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
