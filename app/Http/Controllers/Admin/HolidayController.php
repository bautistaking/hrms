<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use Carbon\Carbon;

use App\Models\Holiday; 

class HolidayController extends ApiBaseController implements HolidayControllerInterface
{
    /********************************
    * 			HOLIDAYS 			*
    *********************************/
    public function getDetails($id)
	{
		try
        {
        	$holiday = Holiday::find($id);
            return $this->response($holiday, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(Request $request)
    {
        try
        {
            $holiday = new Holiday;
            $holiday->name = $request->name;
            $holiday->date_from = $request->date_from;
            $holiday->type = $request->type;
            $holiday->with_pay = ($request->with_pay == 'true') ? true : false;
            $holiday->save();

            return $this->response($holiday, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(Request $request)
    {
        try
        {
            $holiday = Holiday::find($request->id);
            $holiday->name = $request->name;
            $holiday->date_from = $request->date_from;
            $holiday->type = $request->type;
            $holiday->with_pay = ($request->with_pay == 'true') ? true : false;
            $holiday->save();

            return $this->response($holiday, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $holiday = Holiday::find($id);
            $holiday->delete();
            return $this->response($holiday, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function all()
    {
        try
        {
        	$now = Carbon::now();        	
            $holiday = Holiday::when(request('search'), function($query){
                return $query->where('name', 'LIKE', '%' . request('search') . '%');
            })
            ->latest()
            ->paginate(request('perPage'));
            return $this->responsePaginate($holiday, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
