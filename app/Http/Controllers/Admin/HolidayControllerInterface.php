<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

interface HolidayControllerInterface
{
    /**
	 * @param integer $id
	 * @return Response
	 * @SWG\Get(
	 *      path="/holiday/{id}",
	 *      summary="Get holiday details",
	 *      tags={"Admin Dashboard - Holidays"},
	 *      description="Get holiday details",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function getDetails($id);

	/**
	 * @param string $date_from
	 * @param boolean $with_pay
	 * @return Response
	 * @SWG\Post(
	 *      path="/holiday/store",
	 *      summary="Store / Save new holiday",
	 *      tags={"Admin Dashboard - Holidays"},
	 *      description="Store / Save new holiday",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="date_from",
	 *          description="Date From",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="with_pay",
	 *          description="With Pay",
	 *          type="boolean",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */  
    public function store(Request $request);

    /**
	 * @param integer $id
	 * @param string $date_from
	 * @param boolean $with_pay
	 * @return Response
	 * @SWG\Put(
	 *      path="/holiday/update",
	 *      summary="Update / Save holiday",
	 *      tags={"Admin Dashboard - Holidays"},
	 *      description="Update / Save holiday",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="Id",
	 *          type="integer",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="date_from",
	 *          description="Date From",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="with_pay",
	 *          description="With Pay",
	 *          type="boolean",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function update(Request $request);

    /**
	 * @param integer $id
	 * @return Response
	 * @SWG\Get(
	 *      path="/holiday/delete/{id}",
	 *      summary="Delete holiday",
	 *      tags={"Admin Dashboard - Holidays"},
	 *      description="Delete holiday",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="ID",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function delete($id);

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/holiday/all",
	 *      summary="Get all Holidays",
	 *      tags={"Admin Dashboard - Holidays"},
	 *      description="Get all Holidays",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function all();
}
