<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use Carbon\Carbon;

use App\Models\InsurancePolicy; 

class InsurancePolicYController extends ApiBaseController implements InsurancePolicYControllerInterface
{
    /********************************
    * 			POLICY   			*
    *********************************/
    public function getInsurancePolicy($id)
	{
		try
        {
        	$insurance_policy = InsurancePolicy::find($id);
            return $this->response($insurance_policy, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(Request $request)
    {
        try
        {
            $insurance_policy = new InsurancePolicy;
            $insurance_policy->name = $request->name;
            $insurance_policy->description = $request->description;
            $insurance_policy->save();

            return $this->response($insurance_policy, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(Request $request)
    {
        try
        {
            $insurance_policy = InsurancePolicy::find($request->id);
            $insurance_policy->name = $request->name;
            $insurance_policy->description = $request->description;
            $insurance_policy->save();

            return $this->response($insurance_policy, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $insurance_policy = InsurancePolicy::find($id);
            $insurance_policy->delete();
            return $this->response($insurance_policy, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $insurance_policy = InsurancePolicy::when(request('search'), function($query){
                return $query->where('name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('description', 'LIKE', '%' . request('search') . '%');
            })
            ->latest()
            ->paginate(request('perPage'));
            return $this->responsePaginate($insurance_policy, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function all(Request $request)
    {
        try
        {
            $insurance_policy = InsurancePolicy::orderBy('name')->get();
            return $this->response($insurance_policy, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
