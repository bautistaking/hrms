<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Models\JobOrder; 
use App\Models\JobOrderDetails; 
use App\Models\CatalogRequestDetails; 
use App\Models\PaymentRequest;

class JobOrderController extends ApiBaseController implements JobOrderControllerInterface
{
    /********************************
    * 			JOB ORDERS			*
    *********************************/
    public function getDetails($id)
	{
		try
        {
        	$job_order = JobOrder::find($id);
            return $this->response($job_order, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(Request $request)
    {
        try
        {
            $job_order = JobOrder::create($request->all());
            return $this->response($job_order, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return $e->getMessage();
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(Request $request)
    {
        try
        {
            $job_order = JobOrder::find($request->id);
            $job_order->update($request->all());
            return $this->response($job_order, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $job_order = JobOrder::find($id);
            $job_order->delete();
            return $this->response($job_order, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $job_order = JobOrder::when(request('search'), function($query){
                return $query->where('job_orders.order_number', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('job_orders.po_number', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('clients.client_name', 'LIKE', '%' . request('search') . '%');
            })
            ->select('job_orders.*', 'clients.client_name')
            ->join('clients', 'job_orders.client_id', '=', 'clients.id')
            ->orderByDesc('job_orders.created_at')
            ->paginate(request('perPage'));
            return $this->responsePaginate($job_order, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getJobOrderDetails($id)
    {
        try
        {
            $job_order_details = JobOrderDetails::find($id);
            return $this->response($job_order_details, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function storeOrderDetails(Request $request)
    {
        try
        {
            // GET JOB ORDER
            $job_order = JobOrder::find($request->job_order_id);

            // CREATE JOB ORDER DETAIL
            $job_order_details = JobOrderDetails::create($request->all());
            $job_order_details->balance_amount = $job_order_details->badget;
            $job_order_details->save();

            // GET SUM OF TOTAL UPDATED BOQ AMOUNT
            $update_po_amount = JobOrderDetails::where('job_order_id', $request->job_order_id)
            ->get()->sum('updated_bill_of_quantities');

            // UPDATE JOB ORDER UPDATED PO AMOUNT
            $job_order->updated_po_amount = $update_po_amount;
            $job_order->save();

            return $this->response($job_order_details, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return $e->getMessage();
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function updateOrderDetails(Request $request)
    {
        try
        {
            // GET JOB ORDER
            $job_order = JobOrder::find($request->job_order_id);

            // UPDATE JOB ORDER DETAIL
            $job_order_details = JobOrderDetails::find($request->id);
            $job_order_details->update($request->all());
            // UPDATE JOB ORDER UPDATED PO AMOUNT
            $job_order->updated_po_amount = JobOrderDetails::where('job_order_id', $request->job_order_id)
            ->get()->sum('updated_bill_of_quantities');
            $job_order->save();

            $job_order_details->balance_amount = ($job_order_details->badget-$this->getNonCatalog($request->id));
            $job_order_details->save();
            
            return $this->response($job_order_details, 'Successfully Updated!', $this->successStatus);
            }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getNonCatalog($id)
    {
        return PaymentRequest::where('job_order_details_id', $id)
        ->where('status_id', 7)
        ->get()->sum('gross_amount');
    }

    public function getTotalCatalog($id)
    {
        return CatalogRequestDetails::where('catalog_request_id', $id)
        ->where('catalog_requests.status_id', 7)
        ->join('catalog_requests', 'catalog_request_details.catalog_request_id', '=', 'catalog_requests.id')
        ->get()->sum('total_price');
    }

    public function deleteOrderDetails($id)
    {
        try
        {
            $job_order_details = JobOrderDetails::find($id);
            $job_order_details->delete();
            return $this->response($job_order_details, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function listOrderDetails($id, Request $request)
    {
        try
        {
            $job_order_details = JobOrderDetails::when(request('search'), function($query){
                return $query->where('site_name', 'LIKE', '%' . request('search') . '%');
            })
            ->where('job_order_id', $id)
            ->latest()
            ->paginate(request('perPage'));
            return $this->responsePaginate($job_order_details, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getBalanceAmount()
    {
        # code...
    }

}
