<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

interface JobOrderControllerInterface
{
    /**
	 * @param integer $id
	 * @return Response
	 * @SWG\Get(
	 *      path="/job-order/{id}",
	 *      summary="Get job order details",
	 *      tags={"Admin Dashboard - Job Order"},
	 *      description="Get job-order details",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="ID",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function getDetails($id);

	/**
	 * @param integer $client_id
	 * @param string $order_number
	 * @param string $project_name
	 * @param string $project_solution
	 * @param string $po_date
	 * @param string $po_number
	 * @param number $po_amount
	 * @return Response
	 * @SWG\Post(
	 *      path="/job-order/store",
	 *      summary="Store / Save new job order",
	 *      tags={"Admin Dashboard - Job Order"},
	 *      description="Store / Save new job-order",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="client_id",
	 *          description="Client ID",
	 *          type="integer",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="order_number",
	 *          description="Order Number",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="project_name",
	 *          description="Project Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	 	 
	 *       @SWG\Parameter(
	 *          name="project_solution",
	 *          description="Project Solution",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	 	 
	 *       @SWG\Parameter(
	 *          name="po_date",
	 *          description="PO Date",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	 	 
	 *       @SWG\Parameter(
	 *          name="po_number",
	 *          description="PO Number",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	 	 
	 *       @SWG\Parameter(
	 *          name="po_amount",
	 *          description="PO Amount",
	 *          type="number",
	 *          required=true,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */  
    public function store(Request $request);

    /**
	 * @param integer $id
	 * @param integer $client_id
	 * @param string $order_number
	 * @param string $project_name
	 * @param string $project_solution
	 * @param string $po_date
	 * @param string $po_number
	 * @param number $po_amount
	 * @return Response
	 * @SWG\Put(
	 *      path="/job-order/update",
	 *      summary="Update / Save job order",
	 *      tags={"Admin Dashboard - Job Order"},
	 *      description="Update / Save job-order",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="Id",
	 *          type="integer",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="client_id",
	 *          description="Client ID",
	 *          type="integer",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="order_number",
	 *          description="Order Number",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="project_name",
	 *          description="Project Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	 	 
	 *       @SWG\Parameter(
	 *          name="project_solution",
	 *          description="Project Solution",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	 	 
	 *       @SWG\Parameter(
	 *          name="po_date",
	 *          description="PO Date",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	 	 
	 *       @SWG\Parameter(
	 *          name="po_number",
	 *          description="PO Number",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	 	 
	 *       @SWG\Parameter(
	 *          name="po_amount",
	 *          description="PO Amount",
	 *          type="number",
	 *          required=true,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function update(Request $request);

    /**
	 * @param integer $id
	 * @return Response
	 * @SWG\Get(
	 *      path="/job-order/delete/{id}",
	 *      summary="Delete job order",
	 *      tags={"Admin Dashboard - Job Order"},
	 *      description="Delete job-order",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="ID",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function delete($id);

    /**
	 * @param string $search
	 * @return Response
	 * @SWG\Get(
	 *      path="/job-order/list",
	 *      summary="List of Job Order with pagination",
	 *      tags={"Admin Dashboard - Job Order"},
	 *      description="List of Job Order with pagination",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="search",
	 *          description="Search String",
	 *          type="string",
	 *          required=false,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function list(Request $request);

    /**
	 * @param integer $id
	 * @return Response
	 * @SWG\Get(
	 *      path="/job-order/details/{id}",
	 *      summary="Get job order site details",
	 *      tags={"Admin Dashboard - Job Order"},
	 *      description="Get job order site details",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="ID",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function getJobOrderDetails($id);

	/**
	 * @param integer $job_order_id
	 * @param string $site_number
	 * @param string $site_name
	 * @param number $bill_of_quantities
	 * @param number $badget
	 * @return Response
	 * @SWG\Post(
	 *      path="/job-order/details/store",
	 *      summary="Store / Save new job site order",
	 *      tags={"Admin Dashboard - Job Order"},
	 *      description="Store / Save new job site order",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="job_order_id",
	 *          description="Job Order ID",
	 *          type="integer",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="site_number",
	 *          description="Site Number",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="site_name",
	 *          description="Site Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	 	 
	 *       @SWG\Parameter(
	 *          name="bill_of_quantities",
	 *          description="BOQ",
	 *          type="number",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="badget",
	 *          description="Badget",
	 *          type="number",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */  
    public function storeOrderDetails(Request $request);

    /**
	 * @param integer $id
	 * @param integer $job_order_id
	 * @param string $site_number
	 * @param string $site_name
	 * @param number $bill_of_quantities
	 * @param number $badget
	 * @return Response
	 * @SWG\Put(
	 *      path="/job-order/details/update",
	 *      summary="Update / Save job site order",
	 *      tags={"Admin Dashboard - Job Order"},
	 *      description="Update / Save job site order",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="job_order_id",
	 *          description="Job Order ID",
	 *          type="integer",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="site_number",
	 *          description="Site Number",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="site_name",
	 *          description="Site Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	 	 
	 *       @SWG\Parameter(
	 *          name="bill_of_quantities",
	 *          description="BOQ",
	 *          type="number",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="badget",
	 *          description="Badget",
	 *          type="number",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function updateOrderDetails(Request $request);

    /**
	 * @param integer $id
	 * @return Response
	 * @SWG\Get(
	 *      path="/job-order/details/delete/{id}",
	 *      summary="Delete job order",
	 *      tags={"Admin Dashboard - Job Order"},
	 *      description="Delete job-order",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="ID",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function deleteOrderDetails($id);

    /**
	 * @param string $search
	 * @return Response
	 * @SWG\Get(
	 *      path="/job-order/details/list",
	 *      summary="List of site per job order with pagination",
	 *      tags={"Admin Dashboard - Job Order"},
	 *      description="List of site per job order with pagination",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="search",
	 *          description="Search String",
	 *          type="string",
	 *          required=false,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function listOrderDetails($id, Request $request);
}
