<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Models\Loan;
use App\Models\LoanTypes;
use App\ViewModels\EmployeeProfile;

class LoanController extends ApiBaseController implements LoanControllerInterface
{
    /********************************
    * 			LOAN 				*
    *********************************/
    public function getDetails($id)
	{
		try
        {
        	$loan = Loan::find($id);
        	$loan->employee_id = EmployeeProfile::find($loan->employee_id);
        	$loan->loan_type_id = LoanTypes::find($loan->loan_type_id);
            return $this->response($loan, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(Request $request)
    {
        try
        {
            $loan = new Loan;
            $loan->employee_id = $request->employee_id['id'];
            $loan->loan_type_id = $request->loan_type_id['id'];
            $loan->total_loan_amount = $request->total_loan_amount;
            $loan->monthly_amortization = $request->monthly_amortization;
            $loan->total_paid_amount = $request->total_paid_amount;
            $loan->remaining_balance = $request->remaining_balance;
            $loan->deduction_count = $request->deduction_count;
            $loan->total_deductions = $request->total_deductions;
            $loan->remarks = $request->remarks;
            $loan->save();

            return $this->response($loan, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(Request $request)
    {
        try
        {
            $loan = Loan::find($request->id);
            $loan->employee_id = $request->employee_id['id'];
            $loan->loan_type_id = $request->loan_type_id['id'];
            $loan->total_loan_amount = $request->total_loan_amount;
            $loan->monthly_amortization = $request->monthly_amortization;
            $loan->total_paid_amount = $request->total_paid_amount;
            $loan->remaining_balance = $request->remaining_balance;
            $loan->deduction_count = $request->deduction_count;
            $loan->total_deductions = $request->total_deductions;
            $loan->remarks = $request->remarks;
            $loan->save();

            return $this->response($loan, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $loan = Loan::find($id);
            $loan->delete();
            return $this->response($loan, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $loan = Loan::when(request('search'), function($query){
                return $query->where('employee.name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('loan_types.name', 'LIKE', '%' . request('search') . '%');
            })
            ->join('employee', 'loans.employee_id', '=', 'employee.id')
            ->leftJoin('loan_types', 'loans.loan_type_id', '=', 'loan_types.id')
            ->select('loans.*')
            ->latest()
            ->paginate(request('perPage'));
            return $this->responsePaginate($loan, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
