<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use Maatwebsite\Excel\Facades\Excel;

use App\Models\LogisticMaster; 
use App\Models\Category; 
use App\Imports\LogisticsImport;
use App\Exports\LogisticsExport;

class LogisticController extends ApiBaseController implements LogisticControllerInterface
{
    /********************************
    * 			LOGISTICS 			*
    *********************************/
    public function getDetails($id)
	{
		try
        {
        	$logisticMaster = LogisticMaster::find($id);
            return $this->response($logisticMaster, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(Request $request)
    {
        try
        {
            $logisticMaster = new LogisticMaster;
            $logisticMaster->sku = $request->sku;
            $logisticMaster->item_name = $request->item_name;
            $logisticMaster->unit_of_measurement = $request->unit_of_measurement;
            $logisticMaster->category_id = $request->category_id;
            $logisticMaster->in_stock = $request->in_stock;
            $logisticMaster->save();

            return $this->response($logisticMaster, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(Request $request)
    {
        try
        {
            $logisticMaster = LogisticMaster::find($request->id);
            $logisticMaster->sku = $request->sku;
            $logisticMaster->item_name = $request->item_name;
            $logisticMaster->unit_of_measurement = $request->unit_of_measurement;
            $logisticMaster->category_id = $request->category_id;
            $logisticMaster->in_stock = $request->in_stock;
            $logisticMaster->save();

            return $this->response($logisticMaster, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $logisticMaster = LogisticMaster::find($id);
            $logisticMaster->delete();
            return $this->response($logisticMaster, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $logisticMaster = LogisticMaster::select('logistic_masters.id', 'logistic_masters.sku', 'logistic_masters.item_name', 'logistic_masters.unit_of_measurement', 'logistic_masters.category_id', 'categories.name as category_name', 'logistic_masters.in_stock', 'logistic_masters.created_at')
            ->when(request('search'), function($query){
                return $query->where('logistic_masters.sku', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('logistic_masters.item_name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('categories.name', 'LIKE', '%' . request('search') . '%');
            })
            ->join('categories', 'logistic_masters.category_id', '=', 'categories.id')
            ->orderByDesc('logistic_masters.created_at')
            ->paginate(request('perPage'));
            return $this->responsePaginate($logisticMaster, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function all()
    {
        try
        {
            $logisticMaster = LogisticMaster::get();
            return $this->response($logisticMaster, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function batchUpload(Request $request)
    {
        try
        {
            Excel::import(new LogisticsImport, $request->file('file'));
            return $this->response(true, 'Successfully Uploaded!', $this->successStatus);  
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
