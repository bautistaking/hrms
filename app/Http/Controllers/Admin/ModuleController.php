<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Requests\Module\ModuleRequest;
use App\Http\Requests\Module\ModuleUpdateRequest;
use App\Models\Module; 

class ModuleController extends ApiBaseController implements ModuleControllerInterface
{
    /****************************
    * 			MODULE 			*
    ****************************/

    public function create(ModuleRequest $request)
	{
		try
        {
            $input = $request->all();
            $module = Module::create($input); 
            return $this->response($module, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}


    public function show($moduleId)
	{
		try
        {
        	$module = Module::find($moduleId);
            return $this->response($module, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}


    public function update(ModuleUpdateRequest $request)
	{
		try
        {
            $input = $request->all();

        	$module = Module::find($input['id']);
        	$module->name = $input['name'];
        	$module->description = $input['description'];
        	$module->save();
            return $this->response($module, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

	
    public function delete($moduleId)
	{
		try
        {
            $module = Module::find($moduleId);
            $module->delete();
            return $this->response($module, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}


    public function list()
	{
		try
        {
            $modules = Module::get();
            return $this->response($modules, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}	

}
