<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use Maatwebsite\Excel\Facades\Excel;

use App\Models\PayPeriod;
use App\Models\Employee;
use App\Models\AttendanceLog;
use App\Models\Payroll;
use App\Models\PayrollOtherIncome;
use App\Models\Loan;
use App\Models\LoanHistory;
use App\ViewModels\EmployeeProfile;
use App\ViewModels\EmployeePayroll;

use App\Repositories\AttendanceLog\Interfaces\AttendanceLogRepositoryInterface;

use App\Exports\PayrollExport;
use Storage;

class PayrollController extends ApiBaseController implements PayrollControllerInterface
{
    /********************************
    * 			PAYROLL 			*
    ********************************/
    private $attendaceLogs;

    public function __construct(AttendanceLogRepositoryInterface $attendanceLogRepositoryInterface){
        $this->attendaceLogs = $attendanceLogRepositoryInterface;
    }

    public function getDetails($id)
	{
		try
        {
        	$payperiod = PayPeriod::find($id);
        	$payperiod['payrolls'] = Payroll::where('pay_period', $id)->get();
            return $this->response($payperiod, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(Request $request)
    {
        try
        {
            $payperiod = PayPeriod::create($request->all());
            $employee_ids = $this->getEmployeeByCategory($payperiod->category_id);
            foreach ($employee_ids as $employee_id) {
                $payroll = [
                    'pay_period' => $payperiod['id'],
                    'employee_id' =>  $employee_id,
                ];
                Payroll::create($payroll);
            }
            
            return $this->response($payperiod, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(Request $request)
    {
        try
        {
            $payperiod = PayPeriod::find($request->id);
            unset($request['id']);
	        $payperiod->update($request->all());
            return $this->response($payperiod, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $payperiod = PayPeriod::find($id);
            $payperiod->delete();
            return $this->response($payperiod, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $payperiod = PayPeriod::when(request('search'), function($query){
                return $query->where('date_from', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('date_to', 'LIKE', '%' . request('search') . '%');
            })
            ->latest()
            ->paginate(request('perPage'));
            return $this->responsePaginate($payperiod, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getEmployeeByCategory($category_id)
    {
        return EmployeePayroll::where('category_id', $category_id)
        ->where('active', 1)->get()->pluck('id');
    }

    public function processPayroll(Request $request)
    {
    	try
        {
            $this->attendaceLogs->setPayroll($request);
            $payroll = $this->attendaceLogs->generatePayroll();
            if(Payroll::where('id',$request->payroll['id'])->update($payroll))
                return true;
            return false;
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function addOtherIncome(Request $request)
    {
        try
        {
            $this->attendaceLogs->setPayroll($request);            

            if(!empty($request->other_incomes[0]['amount']) && !empty($request->other_incomes[0]['type'])) {
                $total_other_income = $this->attendaceLogs->saveOtherIncome($request);
                return $this->attendaceLogs->updatePayroll($request, $total_other_income);
            }
            return $this->attendaceLogs->updatePayroll($request, 0);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function deleteOtherIncome($id)
    {
        try
        {
            $other_income = PayrollOtherIncome::find($id);
            $other_income->delete();
            return $this->response($other_income, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
        
        # code...
    }

    public function downloadCsv($id)
    {
        try
        {
            $payperiod = PayPeriod::find($id);

            $directory = 'public/export/payroll/';
            $files = Storage::files($directory);
            foreach ($files as $file) {
                Storage::delete($file);
            }

            $filename = "payperiod-(".$payperiod->date_from." to ".$payperiod->date_to.").csv";
            // Store on default disk
            Excel::store(new PayrollExport($id), $directory.$filename);

            $data = [
                'filepath' => '/storage/export/payroll/'.$filename,
                'filename' => $filename
            ];
            
            if(Storage::exists($directory.$filename))
                return $this->response($data, 'Successfully Retreived!', $this->successStatus); 

            return $this->response(false, 'Successfully Retreived!', $this->successStatus); 
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function lockPayPeriod($id)
    {
        try
        {
            $employee_ids = Payroll::where('pay_period', $id)
            ->where('total_loans', '>', 0)
            ->get()->pluck('employee_id');

            $loans = Loan::whereIn('employee_id', $employee_ids)
            ->wherecolumn('deduction_count', '<', 'total_deductions')
            ->get();

            foreach ($loans as $data) {
                $loan = Loan::find($data->id);
                $loan_history = [
                    'employee_id' => $loan->employee_id,
                    'pay_period' => $id,
                    'loan_id' => $loan->id,
                    'total_paid_amount_before' => $loan->total_paid_amount,
                    'total_paid_amount_after' => $loan->total_paid_amount+$loan->monthly_amortization,
                    'remaining_balance_before' => $loan->remaining_balance,
                    'remaining_balance_after' => $loan->remaining_balance-$loan->monthly_amortization,
                    'deduction_count_before' => $loan->deduction_count,
                    'deduction_count_after' => $loan->deduction_count+1,
                    'paid_amount' => $loan->monthly_amortization
                ];

                if(LoanHistory::create($loan_history)) {
                    $loan->total_paid_amount = $loan->total_paid_amount+$loan->monthly_amortization;
                    $loan->remaining_balance = $loan->remaining_balance-$loan->monthly_amortization;
                    $loan->deduction_count = $loan->deduction_count+1;
                    $loan->save();
                }
            }
            
            $pay_period = PayPeriod::find($id);
            $pay_period->is_lock = true;
            $pay_period->save();

            return $this->response($pay_period, 'Successfully Locked!', $this->successStatus); 
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    
}