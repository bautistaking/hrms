<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppBaseController as ApiBaseController;
use Illuminate\Http\Request;
use Carbon\Carbon;
use QrCode;

class QrCodeController extends ApiBaseController implements QrCodeControllerInterface
{
	/************************************
    *           QR CODE                 *
    ************************************/
    public function generateQrCode(Request $request)
    {
    	try
        {
        	$employeeInfo = $request->employee_id.', '.$request->email.', '.$request->fullname .', '.$request->mobile_number .', '.$request->home_address .', '.$request->company_name;

            return $qrCode = QrCode::size(200)
            					   ->errorCorrection('H')
            					   ->generate($employeeInfo);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }
}
