<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

interface QrCodeControllerInterface
{
    /**
	 * @param string $employee_id
	 * @param string $email
	 * @param string $fullname
	 * @param string $mobile_number
	 * @param string $home_address
	 * @param string $company_name
	 * @return Response
	 * @SWG\Post(
	 *      path="/qr-code/generate",
	 *      summary="Generate QrCode",
	 *      tags={"Admin Dashboard - QrCode"},
	 *      description="Generate QrCode",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },	 
	 *       @SWG\Parameter(
	 *          name="employee_id",
	 *          description="Employee ID",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="email",
	 *          description="Email",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="fullname",
	 *          description="Full Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="mobile_number",
	 *          description="Mobile Number",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="home_address",
	 *          description="Home Address",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function generateQrCode(Request $request);
}
