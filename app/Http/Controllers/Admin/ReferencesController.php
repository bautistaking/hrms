<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppBaseController as ApiBaseController;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Models\PlansReference; 

class ReferencesController extends ApiBaseController implements ReferencesControllerInterface
{
    public function getDetails($id)
	{
		try
        {
        	$plans_reference = PlansReference::find($id);
            return $this->response($plans_reference, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function list(Request $request)
    {
    	try
        {
            $plans_reference = PlansReference::when(request('search'), function($query){
                return $query->where('name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('references', 'LIKE', '%' . request('search') . '%');
            })
            ->latest()
            ->paginate(request('perPage'));
            return $this->responsePaginate($plans_reference, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function store(Request $request)
    {
    	try
        {
            $plans_reference = PlansReference::find($request->id);            
			if($plans_reference) {
				$plans_reference->name = $request->name;
                $plans_reference->solution = $request->solution;
				$plans_reference->simple = $request->simple;
				$plans_reference->simple_skip = $request->simple_skip;
				$plans_reference->medium = $request->medium;
				$plans_reference->medium_skip = $request->medium_skip;
				$plans_reference->complex = $request->complex;
				$plans_reference->complex_skip = $request->complex_skip;
				$plans_reference->references = $request->references;
				$plans_reference->save();
			}
			else {
				$new_plans_reference = [
					'name' => $request->name,
                    'solution' => $request->solution,
					'simple' => $request->simple,
					'simple_skip' => $request->simple_skip,
					'medium' => $request->medium,
					'medium_skip' => $request->medium_skip,
					'complex' => $request->complex,
					'complex_skip' => $request->complex_skip,
					'references' => $request->references
				];

				$plans_reference = PlansReference::create($new_plans_reference);
			}

            return $this->response($plans_reference, 'Successfully Saved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $plans_reference = PlansReference::find($id);
            $plans_reference->delete();
            return $this->response($plans_reference, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
