<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

use App\Models\Employee;
use App\Models\EmployeeRequest;
use App\ViewModels\EmployeeProfile;

use App\Exports\OverTimeExport;
use App\Exports\OfficialBusinessExport;
use App\Exports\LeaveRequestExport;
use Storage;

class RequestController extends ApiBaseController implements RequestControllerInterface
{
    /************************************
    *           REQUEST APP             *
    ************************************/
    public function getOverTime()
    {
        return $this->list(['OT']);
    }

    public function getOfficialBusiness()
    {
        return $this->list(['OB']);
    }

    public function getLeaveRequest()
    {
        return $this->list(['VL','SL','EL','ML','PL','WFH','SPL','BL']);
    }

    public function list($request_type)
    {
        try
        {
            $requestList = EmployeeRequest::whereIn('request_type', $request_type)
            ->when(request('search'), function($query){
                return $query->where('employee.name', 'LIKE', '%' . request('search') . '%');
            })
            ->join('employee', 'requests.employee_id', '=', 'employee.id')
            ->select('requests.id','requests.employee_id','requests.request_type','requests.date_from','requests.date_to','requests.time_start','requests.time_end','requests.number_of_hours','requests.description','requests.attachment','requests.approved','requests.approver_id','requests.created_at')
            ->orderByDesc('requests.created_at')
            ->paginate(request('perPage'));            
            
            return $this->responsePaginate($requestList, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(Request $request)
    {
        try
        {
            $employeeRequest = EmployeeRequest::find($request->id);
            $employeeRequest->approved = $request->status;
            $employeeRequest->save();

            return $this->response($employeeRequest, 'Successfully Updated!', $this->successStatus);                      
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }    
    }

    public function downloadCsv(Request $request)
    {
        try
        {
            $directory = 'public/export/request/';
            $files = Storage::files($directory);
            foreach ($files as $file) {
                Storage::delete($file);
            }

            $filename = $request->employee_id['name']." (".$request->date_from." to ".$request->date_to.").csv";
            $category_id = $request->category_id;
            // Store on default disk
            $id = 0;
            if($request->employee_id['id'])
                $id = $request->employee_id['id'];

            switch ($request->request_type) {
                case 'OT':
                    $filename = "Over-Time-".$filename;
                    Excel::store(new OverTimeExport($id, $request->date_from, $request->date_to), $directory.$filename);
                    break;
                case 'OB':
                    $filename = "Official-Business-".$filename;
                    Excel::store(new OfficialBusinessExport($id, $category_id, $request->date_from, $request->date_to), $directory.$filename);
                    break;
                case 'LV':
                    $filename = "Leave-".$filename;
                    Excel::store(new LeaveRequestExport($id, $request->date_from, $request->date_to), $directory.$filename);
                    break;
            }

            $data = [
                'filepath' => '/storage/export/request/'.$filename,
                'filename' => $filename
            ];
            
            if(Storage::exists($directory.$filename))
                return $this->response($data, 'Successfully Retreived!', $this->successStatus); 

            return $this->response(false, 'Successfully Retreived!', $this->successStatus); 
        }
        catch (\Exception $e)
        {
            return $e->getMessage();
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
