<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

use App\Models\RequestList;

class RequestListController extends ApiBaseController implements RequestListControllerInterface
{
    public function details($id)
    {
        try
        {
            $request_list = RequestList::find($id);
            return $this->response($request_list, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $request_list = RequestList::when(request('search'), function($query){
                return $query->where('name', 'LIKE', '%' . request('search') . '%');
            })
            ->latest()
            ->paginate(request('perPage'));        
            
            return $this->responsePaginate($request_list, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function store(Request $request)
    {
        try
        {
            $request_list = new RequestList;
            $request_list->name = $request->name;
            //$request_list->department = $request->department;
            $request_list->save();

            return $this->response($request_list, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(Request $request)
    {
        try
        {
            $request_list = RequestList::find($request->id);
            $request_list->name = $request->name;
            //$request_list->department = $request->department;
            $request_list->save();

            return $this->response($request_list, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function delete($id)
    {
        try
        {
            $request_list = RequestList::find($id);
            $request_list->delete();
            return $this->response($request_list, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
