<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Storage;

use App\ViewModels\CatalogRequestViewModel;
use App\Models\CatalogRequest;
use App\Models\PaymentRequest;
use App\Models\JobOrderDetailsTag;
use App\Models\JobOrderDetails;
use App\Models\RequestStatus;
use App\Models\Employee;
use App\ViewModels\EmployeeProfile;

use App\Mail\NonCatalogNotification;
use App\Mail\CatalogRequestNotification;

class RequisitionController extends ApiBaseController implements RequisitionControllerInterface
{
    public function details($id)
    {
    	try
        {
            $catalogs = CatalogRequestViewModel::find($id);
            return $this->response($catalogs, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function list(Request $request)
    {
    	try
        {
            $filters = json_decode($request->filters);

            $catalogs = CatalogRequestViewModel::when(request('search'), function($query){
                return $query->where('catalog_requests.request_number', 'LIKE', '%' . request('search') . '%');
            })
            ->whereIn('status_id', [4,7,8,9,10])
            ->when($filters, function($query) use ($filters){
                return $query->when($filters->id, function($query) use ($filters) {
                    $query->where('status_id', $filters->id);
                });                
            })
            ->orderBy('status_id', 'asc')
            ->latest()
            ->paginate(request('perPage'));        
            
            return $this->responsePaginate($catalogs, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(Request $request)
    {
        try
        {
            // $catalog_request = CatalogRequest::find($request->id);
            // $catalog_request->status_id = $request->status;
            // $catalog_request->save();

            $catalog_request = CatalogRequestViewModel::find($request->id);
            $catalog_request->status_id = $request->status;
            $catalog_request->save();

            $details = [
                'request_number' => $catalog_request->request_number,
                'requestor' => $catalog_request->requestor,
                'supplier_name' => $catalog_request->supplier_name,
                'po_number' => $catalog_request->po_number,
                'supplier_name' => $catalog_request->supplier_name,
                'remarks' => $catalog_request->remarks,
                'status_name' => $catalog_request->status_name,
                'total_price' => $catalog_request->total_price_formated,
                'catalog_details' => $catalog_request->catalog_details,
                'subject' => 'Catalog Request - '. $catalog_request->status_name,
            ];

            // $requestor = EmployeeProfile::find($catalog_request->employee_id);            
            // $to[] = $requestor->email;

            // $send = Mail::to($to)->queue(new CatalogRequestNotification($details));
            // if($send)
            //     return response([
            //         'message' => 'Status has been updated but with error sending email notification.',
            //         'status' => false,
            //         'status_code' => $this->notFoundStatus,
            //     ], $this->notFoundStatus);

            return $this->response($catalog_request, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function paymentList(Request $request)
    {
        try
        {
            $filters = json_decode($request->filters);

            $catalogs = PaymentRequest::when(request('search'), function($query){
                return $query->where('request_number', 'LIKE', '%' . request('search') . '%');
            })
            ->when($filters, function($query) use ($filters){
                return $query->when($filters->type, function($query) use ($filters) {
                    $query->where('request_type', $filters->type);
                });                
            })
            ->whereIn('status_id', [6,7,8,9])
            ->when($filters, function($query) use ($filters){
                return $query->when($filters->id, function($query) use ($filters) {
                    $query->where('status_id', $filters->id);
                });                
            })
            ->orderBy('status_id', 'asc')
            ->latest()
            ->paginate(request('perPage'));        
            
            return $this->responsePaginate($catalogs, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function paymentDetails($id)
    {
        try
        {
            $payment_request = PaymentRequest::find($id);
            return $this->response($payment_request, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function updateStatusTagging(Request $request)
    {
        try
        {
            $payment_tags = '';
            $payment_request = PaymentRequest::find($request->id);
            $job_order_details = JobOrderDetails::find($payment_request->job_order_details_id);

            if($request->status == 8 || $request->status == 9) {
                JobOrderDetailsTag::where('job_order_detail_id', $payment_request->job_order_details_id)
                                  ->where('reference_id', $payment_request->id)
                                  ->delete();

                $payment_request->remarks = $request->remarks;

                $job_order_details->updateBalance();
            }   

            if($request->status == 6) {
                $payment_request->net_amount = $request->net_amount;
                $payment_request->vat_amount = $request->vat_amount;
                $payment_request->ewt_amount = $request->ewt_amount;
                $payment_request->ewt_percent = $request->ewt_percent;
                $payment_request->ewt_type = $request->ewt_type;
                $payment_request->total_amount_due = $request->total_amount_due;

                $payment_tags = JobOrderDetailsTag::updateOrCreate(
                    [
                        'job_order_detail_id' => $payment_request->job_order_details_id,
                        'reference_id' => $payment_request->id,
                        'reference_name' => 'payment_request',
                    ],
                    [
                        'total' => $payment_request->gross_amount
                    ]
                );

                $job_order_details->updateBalance();
            }

            $payment_request->status_id = $request->status;
            $payment_request->save();

            $details = [
                'pay_to' => $payment_request->pay_to,
                'site_name' => $payment_request->site_name,
                'date_requested' => $payment_request->created_at,
                'date_needed' => $payment_request->required_date,
                'pf_number' => $payment_request->request_number,
                'requestor' => $payment_request->requestor_name,
                'particulars' => $payment_request->request_name. ' - '.$payment_request->remarks,
                'amount' => $payment_request->amount_formated,
                'status' => RequestStatus::find($payment_request->status_id)->status,
                'subject' => 'Payment Request'
            ];  
            
            //$to = 'fpy@fabriquem.com';
            // $to = 'kelsey_delacruz@fabriquem.com';            
            // if($request->status == 6) {
            //     //$to = 'fpy@fabriquem.com';
            //     $to = 'kelsey_delacruz@fabriquem.com';            
            //     $send = Mail::to($to)
            //     ->queue(new NonCatalogNotification($details));            
            // }else if($request->status == 7) {
            //     $to = 'maritoni_modesto@fabriquem.com';
            //     $cc = Employee::find($payment_request->employee_id)->email;
            //     $send = Mail::to($to) 
            //     ->cc($cc)
            //     ->queue(new NonCatalogNotification($details));            
            // }

            return $this->response($payment_request, 'Successfully updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function bulkUpdate(Request $request)
    {
        try
        {
            if(!$request->action)
                return response([
                    'message' => 'Please select action',
                    'status' => false,
                    'status_code' => 401,
                ], 401);

            $requests = CatalogRequest::whereIn('id', $request->params)->update(['status_id' => $request->action]);
            return $this->response($requests, 'Successfully Modified!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function paymentBulkUpdate(Request $request)
    {
        try
        {
            if(!$request->action)
                return response([
                    'message' => 'Please select action',
                    'status' => false,
                    'status_code' => 401,
                ], 401);

            $requests = PaymentRequest::whereIn('id', $request->params)->update(['status_id' => $request->action]);
            return $this->response($requests, 'Successfully Modified!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

}
