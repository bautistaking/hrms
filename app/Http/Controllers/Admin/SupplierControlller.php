<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use Maatwebsite\Excel\Facades\Excel;

use App\Models\Supplier; 
use App\Imports\SupplierImport;
use App\Exports\SupplierExport;

class SupplierControlller extends ApiBaseController implements SupplierControlllerInterface
{
    /********************************
    * 			SUPPLIERS 			*
    *********************************/
    public function getDetails($id)
	{
		try
        {
        	$supplier = Supplier::find($id);
            return $this->response($supplier, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(Request $request)
    {
        try
        {
            $supplier = new Supplier;
            $supplier->name = $request->name;
            $supplier->address = $request->address;
            $supplier->tin = $request->tin;
            $supplier->terms = $request->terms;
            $supplier->contact_person = $request->contact_person;
            $supplier->contact_number = $request->contact_number;
            $supplier->email = $request->email;
            $supplier->save();

            return $this->response($supplier, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(Request $request)
    {
        try
        {
            $supplier = Supplier::find($request->id);
            $supplier->name = $request->name;
            $supplier->address = $request->address;
            $supplier->tin = $request->tin;
            $supplier->terms = $request->terms;
            $supplier->contact_person = $request->contact_person;
            $supplier->contact_number = $request->contact_number;
            $supplier->email = $request->email;
            $supplier->save();

            return $this->response($supplier, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $supplier = Supplier::find($id);
            $supplier->delete();
            return $this->response($supplier, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $supplier = Supplier::when(request('search'), function($query){
                return $query->where('name', 'LIKE', '%' . request('search') . '%');
            })
            ->latest()
            ->paginate(request('perPage'));
            return $this->responsePaginate($supplier, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function all()
    {
        try
        {
            $supplier = Supplier::get();
            return $this->response($supplier, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function batchUpload(Request $request)
    {
        try
        {
            Excel::import(new SupplierImport, $request->file('file'));
            return $this->response(true, 'Successfully Uploaded!', $this->successStatus);  
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
