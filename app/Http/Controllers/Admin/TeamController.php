<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Services\Helpers\PasswordHelper;
use Carbon\Carbon;

use App\Models\Team; 
use App\Models\TeamMember; 

class TeamController extends ApiBaseController implements TeamControllerInterface
{
	public function getDetails($id)
	{
		try
        {
        	$team = Team::find($id);
            return $this->response($team, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function list(Request $request)
    {
    	try
        {
            $team = Team::when(request('search'), function($query){
                return $query->where('name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('team_lead', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('email', 'LIKE', '%' . request('search') . '%');
            })
            ->latest()
            ->paginate(request('perPage'));
            return $this->responsePaginate($team, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function store(Request $request)
    {
    	try
        {
        	$salt = bcrypt(PasswordHelper::generateSalt());

            $team = Team::find($request->id);            
			if($team) {
				$team->name = $request->name;
				$team->type = $request->type;
				$team->team_lead = $request->team_lead;
				$team->mobile_number = $request->mobile_number;
				$team->email = $request->email;
				if($request->password)
	                $team->password = PasswordHelper::generate($team->salt, $request->password);
				$team->save();
			}
			else {
				$new_team = [
					'name' => $request->name,
					'type' => $request->type,
					'address' => $request->address,
					'team_lead' => $request->team_lead,
					'mobile_number' => $request->mobile_number,
					'email' => $request->email,
					'salt' => $salt,
					'password' => PasswordHelper::generate($salt, $request->password)
				];

				$team = Team::create($new_team);
			}

			if($team && $request->team_members) {
				$this->storeMember($request->team_members, $team->id);
			}

            return $this->response($team, 'Successfully Saved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function storeMember($team_members, $team_id)
    {
    	foreach ($team_members as $member) {
			$data = TeamMember::find($member['id']);
			if($data) {
				$data->name = $member['name'];
				$data->designation = $member['designation'];
				$data->mobile_number = $member['mobile_number'];
				$data->email = $member['email'];
				$data->save();
			}
			else {
				$new_member = [
					'name' => $member['name'],
					'team_id' => $team_id,
					'designation' => $member['designation'],
					'mobile_number' => $member['mobile_number'],
					'email' => $member['email']
				];

				TeamMember::create($new_member);
			}
		}
    }

    public function delete($id)
    {
        try
        {
            $team = Team::find($id);
            $team->delete();
            return $this->response($team, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
