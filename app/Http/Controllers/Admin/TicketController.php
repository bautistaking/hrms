<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Services\Helpers\PasswordHelper;
use Carbon\Carbon;

use App\Models\Ticket; 
use App\Models\TicketDetails; 
use App\Models\PlansReference; 
use App\Models\Holiday;

class TicketController extends ApiBaseController implements TicketControllerInterface
{
    public function getDetails($id)
	{
		try
        {
        	$ticket = Ticket::find($id);
            return $this->response($ticket, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function list(Request $request)
    {
    	try
        {
            $team = Ticket::when(request('search'), function($query){
                return $query->where('tickets.ticket_number', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('clients.client_name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('job_orders.order_number', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('job_order_details.site_name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('teams.name', 'LIKE', '%' . request('search') . '%');
            })
            ->select('tickets.*')
            ->leftJoin('clients', 'tickets.client_id', '=', 'clients.id')
            ->leftJoin('job_orders', 'tickets.job_order_id', '=', 'job_orders.id')
            ->leftJoin('job_order_details', 'tickets.job_order_details_id', '=', 'job_order_details.id')
            ->leftJoin('teams', 'tickets.team_id', '=', 'teams.id')
            ->latest()
            ->paginate(request('perPage'));
            return $this->responsePaginate($team, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function store(Request $request)
    {
    	try
        {
            $ticket = Ticket::find($request->id);            
			if($ticket) {
				$ticket->client_id = $request->client_id['id'];
				$ticket->job_order_id = $request->job_order_id['id'];
				$ticket->job_order_details_id = $request->job_order_details_id['id'];
				$ticket->team_id = $request->team_id['id'];
				$ticket->service_type = $request->service_type;
				$ticket->remarks = $request->remarks;
				$ticket->start_date = $request->start_date;
				$ticket->is_priority = ($request->is_priority) ? $request->is_priority : 0;
				$ticket->save();
			}
			else {
				$new_ticket = [
					'client_id' => $request->client_id['id'],
					'job_order_id' => $request->job_order_id['id'],
					'job_order_details_id' => $request->job_order_details_id['id'],
					'team_id' => $request->team_id['id'],
					'service_type' => $request->service_type,
					'remarks' => $request->remarks,
					'start_date' => $request->start_date,
					'is_priority' => ($request->is_priority) ? $request->is_priority : 0,
					'track_code' => 1,
					'status' => 1
				];

				$ticket = Ticket::create($new_ticket);

				$ticket->ticket_number = 'IOCC-'.date('mdy').str_pad($ticket->id, 5, "0", STR_PAD_LEFT);
	            $ticket->save();
			}

			if($ticket) {
				$this->storeTicketDetails($ticket);
			}

            return $this->response($ticket, 'Successfully Saved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function storeTicketDetails($ticket)
    {
    	$plans_reference = PlansReference::get();
    	foreach ($plans_reference as $reference) {
    		$skip_days = 0;
    		$number_of_days = 0;
    		$ticket_start_date = '';
    		$ticket_end_date = '';

    		switch ($ticket->service_type) {
    			case 'SIMPLE':
		    		$skip_days = $reference->simple_skip;
		    		$number_of_days = $reference->simple;
    			break;
    			case 'MEDIUM':
		    		$skip_days = $reference->medium_skip;
		    		$number_of_days = $reference->medium;
    			break;
    			case 'COMPLEX':
		    		$skip_days = $reference->complex_skip;
		    		$number_of_days = $reference->complex;
    			break;
       		}

       		$ticket_start_date = $this->validateDate($ticket->start_date, $skip_days);
       		$ticket_end_date = $this->validateDate($ticket_start_date, $number_of_days);

    		TicketDetails::updateOrCreate(
		    	[
		            'ticket_id' => $ticket->id,
	    			'plans_reference_id' => $reference->id,
		    	],	
		    	[
	    			'start_date' => $ticket_start_date,
	    			'end_date' => $ticket_end_date,
	    			'status' => 1
			    ]
			);
    	}

    	$ticket->end_date = $ticket_end_date;
    	$ticket->save();
    }

    public function validateDate($start_date, $number_of_days)
    {
    	$holidays = Holiday::get()->pluck('date_from')->toArray();

    	$MyDateCarbon = Carbon::createFromFormat('Y-m-d',$start_date);
    	$MyDateCarbon->addWeekdays($number_of_days);

    	for ($i=1; $i < $number_of_days; $i++) { 
    		if (in_array(Carbon::createFromFormat('Y-m-d',$start_date)->addWeekdays($i)->toDateString(), $holidays)) {
		        $MyDateCarbon->addWeekdays();
		    }
    	}

    	if (in_array(Carbon::createFromFormat('Y-m-d',$MyDateCarbon->format('Y-m-d'))->toDateString(), $holidays)) {
	        $MyDateCarbon->addWeekdays();
	    }

    	return $MyDateCarbon->format('Y-m-d');
    }

    public function delete($id)
    {
        try
        {
            $team = Team::find($id);
            $team->delete();
            return $this->response($team, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
