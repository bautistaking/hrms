<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

use App\Models\AttendanceLog;
use App\ViewModels\EmployeeProfile;

use App\Exports\OverTimeExport;
use App\Exports\TimeLogsExport;
use Storage;

class TimeKeepingController extends ApiBaseController implements TimeKeepingControllerInterface
{
    /************************************
    * 			TIME KEEPING 			*
    ************************************/
    public function getDetails($id)
	{
		try
        {
        	$attendance = AttendanceLog::find($id);
        	$attendance->employee_id = EmployeeProfile::find($attendance->employee_id);
            return $this->response($attendance, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(Request $request)
    {
        try
        {
            $attendance = new AttendanceLog;
			$attendance->employee_id = $request->employee_id['id'];
            $attendance->date = $request->date;
            $attendance->time_in = date("G:i", strtotime($request->time_in));
            $attendance->time_in_location = 'HEAD OFFICE REQUEST';
            $attendance->time_out = date("G:i", strtotime($request->time_out));
            $attendance->time_out_location = 'HEAD OFFICE REQUEST';
            $attendance->duration = $this->getDuration($request);
            $attendance->save();
            return $this->response($attendance, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(Request $request)
    {
        try
        {
            $attendance = AttendanceLog::find($request->id);
            $attendance->employee_id = $request->employee_id['id'];
            $attendance->date = $request->date;
            $attendance->time_in = date("G:i", strtotime($request->time_in));
            $attendance->time_in_location = ($attendance->time_in_location) ? $attendance->time_in_location : 'HEAD OFFICE REQUEST';
            $attendance->time_out = date("G:i", strtotime($request->time_out));
            $attendance->time_out_location = ($attendance->time_out_location) ? $attendance->time_out_location : 'HEAD OFFICE REQUEST';
            $attendance->duration = $this->getDuration($request);
            $attendance->save();

            return $this->response($attendance, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $attendance = AttendanceLog::find($id);
            $attendance->delete();
            return $this->response($attendance, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $attendance = AttendanceLog::when(request('search'), function($query){
                return $query->where('attendance_logs.date', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('employee.name', 'LIKE', '%' . request('search') . '%');
            })
            ->join('employee', 'attendance_logs.employee_id', '=', 'employee.id')
            ->select('attendance_logs.id','attendance_logs.employee_id','attendance_logs.date','attendance_logs.time_in','attendance_logs.time_out','attendance_logs.duration','attendance_logs.time_in_location','attendance_logs.time_out_location')
            ->orderByDesc('attendance_logs.date')
            ->paginate(request('perPage'));
            return $this->responsePaginate($attendance, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getDuration($request)
    {
        $timeIn = $request->date.' '.date("G:i:s", strtotime($request->time_in));
        $timeOut = $request->date.' '.date("G:i:s", strtotime($request->time_out));

        $endDate = Carbon::parse($timeOut);

        if(strtotime($timeOut) < strtotime($timeIn))
            $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $timeOut)->addDays()->format('Y-m-d G:i:s');

        $dateNow = Carbon::parse($timeIn);
        $inMinutes = $dateNow->diffInMinutes($endDate);        
        return round($inMinutes/60, 2);
    }

    public function downloadCsv(Request $request)
    {
        try
        {
            $directory = 'public/export/timekeeping/';
            $files = Storage::files($directory);
            foreach ($files as $file) {
                Storage::delete($file);
            }

            $filename = $request->employee_id['name']." (".$request->date_from." to ".$request->date_to.").csv";
            // Store on default disk
            $id = 0;
            if($request->employee_id['id'])
                $id = $request->employee_id['id'];

            Excel::store(new TimeLogsExport($id, $request->date_from, $request->date_to), $directory.$filename);

            $data = [
                'filepath' => '/storage/export/timekeeping/'.$filename,
                'filename' => $filename
            ];
            
            if(Storage::exists($directory.$filename))
                return $this->response($data, 'Successfully Retreived!', $this->successStatus); 

            return $this->response(false, 'Successfully Retreived!', $this->successStatus); 
        }
        catch (\Exception $e)
        {
            return $e->getMessage();
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
