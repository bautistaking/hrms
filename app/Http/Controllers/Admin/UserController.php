<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Requests\User\UserUpdateRequest;
use App\Http\Requests\User\UserAddRequest;
use App\Notifications\AddNewUser;
use App\Services\Helpers\PasswordHelper;
use App\Models\User; 
use App\ViewModels\UserProfile; 

class UserController extends ApiBaseController implements UserControllerInterface
{
    /****************************
    * 			USER 			*
    ****************************/
    public function details($id)
	{
		try
        {
        	$user = UserProfile::find($id);
            return $this->response($user, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(UserAddRequest $request)
    {
        try
        {
            $salt = bcrypt(PasswordHelper::generateSalt());

            if(User::where('email', $request->email)->first())
                return response([
                    'message' => 'Email already exist.',
                    'status' => false,
                    'status_code' => $this->unauthorizedStatus,
                ], $this->unauthorizedStatus);

            $user = new User;
            $user->name = $request->userName;
            $user->email = $request->email;
            $user->salt = $salt;
            $user->active = 1;
            $user->password = PasswordHelper::generate($salt, $request->password);
            $user->save();

            $user->assignRole($request->role);

            $other_details = ["first_name" => $request->firstName, "last_name" => $request->lastName];
            $user->addMeta($other_details);

            if($request->emailNotification)
                $user->notify(new AddNewUser($request->password));

            return $this->response($user, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->notFoundStatus,
            ], $this->notFoundStatus);
        }
    }

    public function update(UserUpdateRequest $request)
    {
        try
        {
            $user = User::find($request->id);
            $user->name = $request->userName;
            $user->email = $request->email;
            $user->active = $request->isActive;

            if($request->password)
                $user->password = PasswordHelper::generate($user->salt, $request->password);
            
            $user->save();

            $user->assignRole($request->role);

            $other_details = ["first_name" => $request->firstName, "last_name" => $request->lastName];
            $user->addMeta($other_details);

            if($request->emailNotification)
                $user->notify(new AddNewUser($request->password));

            return $this->response($user, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $user = User::find($id);
            $user->delete();
            return $this->response($user, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $user = User::when(request('search'), function($query){
                return $query->where('name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('email', 'LIKE', '%' . request('search') . '%');
            })
            ->where('name', '<>', 'Administrator')
            ->select('id' ,'name', 'email', 'active', 'created_at')
            ->latest()
            ->paginate(request('perPage'));
            return $this->responsePaginate($user, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
