<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Services\Helpers\PasswordHelper;

// MODELS
use App\Models\User;
use App\ViewModels\UserProfile;

class UserProfileController extends ApiBaseController implements UserProfileControllerInterface
{
    /****************************************
    * 			ADMIN USER PROFILE 			*
    ****************************************/
    public function userDetails()
    {
	    try
        {  
        	$user = auth()->user();
        	$user = UserProfile::find($user->id);
            return $this->response($user, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e) 
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function editProfile(Request $request)
    {
	    try
        { 
        	$user = auth()->user();
        	$user = User::find($user->id);

            // SET PASSWORD
            if($request->password && $request->password_c && ($request->password === $request->password_c))
                $user->password = PasswordHelper::generate($user->salt, $request->password);
                $user->save();

            // SET BIRTH DATE
            $year = $request->year;
            $month = sprintf("%02d", ($request->month+1));
            $day = $request->day;
            $request['birth_date'] = $year.'-'.$month.'-'.$day;

            unset($request['year'], $request['month'], $request['day']);
            unset($request['email'], $request['password'], $request['password_c']);
        	$user->addMeta($request->all());

            return $this->response($user, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e) 
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
