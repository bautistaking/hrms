<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

interface UserProfileControllerInterface
{
    /**
	 * @param integer $id
	 * @return Response
	 * @SWG\Get(
	 *      path="/user-profile",
	 *      summary="Get user profile details",
	 *      tags={"Admin Dashboard - User Profile"},
	 *      description="Get user profile details",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function userDetails();

    /**
	 * @param integer $id
	 * @return Response
	 * @SWG\Put(
	 *      path="/edit-profile",
	 *      summary="Edit user profile",
	 *      tags={"Admin Dashboard - User Profile"},
	 *      description="Edit user profile",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Parameter(
	 *          name="email",
	 *          description="Email",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="password",
	 *          description="Password",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="password_c",
	 *          description="Confirm Password",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="profile_picture",
	 *          description="Profile picture",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="job_title",
	 *          description="Job Title",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 
	 *      @SWG\Parameter(
	 *          name="first_name",
	 *          description="First name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	 
	 *      @SWG\Parameter(
	 *          name="last_name",
	 *          description="Last name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	 
	 *      @SWG\Parameter(
	 *          name="middle_name",
	 *          description="Middle name",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 
	 *      @SWG\Parameter(
	 *          name="birth_date",
	 *          description="Birth date (YYYY-MM-DD)",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	 
	 *       @SWG\Parameter(
	 *          name="gender",
	 *          description="Gender",
	 *          type="string",
	 *			enum={"Female", "Male", "Rather not say"},
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="mobile_number",
	 *          description="Mobile number",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function editProfile(Request $request);
}
