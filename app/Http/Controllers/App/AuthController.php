<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use Illuminate\Support\Facades\Auth; 
use App\Services\Helpers\PasswordHelper;

use Illuminate\Support\Str;
use Carbon\Carbon;

use App\Models\Employee;
use Hash;

class AuthController extends ApiBaseController implements AuthControllerInterface
{
	/****************************************
    * 			MOBILE APP AUTH 			*
    ****************************************/
    public function login(Request $request)
    {
    	try
    	{
            $employee = Employee::when(request('username'), function($query){
                return $query->where('email', '=', request('username'))
                             ->orWhere('id_number', '=', request('username'));
            })
            ->where('active', true)
            ->first();

    		if (!Hash::check(Employee::getSalt(request('username')).env("PEPPER_HASH").request('password'), $employee->password)) 
    			return response([
                    'message' => 'Invalid Employee ID / Email or Password.',
                    'status' => false,
                    'status_code' => $this->notFoundStatus,
                ], $this->notFoundStatus);

    		Auth::login($employee);
		    // get new token
		    $tokenResult = $employee->createToken(env('APP_NAME'));

	        $data = [
                'token' => $tokenResult->accessToken,
                'user'  => $employee,
             ];

            return $this->response($data, 'Successfully Loged!', $this->successStatus);
    	}
    	catch (\Exception $e) 
        {
    		 return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
    	}
    }

    public function logout(Request $request)
    {
        try
        {
            $request->user()->token()->revoke();
            $request->user()->tokens->each(function ($token,$key) {
                $token->delete();
            });

            return $this->response(null, 'Successfully logged out!', $this->successStatus);
        }
        catch (\Exception $e) 
        {
             return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
