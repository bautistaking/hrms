<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Exports\CollectionExport;
use App\Models\Collection; 

use Storage;

class CollectionController extends ApiBaseController
{
    /********************************
    * 			COLLECTIONS			*
    *********************************/
    public function getDetails($id)
	{
		try
        {
        	$collection = Collection::find($id);
            return $this->response($collection, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(Request $request)
    {
        try
        {
            $collection = new Collection;
            $collection->client_id = $request->client_id['id'];
            $collection->job_order_id = $request->job_order_id['id'];
            $collection->job_order_details_id = $request->job_order_details_id['id'];
            $collection->billing_progress = ($request->billing_progress['id']/100);
            $collection->remarks = $request->remarks;
            $collection->invoice_date = $request->invoice_date;
            $collection->invoice_number = $request->invoice_number;
            $collection->or_number = $request->or_number;
            $collection->invoice_amount = $request->invoice_amount;
            $collection->amount_wo_vat = $request->amount_wo_vat;
            $collection->vat_amount = $request->vat_amount;
            $collection->ewt_amount = $request->ewt_amount;
            $collection->net_amount = $request->net_amount;
            $collection->payment_due = $request->payment_due;
            $collection->insurer_id = $request->insurer_id['id'];
            $collection->insurance_policy_id = $request->policy_id['id'];
            $collection->policy_number = $request->policy_number;
            $collection->coverage_due = $request->coverage_due;
            $collection->save();

            return $this->response($collection, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(Request $request)
    {
        try
        {
            $collection = Collection::find($request->id);
            $collection->client_id = $request->client_id['id'];
            $collection->job_order_id = $request->job_order_id['id'];
            $collection->job_order_details_id = $request->job_order_details_id['id'];
            $collection->billing_progress = ($request->billing_progress['id']/100);
            $collection->remarks = $request->remarks;
            $collection->invoice_date = $request->invoice_date;
            $collection->invoice_number = $request->invoice_number;
            $collection->or_number = $request->or_number;
            $collection->invoice_amount = $request->invoice_amount;
            $collection->amount_wo_vat = $request->amount_wo_vat;
            $collection->vat_amount = $request->vat_amount;
            $collection->ewt_amount = $request->ewt_amount;
            $collection->net_amount = $request->net_amount;
            $collection->payment_due = $request->payment_due;
            $collection->insurer_id = $request->insurer_id['id'];
            $collection->insurance_policy_id = $request->policy_id['id'];
            $collection->policy_number = $request->policy_number;
            $collection->coverage_due = $request->coverage_due;
            $collection->save();

            return $this->response($collection, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $collection = Collection::find($id);
            $collection->delete();
            return $this->response($collection, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $collection = Collection::select('collections.*', 'clients.client_name', 'job_order_details.site_name')
            ->selectRaw("concat(job_orders.order_number,'(',job_orders.po_number, ')') as order_number")
            ->selectRaw("FORMAT(job_order_details.updated_bill_of_quantities, 2) as updated_bill_of_quantities")            
            ->when(request('search'), function($query){
                return $query->where('collections.remarks', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('collections.invoice_number', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('collections.or_number', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('collections.policy_number', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('job_orders.order_number', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('job_orders.po_number', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('job_order_details.site_name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('clients.client_name', 'LIKE', '%' . request('search') . '%');
            })
            ->leftJoin('clients', 'collections.client_id', '=', 'clients.id')
            ->leftJoin('job_orders', 'collections.job_order_id', '=', 'job_orders.id')
            ->leftJoin('job_order_details', 'collections.job_order_details_id', '=', 'job_order_details.id')
            ->latest()
            ->paginate(request('perPage'));
            return $this->responsePaginate($collection, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function downloadCsv(Request $request)
    {
        try
        {
            $directory = 'public/export/collections/';
            $files = Storage::files($directory);
            foreach ($files as $file) {
                Storage::delete($file);
            }

            $filename = $request->date_from." to ".$request->date_to.".csv";
            // Store on default disk
            Excel::store(new CollectionExport($request->date_from, $request->date_to), $directory.$filename);

            $data = [
                'filepath' => '/storage/export/collections/'.$filename,
                'filename' => $filename
            ];
            
            if(Storage::exists($directory.$filename))
                return $this->response($data, 'Successfully Retreived!', $this->successStatus); 

            return $this->response(false, 'Successfully Retreived!', $this->successStatus);  
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }


}
