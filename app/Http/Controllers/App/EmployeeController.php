<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Services\Helpers\PasswordHelper;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Models\Employee;
use App\Models\Loan;
use App\Models\AttendanceLog;
use App\Models\PayPeriod;
use App\Models\Payroll;
use App\Models\EmployeeRequest;
use App\Models\CatalogRequest;
use App\Models\Requestor;
use App\ViewModels\EmployeeProfile;
use App\ViewModels\EmployeePaySlip;

class EmployeeController extends ApiBaseController implements EmployeeControllerInterface
{
    /************************************
    *           EMPLOYEE APP            *
    ************************************/
    public function timeIn(Request $request)
    {
        try
        {
            $employee = $this->getEmployee($request);

            if(!$employee)
                return response([
                    'message' => 'Employee ID / Email doesn\'t exist',
                    'status' => false,
                    'status_code' => $this->unauthorizedStatus,
                ], $this->unauthorizedStatus);

            $date_to_day = Carbon::now()->format('Y-m-d H:i:s');
            $timeOut = Carbon::parse($date_to_day)->addHour(9)->format('H:i:s');

            if(AttendanceLog::getTodayTimeIn($date_to_day, $employee->id))
                return $this->timeOut($request);

            $attendance = [
                'employee_id' => $employee->id,
                'date' => Carbon::parse($date_to_day)->format('Y-m-d'),
                'time_in' => Carbon::parse($date_to_day)->format('H:i:s'),
                'time_in_location' => request('address'),
                'time_out' => $timeOut,
                'duration' => 9
            ];

            $attendanceLog = AttendanceLog::create($attendance);

            return $this->response($attendanceLog, 'Successfully Time-In!', $this->successStatus);           
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function timeOut($request)
    {
        try
        {
            $employee = $this->getEmployee($request);

            if(!$employee)
                return response([
                    'message' => 'Employee ID / Email doesn\'t exist',
                    'status' => false,
                    'status_code' => $this->unauthorizedStatus,
                ], $this->unauthorizedStatus);

            $date_to_day = Carbon::now()->format('Y-m-d H:i:s');
            if($request->required_date) {
                $date_to_day = Carbon::parse($request->required_date)->addHour(9)->format('Y-m-d H:i:s');
            }

            // GET EXISTING ATTENDACE CURRENT DAY
            $attendance_log = AttendanceLog::where('date', Carbon::parse($date_to_day)->format('Y-m-d'))
            ->where('employee_id', $employee->id)
            ->where('is_logout', false)
            ->first();
            
            if(!$attendance_log) {
                $date_last_day = Carbon::parse($date_to_day)->subDays()->format('Y-m-d');
                $attendance_log = AttendanceLog::where('date', $date_last_day)->where('employee_id', $employee->id)
                ->where('is_logout', false)
                ->first();
            }

            $attendance = [
                'time_out' => Carbon::parse($date_to_day)->format('H:i:s'),
                'time_out_location' => $request->address,
                'duration' => $this->getDuration($attendance_log, $date_to_day),
                'is_logout' => true
            ];

            $attendance_log->update($attendance);

            return $this->response($attendance_log, 'Successfully Time-Out!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function getEmployee($request)
    {
        return Employee::where('id_number', '=', request('username'))
        ->where('name', 'like', '%'.request('lastname').'%')
        ->first();
    }

    public function getDuration($attendance_log, $time_out)
    {
        $time_in = $attendance_log->date.' '.date("G:i:s", strtotime($attendance_log->time_in));
        $dateNow = Carbon::parse($time_in);
        $inMinutes = $dateNow->diffInMinutes($time_out);        
        return round($inMinutes/60, 2);
    }

    public function details(Request $request)
    {        
        try
        {
            $user = auth()->user();
            $userProfile = EmployeeProfile::find($user->id);
            $userProfile['notification_count'] = $this->getNotificationCount($user->id);

            return $this->response($userProfile, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function getNotificationCount($user_id)
    {
        $request = EmployeeRequest::where('approver_id', $user_id)
        ->where('approved', 0)
        ->count();

        $catalog = CatalogRequest::where('approver_id', $user_id)
        ->whereIn('status_id', [1,2,3,4])
        ->count();

        return ($request+$catalog);
    }

    public function attendanceLog(Request $request)
    {
        try
        {
            $user = auth()->user();
            $attendanceLogs = AttendanceLog::where('employee_id', $user->id)
            ->when(request('search'), function($query){
                return $query->where('date', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('time_in_location', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('time_out_location', 'LIKE', '%' . request('search') . '%');
            })
            ->select('id','employee_id', 'date', 'time_in', 'time_in_location', 'time_out_location')
            ->selectRaw('CASE WHEN is_logout = 0 THEN "" ELSE time_out END AS time_out')
            ->selectRaw('CASE WHEN is_logout = 0 THEN "" ELSE duration END AS duration')            
            ->latest()
            ->paginate(request('perPage'));            
            return $this->responsePaginate($attendanceLogs, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function updateProfile(Request $request)
    {
        try
        {
            $user = auth()->user();
            $employee = Employee::find($user->id);
            $employee->email = $request->email;
            if($request->password)
                $employee->password = PasswordHelper::generate($employee->salt, $request->password);

            $employee->save();

            $sign = $request->file('sign');
            $sign_path = '';
            if($sign) {
                $originalname = $sign->getClientOriginalName();
                $sign_path = $sign->move('sign/', str_replace(' ','-', $originalname)); 
            }

            $request['sign_path'] = $sign_path;

            unset($request['id'], $request['email'], $request['password'], $request['designation_id'], $request['category_id'], $request['id_number'], $request['birth_year'], $request['birth_month'], $request['birth_day'], $request['date_hired_year'], $request['date_hired_month'], $request['date_hired_day'], $request['date_regularization_year'], $request['date_regularization_month'], $request['date_regularization_day'], $request['sign']);

            $employee->addMeta($request->all());
            return $this->response($employee, 'Successfully Updated!', $this->successStatus);
            
        }
        catch (\Exception $e) 
        {
             return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getLoans()
    {
        try
        {
            $user = auth()->user();
            $loans = Loan::where('employee_id', $user->id)
            ->when(request('search'), function($query){
                return $query->where('employee.name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('loan_types.name', 'LIKE', '%' . request('search') . '%');
            })
            ->latest()
            ->paginate(request('perPage'));
            return $this->responsePaginate($loans, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }    
    }

    public function getPayslip()
    {
        try
        {
            $user = auth()->user();
            $employee_payslip = EmployeePaySlip::where('payrolls.employee_id',$user->id)
            ->where('pay_periods.is_lock', true)
            ->whereNull('pay_periods.deleted_at')
            ->join('pay_periods', 'pay_periods.id', '=', 'payrolls.pay_period')
            ->orderByDesc('pay_periods.created_at')
            ->paginate(request('perPage'));         
            return $this->responsePaginate($employee_payslip, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }    
    }

    public function manualTimeInOut(Request $request)
    {
        try
        {
            $employee = $this->getEmployee($request);

            if(!$employee)
                return response([
                    'message' => 'Employee ID / Email doesn\'t exist',
                    'status' => false,
                    'status_code' => $this->unauthorizedStatus,
                ], $this->unauthorizedStatus);

            $date_to_day = Carbon::parse($request->required_date)->format('Y-m-d');
            $timeIn = Carbon::parse($date_to_day.' '.$request->time_in)->format('H:i:s');
            $timeOut = Carbon::parse($date_to_day.' '.$request->time_out)->format('H:i:s');

            $time_in = $date_to_day.' '.date("G:i:s", strtotime($timeIn));
            $dateNow = Carbon::parse($time_in);
            $inMinutes = $dateNow->diffInMinutes($date_to_day.' '.$timeOut);        

            $attendance = [
                'employee_id' => $employee->id,
                'date' => $date_to_day,
                'time_in' => $timeIn,
                'time_in_location' => request('address'),
                'time_out' => $timeOut,
                'duration' => round($inMinutes/60, 2)
            ];

            $attendanceLog = AttendanceLog::create($attendance);

            return $this->response($attendanceLog, 'Successfully Time-In!', $this->successStatus);           
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function attendanceLogs(Request $request)
    {
        try
        {
            $user = auth()->user();
            $employee_ids = Requestor::where('approver_id', $user->id)->get()->pluck('requestor_id');

            $attendance = AttendanceLog::when(request('search'), function($query){
                return $query->where('attendance_logs.date', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('employee.name', 'LIKE', '%' . request('search') . '%');
            })
            ->whereIn('employee_id', $employee_ids)
            ->join('employee', 'attendance_logs.employee_id', '=', 'employee.id')
            ->select('attendance_logs.id','attendance_logs.employee_id','attendance_logs.date','attendance_logs.time_in','attendance_logs.time_out','attendance_logs.duration','attendance_logs.time_in_location','attendance_logs.time_out_location')
            ->orderByDesc('attendance_logs.date')
            ->paginate(request('perPage'));
            return $this->responsePaginate($attendance, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function all()
    {
        try
        {
            $user = auth()->user();
            $employee_ids = Requestor::where('approver_id', $user->id)->get()->pluck('requestor_id');

            $employee = EmployeeProfile::whereIn('employee_id', $employee_ids)->get();
            return $this->response($employee, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
    


}
