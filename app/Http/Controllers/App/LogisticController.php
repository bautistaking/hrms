<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\AppBaseController as ApiBaseController;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

use App\Imports\LogisticsImport;
use App\Exports\LogisticsExport;
use App\Models\LogisticMaster; 
use App\Models\Category; 
use App\Models\Inbound; 
use App\Models\InboundDetails; 
use App\Models\Outbound;
use App\Models\OutboundDetails;
use App\Models\CatalogRequestPurchaseOrder; 
use App\Models\RequestStatus;
use App\Models\JobOrderDetailsTag;
use App\Models\JobOrderDetails;
use App\ViewModels\EmployeeProfile;

use App\Mail\CancelOutBound;

use Storage;

class LogisticController extends ApiBaseController implements LogisticControllerInterface
{
    /********************************
    *           LOGISTICS           *
    *********************************/
    public function getDetails($id)
    {
        try
        {
            $logisticMaster = LogisticMaster::find($id);
            return $this->response($logisticMaster, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function store(Request $request)
    {
        try
        {
            $logisticMaster = new LogisticMaster;
            $logisticMaster->sku = $request->sku;
            $logisticMaster->item_name = $request->item_name;
            $logisticMaster->unit_of_measurement = $request->unit_of_measurement;
            $logisticMaster->category_id = $request->category_id;
            $logisticMaster->in_stock = $request->in_stock;
            $logisticMaster->unit_price = $request->unit_price;
            $logisticMaster->save();

            return $this->response($logisticMaster, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(Request $request)
    {
        try
        {
            $logisticMaster = LogisticMaster::find($request->id);
            $logisticMaster->sku = $request->sku;
            $logisticMaster->item_name = $request->item_name;
            $logisticMaster->unit_of_measurement = $request->unit_of_measurement;
            $logisticMaster->category_id = $request->category_id;
            $logisticMaster->in_stock = $request->in_stock;
            $logisticMaster->unit_price = $request->unit_price;
            $logisticMaster->save();

            return $this->response($logisticMaster, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $logisticMaster = LogisticMaster::find($id);
            $logisticMaster->delete();
            return $this->response($logisticMaster, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $logisticMaster = LogisticMaster::select('logistic_masters.*', 'categories.name as category_name')
            ->when(request('search'), function($query){
                return $query->where('logistic_masters.sku', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('logistic_masters.item_name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('categories.name', 'LIKE', '%' . request('search') . '%');
            })
            ->leftJoin('categories', 'logistic_masters.category_id', '=', 'categories.id')
            ->orderByDesc('logistic_masters.updated_at')
            ->paginate(request('perPage'));
            return $this->responsePaginate($logisticMaster, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function all()
    {
        try
        {
            $logisticMaster = LogisticMaster::get();
            return $this->response($logisticMaster, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function batchUpload(Request $request)
    {
        try
        {
            Excel::import(new LogisticsImport, $request->file('file'));
            return $this->response(true, 'Successfully Uploaded!', $this->successStatus);  
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function downloadCsv()
    {
        try
        {
            $directory = 'public/export/logistics/';
            $files = Storage::files($directory);
            foreach ($files as $file) {
                Storage::delete($file);
            }

            $filename = "logistics.csv";
            // Store on default disk
            Excel::store(new LogisticsExport, $directory.$filename);

            $data = [
                'filepath' => '/storage/export/logistics/'.$filename,
                'filename' => $filename
            ];
            
            if(Storage::exists($directory.$filename))
                return $this->response($data, 'Successfully Retreived!', $this->successStatus); 

            return $this->response(false, 'Successfully Retreived!', $this->successStatus); 
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function inboundList(Request $request)
    {
        try
        {
            $inbound_list = Inbound::when(request('search'), function($query){
                return $query->where('reference_number', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('transaction_type', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('po_number', 'LIKE', '%' . request('search') . '%');
            })
            ->latest()
            ->paginate(request('perPage'));
            return $this->responsePaginate($inbound_list, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getPoDetails(Request $request)
    {
        try
        {
            $purchase_order = CatalogRequestPurchaseOrder::where('po_number', $request->po_number)->first();
            return $this->response($purchase_order, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function storeInbound(Request $request)
    {
        try
        {
            $user = auth()->user();
            $data = [
                'employee_id' => $user->id,
                'client_id' => $request->client_id['id'],
                'job_order_id' => $request->job_order_id['id'],
                'job_order_details_id' => $request->job_order_details_id['id'],
                'reference_number' => $request->reference_number,
                'transaction_type' => $request->transaction_type,
                'po_number' => $request->po_number,
                'remarks' => $request->remarks,
                'status_id' => $request->status_id,
            ];

            $inbound = Inbound::create($data);
            if($inbound) {
                foreach ($request->particulars as $particular) {
                    if($particular['is_checked']) {
                        $data_details = [
                            'inbound_id' => $inbound->id,
                            'catalog_request_details_id' => ($request->transaction_type == 'RETURN') ? 0 : $particular['id'],
                            'logistic_master_id' => ($particular['item']) ? $particular['item']['id'] : $particular['logistic_master_id'],
                            'quantity_receive' => $particular['qty_receive'],
                            'condition' => $particular['condition'],
                        ];

                        InboundDetails::create($data_details);
                    }
                }
            }

            return $this->response($inbound, 'Successfully Saved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function outboundList(Request $request)
    {
        try
        {
            $filters = json_decode($request->filters);            
            $outbound_list = Outbound::when(request('search'), function($query){
                return $query->where('outbounds.request_number', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('clients.client_name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('job_orders.order_number', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('job_orders.po_number', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('outbounds.deliver_to', 'LIKE', '%' . request('search') . '%');
            })
            ->leftJoin('clients', 'outbounds.client_id', '=', 'clients.id')
            ->leftJoin('job_orders', 'outbounds.job_order_id', '=', 'job_orders.id')
            ->when($filters, function($query) use ($filters){
                return $query->where('status_id', $filters);
            })
            ->select('outbounds.*')
            ->orderBy('updated_at', 'DESC')
            ->paginate(request('perPage'));
            return $this->responsePaginate($outbound_list, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function storeOutbound(Request $request)
    {
        try
        {
            $user = auth()->user();
            $outbound_request = [
                'employee_id' => $user->id,
                'client_id' => $request->client_id['id'],
                'job_order_id' => $request->job_order_id['id'],
                'job_order_details_id' => $request->job_order_details_id['id'],
                'approver_id' => $user->approver_id,
                'total_price' => $request->grand_total,
                'pull_out_from' => $request->pull_out_from,
                'deliver_to' => $request->deliver_to,
                'required_date' => $request->required_date,
                'remarks' => $request->remarks,
                'status_id' => 1
            ];

            $outbound = Outbound::create($outbound_request);
            $outbound->request_number = 'GP-'.date('mdy').str_pad($outbound->id, 5, "0", STR_PAD_LEFT);
            $outbound->save();

            // SAVING OUTBOUND DETAILS
            foreach ($request->particulars as $particular) {
                $outbound_details = [
                    'outbound_id' => $outbound->id,
                    'logistic_master_id' => $particular['item']['id'],
                    'quantity' => $particular['quantity'],
                    'uom' => $particular['uom'],
                    'unit_price' => $particular['unit_price'],
                    'total_price' => $particular['total_price'],
                ];
                OutboundDetails::create($outbound_details);
            }

            return $this->response($outbound, 'Successfully Created!', $this->successStatus);  
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function deleteOutbound($id)
    {
        try
        {
            $outbound = Outbound::find($id);
            $outbound->delete();
            return $this->response($outbound, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }


    public function deleteOutboundDetail($id)
    {
        try
        {
            $outbound_detail = OutboundDetails::find($id);
            
            $outbound = Outbound::find($outbound_detail->outbound_id);
            $outbound->total_price = $outbound->total_price-$outbound_detail->total_price;
            $outbound->save();

            $outbound_detail->delete();
            return $this->response($outbound, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function updateOutbound(Request $request)
    {
        try
        {
            $user = auth()->user();
            $outbound = Outbound::find($request->id);
            $outbound_request = [
                'employee_id' => $user->id,
                'client_id' => $request->client_id['id'],
                'job_order_id' => $request->job_order_id['id'],
                'job_order_details_id' => $request->job_order_details_id['id'],
                'approver_id' => $user->approver_id,
                'total_price' => $request->grand_total,
                'pull_out_from' => $request->pull_out_from,
                'deliver_to' => $request->deliver_to,
                'required_date' => $request->required_date,
                'remarks' => $request->remarks,
                'status_id' => 1
            ];

            $outbound->update($outbound_request);

            // SAVING CATALOG DETAILS
            $catalogDetails = [];

            foreach ($request->particulars as $particular) {
                $outbound_details = [
                    'outbound_id' => $outbound->id,
                    'logistic_master_id' => $particular['item']['id'],
                    'quantity' => $particular['quantity'],
                    'uom' => $particular['uom'],
                    'unit_price' => $particular['unit_price'],
                    'total_price' => $particular['total_price'],
                ];                

                if($particular['id']) {
                    $outbound_details = OutboundDetails::find($particular['id'])->update($outbound_details);
                }
                else {                    
                    $outbound_details = OutboundDetails::create($outbound_details);                    
                }

            }

            return $this->response($outbound_details, 'Successfully Updated!', $this->successStatus);                      
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function updateStatus(Request $request)
    {
        try
        {  
            $outbound = Outbound::find($request->id);
            $outbound_details = OutboundDetails::where('outbound_id', $request->id)->get();
            if(!$outbound_details->count()) 
                return response([
                    'message' => 'Your request details is empty, please save your request first.',
                    'status' => false,
                    'status_code' => $this->notFoundStatus,
                ], $this->notFoundStatus);

            $outbound->status_id = $request->status_id;            
            if($request->remarks) {
                $outbound->remarks = $request->remarks;
            }            
            $outbound->save();

            if ($request->status_id == 11) {

                foreach ($outbound_details as $particular) {
                    $logistic_item = LogisticMaster::find($particular['logistic_master_id']);
                    if($logistic_item) {
                        $logistic_item->in_stock = $logistic_item->in_stock-$particular['quantity'];
                        $logistic_item->save();
                    }
                }
    
                $job_order_details = JobOrderDetails::find($outbound->job_order_details_id);
                $payment_tags = JobOrderDetailsTag::updateOrCreate(
                    [
                        'job_order_detail_id' => $outbound->job_order_details_id,
                        'reference_id' => $outbound->id,
                        'reference_name' => 'outbound_request',
                    ],
                    [
                        'total' => $outbound->total_price
                    ]
                );
                $job_order_details->updateBalance();

                // ADMIN 
                $job_order_details = JobOrderDetails::find(756);
                $payment_tags = JobOrderDetailsTag::updateOrCreate(
                    [
                        'job_order_detail_id' => 756,
                        'reference_id' => $outbound->id,
                        'reference_name' => 'adding_income',
                    ],
                    [
                        'total' => $outbound->total_price
                    ]
                );
                $job_order_details->updateBalance();
            }

            // $details = [
            //     'request_number' => $outbound->request_number,
            //     'client_name' => $outbound->client_name,
            //     'po_number' => $outbound->po_number,
            //     'site_name' => $outbound->site_details->site_name,
            //     'required_date' => $outbound->required_date,
            //     'pull_out_from' => $outbound->pull_out_from,
            //     'deliver_to' => $outbound->deliver_to,
            //     'remarks' => $outbound->remarks,
            //     'status' => RequestStatus::find($outbound->status_id)->status,
            //     'outbound_details' => $outbound->outbound_details,
            //     'subject' => 'Outbound Request - '. RequestStatus::find($outbound->status_id)->status,
            // ];  

            // $user = auth()->user();
            // $approver = EmployeeProfile::find($outbound->approver_id);            
            // $to[] = $user->email;
            // $to[] = $approver->email;
            // $send = Mail::to($to)
            //         ->queue(new CancelOutBound($details));  

            return $this->response($outbound, 'Your request has been updated.', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }    
    }

    public function markedAsTaged(Request $request)
    {
        try
        {
            $outbound_detail = OutboundDetails::find($request->id);
            return $outbound_detail;
            //return $this->response($outbound, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    function validateInbound(Request $request) 
    {
        try
        {
            $logistic_items = [];

            $inbound = Inbound::find($request->id);
            foreach ($inbound->inbound_details as $particular) {
                $logistic_item = LogisticMaster::find($particular['logistic_master_id']);
                if($logistic_item) {
                    $logistic_item->in_stock = $logistic_item->in_stock+$particular['quantity_receive'];
                    $logistic_item->unit_price = $particular['unit_price'];
                    $logistic_item->save();

                    $logistic_items[] = $logistic_item;
                }
            }
            $inbound->status_id = 4;
            $inbound->save();

            return $this->response($logistic_items, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }        
    }

}
