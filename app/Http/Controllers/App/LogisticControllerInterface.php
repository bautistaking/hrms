<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;

interface LogisticControllerInterface
{
    /**
     * @param integer $id
     * @return Response
     * @SWG\Get(
     *      path="/requisition/logistic/{id}",
     *      summary="Get logistic details",
     *      tags={"Admin Dashboard - Logistic Master"},
     *      description="Get logistic details",
     *      produces={"application/json"},
     *      security={
     *         {
     *             "default": {}
     *         }
     *      },
     *       @SWG\Parameter(
     *          name="id",
     *          description="ID",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="data",
     *                  type="object"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="status_code",
     *                  type="integer"
     *              ),
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="resource",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function getDetails($id);

    /**
     * @param string $sku
     * @param string $item_name
     * @param string $unit_of_measurement
     * @param integer $category_id
     * @param integer $in_stock
     * @return Response
     * @SWG\Post(
     *      path="/requisition/logistic/store",
     *      summary="Store / Save new logistic",
     *      tags={"Admin Dashboard - Logistic Master"},
     *      description="Store / Save new logistic",
     *      produces={"application/json"},
     *      security={
     *         {
     *             "default": {}
     *         }
     *      },
     *       @SWG\Parameter(
     *          name="sku",
     *          description="SKU",
     *          type="string",
     *          required=true,
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="item_name",
     *          description="Item Name",
     *          type="string",
     *          required=true,
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="unit_of_measurement",
     *          description="Unit Of Measurement",
     *          type="string",
     *          required=true,
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="category_id",
     *          description="Category ID",
     *          type="integer",
     *          required=true,
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="in_stock",
     *          description="In Stock",
     *          type="integer",
     *          required=false,
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="data",
     *                  type="object"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="status_code",
     *                  type="integer"
     *              ),
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="resource",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */  
    public function store(Request $request);

    /**
     * @param integer $id
     * @param string $name
     * @param string $description
     * @return Response
     * @SWG\Put(
     *      path="/requisition/logistic/update",
     *      summary="Update / Save logistic",
     *      tags={"Admin Dashboard - Logistic Master"},
     *      description="Update / Save logistic",
     *      produces={"application/json"},
     *      security={
     *         {
     *             "default": {}
     *         }
     *      },
     *       @SWG\Parameter(
     *          name="id",
     *          description="Id",
     *          type="integer",
     *          required=false,
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="name",
     *          description="Name",
     *          type="string",
     *          required=true,
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="description",
     *          description="Description",
     *          type="string",
     *          required=true,
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="data",
     *                  type="object"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="status_code",
     *                  type="integer"
     *              ),
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="resource",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update(Request $request);

    /**
     * @param integer $id
     * @return Response
     * @SWG\Get(
     *      path="/requisition/logistic/delete/{id}",
     *      summary="Delete logistic",
     *      tags={"Admin Dashboard - Logistic Master"},
     *      description="Delete logistic",
     *      produces={"application/json"},
     *      security={
     *         {
     *             "default": {}
     *         }
     *      },
     *       @SWG\Parameter(
     *          name="id",
     *          description="ID",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="data",
     *                  type="object"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="status_code",
     *                  type="integer"
     *              ),
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="resource",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function delete($id);

    /**
     * @return Response
     * @SWG\Get(
     *      path="/requisition/logistic/list",
     *      summary="List of logistic with pagination",
     *      tags={"Admin Dashboard - Logistic Master"},
     *      description="List of logistic with pagination",
     *      produces={"application/json"},
     *      security={
     *         {
     *             "default": {}
     *         }
     *      },
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="data",
     *                  type="object"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="status_code",
     *                  type="integer"
     *              ),
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="resource",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function list(Request $request);

    /**
     * @return Response
     * @SWG\Get(
     *      path="/requisition/logistic/all",
     *      summary="Get all Logistic Master",
     *      tags={"Admin Dashboard - Logistic Master"},
     *      description="Get all Logistic Master",
     *      produces={"application/json"},
     *      security={
     *         {
     *             "default": {}
     *         }
     *      },
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="data",
     *                  type="object"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="status_code",
     *                  type="integer"
     *              ),
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="resource",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function all();

    /**
     * @param file $file
     * @return Response
     * @SWG\Post(
     *      path="/requisition/logistic/batch-upload",
     *      summary="Employee batch upload",
     *      tags={"Admin Dashboard - Logistic Master"},
     *      description="Employee batch upload",
     *      produces={"application/json"},
     *      security={
     *         {
     *             "default": {}
     *         }
     *      },
     *       @SWG\Parameter(
     *          name="file",
     *          description="File Upload",
     *          type="file",
     *          required=false,
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="data",
     *                  type="object"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="status_code",
     *                  type="integer"
     *              ),
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="resource",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */ 
    public function batchUpload(Request $request);
}
