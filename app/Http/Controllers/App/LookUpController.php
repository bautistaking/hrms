<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Models\Country;
use App\Models\Region;
use App\Models\Province;
use App\Models\City;
use App\Models\Barangay;
use App\Models\ZipCode;
use App\Models\Category;
use App\Models\Client;
use App\Models\JobOrder;
use App\Models\JobOrderDetails;
use App\Models\LogisticMaster;
use App\Models\Supplier;
use App\Models\RequestList;
use App\Models\Team;
use App\Models\PlansReference;
use App\Models\Insurer;
use App\Models\InsurancePolicy;
use App\Models\RequestStatus;


class LookUpController extends ApiBaseController implements LookUpControllerInterface
{
    
    public function getCountries()
    {
        try
        {
            $country = Country::get();

            return $this->response($country, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getRegions()
    {
        try
        {
            $region = Region::get();

            return $this->response($region, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getProvinces(Request $request)
    {
        try
        {
            $province = Province::when(request('regionId'), function($query){
                return $query->where('region_id', '=', request('regionId'));
            })
            ->get();

            return $this->response($province, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getCities(Request $request)
    {
    	try
        {
        	$city = City::when(request('regionId'), function($query){
                return $query->where('region_id', request('regionId'));
            })
            ->when(request('provinceId'), function($query){
	            return $query->where('province_id', request('provinceId'));
	        })
            ->get();

            return $this->response($city, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getBarangays(Request $request)
    {
        try
        {
            $barangay = Barangay::when(request('regionId'), function($query){
                return $query->where('region_id', request('regionId'));
            })
            ->when(request('provinceId'), function($query){
                return $query->where('province_id', request('provinceId'));
            })
            ->when(request('cityId'), function($query){
                return $query->where('city_id', request('cityId'));
            })
            ->get();

            return $this->response($barangay, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function checkZipCode(Request $request)
    {
        try
        {
            $zipCode = ZipCode::where('zip_code', request('zip_code'))->first();
            return $this->response($zipCode, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getClient()
    {
        try
        {
            $client = Client::get();
            return $this->response($client, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getJobOrder(Request $request)
    {
        try
        {
            $job_order = JobOrder::where('client_id', $request->client_id)->get();
            return $this->response($job_order, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }        
    }

    public function getSiteList(Request $request)
    {
        try
        {
            $job_order = JobOrderDetails::where('job_order_id', $request->job_order_id)->get();
            return $this->response($job_order, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }        
    }

    public function getLogistics()
    {
        try
        {
            $logistics = LogisticMaster::get();
            return $this->response($logistics, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }        
    }    

    public function getSuppliers()
    {
        try
        {
            $supplier = Supplier::get();
            return $this->response($supplier, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }        
    } 

    public function getRequestList()
    {
        try
        {
            $Request_list = RequestList::where('name', '!=', 'CATALOG')->get();
            return $this->response($Request_list, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getTeamList()
    {
        try
        {
            $teams = Team::get();
            return $this->response($teams, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function serviceTickets(Request $request)
    {
        try
        {
            $plans_reference = PlansReference::when(request('solution'), function($query){
                return $query->where('solution', strtolower(request('solution')));
            })
            ->get();
            return $this->response($plans_reference, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getCategory(Request $request)
    {
        try
        {
            $category = Category::where('module', $request->module)->orderBy('name')->get();
            return $this->response($category, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getInsurers()
    {
        try
        {
            $insurers = Insurer::orderBy('name')->get();
            return $this->response($insurers, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getPolicies()
    {
        try
        {
            $policies = InsurancePolicy::orderBy('name')->get();
            return $this->response($policies, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    function requestStatuses() {
        try
        {
            $statuses = RequestStatus::orderBy('name')->get();
            return $this->response($statuses, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

}
