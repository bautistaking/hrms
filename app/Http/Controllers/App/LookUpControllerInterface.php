<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;

interface LookUpControllerInterface
{
	/**
	 * @return Response
	 * @SWG\Get(
	 *      path="/lookup/countries",
	 *      summary="Get all countries",
	 *      tags={"Mobile App - Look Up"},
	 *      description="Get all countries",
	 *      produces={"application/json"},
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getCountries();

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/lookup/regions",
	 *      summary="Get all regions",
	 *      tags={"Mobile App - Look Up"},
	 *      description="Get all regions",
	 *      produces={"application/json"},
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getRegions();

    /**
	 * @param integer $regionId
	 * @return Response
	 * @SWG\Get(
	 *      path="/lookup/provinces",
	 *      summary="Get all provinces",
	 *      tags={"Mobile App - Look Up"},
	 *      description="Get all provinces",
	 *      produces={"application/json"},
	 *      @SWG\Parameter(
	 *          name="regionId",
	 *          description="Region ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getProvinces(Request $request);

	/**
	 * @param integer $regionId
	 * @param integer $provinceId
	 * @return Response
	 * @SWG\Get(
	 *      path="/lookup/cities",
	 *      summary="Get all cities",
	 *      tags={"Mobile App - Look Up"},
	 *      description="Get all cities",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="regionId",
	 *          description="Region ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="provinceId",
	 *          description="Province ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getCities(Request $request);

    /**
	 * @param integer $regionId
	 * @param integer $provinceId
	 * @param integer $cityId
	 * @return Response
	 * @SWG\Get(
	 *      path="/lookup/barangays",
	 *      summary="Get all barangays",
	 *      tags={"Mobile App - Look Up"},
	 *      description="Get all barangays",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="regionId",
	 *          description="Region ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="provinceId",
	 *          description="Province ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="cityId",
	 *          description="City ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getBarangays(Request $request);

    /**
	 * @param string $zip_code
	 * @return Response
	 * @SWG\query(
	 *      path="/lookup/check-zipCode",
	 *      summary="Check if zip code is valid",
	 *      tags={"Mobile App - Look Up"},
	 *      description="Check if zip code is valid",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="zip_code",
	 *          description="Zip Code",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function checkZipCode(Request $request);

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/lookup/clients",
	 *      summary="Get all clients",
	 *      tags={"Mobile App - Look Up"},
	 *      description="Get all clients",
	 *      produces={"application/json"},
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getClient();

    /**
	 * @param integer $client_id
	 * @return Response
	 * @SWG\Put(
	 *      path="/lookup/job-orders",
	 *      summary="Get job orders filter by client ID",
	 *      tags={"Mobile App - Look Up"},
	 *      description="Get job orders filter by client ID",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="client_id",
	 *          description="Client ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),	 
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getJobOrder(Request $request);

    /**
	 * @param integer $job_order_id
	 * @return Response
	 * @SWG\Put(
	 *      path="/lookup/site-list",
	 *      summary="Get job orders details filter by job order ID",
	 *      tags={"Mobile App - Look Up"},
	 *      description="Get job orders details filter by job order ID",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="job_order_id",
	 *          description="Job Order ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),	 
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getSiteList(Request $request);

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/lookup/logistics",
	 *      summary="Get logistics master list",
	 *      tags={"Mobile App - Look Up"},
	 *      description="Get logistics master list",
	 *      produces={"application/json"},
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getLogistics();

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/lookup/suppliers",
	 *      summary="Get suppliers master list",
	 *      tags={"Mobile App - Look Up"},
	 *      description="Get suppliers master list",
	 *      produces={"application/json"},
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getSuppliers();
}
