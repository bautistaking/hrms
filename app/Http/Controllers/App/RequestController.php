<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Services\Helpers\PasswordHelper;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Models\Employee;
use App\Models\EmployeeRequest;
use App\ViewModels\EmployeeProfile;

use App\Mail\LeaveRequest;

class RequestController extends ApiBaseController implements RequestControllerInterface
{
    /************************************
    *           REQUEST APP             *
    ************************************/
    public function getOverTime()
    {
        return $this->list(['OT']);
    }

    public function getOfficialBusiness()
    {
        return $this->list(['OB']);
    }

    public function getVacationLeave()
    {
        return $this->list(['VL']);
    }

    public function getSickLeave()
    {
        return $this->list(['SL']);
    }

    public function getOtherLeave()
    {
        return $this->list(['EL','ML','PL','WFH','SPL','BL']);
    }

    public function getDetails(Request $request)
    {
        try
        {
            $requestDetails = EmployeeRequest::find($request->id);
            return $this->response($requestDetails, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function list($request_type)
    {
        try
        {
            $user = auth()->user();
            $userProfile = EmployeeProfile::find($user->id);
            $requestList = $userProfile->getRequest()
            ->whereIn('request_type', $request_type)
            ->latest()
            ->paginate(request('perPage'));            
            
            return $this->responsePaginate($requestList, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function store(Request $request)
    {
        try
        {
            $user = auth()->user();            
            $employeeRequest = new EmployeeRequest;
            $employeeRequest->employee_id = $user->id;
            $employeeRequest->request_type = $request->request_type;
            $employeeRequest->date_from = $request->date_from;
            $employeeRequest->number_of_hours = $this->getDuration($request);
            $employeeRequest->approver_id = $user->approver_id;
            $employeeRequest->description = $request->description;
            
            if($request->date_to)
                $employeeRequest->date_to = $request->date_to;
            
            if($request->time_start)
                $employeeRequest->time_start = date("G:i", strtotime($request->time_start));
            
            if($request->time_end)
                $employeeRequest->time_end = date("G:i", strtotime($request->time_end));

            $employeeRequest->save();

            // email notification here
            $details = [
                'date' => $employeeRequest->date_from. ' - '.$employeeRequest->date_to,
                'description' => $employeeRequest->description,
                'number_of_hours' => $employeeRequest->number_of_hours,
                'subject' => $employeeRequest->request_name
            ];  

            // $requestor = EmployeeProfile::find($employeeRequest->employee_id);
            // $approver = EmployeeProfile::find($employeeRequest->approver_id);

            // $to[] = $requestor->email;
            // $to[] = $approver->email;

            // $send = Mail::to($to)->queue(new LeaveRequest($details));
            // if($send)
            //     return response([
            //         'message' => 'Error sending email notification.',
            //         'status' => false,
            //         'status_code' => $this->notFoundStatus,
            //     ], $this->notFoundStatus);

            return $this->response($employeeRequest, 'Successfully Created!', $this->successStatus);                    
        }
        catch (\Exception $e)
        {
            return $e->getMessage();
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }    
    }

    public function update(Request $request)
    {
        try
        {
            $employeeRequest = EmployeeRequest::find($request->id);
            $employeeRequest->date_from = $request->date_from;
            $employeeRequest->number_of_hours = $this->getDuration($request);
            $employeeRequest->description = $request->description;
            
            if($request->date_to)
                $employeeRequest->date_to = $request->date_to;
            
            if($request->time_start)
                $employeeRequest->time_start = date("G:i", strtotime($request->time_start));
            
            if($request->time_end)
                $employeeRequest->time_end = date("G:i", strtotime($request->time_end));

            $employeeRequest->save();

            // email notification here
            $details = [
                'date' => $employeeRequest->date_from. ' - '.$employeeRequest->date_to,
                'description' => $employeeRequest->description,
                'number_of_hours' => $employeeRequest->number_of_hours,
                'subject' => $employeeRequest->request_name
            ];  

            // $requestor = EmployeeProfile::find($employeeRequest->employee_id);
            // $approver = EmployeeProfile::find($employeeRequest->approver_id);

            
            // $to[] = $requestor->email;
            // $to[] = $approver->email;

            // $send = Mail::to($to)->queue(new LeaveRequest($details));
            // if($send)
            //     return response([
            //         'message' => 'Error sending email notification.',
            //         'status' => false,
            //         'status_code' => $this->notFoundStatus,
            //     ], $this->notFoundStatus);
            
            return $this->response($employeeRequest, 'Successfully Updated!', $this->successStatus);                      
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }    
    }

    public function delete($id)
    {
        try
        {
            $requestData = EmployeeRequest::find($id);
            if($requestData->approved == 1)
                return response([
                    'message' => 'Approved request cannot be deleted.',
                    'status' => false,
                    'status_code' => $this->notFoundStatus,
                ], $this->notFoundStatus);

            $requestData->delete();
            return $this->response($requestData, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getDuration($request)
    {
        switch ($request->request_type) {
            case 'OT':
            case 'OB':
                $timeIn = $request->date_from.' '.date("G:i:s", strtotime($request->time_start));
                $timeOut = $request->date_from.' '.date("G:i:s", strtotime($request->time_end));

                $endDate = Carbon::parse($timeOut);

                if(strtotime($timeOut) < strtotime($timeIn))
                    $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $timeOut)->addDays()->format('Y-m-d G:i:s');

                $dateNow = Carbon::parse($timeIn);
                $inMinutes = $dateNow->diffInMinutes($endDate);        
                return round($inMinutes/60, 2);
                
                break;
            case 'VL':
            case 'SL':
            case 'EL':
            case 'ML':
            case 'PL':
            case 'WFH':
            case 'SPL':
            case 'BL':
                $dateNow = Carbon::parse($request->date_to);
                $numDays = $dateNow->diffInDays($request->date_from)+1;
                return $numDays * 8;
                break;
        }
    }
}
