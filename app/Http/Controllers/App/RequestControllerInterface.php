<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;

interface RequestControllerInterface
{
	/**
	 * @return Response
	 * @SWG\Get(
	 *      path="/employee/list/over-time",
	 *      summary="Employee over time request list",
	 *      tags={"Mobile App - Employee"},
	 *      description="Employee over time request list",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      }, 
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getOverTime();

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/employee/list/official-bussiness",
	 *      summary="Employee official bussiness request list",
	 *      tags={"Mobile App - Employee"},
	 *      description="Employee official bussiness request list",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      }, 
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getOfficialBusiness();

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/employee/list/vacation-leave",
	 *      summary="Employee vacation leave request list",
	 *      tags={"Mobile App - Employee"},
	 *      description="Employee vacation leave request list",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      }, 
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getVacationLeave();

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/employee/list/sick-leave",
	 *      summary="Employee sick leave request list",
	 *      tags={"Mobile App - Employee"},
	 *      description="Employee sick leave request list",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      }, 
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getSickLeave();

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/employee/list/other-leave",
	 *      summary="Employee other leave request list",
	 *      tags={"Mobile App - Employee"},
	 *      description="Employee other leave request list",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      }, 
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getOtherLeave();

    /**
	 * @param integer $id
	 * @return Response
	 * @SWG\Post(
	 *      path="/employee/request/{id}",
	 *      summary="Get request details",
	 *      tags={"Mobile App - Employee"},
	 *      description="Get request details",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Parameter(
	 *          name="id",
	 *          description="ID",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getDetails(Request $request);

    /**
	 * @param string $request_type
	 * @param string $date_from
	 * @param string $date_to
	 * @param string $time_start
	 * @param string $time_end
	 * @param string $description
	 * @param file $attachment
	 * @return Response
	 * @SWG\Post(
	 *      path="/employee/request/add",
	 *      summary="Create over time request",
	 *      tags={"Mobile App - Employee"},
	 *      description="Create over time request",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Parameter(
	 *          name="request_type",
	 *          description="Request Type",
	 *          type="string",
	 *			enum={"OT","OB","VL","SL","EL","ML","PL","WFH","SPL"},
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="date_from",
	 *          description="Date",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="date_to",
	 *          description="Date",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="time_start",
	 *          description="Time start",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="time_end",
	 *          description="Time end",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="description",
	 *          description="Request details",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	 
	 *      @SWG\Parameter(
	 *          name="attachment",
	 *          description="Attach file",
	 *          type="file",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function store(Request $request);

    /**
	 * @param integer $id
	 * @param string $request_type
	 * @param string $date_from
	 * @param string $date_to
	 * @param string $time_start
	 * @param string $time_end
	 * @param string $description
	 * @param string $attachment
	 * @return Response
	 * @SWG\Post(
	 *      path="/employee/request/update",
	 *      summary="Update request",
	 *      tags={"Mobile App - Employee"},
	 *      description="Update request",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Parameter(
	 *          name="id",
	 *          description="Request ID",
	 *          type="integer",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="request_type",
	 *          description="Request Type",
	 *          type="string",
	 *			enum={"OT","OB","VL","SL","EL","ML","PL"},
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="date_from",
	 *          description="Date From",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="date_to",
	 *          description="Date To",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="time_start",
	 *          description="Time start",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="time_end",
	 *          description="Time end",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="description",
	 *          description="Request details",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	 
	 *      @SWG\Parameter(
	 *          name="attachment",
	 *          description="Attach file",
	 *          type="file",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function update(Request $request);

    /**
	 * @param string $id
	 * @return Response
	 * @SWG\Get(
	 *      path="/employee/request/delete/{id}",
	 *      summary="Delete request",
	 *      tags={"Mobile App - Employee"},
	 *      description="Delete request",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="Request ID",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function delete($id);
}
