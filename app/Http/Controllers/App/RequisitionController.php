<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\AppBaseController as ApiBaseController;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

use App\Models\Client;
use App\Models\CatalogRequest;
use App\Models\CatalogRequestDetails;
use App\Models\JobOrder;
use App\Models\JobOrderDetails;
use App\Models\EmployeeRequest;
use App\Models\Supplier;
use App\Models\PoNumber;
use App\Models\PaymentRequest;
use App\ViewModels\EmployeeProfile;
use App\ViewModels\CatalogRequestViewModel;
use App\ViewModels\SiteExpensesReportViewModel;
use App\Models\CatalogRequestPurchaseOrder;
use App\Models\RequestStatus;
use App\Models\RequestList;
use App\Models\JobOrderDetailsTag;
use App\Models\LogisticMaster;
use App\Models\CatalogRequestAttachments;

use App\Mail\NonCatalogNotification;
use App\Mail\CatalogRequestNotification;

use App\Exports\SiteExpensesReport;
use Storage;

class RequisitionController extends ApiBaseController implements RequisitionControllerInterface
{
    /************************************ 
    *           REQUEST APP             *
    ************************************/
    public function getDetails($id)
    {
        try
        {
            $catalog_request = CatalogRequestViewModel::find($id);
            return $this->response($catalog_request, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $filters = json_decode($request->filters);
            $user = auth()->user();
            $catalog_request = CatalogRequestViewModel::where('catalog_type', 'catalog')
            ->when(request('search'), function($query){
                return $query->where('request_number', 'LIKE', '%' . request('search') . '%');
            })
            ->when($filters, function($query) use ($filters){
                return $query->where('status_id', $filters);
            })
            ->orderBy('status_id', 'asc')
            ->latest()
            ->paginate(request('perPage'));            
            
            return $this->responsePaginate($catalog_request, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function store(Request $request)
    {
        try
        {
            $user = auth()->user();
            $catalog_request = [
                'employee_id' => $user->id,
                'approver_id' => $user->approver_id,
                'status_id' => 1,
                'catalog_type' => 'catalog',
                'total_price' => $request->grand_total,
                'supplier_id' => $request->supplier['id'],
                'remarks' => $request->remarks
            ];

            $catalog = CatalogRequest::create($catalog_request);
            $catalog->request_number = date('y').'-'.date('md').str_pad($catalog->id, 5, "0", STR_PAD_LEFT);
            $catalog->save();

            // SAVING CATALOG DETAILS
            $catalogDetails = [];

            foreach ($request->particulars as $particular) {
                $catalog_details = [
                    'catalog_request_id' => $catalog->id,
                    'logistic_master_id' => $particular['item']['id'],
                    'quantity' => $particular['quantity'],
                    'uom' => $particular['uom'],
                    'unit_price' => $particular['unit_price'],
                    'total_price' => $particular['total_price'],
                ];
                $catalogDetails[] = CatalogRequestDetails::create($catalog_details);
                $this->updateLogistic($particular);
            }

            if($request->attachments) {
                foreach ($request->attachments as $attachment) {
                    $attachment_details = [
                        'catalog_request_id' => $catalog->id,
                        'file_path' => $attachment['file_path']
                    ];

                    $attachment_details = CatalogRequestAttachments::create($attachment_details);
                }
            }

            return $this->response($catalog, 'Successfully Created!', $this->successStatus);  
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }    
    }

    public function update(Request $request)
    {
        try
        {
            $user = auth()->user();
            $catalog = CatalogRequest::find($request->id);
            $catalog_request = [
                'employee_id' => $user->id,
                'approver_id' => $user->approver_id,
                'status_id' => 1,
                'catalog_type' => 'catalog',
                'total_price' => $request->grand_total,
                'supplier_id' => $request->supplier['id'],
                'remarks' => $request->remarks
            ];

            $catalog->update($catalog_request);

            // SAVING CATALOG DETAILS
            $catalogDetails = [];

            foreach ($request->particulars as $particular) {
                $catalog_details = [
                    'catalog_request_id' => $catalog->id,
                    'logistic_master_id' => $particular['item']['id'],
                    'quantity' => $particular['quantity'],
                    'uom' => $particular['uom'],
                    'unit_price' => $particular['unit_price'],
                    'total_price' => $particular['total_price'],
                ];                

                if($particular['id']) {
                    $catalog_details = CatalogRequestDetails::find($particular['id'])->update($catalog_details);
                }
                else {                    
                    $catalog_details = CatalogRequestDetails::create($catalog_details);
                }
                $this->updateLogistic($particular);
            }

            if($request->attachments) {
                foreach ($request->attachments as $attachment) {
                    $attachment_details = [
                        'catalog_request_id' => $catalog->id,
                        'file_path' => $attachment['file_path']
                    ];

                    if($attachment['id']) {
                        $attachment_details = CatalogRequestAttachments::find($attachment['id'])->update($attachment_details);
                    }
                    else {                    
                        $attachment_details = CatalogRequestAttachments::create($attachment_details);
                    }
                }
            }

            return $this->response($catalog_details, 'Successfully Updated!', $this->successStatus);                      
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }    
    }

    public function updateLogistic($particular)
    {
        $logistic_item = LogisticMaster::find($particular['item']['id']);
        if($logistic_item) {
            $logistic_item->unit_price = $particular['unit_price'];
            $logistic_item->save();
        }
    }

    public function updateStatus(Request $request)
    {
        try
        {
            $catalog = CatalogRequest::find($request->id);
            $catalog_details = CatalogRequestDetails::where('catalog_request_id', $request->id)->count();
            if(!$catalog_details) 
                return response([
                    'message' => 'Your catalog details is empty, please save your request first.',
                    'status' => false,
                    'status_code' => $this->notFoundStatus,
                ], $this->notFoundStatus);

            $catalog->status_id = 3;
            $catalog->save();
            return $this->response($catalog, 'Your request has been submited for review.', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }    
    }


    public function delete($id)
    {
        try
        {
            $catalog_request = CatalogRequest::find($id);

            if($catalog_request->status_id == 8)
                return response([
                    'message' => 'This request already been approved.',
                    'status' => false,
                    'status_code' => $this->notFoundStatus,
                ], $this->notFoundStatus);

            $catalog_request->delete();
            return $this->response($catalog_request, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getItemDetails($id)
    {
        try
        {
            $catalog_item = CatalogRequestDetails::find($id);
            return $this->response($catalog_item, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function deleteDetails($id)
    {
        try
        {
            $catalogRequest = CatalogRequestDetails::find($id);
            $catalogRequest->delete();

            // GET CATALOG BY ID
            $catalog_id = $catalogRequest->catalog_request_id;
            $catalog_request = CatalogRequest::find($catalog_id);
            
            // GET TOTALS
            $total_price = CatalogRequestDetails::where('catalog_request_id', $catalog_id)
            ->get()
            ->sum('total_price');

            // SAVE TOTALS
            $catalog_request->total_price = $total_price;
            $catalog_request->save();

            return $this->response($catalogRequest, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function deleteAttachment($id)
    {
        try
        {
            $attachments = CatalogRequestAttachments::find($id);
            $attachments->delete();

            return $this->response($attachments, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function updateDetails(Request $request)
    {
        try
        {
            $catalog_item = CatalogRequestDetails::find($request->id);
            if($request->unit_price && $request->total_price) {
                $catalog_details = [
                    'supplier_id' => $request->supplier_id['id'],
                    'quantity' => $request->quantity,
                    'unit_price' => $request->unit_price,
                    'total_price' => $request->total_price
                ];                
            }
            else {
                $catalog_details = [
                    'catalog_request_id' => $request->catalog_request_id,
                    'job_order_details_id' => $request->job_order_details_id['id'],
                    'logistic_master_id' => $request->item['id'],
                    'quantity' => $request->quantity,
                    'uom' => $request->uom,
                    'remarks' => $request->remarks,
                ];                
            }

            $catalog_item->update($catalog_details);

            $gross_amount = CatalogRequestDetails::where('catalog_request_id', $catalog_item->catalog_request_id)
            ->sum('total_price');

            $catalog_request = CatalogRequest::find($catalog_item->catalog_request_id);
            $catalog_request->gross_amount = $gross_amount;
            $catalog_request->save();

            return $this->response($catalog_request, 'Successfully Updated!', $this->successStatus);                      
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }    
    }

    public function detailList($id)
    {
        try
        {
            $catalogs = CatalogRequest::find($id);
            return $this->response($catalogs, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }    
    }

    public function storeNonCatalog(Request $request)
    {
        try
        {
            $user = auth()->user();

            $job_order_id = '';
            $job_order_details = '';
            $job_order_details_id = '';

            if($request->client_id['client_name'] == 'Admin') {
                $job_order_id = JobOrder::where('client_id', $request->client_id['id'])->first()->id;
                $job_order_details = JobOrderDetails::where('job_order_id', $job_order_id)->first();
                $job_order_details_id = $job_order_details->id;
            }
            else {
                $job_order_id = $request->job_order_id['id'];
                $job_order_details = JobOrderDetails::find($job_order_details_id);
                $job_order_details_id = $request->job_order_details_id['id'];    
            }

            $non_catalog_request = [
                'employee_id' => $user->id,
                'client_id' => $request->client_id['id'],
                'job_order_id' => $job_order_id,
                'job_order_details_id' => $job_order_details_id,
                'request_id' => $request->request_id['id'],
                'required_date' => $request->required_date,
                'approver_id' => $user->approver_id,
                'status_id' => 3,
                'payment_type' => $request->payment_type,
                'request_type' => 'non-catalog',
                'pay_to' => $request->pay_to,
                'gross_amount' => $request->gross_amount,
                'remarks' => $request->remarks
            ];

            $pf_number = PoNumber::first();
            $pf_request_number = $pf_number->pf_number+1;

            $non_catalog = PaymentRequest::create($non_catalog_request);
            $non_catalog->request_number = date('Y').'-'.$pf_request_number;
            $non_catalog->save();

            $pf_number->pf_number = $pf_request_number;
            $pf_number->save();                

            $details = [
                'pay_to' => $request->pay_to,
                'site_name' => $job_order_details->site_name,
                'date_requested' => $non_catalog->created_at,
                'date_needed' => $request->required_date,
                'pf_number' => $non_catalog->request_number,
                'requestor' => $user->name,
                'particulars' => RequestList::find($request->request_id['id'])->name. ' - '.$request->remarks,
                'amount' => $request->gross_amount,
                'status' => RequestStatus::find(3)->status,
                'subject' => 'Payment Request'
            ];  

            // $approver = EmployeeProfile::find($user->approver_id);

            // $to = $approver->email;
            // $send = Mail::to($to)->queue(new NonCatalogNotification($details));          
            // if($send)
            //     return response([
            //         'message' => 'Status has been updated but with error sending email notification.',
            //         'status' => false,
            //         'status_code' => $this->notFoundStatus,
            //     ], $this->notFoundStatus);
            return $this->response($non_catalog, 'Successfully Created!', $this->successStatus);           
        }
        catch (\Exception $e)
        {
            return $e->getMessage();
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }    
    }

    public function updateNonCatalog(Request $request)
    {
        try
        {
            $user = auth()->user();

            $payment_request = PaymentRequest::find($request->id);

            $job_order_id = $request->job_order_id['id'];
            $job_order_details_id = $request->job_order_details_id['id'];
            $job_order_details = JobOrderDetails::find($job_order_details_id);

            if($request->client_id['client_name'] == 'Admin') {
                $job_order_id = JobOrder::where('client_id', $request->client_id['id'])->first()->id;
                $job_order_details = JobOrderDetails::where('job_order_id', $job_order_id)->first();
                $job_order_details_id = $job_order_details->id;
            }            

            $non_catalog_request = [
                'employee_id' => $user->id,
                'client_id' => $request->client_id['id'],
                'job_order_id' => $job_order_id,
                'job_order_details_id' => $job_order_details_id,
                'request_id' => $request->request_id['id'],
                'required_date' => $request->required_date,
                'approver_id' => $user->approver_id,
                'status_id' => 3,
                'payment_type' => $request->payment_type,
                'request_type' => 'non-catalog',
                'pay_to' => $request->pay_to,
                'gross_amount' => $request->gross_amount,
                'remarks' => $request->remarks
            ];
            $payment_request->update($non_catalog_request);

            return $this->response($payment_request, 'Successfully Updated!', $this->successStatus);          
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }    
    }

    public function updateNonCatalogRequest(Request $request)
    {
        try
        {
            $payment_request = PaymentRequest::find($request->id);
            $payment_request->status_id = $request->status;
            $payment_request->release_type = $request->release_type;
            $payment_request->save();

            $job_order_tag = JobOrderDetailsTag::where('job_order_detail_id', $payment_request->job_order_details_id)
                                  ->where('reference_id', $payment_request->id)
                                  ->first();

            if($job_order_tag && $request->status == 8 || $request->status == 9) {

                $job_order_tag->delete();

                $job_order_details = JobOrderDetails::find($payment_request->job_order_details_id);
                $job_order_details->updateBalance();
            }

            $details = [
                'pay_to' => $payment_request->pay_to,
                'site_name' => $payment_request->site_name,
                'date_requested' => $payment_request->created_at,
                'date_needed' => $payment_request->required_date,
                'pf_number' => $payment_request->request_number,
                'requestor' => $payment_request->requestor_name,
                'particulars' => $payment_request->request_name. ' - '.$payment_request->remarks,
                'amount' => $payment_request->amount_formated,
                'status' => RequestStatus::find($payment_request->status_id)->status,
                'subject' => 'Payment Request - '.RequestStatus::find($payment_request->status_id)->status
            ];  

            $approver_id = 0;
            switch ($payment_request->status_id) {
                case 4:
                    //$approver_id = 26;
                    $approver_id = 7;
                    break;
                case 5:
                    $approver_id = 41;
                    break;
                case 8:
                case 9:
                    $approver_id = $payment_request->employee_id;
                    break;
            }

            // $requestor = EmployeeProfile::find($payment_request->employee_id);
            // $approver = EmployeeProfile::find($approver_id);

            // $to[] = $requestor->email;
            // $to[] = $approver->email;

            // $send = Mail::to($to)->queue(new NonCatalogNotification($details));
            // if($send)
            //     return response([
            //         'message' => 'Status has been updated but with error sending email notification.',
            //         'status' => false,
            //         'status_code' => $this->notFoundStatus,
            //     ], $this->notFoundStatus);

            return $this->response($payment_request, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function liquidateNonCatalogRequest(Request $request)
    {
        try
        {
            $disbursed_date = null;
            if($request->disbursed)
                $disbursed_date = Carbon::now()->format('Y-m-d');

            $payment_request = PaymentRequest::find($request->id);
            $payment_request->disbursed = $request->disbursed;
            $payment_request->disbursed_date = $disbursed_date;
            $payment_request->liquidated = $request->liquidated;
            $payment_request->save();

            return $this->response($payment_request, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function deleteNonCatalog($id)
    {
        try
        {
            $payment_request = PaymentRequest::find($id);
            $payment_request->delete();

            return $this->response($payment_request, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function listNonCatalog()
    {
        try
        {
            $user = auth()->user();
            $payment_request = PaymentRequest::where('employee_id', $user->id)
            ->where('request_type', 'non-catalog')
            ->latest()
            ->paginate(request('perPage')); 
            
            return $this->responsePaginate($payment_request, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function catalogRequests(Request $request)
    {
        try
        {
            $filters = json_decode($request->filters);

            $user = auth()->user();
            $approvers = ['80223', '80016', '80051', '80039', '80230', '80231', '80233', '80147'];

            $catalog_request = CatalogRequestViewModel::when(request('search'), function($query){
                return $query->where('catalog_requests.request_number', 'LIKE', '%' . request('search') . '%')
                             ->where('employee.name', 'LIKE', '%' . request('search') . '%')
                             ->where('suppliers.name', 'LIKE', '%' . request('search') . '%');
            })
            ->when(!in_array($user->id_number, $approvers), function($query) use($user){
                return $query->where('approver_id', $user->id)
                             ->whereIn('status_id', [3,5,6,7,8,9,10]);
            })
            ->when(in_array($user->id_number, $approvers), function($query) use($user){
                return $query->whereIn('status_id', [3,4,5,6,7,8,9,10]);
            })
            ->when($filters, function($query) use ($filters){
                return $query->where('status_id', $filters);
            })
            ->join('employee', 'catalog_requests.employee_id', '=', 'employee.id')
            ->join('suppliers', 'catalog_requests.supplier_id', '=', 'suppliers.id')
            ->select('catalog_requests.*')
            ->orderBy('catalog_requests.status_id', 'asc')
            ->orderBy('catalog_requests.updated_at', 'desc')
            ->paginate(request('perPage'));            
            
            return $this->responsePaginate($catalog_request, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function nonCatalogRequests(Request $request)
    {
        try
        {
            $filters = json_decode($request->filters);

            $user = auth()->user();
            $approvers = ['80223', '80016', '80051', '80039', '80230', '80231', '80233', '80147'];

            $non_catalog_request = PaymentRequest::select('payment_requests.*')
            ->when(request('search'), function($query){
                return $query->where('payment_requests.request_number', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('employee.name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('job_orders.order_number', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('job_orders.po_number', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('job_order_details.site_name', 'LIKE', '%' . request('search') . '%');
            })
            ->when(!in_array($user->id_number, $approvers), function($query) use($user){
                return $query->where('approver_id', $user->id)
                             ->whereIn('status_id', [3,5,6,7,8,9,10]);
            })
            ->when(in_array($user->id_number, $approvers), function($query) use($user){
                return $query->whereIn('status_id', [3,4,5,6,7,8,9,10]);
            })
            ->when($filters, function($query) use ($filters){
                return $query->when($filters->type, function($query) use ($filters) {
                    $query->where('request_type', $filters->type);
                });                
            })
            ->when($filters, function($query) use ($filters){
                return $query->when($filters->id, function($query) use ($filters) {
                    $query->where('status_id', $filters->id);
                });                
            })
            ->join('employee', 'payment_requests.employee_id', '=', 'employee.id')
            ->join('job_orders', 'payment_requests.job_order_id', '=', 'job_orders.id')
            ->join('job_order_details', 'payment_requests.job_order_details_id', '=', 'job_order_details.id')
            ->orderBy('status_id', 'asc')
            // ->orderBy('catalog_request_id')
            ->latest()
            ->paginate(request('perPage'));            
            
            return $this->responsePaginate($non_catalog_request, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function requestsList(Request $request)
    {
        try
        {
            $filters = json_decode($request->filters);
            $user = auth()->user();
            $requestList = EmployeeRequest::when(request('search'), function($query){
                return $query->where('employee.name', 'LIKE', '%' . request('search') . '%');
            })
            ->where('approver_id', $user->id)
            ->join('employee', 'requests.employee_id', '=', 'employee.id')
            ->select('requests.*')
            ->when($filters, function($query) use ($filters){
                return $query->where('approved', $filters);
            })
            ->orderBy('approved', 'asc')
            ->latest()
            ->paginate(request('perPage'));            
            
            return $this->responsePaginate($requestList, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function updateCatalogRequest(Request $request)
    {
        try
        {
            $to = [];
            $catalog_request = CatalogRequestViewModel::find($request->id);
            $catalog_request->status_id = $request->status;
            if($request->remarks)
                $catalog_request->remarks = $request->remarks;

            $catalog_request->save();

            $payment_request = PaymentRequest::where('catalog_request_id', $catalog_request->id)->first();
            if($payment_request && $payment_request->status_id != 10) {
                $payment_request->status_id = 10;
                $payment_request->save();

                $finance = EmployeeProfile::find(21);
                $to[] = $finance->email;
            }

            $details = [
                'request_number' => $catalog_request->request_number,
                'requestor' => $catalog_request->requestor,
                'supplier_name' => $catalog_request->supplier_name,
                'po_number' => $catalog_request->po_number,
                'supplier_name' => $catalog_request->supplier_name,
                'remarks' => $catalog_request->remarks,
                'status_name' => $catalog_request->status_name,
                'total_price' => $catalog_request->total_price_formated,
                'catalog_details' => $catalog_request->catalog_details,
                'subject' => 'Catalog Request - '. $catalog_request->status_name,
            ];  

            $approver_id = $catalog_request->approver_id;
            if($request->status == 4){
                $approver_id = 241;                
            }

            $requestor = EmployeeProfile::find($catalog_request->employee_id);
            $approver = EmployeeProfile::find($approver_id);

            // $to[] = $requestor->email;
            // $to[] = $approver->email;

            // $send = Mail::to($to)->queue(new CatalogRequestNotification($details));
            // if($send)
            //     return response([
            //         'message' => 'Status has been updated but with error sending email notification.',
            //         'status' => false,
            //         'status_code' => $this->notFoundStatus,
            //     ], $this->notFoundStatus);

            return $this->response($catalog_request, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function getTotalCatalog($id)
    {
        return CatalogRequestDetails::where('job_order_details_id', $id)
        ->where('catalog_requests.status_id', 7)
        ->join('catalog_requests', 'catalog_request_details.catalog_request_id', '=', 'catalog_requests.id')
        ->get()->sum('total_price');
    }

    public function updateRequest(Request $request)
    {
        try
        {
            $employeeRequest = EmployeeRequest::find($request->id);
            $employeeRequest->approved = $request->status;
            $employeeRequest->save();

            // email notification here

            return $this->response($employeeRequest, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function canvassingList(Request $request)
    {
        try
        {
            $catalog_request = CatalogRequestViewModel::when(request('search'), function($query){
                return $query->where('request_number', 'LIKE', '%' . request('search') . '%');
            })
            ->where('catalog_type', 'catalog')
            ->whereIn('status_id', [2,3,4,5,6,7,8])
            ->latest()
            ->paginate(request('perPage'));            
            
            return $this->responsePaginate($catalog_request, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function getSuppliers($catalog_id)
    {
        try
        {
            $suppliers_ids = CatalogRequestDetails::where('catalog_request_id', $catalog_id)
            ->groupBy('supplier_id')
            ->pluck('supplier_id');

            $suppliers = Supplier::whereIn('id', $suppliers_ids)->get();

            if($suppliers)
                return $suppliers;
            return null;
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function getGrossAmountPerSupplier(Request $request)
    {
        return CatalogRequestDetails::where('catalog_request_id', $request->request_id)
        ->where('supplier_id', $request->supplier_id)
        ->get()->sum('total_price');
    }

    public function createPoNumber(Request $request)
    {
        try
        {
            // ADD PREPARED BY CURRENT LOGED USER
            $user = auth()->user();
            $request->request->add(['prepared_by' => $user->name]);
            if(isset($request->client_id['id']))            
                $request["client_id"] = $request->client_id['id'];
            
            if(isset($request->job_order_id['id']))
                $request["job_order_id"] = $request->job_order_id['id'];

            if(isset($request->job_order_details_id['id']))
                $request["job_order_details_id"] = $request->job_order_details_id['id'];

            $purchase_order = CatalogRequestPurchaseOrder::where('catalog_request_id', $request->catalog_request_id)
            ->where('supplier_id', $request->supplier_id)->first();

            if(!$purchase_order) {
                $year = Carbon::now()->year;
                $date_now = Carbon::now()->format('md');;
                $po_number = PoNumber::first();

                $request->request->add(['po_number' => $year.'-'.$date_now.'-'.($po_number->sequence+1)]);
                $purchase_order = CatalogRequestPurchaseOrder::create($request->all());

                if($purchase_order)
                    $po_number->update([
                        'sequence' => $po_number->sequence+1
                    ]);
            }
            else {
                $purchase_order->update($request->all());
            }

            return $this->response($purchase_order, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return $e->getMessage();
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function requestPayment(Request $request)
    {
        try
        {
            $user = auth()->user();
            $purchase_order = CatalogRequestPurchaseOrder::where('po_number', $request->po_number)->first();

            $client_id = ($purchase_order->client_id) ? $purchase_order->client_id : Client::where('client_name', 'Admin')->first()->id;
            $job_order_id = ($purchase_order->job_order_id) ? $purchase_order->job_order_id : JobOrder::where('client_id', $client_id)->first()->id;
            $job_order_details_id = ($purchase_order->job_order_details_id) ? $purchase_order->job_order_details_id : JobOrderDetails::where('job_order_id', $job_order_id)->first()->id;

            $supplier = Supplier::find($purchase_order->supplier_id);

            // compute for amount base on percentage
            $non_catalog_request = [
                'employee_id' => $user->id,
                'client_id' => $client_id,
                'job_order_id' => $job_order_id,
                'job_order_details_id' => $job_order_details_id,
                'catalog_request_id' => $request->id,
                'request_id' => 31,
                'required_date' => $purchase_order->required_date,
                'approver_id' => $user->approver_id,
                'status_id' => 5,
                'payment_type' => ($client_id == 4) ? 'Admin Expense': 'Cos Expense',
                'request_type' => 'catalog',
                'release_type' => $request->release_type,
                'pay_to' => $request->supplier['name'],
                'gross_amount' => ($request->grand_total*($request->payment_terms['id']/100)),
                'remarks' => $request->remarks
            ];

            $pf_number = PoNumber::first();
            $pf_request_number = $pf_number->pf_number+1;

            $non_catalog = PaymentRequest::create($non_catalog_request);
            $non_catalog->request_number = date('Y').'-'.$pf_request_number;
            $non_catalog->save();

            $pf_number->pf_number = $pf_request_number;
            $pf_number->save();                

            return $this->response($non_catalog, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return $e->getMessage();
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function downloadReport(Request $request)
    {
        // try
        // {
            $directory = 'public/export/reports/';
            $files = Storage::files($directory);
            foreach ($files as $file) {
                Storage::delete($file);
            }

            $client_id = (isset($request->client_id['id'])) ? $request->client_id['id'] : '';
            $job_order_id = (isset($request->job_order_id['id'])) ? $request->job_order_id['id'] : '';
            $job_order_details_id = (isset($request->job_order_details_id['id'])) ? $request->job_order_details_id['id'] : '';

            $report_requests = SiteExpensesReportViewModel::
            when($client_id, function($query) use($client_id){
                return $query->where('job_orders.client_id', $client_id);
            })
            ->when($job_order_id, function($query) use($job_order_id){
                return $query->where('job_order_details.job_order_id', $job_order_id);
            })
            ->when($job_order_details_id, function($query) use($request){
                return $query->where('job_order_details.id', $job_order_details_id);
            })
            ->leftJoin('job_orders', 'job_order_details.job_order_id', '=', 'job_orders.id')
            ->get();

            $filename = "site-expenses-report.csv";
            // Store on default disk
            Excel::store(new SiteExpensesReport($report_requests), $directory.$filename);

            $data = [
                'filepath' => '/storage/export/reports/'.$filename,
                'filename' => $filename
            ];
            
            if(Storage::exists($directory.$filename))
                return $this->response($data, 'Successfully Retreived!', $this->successStatus); 

            //return $this->response(false, 'Successfully Retreived!', $this->successStatus); 
        // }
        // catch (\Exception $e)
        // {
        //     return response([
        //         'message' => $e->getMessage(),
        //         'status' => false,
        //         'status_code' => $this->unauthorizedStatus,
        //     ], $this->unauthorizedStatus);
        // }
    }

}
