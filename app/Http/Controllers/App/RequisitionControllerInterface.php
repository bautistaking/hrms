<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;

interface RequisitionControllerInterface
{
    /**
	 * @param integer $id
	 * @return Response
	 * @SWG\Get(
	 *      path="/requisition/catalog/{id}",
	 *      summary="Get catalog request details",
	 *      tags={"Mobile App - Requisition"},
	 *      description="Get catalog request details",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Parameter(
	 *          name="id",
	 *          description="ID",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getDetails(Request $request);

    /**
	 * @param integer $employee_id
	 * @param string $pay_to
	 * @param string $date
	 * @param string $pf_number
	 * @param string $project_site_name
	 * @param string $mob_date
	 * @param string $jo_number
	 * @param string $contractor
	 * @param string $supllier
	 * @param string $other
	 * @param string $remarks
	 * @param string $particulars
	 * @param number $amount
	 * @param number $net_vat
	 * @param number $vat_input
	 * @param number $ewt
	 * @return Response
	 * @SWG\Post(
	 *      path="/requisition/catalog/add",
	 *      summary="Create over time request",
	 *      tags={"Mobile App - Requisition"},
	 *      description="Create over time request",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Parameter(
	 *          name="employee_id",
	 *          description="Employee ID",
	 *          type="integer",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="pay_to",
	 *          description="Pay To",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="date",
	 *          description="Date",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="pf_number",
	 *          description="PF Number",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="project_site_name",
	 *          description="Project Site Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	 
	 *      @SWG\Parameter(
	 *          name="mob_date",
	 *          description="MOB Date",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="jo_number",
	 *          description="JO Number",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="contractor",
	 *          description="Contractor",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="supllier",
	 *          description="Supllier",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="other",
	 *          description="Other",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="remarks",
	 *          description="Remarks",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="particulars",
	 *          description="Particulars",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="amount",
	 *          description="Amount",
	 *          type="number",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="net_vat",
	 *          description="Net Vat",
	 *          type="number",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="vat_input",
	 *          description="Vat Input",
	 *          type="number",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="ewt",
	 *          description="EWT",
	 *          type="number",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function store(Request $request);

    /**
	 * @param integer $id
	 * @param integer $employee_id
	 * @param string $pay_to
	 * @param string $date
	 * @param string $pf_number
	 * @param string $project_site_name
	 * @param string $mob_date
	 * @param string $jo_number
	 * @param string $contractor
	 * @param string $supllier
	 * @param string $other
	 * @param string $remarks
	 * @param string $particulars
	 * @param number $amount
	 * @param number $net_vat
	 * @param number $vat_input
	 * @param number $ewt
	 * @return Response
	 * @SWG\Post(
	 *      path="/requisition/catalog/update",
	 *      summary="Update request",
	 *      tags={"Mobile App - Requisition"},
	 *      description="Update request",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Parameter(
	 *          name="id",
	 *          description="Request ID",
	 *          type="integer",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="employee_id",
	 *          description="Employee ID",
	 *          type="integer",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="pay_to",
	 *          description="Pay To",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="date",
	 *          description="Date",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="pf_number",
	 *          description="PF Number",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="project_site_name",
	 *          description="Project Site Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	 
	 *      @SWG\Parameter(
	 *          name="mob_date",
	 *          description="MOB Date",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="jo_number",
	 *          description="JO Number",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="contractor",
	 *          description="Contractor",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="supllier",
	 *          description="Supllier",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="other",
	 *          description="Other",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="remarks",
	 *          description="Remarks",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="particulars",
	 *          description="Particulars",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="amount",
	 *          description="Amount",
	 *          type="number",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="net_vat",
	 *          description="Net Vat",
	 *          type="number",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="vat_input",
	 *          description="Vat Input",
	 *          type="number",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="ewt",
	 *          description="EWT",
	 *          type="number",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function update(Request $request);

    /**
	 * @param string $id
	 * @return Response
	 * @SWG\Get(
	 *      path="/requisition/catalog/delete/{id}",
	 *      summary="Delete request",
	 *      tags={"Mobile App - Requisition"},
	 *      description="Delete request",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="Request ID",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function delete($id);

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/requisition/catalog/list",
	 *      summary="List of catalog per user",
	 *      tags={"Mobile App - Requisition"},
	 *      description="List of catalog per user",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function list(Request $request);

    /**
	 * @param integer $id
	 * @return Response
	 * @SWG\Get(
	 *      path="/requisition/catalog/details/list/{id}",
	 *      summary="List of catalog details",
	 *      tags={"Mobile App - Requisition"},
	 *      description="List of catalog details",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="Request ID",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),	 
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function detailList($id);

    /**
	 * @param integer $employee_id
	 * @param string $pay_to
	 * @param string $date
	 * @param string $pf_number
	 * @param string $project_site_name
	 * @param string $mob_date
	 * @param string $jo_number
	 * @param string $contractor
	 * @param string $supllier
	 * @param string $other
	 * @param string $remarks
	 * @param string $particulars
	 * @param number $amount
	 * @param number $net_vat
	 * @param number $vat_input
	 * @param number $ewt
	 * @return Response
	 * @SWG\Post(
	 *      path="/requisition/non-catalog/add",
	 *      summary="Create over time request",
	 *      tags={"Mobile App - Requisition"},
	 *      description="Create over time request",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Parameter(
	 *          name="employee_id",
	 *          description="Employee ID",
	 *          type="integer",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="pay_to",
	 *          description="Pay To",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="date",
	 *          description="Date",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="pf_number",
	 *          description="PF Number",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="project_site_name",
	 *          description="Project Site Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	 
	 *      @SWG\Parameter(
	 *          name="mob_date",
	 *          description="MOB Date",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="jo_number",
	 *          description="JO Number",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="contractor",
	 *          description="Contractor",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="supllier",
	 *          description="Supllier",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="other",
	 *          description="Other",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="remarks",
	 *          description="Remarks",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="particulars",
	 *          description="Particulars",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="amount",
	 *          description="Amount",
	 *          type="number",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="net_vat",
	 *          description="Net Vat",
	 *          type="number",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="vat_input",
	 *          description="Vat Input",
	 *          type="number",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="ewt",
	 *          description="EWT",
	 *          type="number",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function storeNonCatalog(Request $request);

    /**
	 * @param integer $employee_id
	 * @param string $pay_to
	 * @param string $date
	 * @param string $pf_number
	 * @param string $project_site_name
	 * @param string $mob_date
	 * @param string $jo_number
	 * @param string $contractor
	 * @param string $supllier
	 * @param string $other
	 * @param string $remarks
	 * @param string $particulars
	 * @param number $amount
	 * @param number $net_vat
	 * @param number $vat_input
	 * @param number $ewt
	 * @return Response
	 * @SWG\Post(
	 *      path="/requisition/non-catalog/update",
	 *      summary="Create over time request",
	 *      tags={"Mobile App - Requisition"},
	 *      description="Create over time request",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Parameter(
	 *          name="employee_id",
	 *          description="Employee ID",
	 *          type="integer",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="pay_to",
	 *          description="Pay To",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="date",
	 *          description="Date",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="pf_number",
	 *          description="PF Number",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="project_site_name",
	 *          description="Project Site Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	 
	 *      @SWG\Parameter(
	 *          name="mob_date",
	 *          description="MOB Date",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="jo_number",
	 *          description="JO Number",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="contractor",
	 *          description="Contractor",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="supllier",
	 *          description="Supllier",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="other",
	 *          description="Other",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="remarks",
	 *          description="Remarks",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="particulars",
	 *          description="Particulars",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="amount",
	 *          description="Amount",
	 *          type="number",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="net_vat",
	 *          description="Net Vat",
	 *          type="number",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="vat_input",
	 *          description="Vat Input",
	 *          type="number",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Parameter(
	 *          name="ewt",
	 *          description="EWT",
	 *          type="number",
	 *          required=false,
	 *          in="formData"
	 *      ),	 	 
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function updateNonCatalog(Request $request);

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/requisition/catalog-request/list",
	 *      summary="List of catalog per approver",
	 *      tags={"Mobile App - Requisition"},
	 *      description="List of catalog per approver",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function catalogRequests(Request $request);

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/requisition/request/list",
	 *      summary="List of employee request per approver",
	 *      tags={"Mobile App - Requisition"},
	 *      description="List of employee request per approver",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function requestsList(Request $request);

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/requisition/canvassing/list",
	 *      summary="List of catalog for canvassing",
	 *      tags={"Mobile App - Requisition"},
	 *      description="List of catalog for canvassing",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function canvassingList(Request $request);

    /**
	 * @return Response
	 * @param integer $request_id
	 * @param integer $supplier_id
	 * @SWG\Post(
	 *      path="/requisition/purchase-order/create",
	 *      summary="Create purchase order",
	 *      tags={"Mobile App - Requisition"},
	 *      description="Create purchase order",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Parameter(
	 *          name="request_id",
	 *          description="Request ID",
	 *          type="integer",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="supplier_id",
	 *          description="Supplier ID",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function createPoNumber(Request $request);

    /**
	 * @return Response
	 * @param integer $request_id
	 * @param integer $supplier_id
	 * @SWG\Put(
	 *      path="/requisition/supplier/gross-amount",
	 *      summary="List of catalog for canvassing",
	 *      tags={"Mobile App - Requisition"},
	 *      description="List of catalog for canvassing",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Parameter(
	 *          name="request_id",
	 *          description="Request ID",
	 *          type="integer",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="supplier_id",
	 *          description="Supplier ID",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function getGrossAmountPerSupplier(Request $request);
}
