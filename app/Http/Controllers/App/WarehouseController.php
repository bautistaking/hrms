<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\AppBaseController as ApiBaseController;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

use App\ViewModels\CatalogRequestViewModel;
use App\Models\Outbound;
use App\Models\Inbound;
use App\Models\InboundDetails;
use App\Models\OutboundDetails;
use App\Models\LogisticMaster;

class WarehouseController extends ApiBaseController
{
    /************************************** 
    *           WAREHOUSE APP             *
    **************************************/
    function getPurchaseOrders() {
        try
        {
            $catalog_request = CatalogRequestViewModel::where('catalog_type', 'catalog')
            ->orderBy('status_id', 'asc')
            ->latest()
            ->limit(10)
            ->get();            
            
            return $this->response($catalog_request, 'Successfully Retreived!', $this->successStatus);          
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function getOutboundRequest()
    {
        try
        {
            $outbound_list = Outbound::select('outbounds.*')
            ->leftJoin('clients', 'outbounds.client_id', '=', 'clients.id')
            ->leftJoin('job_orders', 'outbounds.job_order_id', '=', 'job_orders.id')
            ->orderBy('outbounds.status_id', 'ASC')
            ->limit(10)
            ->get(); 
            
            return $this->response($outbound_list, 'Successfully Retreived!', $this->successStatus);    
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getInboundRequest()
    {
        try
        {
            $inbound_list = Inbound::select('inbounds.*')
            ->orderBy('status_id', 'ASC')
            ->limit(10)
            ->get();

            return $this->response($inbound_list, 'Successfully Retreived!', $this->successStatus);    
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getInventory()
    {
        try
        {
            $logisticMaster = LogisticMaster::select('logistic_masters.*', 'categories.name as category_name')
            ->leftJoin('categories', 'logistic_masters.category_id', '=', 'categories.id')
            ->orderByDesc('logistic_masters.updated_at')
            ->limit(10)
            ->get();

            return $this->response($logisticMaster, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    function getTotalRequest() {
        try
        {
            $inbounds = Inbound::get();
            $outbounds = Outbound::get();
            $catalog_request = CatalogRequestViewModel::where('catalog_type', 'catalog')->get();  

            $total_requests = [
                'total_inbound' => number_format($inbounds->count()),
                'total_inbound_verifications' => number_format($inbounds->where('status_id', 3)->count()),
                'total_outbound' => number_format($outbounds->count()),
                'total_outbound_verifications' => number_format($outbounds->where('status_id', 3)->count()),
                'total_purchase_orders' => number_format($catalog_request->count()),
                'total_po_verifications' => number_format($catalog_request->where('status_id', 3)->count()),
            ];

            return $this->response($total_requests, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    function verifiedInbound(Request $request) 
    {
        try
        {
            $with_discrepancy = 0;
            foreach ($request->inbound_details as $particular) {
                if($particular['condition'] == 'Incomplete' || $particular['condition'] == 'With Defective')
                    $with_discrepancy++;

                if($particular['condition'] == 'Damage')
                    continue;

                $inbount_details = InboundDetails::find($particular['id']);
                if($inbount_details->condition == 'Complete' || $inbount_details->condition == 'Added')
                    continue;

                $logistic_item = LogisticMaster::find($particular['logistic_master_id']);
                if($logistic_item && 
                ($particular['condition'] == 'Complete' 
                || $particular['condition'] == 'New'
                || $particular['condition'] == 'Good'
                || $particular['condition'] == 'Retread'
                || $particular['condition'] == 'Fair'
                || $particular['condition'] == 'Poor'                
                )) 
                {
                    $logistic_item->in_stock = $logistic_item->in_stock+$particular['quantity_receive'];
                    $logistic_item->unit_price = $particular['unit_price'];
                    $logistic_item->save();

                    $condition = 'Complete';

                    switch($particular['condition']) {
                        case 'New':
                        case 'Good':
                        case 'Retread':
                        case 'Fair':
                        case 'Poor':
                            $condition = 'Added';
                        break;
                    }

                    $inbount_details->quantity_receive = $particular['quantity_receive'];
                    $inbount_details->condition = $condition;
                    $inbount_details->save();
                }
            }

            $inbound = Inbound::find($request->id); 
            $inbound->status_id = ($with_discrepancy) ? 12 : 4;
            $inbound->save();

            return $this->response($inbound, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }        
    }

    function verifiedOutbound(Request $request) {
        try
        {
            $user = auth()->user();

            $with_discrepancy = 0;
            foreach ($request->particulars as $particular) {
                if($particular['item_status'] == 'Out Stock')
                    $with_discrepancy++;

                $outbound_details = OutboundDetails::where('id',$particular['id'])->update(['item_status'=>$particular['item_status']]);
            }

            $outbound = Outbound::find($request->id); 
            $status_id = ($with_discrepancy) ? 12 : 6;
            $outbound->status_id = $status_id; 
            // if($status_id == 6){
            //     $outbound->approver_id = $user->id;
            // }
            $outbound->save();

            return $this->response($outbound, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        } 
    }

}
