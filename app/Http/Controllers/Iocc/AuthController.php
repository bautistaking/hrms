<?php

namespace App\Http\Controllers\Iocc;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use Illuminate\Support\Facades\Auth; 
use App\Services\Helpers\PasswordHelper;

use Illuminate\Support\Str;
use Carbon\Carbon;

use App\Models\Team;
use Hash;

class AuthController extends ApiBaseController implements AuthControllerInterface
{
    /****************************************
    * 			MOBILE APP AUTH 			*
    ****************************************/
    public function login(Request $request)
    {
    	try
    	{
            $team = Team::when(request('username'), function($query){
                return $query->where('email', '=', request('username'));
            })
            ->where('active', true)
            ->first();

    		if (!Hash::check(Team::getSalt(request('username')).env("PEPPER_HASH").request('password'), $team->password)) 
    			return response([
                    'message' => 'Invalid Email or Password.',
                    'status' => false,
                    'status_code' => $this->notFoundStatus,
                ], $this->notFoundStatus);

    		Auth::login($team);
		    // get new tokenl
		    $tokenResult = $team->createToken(env('APP_NAME'));

	        $data = [
                'token' => $tokenResult->accessToken,
                'user'  => $team,
             ];

            return $this->response($data, 'Successfully Loged!', $this->successStatus);
    	}
    	catch (\Exception $e) 
        {
    		 return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
    	}
    }

    public function logout(Request $request)
    {
        try
        {
            $request->user()->token()->revoke();
            $request->user()->tokens->each(function ($token,$key) {
                $token->delete();
            });

            return $this->response(null, 'Successfully logged out!', $this->successStatus);
        }
        catch (\Exception $e) 
        {
             return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
