<?php

namespace App\Http\Controllers\Iocc;

use App\Http\Controllers\AppBaseController as ApiBaseController;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\ViewModels\DashboardTicketDetails;
use App\ViewModels\RegionViewModel;
use App\Models\Ticket;
use App\Models\JobOrderDetails;

class DashboardController extends ApiBaseController implements DashboardControllerInterface
{
    public function siteList(Request $request)
    {
        $filters = json_decode($request->filters);        
        $ticket_detaisl_ids = [];
        $service_tickets = [];
        $statuses = [];
        $regions = [];

        if($filters) {
            $service_tickets = $filters->service_ticket;
            $statuses = $filters->status;
            $regions = $filters->region;
        }

        if($regions) {
            $ticket_detaisl_ids = JobOrderDetails::whereIn('region_id', $regions)
            ->join('tickets', 'job_order_details.id', '=', 'tickets.job_order_details_id')
            ->select('tickets.job_order_details_id')
            ->get()
            ->pluck('job_order_details_id');
        }

    	$site_list = Ticket::when($ticket_detaisl_ids, function($query) use ($ticket_detaisl_ids){
            return $query->whereIn('job_order_details_id', $ticket_detaisl_ids);
        })
        ->when($statuses, function($query) use ($filters){
            return $query->whereIn('status', $filters->status);
        })
        ->latest()
        ->paginate(request('perPage'));

        return $this->responsePaginate($site_list, 'Successfully Retreived!', $this->successStatus);
    }

    public function quickIssues(Request $request)
    {
        $filters = json_decode($request->filters);        
        $ticket_detaisl_ids = [];
        $service_tickets = [];
        $statuses = [];
        $regions = [];

        if($filters) {
            $service_tickets = $filters->service_ticket;
            $statuses = $filters->status;
            $regions = $filters->region;
        }

        if($regions) {
            $ticket_detaisl_ids = JobOrderDetails::whereIn('region_id', $regions)
            ->join('tickets', 'job_order_details.id', '=', 'tickets.job_order_details_id')
            ->select('tickets.id')
            ->get()
            ->pluck('id');
        }

    	$quick_issues = DashboardTicketDetails::whereNull('tickets.deleted_at')
        ->when($ticket_detaisl_ids, function($query) use ($ticket_detaisl_ids){
            return $query->whereIn('ticket_details.ticket_id', $ticket_detaisl_ids);
        })
        ->when($service_tickets, function($query) use ($filters){
            return $query->whereIn('ticket_details.plans_reference_id', $filters->service_ticket);
        })
        ->when($statuses, function($query) use ($filters){
            return $query->whereIn('ticket_details.status', $filters->status);
        })
        ->when(!$statuses, function($query) use ($filters){
            return $query->whereIn('ticket_details.status', ['4','5']);
        })
        ->join('tickets', 'ticket_details.ticket_id', '=', 'tickets.id')
		->orderBy('ticket_details.created_at')
        ->paginate(request('perPage'));

        return $this->responsePaginate($quick_issues, 'Successfully Retreived!', $this->successStatus);
    }

    public function regionalSites(Request $request)
    {
    	$regions = RegionViewModel::get();
    	$regional_sites = [];
    	foreach ($regions as $region) {
    		if($region->name) {
	    		$regional_sites['site_names'][] = $region->name;
	    		$regional_sites['total_done'][] = $region->total_done;
	    		$regional_sites['total_on_going'][] = $region->total_on_going;    			
    		}
    	}

        return $this->response($regional_sites, 'Successfully Retreived!', $this->successStatus);
    }

    public function totalSites(Request $request)
    {
    	$total_sites = [];

    	$done_ticket = Ticket::where('status', 2)->get()->count();
    	$on_going_ticket = Ticket::where('status', 1)->get()->count();

    	$total_sites = [$done_ticket, $on_going_ticket];

        return $this->response($total_sites, 'Successfully Retreived!', $this->successStatus);
    }

    public function fishboneFunnel(Request $request)
    {
    	$service_tickets = [];

    	$ticket_details = DashboardTicketDetails::selectRaw('COUNT(ticket_details.status) AS totals, ticket_details.*')
    	->join('plans_references', 'ticket_details.plans_reference_id', 'plans_references.id')
    	->groupByRaw('ticket_details.status, ticket_details.plans_reference_id')
    	->orderBy('ticket_details.plans_reference_id')
    	->get();

    	foreach ($ticket_details as $ticket) {
            $service_tickets[$ticket->plans_reference_id]['plans_reference_id'] = $ticket->plans_reference_id;
    		$service_tickets[$ticket->plans_reference_id]['title'] = $ticket->ticket_name;
    		$service_tickets[$ticket->plans_reference_id]['status'][] = $ticket->status_name;

    		$color = '';

			switch ($ticket->status_name) {
    			case 'To Start':
					$color = '#31d2f2';
				break;
    			case 'On-Going':
					$color = '#5c636a';
				break;
    			case 'Completed':
					$color = '#157347';
				break;
    			case 'Critical':
					$color = '#ffca2c';
				break;
    			case 'Delayed':
					$color = '#dc3545';
				break;
    		}    		

    		$service_tickets[$ticket->plans_reference_id]['colors'][] = $color;
    		$service_tickets[$ticket->plans_reference_id]['totals'][] = $ticket->totals;
    	}

        return $this->response($service_tickets, 'Successfully Retreived!', $this->successStatus);

    }

    public function servicestatus(Request $request)
    {
        $filters = json_decode($request->filters);        
        $service_tickets = [];

        if($filters) {
            $reference_ids = $filters->service_ticket;
        }

    	$ticket_details = DashboardTicketDetails::when($reference_ids, function($query) use ($filters){
            return $query->where('ticket_details.plans_reference_id', $filters->service_ticket);
        })
        ->selectRaw('COUNT(ticket_details.status) AS totals, ticket_details.*')
    	->join('plans_references', 'ticket_details.plans_reference_id', 'plans_references.id')
    	->groupByRaw('ticket_details.status')
    	->orderBy('ticket_details.plans_reference_id')
    	->get();    

    	foreach ($ticket_details as $ticket) {
    		$service_tickets['status'][] = $ticket->status_name;

    		$color = '';

			switch ($ticket->status_name) {
    			case 'To Start':
					$color = '#31d2f2';
				break;
    			case 'On-Going':
					$color = '#5c636a';
				break;
    			case 'Completed':
					$color = '#157347';
				break;
    			case 'Critical':
					$color = '#ffca2c';
				break;
    			case 'Delayed':
					$color = '#dc3545';
				break;
    		}    		

    		$service_tickets['colors'][] = $color;
    		$service_tickets['totals'][] = $ticket->totals;
    	}

        return $this->response($service_tickets, 'Successfully Retreived!', $this->successStatus);

    }
}
