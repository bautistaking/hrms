<?php

namespace App\Http\Controllers\Iocc;

use App\Http\Controllers\AppBaseController as ApiBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

use App\Models\Team;
use App\Models\Ticket;
use App\Models\TicketDetails;
use App\Mail\RequestUpdate;

class TeamController extends ApiBaseController implements TeamControllerInterface
{
    public function getDetails()
    {
    	try
        {
            $user = auth()->user();
            $userProfile = Team::find($user->id);

            return $this->response($userProfile, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function getTickets()
    {
    	try
        {
            $user = auth()->user();
            $tickets = Ticket::where('team_id',$user->id)->limit(20)->latest()->get();

            return $this->response($tickets, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function requestUpdate(Request $request)
    {
    	try
        {
            $ticket_details = TicketDetails::find($request->id);
            $ticket = Ticket::find($ticket_details->ticket_id);

            $status = '';
            if($request->status == 2){
                $status = 'On Going';
            }
            else if($request->status == 3) {
                $status = 'Done';
            }

            $details = [
				'subject' => "Ticket Status Update for " . $ticket->ticket_number,
				'ticket_number' => $ticket->ticket_number,
				'client_name' => $ticket->client_name,
				'job_order' => $ticket->job_order,
				'site_name' => $ticket->site_name,
				'team_name' => $ticket->team_name,
	            'ticket_name' => $ticket_details->ticket_name,
				'status' => $status,
				'remarks' => $request->remarks,
			];

            // $to = ['bautistael23@gmail.com', 'mark_gana@fabriquem.com', 'john_sarte@fabriquem.com', 'fpy@fabriquem.com'];

			// $send = Mail::to($to)
			// 			->send(new RequestUpdate($details));
            return $this->response($ticket_details, 'Request successfully sent!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }
 
}
