<?php

namespace App\Http\Controllers\Iocc;

use Illuminate\Http\Request;

interface TeamControllerInterface
{
    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/team/details",
	 *      summary="Team details",
	 *      tags={"Team App - Team"},
	 *      description="Team details",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getDetails();

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/team/tickets",
	 *      summary="Team tickets",
	 *      tags={"Team App - Team"},
	 *      description="Team details",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getTickets();

    /**
	 * @param integer $id
	 * @param string $status
	 * @param string $remarks
	 * @return Response
	 * @SWG\Post(
	 *      path="/team/ticket/request-update",
	 *      summary="Request ticket update",
	 *      tags={"Team App - Team"},
	 *      description="Request ticket update",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="Ticket ID",
	 *          type="integer",
	 *          required=true,
	 *          in="formData"
	 *      ),	 
	 *       @SWG\Parameter(
	 *          name="status",
	 *          description="Status",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	
	 *       @SWG\Parameter(
	 *          name="remarks",
	 *          description="Remarks",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),		 
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function requestUpdate(Request $request);

}
