<?php

namespace App\Http\Controllers\Iocc;

use App\Http\Controllers\AppBaseController as ApiBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

use App\Models\Ticket;
use App\Models\TicketDetails;
use App\Models\TicketDetailsRemark;
use App\Models\PlansReference;
use App\Models\Holiday;
use App\Mail\AssignTicket;
use App\Mail\Remarks;

class TicketController  extends ApiBaseController implements TicketControllerInterface
{
	public function getDetails($id)
	{
		try
        {
            $ticket = Ticket::find($id);
            return $this->response($ticket, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function list(Request $request)
    {
    	try
        {
            $tickets = Ticket::when(request('search'), function($query) {
                return $query->where('job_order_details.site_name', 'LIKE', '%' . request('search') . '%');
            })
            ->select('tickets.*')
            ->join('job_order_details', 'tickets.job_order_details_id', '=', 'job_order_details.id')
            ->latest('tickets.created_at')
            ->limit(50)->get();

            return $this->response($tickets, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function getReferences(Request $request)
    {
        $reference_date = [];
        $plans_reference = PlansReference::where('solution', strtolower(request('solution')))
        ->get();

        foreach ($plans_reference as $reference) {
            $skip_days = 0;
            $number_of_days = 0;
            $ticket_start_date = '';
            $ticket_end_date = '';

            switch (request('service_type')) {
                case 'SIMPLE':
                    $skip_days = $reference->simple_skip;
                    $number_of_days = $reference->simple;
                break;
                case 'MEDIUM':
                    $skip_days = $reference->medium_skip;
                    $number_of_days = $reference->medium;
                break;
                case 'COMPLEX':
                    $skip_days = $reference->complex_skip;
                    $number_of_days = $reference->complex;
                break;
            }

            $ticket_start_date = $this->validateDate(request('start_date'), $skip_days);
            $ticket_end_date = $this->validateDate($ticket_start_date, $number_of_days);

            $reference_date[] =
            [
                'plans_reference_id' => $reference->id,
                'plans_reference_name' => $reference->name,
                'number_of_days' => $number_of_days,
                'start_date' => $ticket_start_date,
                'end_date' => $ticket_end_date,
            ];
        }

        return $this->response($reference_date, 'Successfully Retreived!', $this->successStatus);
    }

    public function create(Request $request)
    {
    	try
        {
            $user = auth()->user();
            $ticket = '';
            $ticket = Ticket::find($request->id);            
            if($ticket) {
                $ticket->client_id = $request->client_id['id'];
                $ticket->job_order_id = $request->job_order_id['id'];
                $ticket->job_order_details_id = $request->job_order_details_id['id'];
                $ticket->service_type = $request->service_type;
                $ticket->solution_type = $request->solution_type;
                $ticket->remarks = $request->remarks;
                $ticket->start_date = $request->start_date;
                $ticket->is_priority = ($request->is_priority) ? $request->is_priority : 0;
                $ticket->updated_by = $user->id;
                $ticket->save();
            }
            else {
                $new_ticket = [
                    'client_id' => $request->client_id['id'],
                    'job_order_id' => $request->job_order_id['id'],
                    'job_order_details_id' => $request->job_order_details_id['id'],
                    'service_type' => $request->service_type,
                    'solution_type' => $request->solution_type,
                    'remarks' => $request->remarks,
                    'start_date' => $request->start_date,
                    'is_priority' => ($request->is_priority) ? $request->is_priority : 0,
                    'track_code' => 1,
                    'status' => 1,
                    'created_by' =>  $user->id
                ];

                $ticket = Ticket::create($new_ticket);

                $ticket->ticket_number = 'IOCC-'.date('Ym').'-'.str_pad($ticket->id, 5, "0", STR_PAD_LEFT);
                $ticket->save();
            }

            if($ticket) {
                $this->storeTicketDetails($ticket, $request->ticket_references);
                $ticket->end_date = TicketDetails::where('ticket_id', $ticket->id)
                                                 ->orderBy('id', 'DESC')
                                                 ->first()
                                                 ->end_date;
                $ticket->save();
            }


            return $this->response($ticket, 'Successfully Saved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function storeTicketDetails($ticket, $ticket_references)
    {
        $user = auth()->user();
        $emails = [];

        foreach ($ticket_references as $reference) {
            if($reference['number_of_days'] > 0){
                $ticket_end_date = '';
                TicketDetails::updateOrCreate(
                    [
                        'ticket_id' => $ticket->id,
                        'plans_reference_id' => $reference['plans_reference_id'],
                    ],  
                    [
                        'start_date' => $reference['start_date'],
                        'end_date' => $reference['end_date'],
                        'team_id' => $reference['team_id']['id'],
                        'remarks' => (array_key_exists('remarks',$reference)) ? $reference['remarks'] : '',
                        'status' => 1,
                        'created_by' =>  $user->id,
                    ]
                );

                $remarks = (array_key_exists('remarks',$reference)) ? $reference['remarks'] : '';
                $emails[$reference['team_id']['email']][] = $reference['plans_reference_name'] .' ( Remarks: '.$remarks.')';

                $team_emails = [];
                if(is_array($reference['team_id']['team_members'])) {
                    foreach ($reference['team_id']['team_members'] as $team_member) {
                        $team_emails[] = $team_member['email'];
                    }
                }
                $emails[$reference['team_id']['email']]['team_emails'] = $team_emails;

                $ticket_end_date = $reference['end_date'];
            }
        }

        if(count($emails) > 0) {
            foreach ($emails as $email => $tickets) {

                $cc = [];
                $cc = $tickets['team_emails'];
                unset($tickets['team_emails']);

                $details = [
                    'subject' => "Assign Tickets ". $ticket->ticket_number,
                    'client_name' => $ticket->client_name,
                    'job_order' => $ticket->job_order,
                    'site_name' => $ticket->site_name,
                    'tickets' => $tickets,
                    'remarks' => $remarks,
                ];

                // $to = $email;
                // $send = Mail::to($to)->cc($cc)
                //         ->queue(new AssignTicket($details));
            }
        }

        return $ticket_end_date;
    }

    public function validateDate($start_date, $number_of_days)
    {
        $holidays = Holiday::get()->pluck('date_from')->toArray();

        $MyDateCarbon = Carbon::createFromFormat('Y-m-d',$start_date);
        $MyDateCarbon->addWeekdays($number_of_days);

        for ($i=1; $i < $number_of_days; $i++) { 
            if (in_array(Carbon::createFromFormat('Y-m-d',$start_date)->addWeekdays($i)->toDateString(), $holidays)) {
                $MyDateCarbon->addWeekdays();
            }
        }

        if (in_array(Carbon::createFromFormat('Y-m-d',$MyDateCarbon->format('Y-m-d'))->toDateString(), $holidays)) {
            $MyDateCarbon->addWeekdays();
        }

        return $MyDateCarbon->format('Y-m-d');
    }

    public function update(Request $request)
    {
    	try
        {
            $user = auth()->user();
            $ticket_details = TicketDetails::find($request->id);            
            $ticket_details->status = $request->status;
            $ticket_details->save();

            $status_count = TicketDetails::where('ticket_id', $ticket_details->ticket_id)
                                       ->where('status', '!=', 3)
                                       ->get()->count();

           $ticket = Ticket::find($ticket_details->ticket_id);

            if($status_count == 0) {
               $ticket->status = 2;
               $ticket->track_code = 2;
            }
            else {
               $ticket->status = 1;
               $ticket->track_code = 1;
            }
           $ticket->updated_by = $user->id;
           $ticket->save();
            
            return $this->response($ticket_details, 'Status has been updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function updateStatusByDate()
    {
        try
        {
            $user = auth()->user();

            $ticket_details = TicketDetails::where('tickets.status', 1)
            ->where('ticket_details.status', '!=', 3)
            ->join('tickets', 'ticket_details.ticket_id', '=', 'tickets.id')
            ->select('ticket_details.*')
            ->get();

            foreach ($ticket_details as $ticket) {
                $ticket_info = TicketDetails::find($ticket->id);

                $middle_date = 0;
                $date_now = Carbon::now()->timestamp;                
                $date_from = Carbon::parse($ticket->start_date);
                $date_to = Carbon::parse($ticket->end_date)->timestamp; 

                if($ticket->number_of_days > 15) {
                    $middle_date = $date_from->addWeekdays(($ticket->number_of_days/2))->timestamp;
                }

                if($middle_date > 0 && $middle_date < $date_now) {
                    $ticket_info->status = 4;
                }

                if ($date_to < $date_now) {
                    $ticket_info->status = 5;
                }

                $ticket_info->save();
            }
            
            
            return $this->response($ticket_details, 'Status has been updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function updateSiteStatus(Request $request)
    {
        try
        {
            $user = auth()->user();
            $ticket = Ticket::find($request->id);
            $ticket->status = $request->status;
            $ticket->updated_by = $user->id;
            $ticket->save();

            if($ticket->status == 2) {
                TicketDetails::where('ticket_id', $ticket->id)->update(array('status' => 3));
            }
            
            return $this->response($ticket, 'Status has been updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function delete($id)
    {
    	try
        {
            $user = auth()->user();
            $ticket = Ticket::find($id);
            $ticket->updated_by = $user->id;
            $ticket->delete();
            return $this->response($ticket, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function saveRemarks(Request $request)
    {
        try
        {
            $user = auth()->user();

            $remark = TicketDetailsRemark::find($request->id);
            if($remark) {
                $remark->remarks = $request->remarks;
                $remark->save();
            }
            else {
                $params = [
                    'ticket_details_id' => $request->ticket_id,
                    'remarks_by' => $user->id,
                    'remarks' => $request->remarks
                ];

                $remark = TicketDetailsRemark::create($params);
            }

            $ticket_details = TicketDetails::find($remark->ticket_details_id);

            $ticket = Ticket::find($ticket_details->ticket_id);
            $details = [
                'subject' => "Remarks On ". $ticket->ticket_number,
                'client_name' => $ticket->client_name,
                'job_order' => $ticket->job_order,
                'site_name' => $ticket->site_name,
                'ticket' => $ticket_details->ticket_name,
                'remarks' => $request->remarks,
            ];

            // $to = $ticket_details->team_details->email; 
            // $send = Mail::to($to)
            //         ->queue(new Remarks($details));

            return $this->response($ticket_details, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function deleteRemarks($id)
    {
        try
        {
            $remark = TicketDetailsRemark::find($id);                
            $ticket_details = TicketDetails::find($remark->ticket_details_id);

            $remark->delete();

            return $this->response($ticket_details, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

}
