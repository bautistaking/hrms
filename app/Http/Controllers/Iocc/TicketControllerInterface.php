<?php

namespace App\Http\Controllers\Iocc;

use Illuminate\Http\Request;

interface TicketControllerInterface
{
    public function getDetails($id);

    public function list(Request $request);

    public function create(Request $request);

    public function update(Request $request);

    public function delete($id);
}
