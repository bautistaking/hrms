<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Artisan;

class SpaController extends Controller
{
    public function index()
    {
    	return view('kcms');
    }

    public function webApp()
    {
        // $exitCode = Artisan::call('cache:clear');
        // $exitCode = Artisan::call('optimize');
        // return '<h1>Reoptimized class loader</h1>';
    	return view('welcome');
    }

}
