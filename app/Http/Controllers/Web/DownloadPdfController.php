<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\AppBaseController as ApiBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Carbon\Carbon;
use PDF;

use App\ViewModels\EmployeePaySlip;
use App\ViewModels\SiteExpensesReportViewModel;

class DownloadPdfController extends ApiBaseController
{
    public function downloadPayslip($payperiod, $employee_id)
    {
        $payroll = EmployeePaySlip::where('payrolls.employee_id',$employee_id)
            ->where('pay_periods.is_lock', true)
            ->where('pay_period', $payperiod)
            ->whereNull('pay_periods.deleted_at')
            ->join('pay_periods', 'pay_periods.id', '=', 'payrolls.pay_period')
            ->first();

        $filename = $payroll->employee_details->name.'-'.$payroll->created_at;

        $pdf = PDF::loadView('payslip.index',compact('payroll'));
        return $pdf->download($filename.'.pdf');
    }

    public function downloadExpensesReport($client_id)
    {
        $report_requests = SiteExpensesReportViewModel::where('job_orders.client_id', $client_id)
                            ->join('job_orders', 'job_order_details.job_order_id', '=', 'job_orders.id')
                            ->get();

        $filename = 'site-expenses-report';

        $pdf = PDF::loadView('reports.expenses',compact('report_requests'));
        return $pdf->download($filename.'.pdf');
    }
}
