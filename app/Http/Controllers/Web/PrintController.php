<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\AppBaseController as ApiBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Carbon\Carbon;

use App\Models\CatalogRequest;
use App\Models\CatalogRequestDetails;
use App\Models\CatalogRequestPurchaseOrder;
use App\Models\JobOrderDetails;
use App\Models\EmployeeRequest;
use App\Models\Supplier;
use App\Models\Outbound;
use App\Models\PaymentRequest;
use App\ViewModels\EmployeeProfile;
use App\ViewModels\CatalogRequestViewModel;
use App\ViewModels\SiteExpensesReportViewModel;

class PrintController extends ApiBaseController
{
    public function purchaseOrder($po_number)
    {
    	try
        {
	    	$po_info = $this->getCatalogDetails($po_number);
	    	return view('purchase_order.index', compact("po_info"));
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function paymentForm($po_number)
    {
    	$po_info = $this->getCatalogDetails($po_number);
        return view('payment.index', compact("po_info"));
    }

    public function getCatalogDetails($po_number)
    {
    	return CatalogRequestPurchaseOrder::where('po_number', $po_number)->first();
    }

    public function gatepass($request_number)
    {
        $outbound = Outbound::where('request_number', $request_number)->first();
        return view('gatepass.index', compact("outbound"));
    }

    public function printNonCatalog($id)
    {
        $request_details = PaymentRequest::find($id);
        $request_details->is_printed = 1;
        $request_details->save();
        return view('payment.non-catalog', compact("request_details"));
    }

    public function printExpensesReport($client_id, $order_id = null, $details_id = null)
    {
        $report_requests = SiteExpensesReportViewModel::where('job_orders.client_id', $client_id)
                            ->when($order_id, function($query) use ($order_id){
                                return $query->where('job_order_details.job_order_id', $order_id);
                            })
                            ->when($details_id, function($query) use ($details_id){
                                return $query->where('job_order_details.id', $details_id);
                            })
                            ->join('job_orders', 'job_order_details.job_order_id', '=', 'job_orders.id')
                            ->get();
        return view('reports.expenses', compact("report_requests"));
    }

    
}
