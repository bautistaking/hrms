<?php

namespace App\Http\Requests\Api\BannerAd;

use Illuminate\Foundation\Http\FormRequest;

class AddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|string",
            "short_description" => "nullable|string",
            "image_url" => "required|string",
            "link" => "nullable|string",
            "type" => "required|string",
            "active" => "nullable|string",
        ];
    }
}
