<?php

namespace App\Http\Requests\Api\Customer;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "email" => "required|string",
            "password" => "nullable",
            "first_name" => "nullable",
            "middle_name" => "nullable",
            "last_name" => "nullable",
            "suffix" => "nullable",
            "birth_date" => "nullable",
            "phone_number" => "nullable",
            "mobile_number" => "required|regex:/^([0-9\s\-\+\(\)]*)$/|min:11|max:13",
            "image_url" => "nullable",
        ];
    }
}
