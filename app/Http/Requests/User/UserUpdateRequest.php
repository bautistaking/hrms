<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "id" => "required|integer",
            "userName" => "required|string",
            "email" => "required|string",
            "firstName" => "string",
            "lastName" => "string",
            "password" => "string|nullable",
            "emailNotification" => "integer|nullable",
            "role" => "integer"
        ];
    }
}
