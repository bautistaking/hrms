<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithValidation;

use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use App\Services\Helpers\PasswordHelper;
use App\Models\Employee;

class EmployeeImport implements ToCollection, WithHeadingRow 
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
    	foreach ($rows as $row) 
        {
            $salt = bcrypt(PasswordHelper::generateSalt());

	        $employee = Employee::updateOrCreate(
		    	[
		            'id_number' => $row['employee_id'],
		            'email' => $row['email']
		    	],	
		    	[
		            'name' => $row['last_name'].', '.$row['first_name'].' '.$row['middle_initial'],
		            'password' => PasswordHelper::generate($salt, $row['password']),
		            'salt' => $salt,
		            'designation_id' => $row['designation_id'],
		            'category_id' => $row['category_id']
			    ]
			);

			$request['first_name'] = $row['first_name'];
			$request['last_name'] = $row['last_name'];
			$request['middle_name'] = $row['middle_initial'];
			$request['age'] = $row['age'];
			$request['address'] = $row['address'];
			$request['mobile_number'] = $row['mobile_number'];
			$request['contact_number'] = $row['contact_number'];
			$request['active'] = 1;
			$request['sss'] = $row['sss_number'];
			$request['pag_ibig'] = $row['pag_ibig'];
			$request['philhealth'] = $row['philhealth'];
			$request['tin_number'] = $row['tin_number'];
			$request['basic_salary'] = $row['basic_salary'];
			$request['daily_wage'] = $row['daily_salary'];
			$request['cola'] = $row['cola'];
			$request['sss_prem'] = $row['sss_prem'];
			$request['philhealth_prem'] = $row['pagibig_prem'];
			$request['pagibig_prem'] = $row['philhealth_prem'];
			$request['birth_date'] = $row['date_of_birth'];
			$request['date_hired'] = $row['date_hired'];
			$request['date_regularization'] = $row['date_regularization'];

			$employee->addMeta($request);

	    }
    }
}
