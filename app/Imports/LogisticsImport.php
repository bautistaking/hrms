<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithValidation;

use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use App\Models\LogisticMaster;
use App\Models\Category;

class LogisticsImport implements ToCollection, WithHeadingRow 
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
    	foreach ($rows as $row) {
	        $logistic = LogisticMaster::updateOrCreate(
		    	[
		            'sku' => $row['sku']
		    	],	
		    	[
		            'item_name' => $row['item_name'],
		            'unit_of_measurement' => $row['uom'],
		            'category_id' => $this->getCategoryId($row['category']),
		            'in_stock' => $row['stock'],
                    'unit_price' => $row['unit_price'],
			    ]
			);
	    }
    }

    public function getCategoryId($category_name)
    {
    	$category = Category::select('id')->where('name', $category_name)->first();
    	if($category) {
	    	return $category->id;
    	}
    	return 0;
    }
}
