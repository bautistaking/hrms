<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithValidation;

use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use App\Models\Supplier; 

class SupplierImport implements ToCollection, WithHeadingRow 
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
	        $logistic = Supplier::updateOrCreate(
		    	[
		            'name' => $row['name']
		    	],	
		    	[
		            'address' => $row['address'],
		            'tin' => $row['tin'],
		            'terms' => $row['terms'],
		            'contact_person' => $row['contact_person'],
		            'contact_number' => $row['contact_number'],
		            'email' => $row['email']
			    ]
			);
	    }
    }
}
