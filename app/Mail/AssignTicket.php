<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AssignTicket extends Mailable
{
    use Queueable, SerializesModels;

    protected $client_name;
    protected $job_order;
    protected $site_name;
    protected $tickets;
    protected $remarks;
    public $subject;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->client_name = $details['client_name'];
        $this->job_order = $details['job_order'];
        $this->site_name = $details['site_name'];
        $this->tickets = $details['tickets'];
        $this->remarks = $details['remarks'];
        $this->subject = $details['subject'];    
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.assign-ticket')
            ->with([
                'client_name' => $this->client_name,
                'job_order' => $this->job_order,
                'site_name' => $this->site_name,
                'tickets' => $this->tickets,
                'remarks' => $this->remarks,
            ])
            ->subject($this->subject);
    }
}
