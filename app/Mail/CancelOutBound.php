<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CancelOutBound extends Mailable
{
    use Queueable, SerializesModels;

    protected $request_number;
    protected $client_name;
    protected $po_number;
    protected $site_name;
    protected $required_date;
    protected $pull_out_from;
    protected $deliver_to;
    protected $remarks;
    protected $status;
    protected $outbound_details;    
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->request_number = $details['request_number'];
        $this->client_name = $details['client_name'];
        $this->po_number = $details['po_number'];
        $this->site_name = $details['site_name'];
        $this->required_date = $details['required_date'];
        $this->pull_out_from = $details['pull_out_from'];
        $this->deliver_to = $details['deliver_to'];
        $this->remarks = $details['remarks'];
        $this->status = $details['status'];
        $this->outbound_details = $details['outbound_details'];    
        $this->subject = $details['subject'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.outbound')
            ->with([
                'request_number' => $this->request_number,
                'client_name' => $this->client_name,
                'po_number' => $this->po_number,
                'site_name' => $this->site_name,
                'required_date' => $this->required_date,
                'pull_out_from' => $this->pull_out_from,
                'deliver_to' => $this->deliver_to,
                'remarks' => $this->remarks,
                'status' => $this->status,
                'outbound_details' => $this->outbound_details
            ])
            ->subject($this->subject);
    }
}
