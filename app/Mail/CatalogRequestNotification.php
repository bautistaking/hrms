<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CatalogRequestNotification extends Mailable
{
    use Queueable, SerializesModels;

    protected $request_number;
    protected $requestor;
    protected $supplier_name;
    protected $po_number;
    protected $remarks;
    protected $status_name;
    protected $total_price;
    protected $catalog_details;
    public $subject;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->request_number = $details['request_number'];
        $this->requestor = $details['requestor'];
        $this->supplier_name = $details['supplier_name'];
        $this->po_number = $details['po_number'];
        $this->remarks = $details['remarks'];
        $this->status_name = $details['status_name'];
        $this->total_price = $details['total_price'];
        $this->catalog_details = $details['catalog_details'];
        $this->subject = $details['subject'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.catalog')
            ->with([
                'request_number' => $this->request_number,
                'requestor' => $this->requestor,
                'supplier_name' => $this->supplier_name,
                'po_number' => $this->po_number,
                'remarks' => $this->remarks,
                'status_name' => $this->status_name,
                'total_price' => $this->total_price,
                'catalog_details' => $this->catalog_details
            ])
            ->subject($this->subject);
    }
}
