<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LeaveRequest extends Mailable
{
    use Queueable, SerializesModels;

    protected $date;
    protected $description;
    protected $number_of_hours;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->date = $details['date'];
        $this->description = $details['description'];
        $this->number_of_hours = $details['number_of_hours'];
        $this->subject = $details['subject'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.employee-request')
            ->with([
                'date' => $this->date,
                'description' => $this->description,
                'number_of_hours' => $this->number_of_hours,
            ])
            ->subject($this->subject);
    }
}
