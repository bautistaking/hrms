<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NonCatalogNotification extends Mailable
{
    use Queueable, SerializesModels;

    protected $pay_to;
    protected $site_name;
    protected $date_requested;
    protected $date_needed;
    protected $pf_number;
    protected $requestor;
    protected $particulars;
    protected $amount;
    protected $status;    
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->pay_to = $details['pay_to'];
        $this->site_name = $details['site_name'];
        $this->date_requested = $details['date_requested'];
        $this->date_needed = $details['date_needed'];
        $this->pf_number = $details['pf_number'];
        $this->requestor = $details['requestor'];
        $this->particulars = $details['particulars'];
        $this->amount = $details['amount'];
        $this->status = $details['status'];    
        $this->subject = $details['subject'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.non-catalog-notification')
            ->with([
                'pay_to' => $this->pay_to,
                'site_name' => $this->site_name,
                'date_requested' => $this->date_requested,
                'date_needed' => $this->date_needed,
                'pf_number' => $this->pf_number,
                'requestor' => $this->requestor,
                'particulars' => $this->particulars,
                'amount' => $this->amount,
                'status' => $this->status
            ])
            ->subject($this->subject);
    }
}
