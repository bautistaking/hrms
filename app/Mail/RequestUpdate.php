<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RequestUpdate extends Mailable
{
    use Queueable, SerializesModels;

    protected $ticket_number;
    protected $client_name;
    protected $job_order;
    protected $site_name;
    protected $team_name;
    protected $ticket_name;
    protected $status;
    protected $remarks;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->ticket_number = $details['ticket_number'];
        $this->client_name = $details['client_name'];
        $this->job_order = $details['job_order'];
        $this->site_name = $details['site_name'];
        $this->team_name = $details['team_name'];
        $this->ticket_name = $details['ticket_name'];
        $this->status = $details['status'];
        $this->remarks = $details['remarks'];
        $this->subject = $details['subject'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.request-update')
            ->with([
                'ticket_number' => $this->ticket_number,
                'client_name' => $this->client_name,
                'job_order' => $this->job_order,
                'site_name' => $this->site_name,
                'team_name' => $this->team_name,
                'ticket_name' => $this->ticket_name,
                'status' => $this->status,
                'remarks' => $this->remarks,
            ])
            ->subject($this->subject);
    }
}
