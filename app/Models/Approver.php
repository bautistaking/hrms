<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\Models\Requestor;

class Approver extends Model
{
    use SoftDeletes;
     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id', 
        'requisition_type', 
        'requisition_level', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'approvers';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'approver_name',
        'requestors_count',
    ];

    public function getApproverDetails()
    {
        return $this->hasOne('App\Models\Employee', 'id', 'employee_id');
    }

    public function getApproverNameAttribute() 
    {
        return $this->getApproverDetails()->pluck('name')->first();
    }

    public function getRequestorsCountAttribute()
    {
        return Requestor::where('approver_id', $this->employee_id)->count();
    }

    
}
