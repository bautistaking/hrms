<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class AttendanceLog extends Model
{
    use SoftDeletes;
     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id', 
        'date', 
        'time_in', 
        'time_out',
        'duration', 
        'time_in_location', 
        'time_out_location', 
        'snapshot',
        'is_logout',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'attendance_logs';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'date_formated',
        'id_number',
        'name',
        'time_login',
        'time_logout',
    ];

    public function getEmployeeDetails()
    {
        return $this->hasOne('App\Models\Employee', 'id', 'employee_id');
    }

    public function getIdNumberAttribute() 
    {
        return $this->getEmployeeDetails()->pluck('id_number')->first();
    }

    public function getNameAttribute() 
    {
        return $this->getEmployeeDetails()->pluck('name')->first();
    }

    public function getTimeLoginAttribute() 
    {
        $dateToDay = Carbon::now()->format('Y-m-d').' '.$this->time_in;
        return Carbon::parse($dateToDay)->format('h:i A');
    }

    public function getTimeLogoutAttribute() 
    {
        $dateToDay = Carbon::now()->format('Y-m-d').' '.$this->time_out;
        return Carbon::parse($dateToDay)->format('h:i A');
    }

    public static function getTodayTimeIn($date_to_day, $id)
    {
        $attendance_log = static::where('date', Carbon::parse($date_to_day)->format('Y-m-d'))
        ->where('employee_id', $id)
        ->where('is_logout', false)
        ->first();

        if(!$attendance_log) {
            $date_last_day = Carbon::parse($date_to_day)->subDays()->format('Y-m-d');
            $attendance_log = static::where('date', $date_last_day)->where('employee_id', $id)
            ->where('is_logout', false)
            ->first();
        }
        return $attendance_log;
    }

    public function getDateFormatedAttribute() 
    {
        return Carbon::parse($this->date)->format('M d, Y');
    }
}

