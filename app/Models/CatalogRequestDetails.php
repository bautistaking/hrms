<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class CatalogRequestDetails extends Model
{
    use SoftDeletes;
     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'catalog_request_id', 
        'logistic_master_id', 
        'quantity',
        'uom',
        'unit_price', 
        'total_price', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'catalog_request_details';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'item_name',
        'item_details',
        'total_price_formated',
    ];

    public function getItemDetails()
    {
        return $this->hasOne('App\Models\LogisticMaster', 'id', 'logistic_master_id');
    }

    public function getSupplierDetails()
    {
        return $this->hasOne('App\Models\Supplier', 'id', 'supplier_id');
    }

    /****************************************
    *           ATTRIBUTES PARTS            *
    ****************************************/
    public function getItemNameAttribute() 
    {
        return $this->getItemDetails()->pluck('item_name')->first();
    }

    public function getItemDetailsAttribute() 
    {
        return $this->getItemDetails()->first();
    }

    public function getTotalPriceFormatedAttribute() 
    {
        return number_format($this->total_price, 2);
    }
}
