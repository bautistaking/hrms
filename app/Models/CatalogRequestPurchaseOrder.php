<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use App\Models\Client;
use App\Models\CatalogRequestDetails;
use App\ViewModels\CatalogRequestDetailsViewModel;

class CatalogRequestPurchaseOrder extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'catalog_request_id', 
        'supplier_id', 
        'client_id', 
        'job_order_id', 
        'job_order_details_id', 
        'vatable', 
        'po_number', 
        'gross_amount', 
        'net_amount',
        'vat_amount',
        'ewt_amount', 
        'ewt_percent', 
        'discount_rate',
        'discount_amount',
        'ewt_type', 
        'total_amount_due', 
        'deliver_to', 
        'required_date', 
        'prepared_by', 
        'remarks',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'catalog_request_purchase_orders';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'due_date',
        'catalogs',
        'inbound_catalogs',
        'supplier_details',
        'payment_terms',
        'supplier_name',
        'total_amount_due_formated',
    ];

    public function getSupplierDetails()
    {
        return $this->hasOne('App\Models\Supplier', 'id', 'supplier_id');
    }

    public function getCatalogRequest()
    {
        return $this->hasOne('App\Models\CatalogRequest', 'id', 'catalog_request_id');
    }

    public function getCatalogList()
    {
        return $this->hasMany('App\Models\CatalogRequestDetails', 'catalog_request_id', 'catalog_request_id');
    }    

    /****************************************
    *           ATTRIBUTES PARTS            *
    ****************************************/
    public function getSupplierDetailsAttribute() 
    {
        return $this->getSupplierDetails()->first();
    }    

    public function getDueDateAttribute() 
    {
        $catalog_request =  $this->getCatalogRequest()->first();
        return Carbon::parse($catalog_request->due_date)->format('F d, Y');
    }

    public function getCatalogsAttribute() 
    {
        $catalogs = CatalogRequestDetailsViewModel::where('catalog_request_id', $this->catalog_request_id)
        ->get();
        return $catalogs;
    }

    public function getInboundCatalogsAttribute() 
    {
        $catalogs = CatalogRequestDetails::where('catalog_request_id', $this->catalog_request_id)
        ->leftJoin('inbounds_details', 'catalog_request_details.id', '=', 'inbounds_details.catalog_request_details_id')
        ->select('catalog_request_details.*')
        ->selectRaw('SUM(inbounds_details.quantity_receive) as total_receive_qty')
        ->groupBy('inbounds_details.catalog_request_details_id')
        ->get();
        return $catalogs;
    }

    public function getPaymentTermsAttribute() 
    {
        $catalog_request = CatalogRequest::find($this->catalog_request_id);
        if($catalog_request)
            return $catalog_request->remarks;

        return null;
    }

    public function getSupplierNameAttribute() 
    {
        $supplier = $this->getSupplierDetails()->first();
        if($supplier)
            return $supplier->name;

        return null;
    }

    public function getTotalAmountDueFormatedAttribute() 
    {
        return number_format($this->total_amount_due, 2);
    }

}
