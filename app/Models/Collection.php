<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Collection extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 
        'job_order_id',
        'job_order_details_id',
        'billing_progress',
        'remarks',
        'insurer_id',
        'insurance_policy_id',
        'invoice_date',
        'invoice_number',
        'or_number',
        'invoice_amount',
        'amount_wo_vat',
        'vat_amount',
        'ewt_amount',
        'net_amount',
        'payment_due',
        'policy_number',
        'coverage_due',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'deleted_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'collections';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    public $appends = [
        'client_details',
        'job_order_details',
        'site_details',
        'billing_progress_percent',
        'invoice_amount_formated',
        'amount_wo_vat_formated',
        'vat_amount_formated',
        'ewt_amount_formated',
        'net_amount_formated',
        'insurer_details',
        'policy_details',
    ];

    /****************************************
    *           ATTRIBUTES PARTS            *
    ****************************************/
    public function getClientDetailsAttribute() 
    {
        $client = Client::find($this->client_id);
        return $client;
    }

    public function getJobOrderDetailsAttribute() 
    {
        $job_order = JobOrder::find($this->job_order_id);
        return $job_order;
    }

    public function getSiteDetailsAttribute() 
    {
        $site_details = JobOrderDetails::find($this->job_order_details_id);
        return $site_details;
    }

    public function getBillingProgressPercentAttribute() 
    {
        return ($this->billing_progress*100). '%';
    }

    public function getInvoiceAmountFormatedAttribute() 
    {
        return number_format($this->invoice_amount, 2);
    }

    public function getAmountWoVatFormatedAttribute() 
    {
        return number_format($this->amount_wo_vat, 2);
    }

    public function getVatAmountFormatedAttribute() 
    {
        return number_format($this->vat_amount, 2);
    }

    public function getEwtAmountFormatedAttribute() 
    {
        return number_format($this->ewt_amount, 2);
    }

    public function getNetAmountFormatedAttribute() 
    {
        return number_format($this->net_amount, 2);
    }

    public function getInsurerDetailsAttribute() 
    {
        $insurer_details = Insurer::find($this->insurer_id);
        return $insurer_details;
    }

    public function getPolicyDetailsAttribute() 
    {
        $policy_details = InsurancePolicy::find($this->insurance_policy_id);
        return $policy_details;
    }

}
