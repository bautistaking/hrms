<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\EmployeeMeta;
use App\Models\Requestor;
use App\ViewModels\EmployeeProfile;

class Employee extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;

    protected $guard = 'employee';
    /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'id_number',
        'name', 
        'email', 
        'password', 
        'api_token',
        'salt', 
        'login_attempt',
        'is_blocked',
        'active', 
        'designation_id',
        'category_id',
        'department_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'salt'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'deleted_at' => 'datetime:Y-m-d H:i:s'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'employee';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'approver_id',
        'department_name',
    ];

    public static function getSalt($username)
    {
        $employee = static::where('email', '=', $username)
            ->orWhere('id_number', '=', $username)->first();
        return $employee->salt;
    }

    public function addMeta($metaData)
    {
        foreach ($metaData as $key => $detail) {
            $where = [
                ['employee_id', $this->id],
                ['meta_key', $key]        
            ];            
            $employeeMeta = EmployeeMeta::where($where)->first();

            if(!$employeeMeta)
                $employeeMeta = new EmployeeMeta;
                $employeeMeta->employee_id = $this->id;
                $employeeMeta->meta_key = $key;
                $employeeMeta->meta_value = $detail;
                $employeeMeta->save();

            $employeeMeta->meta_key = $key;
            $employeeMeta->meta_value = $detail;
            $employeeMeta->save();
        }
    }

    /****************************************
    *           ATTRIBUTES PARTS            *
    ****************************************/
    public function getApproverIdAttribute() 
    {
        $requestor = Requestor::where('requestor_id', $this->id)->first();
        if($requestor){
            $approver = EmployeeProfile::find($requestor->approver_id);
            return $approver->id;
        }
        return 0;
    }

    public function getDepartmentNameAttribute() 
    {
        $department = Department::find($this->department_id);
        if($department)
            return $department->name;
        return null;
    }

}
