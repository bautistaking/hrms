<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class EmployeeRequest extends Model
{
    use SoftDeletes;
     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id', 
        'request_type', 
        'date_from', 
        'date_to',
        'time_start', 
        'time_end',
        'number_of_hours', 
        'description', 
        'approved', 
        'approver_id', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'requests';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'time_start' => 'datetime:h:i A',
        'time_end' => 'datetime:h:i A',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'request_name',
        'id_number',
        'employee_name',
        'date_from_formated',
        'date_to_formated',
        'approved_status',
        'leave_credits',
    ];

    public function getRequestNameAttribute() 
    {
        switch ($this->request_type) {
            case 'OT':
                return 'OverTime';
                break;            
            case 'OB':
                return 'Official Business';
                break;            
            case 'VL':
                return 'Vacation Leave';
                break;            
            case 'SL':
                return 'Sick Leave';
                break;   
            case 'EL':
                return 'Emergency Leave';
                break;   
            case 'ML':
                return 'Maternity Leave';
                break;   
            case 'PL':
                return 'Paternity Leave';
                break;   
            case 'WFH':
                return 'Work From Home';
                break;   
            case 'SPL':
                return 'Bereavement Leave';
            case 'BL':
                return 'Birthday Leave';
                break;   
        }
    }

    public function getEmployeeDetails()
    {
        return $this->hasOne('App\Models\Employee', 'id', 'employee_id');
    }

    public function getEmployeeMetaDetails()
    {   
        return $this->hasMany('App\Models\EmployeeMeta', 'employee_id', 'employee_id');
    }

    public function getIdNumberAttribute() 
    {
        return $this->getEmployeeDetails()->pluck('id_number')->first();
    }

    public function getEmployeeNameAttribute() 
    {
        return $this->getEmployeeDetails()->pluck('name')->first();
    }

    public function getDateFromFormatedAttribute() 
    {
        return Carbon::parse($this->date_from)->format('M d, Y');
    }

    public function getDateToFormatedAttribute() 
    {
        return Carbon::parse($this->date_to)->format('M d, Y');
    }

    public function getApprovedStatusAttribute() 
    {
        switch ($this->approved) {
            case '0':
                return 'Pending';
                break;            
            case '1':
                return 'Approved';
                break;            
            case '2':
                return 'Decline';
                break;             
        }
    }

    public function getTotalLeave($type)
    {
        return self::where('employee_id', $this->employee_id)
                                ->where('request_type', $type)
                                ->whereYear('date_from', Carbon::now()->year)
                                ->where('approved', 1)
                                ->selectRaw('(number_of_hours/8) AS days')
                                ->get()
                                ->sum('days');
    }

    public function getLeaveCreditsAttribute() 
    {
        $employee_details = $this->getEmployeeMetaDetails()->pluck('meta_value','meta_key');

        switch ($this->request_type) {
            case 'VL':
                if(isset($employee_details['vacation_leave']))
                    return ($employee_details['vacation_leave']-$this->getTotalLeave('VL'));
                return 0;
                break;            
            case 'SL':
                if(isset($employee_details['sick_leave']))
                    return ($employee_details['sick_leave']-$this->getTotalLeave('SL'));
                return 0;
                break;   
            case 'EL':
                if(isset($employee_details['emergency_leave']))
                    return ($employee_details['emergency_leave']-$this->getTotalLeave('EL'));
                return 0;
                break;   
            case 'ML':
                if(isset($employee_details['maternity_leave']))
                    return ($employee_details['maternity_leave']-$this->getTotalLeave('ML'));
                return 0;
                break;   
            case 'PL':
                if(isset($employee_details['paternity_leave']))
                    return ($employee_details['paternity_leave']-$this->getTotalLeave('PL'));
                return 0;
                break;   
            case 'SPL':
                if(isset($employee_details['bereavement_leave']))
                    return ($employee_details['bereavement_leave']-$this->getTotalLeave('SPL'));
                return 0;
            case 'BL':
                if(isset($employee_details['birthday_leave']))
                    return ($employee_details['birthday_leave']-$this->getTotalLeave('BL'));
                return 0;
                break;   
        }
    }

}
