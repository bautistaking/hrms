<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Inbound extends Model
{
    use SoftDeletes;
     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id', 
        'client_id',
        'job_order_id',
        'job_order_details_id',
        'reference_number',
        'po_number', 
        'transaction_type', 
        'status_id',
        'remarks', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'inbounds';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:M d, Y h:i',
        'updated_at' => 'datetime:M d, Y h:i',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'inbound_details',
        'employee_name',
    ];

    /****************************************
    *           ATTRIBUTES PARTS            *
    ****************************************/
    public function getInboundDetailsAttribute() 
    {
        $inbound_details = InboundDetails::where('inbound_id', $this->id)->get();
        if($inbound_details)
            return $inbound_details;

        return null;
    }

    public function getEmployeeNameAttribute() 
    {
        $employee = Employee::find($this->employee_id);
        if($employee)
            return $employee->name;
        return null;
    }

}
