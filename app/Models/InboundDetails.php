<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class InboundDetails extends Model
{
    use SoftDeletes;
     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'inbound_id',
        'catalog_request_details_id', 
        'logistic_master_id',
        'quantity_receive',
        'condition',
        'quantity_short_damage',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'inbounds_details';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'item_name',
        'unit_of_measurement',
        'unit_price',
        'total_price',
    ];

    public function getLogisticDetails()
    {
        return LogisticMaster::find($this->logistic_master_id);
    }

    public function getCatalogRequestDetails()
    {
        return CatalogRequestDetails::find($this->catalog_request_details_id);
    }

    /****************************************
    *           ATTRIBUTES PARTS            *
    ****************************************/
    public function getItemNameAttribute() 
    {
        $item = $this->getLogisticDetails();
        if($item)
            return $item->item_name;

        return null;
    }

    public function getUnitOfMeasurementAttribute() 
    {
        $item = $this->getLogisticDetails();
        if($item)
            return $item->unit_of_measurement;

        return null;
    }

    public function getUnitPriceAttribute() 
    {
        $data = $this->getCatalogRequestDetails();
        if($data)
            return $data->unit_price;

        return 0;
    }

    public function getTotalPriceAttribute() 
    {
        $data = $this->getCatalogRequestDetails();
        if($data)
            return $data->total_price;

        return 0;
    }
}
