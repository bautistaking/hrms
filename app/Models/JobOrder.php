<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobOrder extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'client_id',
        'order_number',
        'category', 
        'po_number',
        'po_date',
        'po_amount',
        'updated_po_amount',
        'badget_percentage',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:M, d Y H:i:s',
        'deleted_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'job_orders';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'client_name',
        'po_amount_formated',
        'updated_po_amount_formated',
        'po_order_number',
    ];

    public function getClient()
    {   
        return $this->hasMany('App\Models\Client', 'id', 'client_id');
    }

    /****************************************
    *           ATTRIBUTES PARTS            *
    ****************************************/
    public function getClientNameAttribute() 
    {
        $client = $this->getClient()->first();
        return $client->client_name;
    }

    public function getPoAmountFormatedAttribute() 
    {
        return number_format($this->po_amount, 2);
    }

    public function getUpdatedPoAmountFormatedAttribute() 
    {
        return number_format($this->updated_po_amount, 2);
    }

    public function getPoOrderNumberAttribute() 
    {
        return $this->order_number. ' ( ' .$this->po_number.' )';
    }
}