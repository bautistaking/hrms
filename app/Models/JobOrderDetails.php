<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\CatalogRequestDetails;

class JobOrderDetails extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'job_order_id',
        'bcf_name',
        'site_name', 
        'site_details',
        'region_id',
        'province_id',
        'city_id',
        'bill_of_quantities',
        'variation_amount',
        'updated_bill_of_quantities',
        'expenses',
        'badget',
        'balance_amount',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:M, d Y H:i:s',
        'deleted_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'job_order_details';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'bill_of_quantities_formated',
        'badget_formated',
        'variation_amount_formated',
        'updated_bill_of_quantities_formated',
        'balance_amount_formated',
        'region_details',
        'province_details',
        'city_details',
    ];

    public function getExpenses()
    {
        return JobOrderDetailsTag::where('job_order_detail_id', $this->id)
                                 ->whereIn('reference_name', ['payment_request','outbound_request'])
                                 ->get()->sum('total');
    }

    public function getIncome()
    {
        return JobOrderDetailsTag::where('job_order_detail_id', $this->id)
                                 ->whereIn('reference_name', ['adding_income'])
                                 ->get()->sum('total');
    }

    public function updateBalance()
    {
        $expenses = $this->getExpenses();
        $income = $this->getIncome();
        $this->balance_amount = ($this->badget-$expenses)+$income;
        $this->save();
    }

    /****************************************
    *           ATTRIBUTES PARTS            *
    ****************************************/
    public function getBillOfQuantitiesFormatedAttribute() 
    {
        return 'PHP '.number_format($this->bill_of_quantities, 2);
    }

    public function getBadgetFormatedAttribute() 
    {
        return 'PHP '.number_format($this->badget, 2);
    }

    public function getVariationAmountFormatedAttribute() 
    {
        return 'PHP '.number_format($this->variation_amount, 2);
    }

    public function getUpdatedBillOfQuantitiesFormatedAttribute() 
    {
        return 'PHP '.number_format($this->updated_bill_of_quantities, 2);
    }

    public function getBalanceAmountFormatedAttribute() 
    {
        return 'PHP '.number_format($this->balance_amount, 2);
    }

    public function getRegionDetailsAttribute() 
    {
        $region = Region::find($this->region_id);
        if($region) 
            return $region;

        return null;
    }

    public function getProvinceDetailsAttribute() 
    {
        $province = Province::find($this->province_id);
        if($province)
            return $province;

        return null;
    }

    public function getCityDetailsAttribute() 
    {
        $city = City::find($this->city_id);
        if($city)
            return $city;

        return null;
    }

    // public function getBalanceAmountUpdatedAttribute() 
    // {
    //     $total_catalog_amount = CatalogRequestDetails::where('job_order_details_id', $this->id)
    //     ->where('catalog_requests.status_id', '!=', 7)
    //     ->join('catalog_requests', 'catalog_request_details.catalog_request_id', '=', 'catalog_requests.id')
    //     ->get()->sum('total_price');

    //     return 'PHP '.number_format(($this->balance_amount-$total_catalog_amount), 2);
    // }    

}
