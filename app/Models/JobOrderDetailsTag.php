<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobOrderDetailsTag extends Model
{
     use SoftDeletes;

    /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'job_order_detail_id',
        'reference_id',
        'reference_name', 
        'total',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:M, d Y H:i:s',
        'deleted_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'job_order_details_tags';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'request_name',
    ];    

    public function getRequestNameAttribute() 
    {
        switch ($this->reference_name) {
            case 'payment_request':
                $payment_request = PaymentRequest::find($this->reference_id);
                if($payment_request)
                    return $payment_request->payment_type.' - '.$payment_request->remarks;
                return null;
                break;
            case 'outbound_request':
                $outbound_request = Outbound::find($this->reference_id);
                if($outbound_request)
                    return $outbound_request->request_number.' - '.$outbound_request->remarks;
                return null;
                break;                
        }
    }
}
