<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Loan extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id', 
        'loan_type_id',
        'total_loan_amount',
        'monthly_amortization',
        'total_paid_amount',
        'remaining_balance',
        'deduction_count',
        'total_deductions',
        'remarks',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:M, d Y H:i:s',
        'deleted_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'loans';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'employee_name',
        'loan_type',
        'deductions',
        'total_loan_amount_formated',
        'monthly_amortization_formated',
        'total_paid_amount_formated',
        'remaining_balance_formated',
    ];

    public function getEmployee()
    {   
        return $this->hasOne('App\Models\Employee', 'id', 'employee_id');
    }

    public function getLoanType()
    {   
        return $this->hasOne('App\Models\LoanTypes', 'id', 'loan_type_id');
    }

    public function getEmployeeNameAttribute() 
    {
        return $this->getEmployee()->pluck('name')->first();
    }

    public function getLoanTypeAttribute() 
    {
        return $this->getLoanType()->pluck('name')->first();
    }

    public function getDeductionsAttribute() 
    {
        return $this->deduction_count .'/'.$this->total_deductions;
    }

    public function getTotalLoanAmountFormatedAttribute() 
    {
        return number_format($this->total_loan_amount, 2);
    }

    public function getMonthlyAmortizationFormatedAttribute() 
    {
        return number_format($this->monthly_amortization, 2);
    }

    public function getTotalPaidAmountFormatedAttribute() 
    {
        return number_format($this->total_paid_amount, 2);
    }

    public function getRemainingBalanceFormatedAttribute() 
    {
        return number_format($this->remaining_balance, 2);
    }
}
