<?php

namespace App\Models;

use Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MediaLibrary extends Model
{
    use SoftDeletes;
     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file_name', 
        'file_type', 
        'file_size', 
        'dimensions', 
        'caption', 
        'description', 
        'file_url'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'media_library';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    public function upload($file){
        $filename = $file->getClientOriginalName();
        $file->storeAs('public/media_library/', $filename);
        Storage::delete('public/media_library/{$this->file_name}');
        $this->file_url = asset('storage/media_library/'. $filename);
        $this->file_name = $filename;
        $this->save();
    }
}
