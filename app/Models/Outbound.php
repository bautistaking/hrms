<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Outbound extends Model
{
     use SoftDeletes;
     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id',
        'client_id', 
        'job_order_id', 
        'job_order_details_id', 
        'total_price', 
        'pull_out_from',
        'deliver_to',
        'required_date', 
        'remarks', 
        'approver_id',
        'status_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'outbounds';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:M d, Y h:i',
        'updated_at' => 'datetime:M d, Y h:i',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'client_name',
        'order_number',
        'po_number',
        'client_details',
        'job_order_details',
        'site_details',
        'outbound_details',
        'total_price_formated',
        'employee_name',
        'employee_department',
        'employee_signature',
        'required_date_formated',
        'created_date_formated',
        'approver_name',
        'approver_signature',
    ];

    /****************************************
    *           ATTRIBUTES PARTS            *
    ****************************************/
    public function getClientNameAttribute() 
    {
        $client = Client::find($this->client_id);
        return $client->client_name;
    }

    public function getOrderNumberAttribute() 
    {
        $job_order = JobOrder::find($this->job_order_id);
        if($job_order) 
            return $job_order->order_number;
        return null;
    }

    public function getPoNumberAttribute() 
    {
        $job_order = JobOrder::find($this->job_order_id);
        if($job_order)
            return $job_order->order_number .' ( '.$job_order->po_number.' ) ';
        return null;
    }

    public function getOutboundDetailsAttribute() 
    {
        $outbound_details = OutboundDetails::where('outbound_id', $this->id)->get();
        if($outbound_details)
            return $outbound_details;

        return null;
    }

    public function getClientDetailsAttribute() 
    {
        $client = Client::find($this->client_id);
        return $client;
    }

    public function getJobOrderDetailsAttribute() 
    {
        $job_order = JobOrder::find($this->job_order_id);
        return $job_order;
    }

    public function getSiteDetailsAttribute() 
    {
        $site_details = JobOrderDetails::find($this->job_order_details_id);
        return $site_details;
    }

    public function getTotalPriceFormatedAttribute() 
    {
        return number_format($this->total_price, 2);
    }

    public function getEmployeeNameAttribute() 
    {
        $employee = Employee::find($this->employee_id);
        if($employee)
            return $employee->name;
        return null;
    }

    public function getEmployeeSignatureAttribute() 
    {
        $employee = EmployeeMeta::where('employee_id', $this->employee_id)->where('meta_key', 'sign_path')->first();
        if($employee)
            return asset($employee->meta_value);
        return null;
    }

    public function getEmployeeDepartmentAttribute() 
    {
        $employee = Employee::find($this->employee_id);
        if($employee)
            return $employee->department_name;
        return null;
    }

    public function getRequiredDateFormatedAttribute() 
    {
        return Carbon::parse($this->required_date)->format('F d, Y');
    }

    public function getCreatedDateFormatedAttribute() 
    {
        return Carbon::parse($this->created_at)->format('F d, Y');
    }

    public function getApproverNameAttribute() 
    {
        $employee = Employee::find($this->approver_id);
        if($employee)
            return $employee->name;
        return null;
    }

    public function getApproverSignatureAttribute() 
    {
        $employee = EmployeeMeta::where('employee_id', $this->approver_id)->where('meta_key', 'sign_path')->first();
        if($employee)
            return asset($employee->meta_value);
        return null;
    }

}
