<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class PaymentRequest extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'request_number',
        'employee_id',
        'client_id', 
        'job_order_id', 
        'job_order_details_id', 
        'catalog_request_id',
        'request_id', 
        'request_type', 
        'required_date', 
        'pay_to',
        'gross_amount', 
        'net_amount', 
        'vat_amount', 
        'ewt_amount', 
        'ewt_percent', 
        'ewt_type', 
        'total_amount_due', 
        'remarks', 
        'status_id', 
        'approver_id',
        'is_printed',
        'payment_type',
        'release_type',
        'disbursed',
        'disbursed_date',
        'liquidated',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:m/d/Y',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'deleted_at' => 'datetime:Y-m-d H:i:s',
        'required_date' => 'datetime:m/d/Y'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'payment_requests';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'client_details',
        'requestor_name',
        'job_order_details',
        'site_details',
        'request_details',
        'client_name',
        'job_order_number',
        'site_name',
        'request_name',
        'approver_name',
        'approver_signature',
        'amount_formated',
        'po_number',
        'attachments',
        'badget_formated',
        'balance_amount_formated',
        'liquidated_status',
    ];

    public function getClient()
    {   
        return $this->hasOne('App\Models\Client', 'id', 'client_id');
    }

    public function getRequestor()
    {   
        return $this->hasOne('App\Models\Employee', 'id', 'employee_id');
    }

    public function getJobOrder()
    {   
        return $this->hasOne('App\Models\JobOrder', 'id', 'job_order_id');
    }

    public function getSiteName()
    {   
        return $this->hasOne('App\Models\JobOrderDetails', 'id', 'job_order_details_id');
    }

    public function getRequestName()
    {   
        return $this->hasOne('App\Models\RequestList', 'id', 'request_id');
    }

    public function getApprover()
    {   
        return $this->hasOne('App\Models\Employee', 'id', 'approver_id');
    }    

    public function getPoDetails()
    {   
        return $this->hasOne('App\Models\CatalogRequestPurchaseOrder', 'catalog_request_id', 'catalog_request_id');
    }    

    public function getAttachments()
    {
        return $this->hasMany('App\Models\CatalogRequestAttachments', 'catalog_request_id', 'catalog_request_id');
    }

    /****************************************
    *           ATTRIBUTES PARTS            *
    ****************************************/
    public function getClientNameAttribute() 
    {
        $client = $this->getClient()->first();
        return $client->client_name;
    }

    public function getRequestorNameAttribute() 
    {
        $requestor = $this->getRequestor()->first();
        return $requestor->name;
    }

    public function getClientDetailsAttribute() 
    {
        return $this->getClient()->first();
    }

    public function getJobOrderNumberAttribute() 
    {
        $jo = $this->getJobOrder()->first();
        if($jo)
            return $jo->order_number. '('.$jo->po_number.')';
        return null;
    }

    public function getJobOrderDetailsAttribute() 
    {
        return $this->getJobOrder()->first();
    }

    public function getSiteNameAttribute() 
    {
        $site_name = $this->getSiteName()->first();
        if($site_name)
            return $site_name->site_name;
        return null;
    }

    public function getSiteDetailsAttribute() 
    {
        return $this->getSiteName()->first();
    }

    public function getRequestNameAttribute() 
    {
        $request_name = $this->getRequestName()->first();
        if($request_name)
            return $request_name->name;
        return null;
    }

    public function getRequestDetailsAttribute() 
    {
        return $this->getRequestName()->first();
    }

    public function getAmountFormatedAttribute() 
    {
        return number_format($this->gross_amount, 2);
    }

    public function getApproverNameAttribute() 
    {
        $approver = $this->getApprover()->first();
        if($approver)
            return $approver->name;

        return null;
    }

    public function getPoNumberAttribute() 
    {
        $catalog_details = $this->getPoDetails()->first();
        if($catalog_details)
            return $catalog_details->po_number;

        return null;
    }

    public function getAttachmentsAttribute() 
    {
        $attachments = $this->getAttachments()->get();
        if($attachments)
            return $attachments;

        return null;
    }

    public function getBadgetFormatedAttribute()
    {
        $site_name = $this->getSiteName()->first();
        if($site_name)
            return $site_name->badget_formated;
        return null;
    }

    public function getBalanceAmountFormatedAttribute()
    {
        $site_name = $this->getSiteName()->first();
        if($site_name)
            return $site_name->balance_amount_formated;
        return null;
    }

    public function getLiquidatedStatusAttribute()
    {
        if($this->disbursed_date && !$this->liquidated) {
            $due_date = Carbon::createFromFormat('Y-m-d',$this->disbursed_date)->addDays(15)->format('Y-m-d');
            $nowDate = Carbon::now();

            $result = $nowDate->gt($due_date);
            if($result)
                return 2;
        }

        return $this->liquidated;
    }

    public function getApproverSignatureAttribute() 
    {
        $employee = EmployeeMeta::where('employee_id', $this->approver_id)->where('meta_key', 'sign_path')->first();
        if($employee)
            return asset($employee->meta_value);
        return null;
    }
}
