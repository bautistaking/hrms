<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use App\ViewModels\EmployeePayroll;
use App\Models\PayrollOtherIncome;
use App\Models\PayPeriod;

class Payroll extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pay_period', 
        'employee_id',
        'number_of_days',
        'regular_holidays',
        'approved_leaves',
        'total_cola',
        'other_income',
        'total_basic_salary',
        'over_time_no_of_hrs',
        'over_time_rate_per_hrs',
        'over_time_total_amount',
        'night_differential_no_of_hrs',
        'night_differential_rate_per_hrs',
        'night_differential_total_amount',
        'special_day_no_of_hrs',
        'special_day_rate_per_hrs',
        'special_day_total_amount',
        'special_day_over_time_no_of_hrs',
        'special_day_over_time_rate_per_hrs',
        'special_day_over_time_total_amount',
        'holiday_day_no_of_hrs',
        'holiday_day_rate_per_hrs',
        'holiday_day_total_amount',
        'holiday_day_over_time_no_of_hrs',
        'holiday_day_over_time_rate_per_hrs',
        'holiday_day_over_time_total_amount',
        'total_over_time',
        'late_no_of_minutes',
        'late_rate_per_hrs',
        'late_total_amount',
        'total_adjustment',
        'total_gross_salary',
        'withholding_tax',
        'sss_prem',
        'philhealth_prem',
        'pagibig_prem',
        'loans',
        'total_loans',
        'total_deductions',
        'net_salary',
        'taxable_income'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'payrolls';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'employee',
    ];

    public function getEmployeeDetails()
    {
        $employee = EmployeePayroll::find($this->employee_id);
        $employee['other_incomes'] = PayrollOtherIncome::where('employee_id', $this->employee_id)
        ->where('pay_period', $this->pay_period)
        ->whereIn('type', ['allowance', 'incentive', 'adjustment', 'less-adjustment', '13th-mon','tax-refund'])
        ->get();
        $employee['other_deductions'] = PayrollOtherIncome::where('employee_id', $this->employee_id)
        ->where('pay_period', $this->pay_period)
        ->where('type', 'deduction')
        ->get();

        return $employee;
    }

    public function getEmployeeAttribute() 
    {
        return $this->getEmployeeDetails();
    }
}
