<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoNumber extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sequence'
    ];
    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'po_number';

    protected $primaryKey = null;
    public $timestamps = false;
}
