<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\ViewModels\EmployeeProfile;

class Requestor extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'approver_id', 
        'requestor_id', 
    ];

    /**
     * set timestamps to false
     *
     * @var boolean
    */
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'requestors';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'approver_id';

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'details',
    ];

    public function getDetailsAttribute() 
    {
        return EmployeeProfile::find($this->requestor_id);
    }
}
