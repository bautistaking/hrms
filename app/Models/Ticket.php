<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Ticket extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ticket_number',
        'client_id', 
        'job_order_id',
        'job_order_details_id',
        'team_id',
        'service_type',
        'solution_type',
        'remarks',
        'start_date',
        'end_date',
        'is_priority',
        'track_code',
        'status',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'deleted_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'tickets';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'client_name',
        'job_order',
        'site_name',
        'client_details',
        'job_order_details',
        'site_details',
        'ticket_details',
        'start_date_formated',
        'end_date_formated',
        'region_name',
        'province_name',
        'city_municipality_name',
    ];

    public function getClientNameAttribute()
    {
        $client = Client::find($this->client_id);
        if($client)
            return $client->client_name;

        return null;
    }

    public function getJobOrderAttribute()
    {
        $job_order = JobOrder::find($this->job_order_id);
        if($job_order)
            return $job_order->order_number. ' ('.$job_order->po_number.') ' ;

        return null;
    }

    public function getSiteNameAttribute()
    {
        $job_order_details = JobOrderDetails::find($this->job_order_details_id);
        if($job_order_details)
            return $job_order_details->site_name;

        return null;
    }

    public function getClientDetailsAttribute()
    {
        return Client::find($this->client_id);
    }

    public function getJobOrderDetailsAttribute()
    {
        return JobOrder::find($this->job_order_id);
    }

    public function getSiteDetailsAttribute()
    {
        $job_order_details = JobOrderDetails::find($this->job_order_details_id);
        if($job_order_details) 
            return $job_order_details;

        return null;
    }

    public function getTicketDetailsAttribute()
    {
        return TicketDetails::where('ticket_id', $this->id)->get();
    }

    public function getStartDateFormatedAttribute()
    {
        if($this->start_date)
            return Carbon::createFromFormat('Y-m-d', $this->start_date)->format('M d, Y'); 
        return null;
    }

    public function getEndDateFormatedAttribute()
    {
        if($this->end_date)
            return Carbon::createFromFormat('Y-m-d', $this->end_date)->format('M d, Y'); 
        return null;
    }

    public function getRegionNameAttribute()
    {
        $job_order_details = JobOrderDetails::find($this->job_order_details_id);
        if($job_order_details->region_details) 
            return $job_order_details->region_details->name;

        return null;
    }

    public function getProvinceNameAttribute()
    {
        $job_order_details = JobOrderDetails::find($this->job_order_details_id);
        if($job_order_details->province_details) 
            return $job_order_details->province_details->province_name;

        return null;
    }

    public function getCityMunicipalityNameAttribute()
    {
        $job_order_details = JobOrderDetails::find($this->job_order_details_id);
        if($job_order_details->city_details) 
            return $job_order_details->city_details->city_municipality_name;

        return null;
    }

}
