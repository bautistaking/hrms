<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\Models\Holiday;

class TicketDetails extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    private $number_of_days = 0;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ticket_id', 
        'plans_reference_id',
        'team_id',
        'start_date',
        'end_date',
        'status',
        'remarks',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'deleted_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'ticket_details';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'ticket_name',
        'ticket_references',
        'start_date_formated',
        'end_date_formated',
        'end_date_comparison',
        'status_name',
        'number_of_days',
        'middle_date',
        'comments',
        'team_name',
        'team_details',
    ];

    public function getTicketNameAttribute()
    {
        $reference = PlansReference::find($this->plans_reference_id);
        if($reference)
            return $reference->name;

        return null;
    }

    public function getTicketReferencesAttribute()
    {
        $reference = PlansReference::find($this->plans_reference_id);
        if($reference)
            return $reference->references;

        return null;
    }

    public function getStartDateFormatedAttribute()
    {
        return Carbon::createFromFormat('Y-m-d', $this->start_date)->format('M d, Y'); 
    }

    public function getEndDateFormatedAttribute()
    {
        return Carbon::createFromFormat('Y-m-d', $this->end_date)->format('M d, Y'); 
    }

    public function getEndDateComparisonAttribute()
    {
        return Carbon::createFromFormat('Y-m-d', $this->end_date)->format('m-d-Y'); 
    }

    public function getStatusNameAttribute()
    {
        switch ($this->status) {
            case 1:
                return 'To Start';
            break;
            case 2:
                return 'On-Going';
            break;
            case 3:
                return 'Completed';
            break;
            case 4:
                return 'Critical';
            break;
            case 5:
                return 'Delayed';
            break;
        }
    }

    public function getNumberOfDaysAttribute()
    {
        $holidays = Holiday::get()->pluck('date_from')->toArray();

        $date_from = Carbon::parse($this->start_date);
        $date_to = Carbon::parse($this->end_date);

        $this->number_of_days = $date_from->diffInDaysFiltered(function (Carbon $date) use ($holidays) {

            return $date->isWeekday() && !in_array($date, $holidays);

        }, $date_to);

        return $this->number_of_days;
    }

    public function getMiddleDateAttribute()
    {
        $date_from = Carbon::parse($this->start_date);
        if($this->number_of_days > 15) 
            return $date_from->addWeekdays(($this->number_of_days/2))->format('m-d-Y');

        return null;
    }

    public function getCommentsAttribute()
    {
        return TicketDetailsRemark::where('ticket_details_id', $this->id)
                                  ->latest()
                                  ->get();
    }

    public function getTeamNameAttribute()
    {
        $team = Team::find($this->team_id);
        if($team)
            return $team->name;

        return null;
    }
    
    public function getTeamDetailsAttribute()
    {
        return Team::find($this->team_id);
    }

}
