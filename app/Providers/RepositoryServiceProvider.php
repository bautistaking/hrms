<?php

namespace App\Providers;

use App\Repositories\AttendanceLog\AttendanceLogRepository;
use App\Repositories\AttendanceLog\Interfaces\AttendanceLogRepositoryInterface;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            AttendanceLogRepositoryInterface::class,
            AttendanceLogRepository::class
        );
    }
}
