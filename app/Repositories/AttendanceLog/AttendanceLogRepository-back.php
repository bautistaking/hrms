<?php
namespace App\Repositories\AttendanceLog;

use DB;
use App\Base\BaseRepository;
use App\Repositories\AttendanceLog\Interfaces\AttendanceLogRepositoryInterface;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

use App\Models\PayPeriod;
use App\Models\AttendanceLog;
use App\Models\Holiday;
use App\Models\Loan;
use App\Models\PayrollOtherIncome;
use App\Models\Payroll;
use App\Models\EmployeeRequest;
use App\ViewModels\EmployeePayroll;

class AttendanceLogRepository extends BaseRepository implements AttendanceLogRepositoryInterface
{
	protected $model;
    private $pay_period;
    private $employee;
    private $holidays;
    private $regular_holidays;
    private $special_holidays;
    private $government_contribution;

	public function __construct(AttendanceLog $attendanceLog)
    {
        $this->model = $attendanceLog;
    }

    public function setPayroll($request)
    {
        $this->pay_period = PayPeriod::find($request->payperiod_id);
        $this->employee = $request->payroll['employee'];
        $this->government_contribution = $request->with_contri;
        $this->holidays = $this->getHolidays()->get()->pluck('date_from');
        $this->regular_holidays = $this->getHolidays()->where('type', 'regular')->get()->pluck('date_from');
        $this->special_holidays = $this->getHolidays()->where('type', 'special')->get()->pluck('date_from');
    }

    public function generatePayroll()
    {
        // GET EMPLOYEE DAILY WAGE & COLA
        $daily_wage_wo_cola = $this->employee['daily_wage_wo_cola'];
        $other_income = 0;
        // GET RATE PER HOURS
        $rate_per_hours = round(($daily_wage_wo_cola/8)*1.25, 2);
        $night_rate_per_hours = round(($daily_wage_wo_cola/8)*0.1, 2);
        $special_day_rate_per_hours = round(($daily_wage_wo_cola/8)*1.3, 2);
        $special_day_over_time_rate_per_hours = round(($daily_wage_wo_cola/8)*1.69, 2);
        $holiday_day_rate_per_hours = round(($daily_wage_wo_cola/8)+(10/8), 2);
        $holiday_day_over_time_rate_per_hours = round(($daily_wage_wo_cola/8)*2.6, 2);
        $late_rate_per_minutes = round(($daily_wage_wo_cola/8)/60, 2);

        // GET REGULAR DAY total_count
        $regular_days = ($this->employee['category_id'] == 3) ? 15 : $this->getRegularDays();
        // GET REGULAR HOLIDAYS
        $regular_holidays = 0;
        switch ($this->employee['category_id']) {
            case 1:
            case 15:
                // CHECKING 
                $regular_holidays = count($this->regular_holidays)+count($this->special_holidays);
                break;
            case 3:
                $regular_holidays = 0;
                break;
            default:
                $regular_holidays = count($this->regular_holidays);
                break;
        }
        // GET LEAVES
        $approved_leaves = ($this->employee['category_id'] == 3) ? 0 : $this->getApprovedLeaves();
        // GET TOTAL COLA
        $cola = ($this->employee['category_id'] == 3) ? 0 : ($regular_days+$regular_holidays+$approved_leaves)*$this->employee['cola'];
        // TOTAL BASIC SALARY
        $total_basic_salary = round($daily_wage_wo_cola*($regular_days+$regular_holidays+$approved_leaves))+$cola;
        // GET REGULAR OVER TIME
        $total_over_time = ($this->employee['category_id'] == 3) ? 0 : $this->getTotalOverTime();
        // GET REGULAR NIGHT DIFFERENTIALS
        $total_night_differentials = ($this->employee['category_id'] == 3) ? 0 : $this->getNightDifferentials();
        // GET SPECIAL AND SUNDAY
        $special_days_or_sunday = $this->getSpecialDayOrSunday();
        // GET REGULAR HOLIDAYS
        $total_regular_holiday = $this->getRegularHoliday();
        // GET MINUTES OF LATE
        $total_lates = ($this->employee['category_id'] == 3) ? 0 : $this->getLate();
        // GET OVER TIME TOTALS
        $over_time_total_amount =round($total_over_time*$rate_per_hours,2);
        $night_differential_total_amount = round($total_night_differentials*$night_rate_per_hours,2);
        $special_day_total_amount = round($special_day_rate_per_hours*$special_days_or_sunday['totalRegularHrs'],2);
        $special_day_over_time_total_amount = round($special_day_over_time_rate_per_hours*$special_days_or_sunday['totalOverTimeHrs'],2);
        $holiday_day_total_amount = round($holiday_day_rate_per_hours*$total_regular_holiday['totalRegularHrs'],2);
        $holiday_day_over_time_total_amount = round($holiday_day_over_time_rate_per_hours*$total_regular_holiday['totalOverTimeHrs'],2);
        $over_time_grand_total_amount = $over_time_total_amount+$night_differential_total_amount+$special_day_total_amount+$special_day_over_time_total_amount+$holiday_day_total_amount+$holiday_day_over_time_total_amount;
        // GET LATE TOTALS
        $late_total_amount = $late_rate_per_minutes*$total_lates;
        $total_gross_salary = ($total_basic_salary > 0) ? round($total_basic_salary+$over_time_grand_total_amount-$late_total_amount,2) : 0;
        // TOTAL GOVERMENT DEDUCTIONS
        $sss_prem = ($this->government_contribution && $total_basic_salary > 0) ? $this->employee['sss_prem'] : 0;
        $sss_mpf = ($this->government_contribution && $total_basic_salary > 0) ? $this->employee['sss_mpf'] : 0;
        $philhealth_prem = ($this->government_contribution && $total_basic_salary > 0) ? $this->employee['philhealth_prem'] : 0;
        $pagibig_prem = ($this->government_contribution && $total_basic_salary > 0) ? $this->employee['pagibig_prem'] : 0;

        $taxable_income = round($total_gross_salary-$cola-$other_income-$sss_prem-$sss_mpf-$philhealth_prem-$pagibig_prem,2);
        $taxable_income = ($taxable_income <= 0)? 0 : $taxable_income;
        $withholding_tax = $this->getWithholdingTax($taxable_income, $this->pay_period['category_id']);

        $loans = $this->getLoans();
        $total_loans = $loans['total_loans'];
        $total_deductions = $sss_prem+$sss_mpf+$philhealth_prem+$pagibig_prem+$total_loans+$withholding_tax;
        $net_salary = $total_gross_salary-$total_deductions;
        $net_salary = ($net_salary <= 0)? 0 : $net_salary;

        return [
            'pay_period' => $this->pay_period['id'],
            'employee_id' => $this->employee['id'],
            'number_of_days' => $regular_days,
            'regular_holidays' => $regular_holidays,
            'approved_leaves' => $approved_leaves,
            'total_cola' => $cola,
            'other_income' => $other_income,
            'total_basic_salary' => $total_basic_salary,
            'over_time_no_of_hrs' => $total_over_time,
            'over_time_rate_per_hrs' => $rate_per_hours,
            'over_time_total_amount' => $over_time_total_amount,
            'night_differential_no_of_hrs' => $total_night_differentials, 
            'night_differential_rate_per_hrs' => $night_rate_per_hours, 
            'night_differential_total_amount' => $night_differential_total_amount, 
            'special_day_no_of_hrs' => $special_days_or_sunday['totalRegularHrs'], 
            'special_day_rate_per_hrs' => $special_day_rate_per_hours, 
            'special_day_total_amount' => $special_day_total_amount, 
            'special_day_over_time_no_of_hrs' => $special_days_or_sunday['totalOverTimeHrs'], 
            'special_day_over_time_rate_per_hrs' => $special_day_over_time_rate_per_hours, 
            'special_day_over_time_total_amount' => $special_day_over_time_total_amount, 
            'holiday_day_no_of_hrs' => $total_regular_holiday['totalRegularHrs'], 
            'holiday_day_rate_per_hrs' => $holiday_day_rate_per_hours, 
            'holiday_day_total_amount' => $holiday_day_total_amount, 
            'holiday_day_over_time_no_of_hrs' => $total_regular_holiday['totalOverTimeHrs'], 
            'holiday_day_over_time_rate_per_hrs' => $holiday_day_over_time_rate_per_hours, 
            'holiday_day_over_time_total_amount' => $holiday_day_over_time_total_amount, 
            'total_over_time' => $over_time_grand_total_amount, 
            'late_no_of_minutes' => $total_lates, 
            'late_rate_per_hrs' => $late_rate_per_minutes, 
            'late_total_amount' => $late_total_amount, 
            'total_gross_salary' => $total_gross_salary,
            'taxable_income' => $taxable_income,
            'withholding_tax' => $withholding_tax,
            'sss_prem' => $sss_prem,
            'sss_mpf' => $sss_mpf,
            'philhealth_prem' => $philhealth_prem,
            'pagibig_prem' => $pagibig_prem,
            'loans' => $loans['loans_list'],
            'total_loans' => $total_loans,
            'total_deductions' => $total_deductions,
            'net_salary' => $net_salary
        ];
    }

    public function getRegularDays()
    {
        $saturdays = [];
        if($this->employee['category_id'] == 1 || $this->employee['category_id'] == 2) {
            $holidays = $this->holidays->toArray();

            $period = CarbonPeriod::create($this->pay_period['date_from'], $this->pay_period['date_to']);
            foreach ($period as $date) {
                if($date->isSaturday() == true) {
                    $saturdays[] = $date->format('Y-m-d');
                }
            }
        }

        $number_saturdays = count($saturdays);

        $total_days = $this->model::where('attendance_logs.employee_id', $this->employee['id'])
        ->whereBetween('date', [$this->pay_period['date_from'], $this->pay_period['date_to']])
        ->whereRaw('DATE_FORMAT(date, \'%a\') <> \'Sun\'')
        ->whereNotIn('date', $this->holidays)
        ->when($number_saturdays, function($query) use($saturdays) {
            return $query->whereNotIn('date', $saturdays);
        })
        ->get()
        ->count();

        // return $total_days;

        return (($this->employee['category_id'] == 1 || $this->employee['category_id'] == 2) && $total_days > 0) ? $total_days+$number_saturdays : $total_days;
    }

    public function getApprovedLeaves()
    {
        $approved_leaves = EmployeeRequest::where('employee_id', $this->employee['id'])
        ->whereIn('request_type', ['VL','SL','EL','ML','PL','SPL','BL','WFH'])
        ->where('approved', true)
        ->whereBetween('date_from', [$this->pay_period['date_from'], $this->pay_period['date_to']])
        ->get();

        if(count($approved_leaves) <= 0) {
            $approved_leaves = EmployeeRequest::where('employee_id', $this->employee['id'])
            ->whereIn('request_type', ['VL','SL','EL','ML','PL','SPL','BL','WFH'])
            ->where('approved', true)
            ->whereBetween('date_to', [$this->pay_period['date_from'], $this->pay_period['date_to']])
            ->get();
        }

        if($approved_leaves) {
            $numberOfDays = 0;
            foreach ($approved_leaves as $leave) {
                $numberOfDays += $this->getNumberOfDays($leave);
            }
            return $numberOfDays;
        }
        return 0;
    }

    public function getNumberOfDays($leave)
    {
        $leave_date_from = Carbon::parse($leave['date_from'])->timestamp;
        $leave_date_to = Carbon::parse($leave['date_to'])->timestamp;

        $payperiod_date_from = Carbon::parse($this->pay_period['date_from'])->timestamp;
        $payperiod_date_to = Carbon::parse($this->pay_period['date_to'])->timestamp;

        $period = array();
        $days = 0;

        $period = CarbonPeriod::create($leave['date_from'], $leave['date_to']);
        if(count($period) > 1) {
            if($payperiod_date_from < $leave_date_from && $payperiod_date_to > $leave_date_to) {
                $period = CarbonPeriod::create($leave['date_from'], $leave['date_to']);
            }
            else if($payperiod_date_from > $leave_date_from && $payperiod_date_to > $leave_date_to) {
                $period = CarbonPeriod::create($this->pay_period['date_from'], $leave['date_to']);
            }
            else if($payperiod_date_from < $leave_date_from && $payperiod_date_to < $leave_date_to) {
                $period = CarbonPeriod::create($leave['date_from'], $this->pay_period['date_to']);
            }
        }

        foreach ($period as $date) {
            if($date->format('D') != 'Sun')
                $days++;   
        }
        return $days;
    }

    public function getHolidays()
    {
        $now = Carbon::now();
        // return Holiday::whereYear('date_from', $now->year)
        // ->whereBetween('date_from', [$this->pay_period['date_from'], $this->pay_period['date_to']]);
        return Holiday::whereBetween('date_from', [$this->pay_period['date_from'], $this->pay_period['date_to']]);
    }

    public function getTotalOverTime()
    {
        return EmployeeRequest::where('employee_id', $this->employee['id'])
        ->where('request_type', 'OT')
        ->where('approved', true)
        ->whereBetween('date_from', [$this->pay_period['date_from'], $this->pay_period['date_to']])
        ->whereRaw('DATE_FORMAT(date_from, \'%a\') <> \'Sun\'')
        ->whereNotIn('date_from', $this->holidays)        
        ->selectRaw('CASE WHEN number_of_hours > 8 THEN number_of_hours - 1 ELSE number_of_hours END AS total_hours')
        ->get()
        ->sum('total_hours');
    }

    public function getNightDifferentials()
    {
        $noOfHrs = array();
        $night_differential_regular_days = $this->model::where('attendance_logs.employee_id', $this->employee['id'])
        ->whereBetween('date', [$this->pay_period['date_from'], $this->pay_period['date_to']])
        ->whereNotIn('date', $this->holidays)
        ->select('date', 'time_in', 'time_out')
        ->get()
        ->toArray();

        $night_differential_over_time = EmployeeRequest::where('employee_id', $this->employee['id'])
        ->where('request_type', 'OT')
        ->where('approved', true)
        ->whereBetween('date_from', [$this->pay_period['date_from'], $this->pay_period['date_to']])
        ->select('date_from as date', 'time_start as time_in', 'time_end as time_out')
        ->get()
        ->toArray();
        
        $night_differentials = array_merge($night_differential_regular_days, $night_differential_over_time);

        foreach ($night_differentials as $attendanceLog) {
            $start_work = Carbon::parse($attendanceLog['date'].' '.$attendanceLog['time_in'])->timestamp;
            $end_work = Carbon::parse($attendanceLog['date'].' '.$attendanceLog['time_out'])->timestamp;

            if($end_work < $start_work) {
                $end_work = Carbon::createFromFormat('Y-m-d H:i:s',$attendanceLog['date'].' '.$attendanceLog['time_out'])->addDays()->timestamp;
            }

            if($attendanceLog['time_in'] && $attendanceLog['time_out'])
                $noOfHrs[] = $this->nightDifference($start_work,$end_work);
        }
        return array_sum($noOfHrs);
    }    

    public function getSpecialDayOrSunday()
    {
        // GET OVER TIME REQUEST ON SPECIAL HOLIDAY OR SUNDAY
        $holidaysOT = EmployeeRequest::where('employee_id', $this->employee['id'])
        ->where('request_type', 'OT')
        ->where('approved', true)
        ->whereBetween('date_from', [$this->pay_period['date_from'], $this->pay_period['date_to']])
        ->whereIn('date_from', $this->special_holidays)        
        ->get()
        ->toArray();

        $sundaysOT = EmployeeRequest::where('employee_id', $this->employee['id'])
        ->where('request_type', 'OT')
        ->where('approved', true)
        ->whereBetween('date_from', [$this->pay_period['date_from'], $this->pay_period['date_to']])
        ->whereRaw('DATE_FORMAT(date_from, \'%a\') = \'Sun\'')
        ->get()
        ->toArray();

        $overTimes = array_merge($holidaysOT, $sundaysOT);

        $totalRegularHrs = 0;
        $totalOverTimeHrs = 0;

        foreach ($overTimes as $hours) {
            $regularHrs = 0;
            $overTimeHrs = 0;

            if($hours['number_of_hours'] > 9) {
                $regularHrs = 8;
                $overTimeHrs = $hours['number_of_hours']-9;
            }
            else if($hours['number_of_hours'] == 9) {
                $regularHrs = 8;
                $overTimeHrs = 0;
            }
            else {
                $regularHrs = $hours['number_of_hours'];
                $overTimeHrs = 0;
            }

            $totalRegularHrs += $regularHrs;
            $totalOverTimeHrs += $overTimeHrs;
        }

        return array(
            'totalRegularHrs' => $totalRegularHrs,
            'totalOverTimeHrs' => $totalOverTimeHrs
        );
    }

    public function getRegularHoliday()
    {
        // GET OVER TIME REQUEST ON REGULAR HOLIDAYS
        $regularHolidayHrs = EmployeeRequest::where('employee_id', $this->employee['id'])
        ->where('request_type', 'OT')
        ->where('approved', true)
        ->whereBetween('date_from', [$this->pay_period['date_from'], $this->pay_period['date_to']])
        ->whereIn('date_from', $this->regular_holidays)        
        ->get()
        ->toArray();

        $totalRegularHrs = 0;
        $totalOverTimeHrs = 0;

        foreach ($regularHolidayHrs as $hours) {
            $regularHrs = 0;
            $overTimeHrs = 0;

            if($hours['number_of_hours'] > 9) {
                $regularHrs = 8;
                $overTimeHrs = $hours['number_of_hours']-9;
            }
            else if($hours['number_of_hours'] == 9) {
                $regularHrs = 8;
                $overTimeHrs = 0;
            }
            else {
                $regularHrs = $hours['number_of_hours'];
                $overTimeHrs = 0;
            }

            $totalRegularHrs += $regularHrs;
            $totalOverTimeHrs += $overTimeHrs;
        }

        return array(
            'totalRegularHrs' => $totalRegularHrs,
            'totalOverTimeHrs' => $totalOverTimeHrs
        );
    }

    public function getLate()
    {
        $number_of_hours = 9;
        if($this->employee['category_id'] == 1 || $this->employee['category_id'] == 2) {
            $number_of_hours = 10;
        }

        $late = $this->model::selectRaw($number_of_hours.' - duration as late')
        ->where('attendance_logs.employee_id', $this->employee['id'])
        ->whereBetween('date', [$this->pay_period['date_from'], $this->pay_period['date_to']])
        ->whereRaw('DATE_FORMAT(attendance_logs.date, \'%a\') <> \'Sun\'')
        ->whereNotIn('date', $this->holidays)
        ->where('attendance_logs.duration', '<', 9)
        ->get()
        ->sum('late');

        return ($late > 1) ? round($late) : 0;
    }

    public function getLoans()
    {
        $loans = Loan::where('employee_id',$this->employee['id'])
        ->wherecolumn('deduction_count', '<', 'total_deductions')
        ->get();
        $total_loans = 0;
        $loans_list = '<table class="table table-hover table-bordered">';
        foreach ($loans as $loan) {
            $total_loans += $loan['monthly_amortization'];
            $loans_list .= '<tr>';
            $loans_list .= '<td>'.$loan['loan_type'].'</td>';
            $loans_list .= '<td>'.$loan['monthly_amortization_formated'].'</td>';
            $loans_list .= '</tr>';
        }
        $loans_list .= '</table>';

        return [
            'loans_list' => $loans_list,
            'total_loans' => $total_loans
        ];
    }

    function nightDifference($start_work,$end_work)
    {
        try {
            $START_NIGHT_HOUR = '22';
            $START_NIGHT_MINUTE = '00';
            $START_NIGHT_SECOND = '00';
            $END_NIGHT_HOUR = '06';
            $END_NIGHT_MINUTE = '00';
            $END_NIGHT_SECOND = '00';    

            $start_night = mktime($START_NIGHT_HOUR,$START_NIGHT_MINUTE,$START_NIGHT_SECOND,date('m',$start_work),date('d',$start_work),date('Y',$start_work));
            $end_night   = mktime($END_NIGHT_HOUR,$END_NIGHT_MINUTE,$END_NIGHT_SECOND,date('m',$start_work),date('d',$start_work) + 1,date('Y',$start_work));

            if($start_work >= $start_night && $start_work <= $end_night)
            {
                if($end_work >= $end_night)
                {
                    return ($end_night - $start_work) / 3600;
                }
                else
                {
                    return ($end_work - $start_work) / 3600;
                }
            }
            elseif($end_work >= $start_night && $end_work <= $end_night)
            {
                if($start_work <= $start_night)
                {
                    return ($end_work - $start_night) / 3600;
                }
                else
                {
                    return ($end_work - $start_work) / 3600;
                }
            }
            else
            {
                if($start_work < $start_night && $end_work > $end_night)
                {
                    return ($end_night - $start_night) / 3600;
                }
                return 0;
            }
        }
        catch(\Exception $e) {
            return 'test';
        }
    }

    public function getWithholdingTax($taxable_income, $category_id)
    {
        switch ($category_id) {
            case '1':
            case '2':
            case '3':
            case '15':
            case '16':
            case '17':
                if($taxable_income <= 10417){
                    return 0;
                }
                else if($taxable_income > 10417 && $taxable_income < 16667) {
                    $overTaxable_income = ($taxable_income-10417)*0.20;
                    return $overTaxable_income;
                }
                else if($taxable_income > 16667 && $taxable_income < 33333) {
                    $overTaxable_income = ($taxable_income-16667)*0.25;
                    return $overTaxable_income+1250.00;
                }
                else if($taxable_income > 33333 && $taxable_income < 83333) {
                    $overTaxable_income = ($taxable_income-33333)*0.30;
                    return $overTaxable_income+5416.67;
                }
                else if($taxable_income > 83333 && $taxable_income < 333333){
                    $overTaxable_income = ($taxable_income-83333)*0.32;
                    return $overTaxable_income+20416.67;
                }
                else if($taxable_income >= 333333) {
                    $overTaxable_income = ($taxable_income-333333)*0.35;
                    return $overTaxable_income+100416.67;
                }
                break;
            case '11':
                return 0;
                // if($taxable_income <= 4808){
                //     return 0;
                // }
                // else if($taxable_income > 4808 && $taxable_income < 7692) {
                //     $overTaxable_income = ($taxable_income-4808)*0.20;
                //     return $overTaxable_income;
                // }
                // else if($taxable_income > 7692 && $taxable_income < 15385) {
                //     $overTaxable_income = ($taxable_income-7692)*0.25;
                //     return $overTaxable_income+576.92;
                // }
                // else if($taxable_income > 15385 && $taxable_income < 38462) {
                //     $overTaxable_income = ($taxable_income-15385)*0.30;
                //     return $overTaxable_income+2500;
                // }
                // else if($taxable_income > 38462 && $taxable_income < 153846){
                //     $overTaxable_income = ($taxable_income-38462)*0.32;
                //     return $overTaxable_income+9423.08;
                // }
                // else if($taxable_income >= 153846){
                //     $overTaxable_income = ($taxable_income-153846)*0.35;
                //     return $overTaxable_income+46346.15;
                // }
                // break;
        }
    }

    public function saveOtherIncome($request)
    {
        $total_other_income = 0;
        if(count($request->other_incomes) > 0) {
            foreach ($request->other_incomes as $other_income) {
                $where = [
                    ['id', $other_income['id']]  
                ];            
                $payrollOtherIncome = PayrollOtherIncome::where($where)->first();
                if(!$payrollOtherIncome)
                    $payrollOtherIncome = new PayrollOtherIncome;
                    $payrollOtherIncome->pay_period = $request->payperiod_id;
                    $payrollOtherIncome->employee_id = $request->employee_id;
                    $payrollOtherIncome->amount = $other_income['amount'];
                    $payrollOtherIncome->type = $other_income['type'];
                    $payrollOtherIncome->remarks = $other_income['remarks'];
                    $payrollOtherIncome->save();

                $payrollOtherIncome->amount = $other_income['amount'];
                $payrollOtherIncome->type = $other_income['type'];
                $payrollOtherIncome->remarks = $other_income['remarks'];
                $payrollOtherIncome->save();

                $other_income_amount = ($other_income['type'] == 'less-adjustment') ? (-1*$other_income['amount']) : $other_income['amount'];
                $total_other_income += $other_income_amount;
            }
            return $total_other_income;
        }
        return false;
    }

    public function updatePayroll($request, $total_other_income)
    {
        $payroll = Payroll::where('pay_period', $request->payperiod_id)
        ->where('employee_id', $request->employee_id)
        ->first();
        // GET EMPLOYEE DAILY WAGE & COLA        
        $daily_wage_wo_cola = $this->employee['daily_wage_wo_cola'];
        // GET REGULAR DAY total_count
        $regular_days = ($this->employee['category_id'] == 3) ? 15 : $this->getRegularDays();
        // GET REGULAR HOLIDAYS
        $regular_holidays = 0;
        switch ($this->employee['category_id']) {
            case 1:
                // CHECKING 
                $regular_holidays = count($this->regular_holidays)+count($this->special_holidays);
                break;
            case 3:
                $regular_holidays = 0;
                break;
            default:
                $regular_holidays = count($this->regular_holidays);
                break;
        }
        // GET LEAVES
        $approved_leaves = ($this->employee['category_id'] == 3) ? 0 : $this->getApprovedLeaves();
        // GET TOTAL COLA
        $cola = ($this->employee['category_id'] == 3) ? 0 : ($regular_days+$regular_holidays)*$this->employee['cola'];
        // TOTAL BASIC SALARY
        $total_basic_salary = round($daily_wage_wo_cola*($regular_days+$regular_holidays+$approved_leaves))+$cola;
        // NON TAXABLE INCOME
        $non_taxable_income = PayrollOtherIncome::select('amount')
        ->where('pay_period', $request->payperiod_id)
        ->where('employee_id', $request->employee_id)
        ->whereIn('type', ['allowance', 'incentive', '13th-mon', 'tax-refund'])
        ->get()
        ->sum('amount');
        // TAXABLE INCOME
        $taxable_income = PayrollOtherIncome::select('amount')
        ->where('pay_period', $request->payperiod_id)
        ->where('employee_id', $request->employee_id)
        ->where('type', 'adjustment')
        ->get()
        ->sum('amount');
        // NEGATIVE ADJUSTMENT
        $adjustment = PayrollOtherIncome::select('amount')
        ->where('pay_period', $request->payperiod_id)
        ->where('employee_id', $request->employee_id)
        ->where('type', 'less-adjustment')
        ->get()
        ->sum('amount');
        // OTHER DEDUCTIONS
        $other_deductions = PayrollOtherIncome::select('amount')
        ->where('pay_period', $request->payperiod_id)
        ->where('employee_id', $request->employee_id)
        ->where('type', 'deduction')
        ->get()
        ->sum('amount');
        // GET TOTAL GROSS SALARAY
        $total_gross_salary = round($total_basic_salary+$non_taxable_income+$taxable_income+$payroll->total_over_time-$payroll->late_total_amount-$adjustment,2);
        // GET TOTAL GOVERMENT DEDUCTIONS
        $total_government_deductions = $payroll->sss_prem+$payroll->sss_mpf+$payroll->philhealth_prem+$payroll->pagibig_prem;
        // GET TAXALBE INCOME
        $total_taxable_income = round($total_gross_salary-$payroll->total_cola-$non_taxable_income-$total_government_deductions,2);
        $total_taxable_income = ($total_taxable_income <= 0)? 0 : $total_taxable_income;
        // GET WITHHOLDING TAX
        $withholding_tax = $this->getWithholdingTax($total_taxable_income, $payroll->employee['category_id']);
        // GET TOTAL LOANS
        $total_loans = $payroll->total_loans;
        // GET TOTAL DEDUCTIONS
        $total_deductions = $total_government_deductions+$total_loans+$withholding_tax+$other_deductions;
        $net_salary = $total_gross_salary-$total_deductions;
        $net_salary = ($net_salary <= 0)? 0 : $net_salary;

        $payroll->other_income = ($non_taxable_income+$taxable_income-$adjustment);
        $payroll->total_basic_salary = $total_basic_salary;
        $payroll->total_gross_salary = $total_gross_salary;
        $payroll->taxable_income = $total_taxable_income;
        $payroll->withholding_tax = $withholding_tax;
        $payroll->other_deductions = $other_deductions;
        $payroll->total_deductions = $total_deductions;
        $payroll->net_salary = $net_salary;
        return $payroll->save();
    }

}