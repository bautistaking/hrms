<?php
namespace App\Repositories\AttendanceLog\Interfaces;

interface AttendanceLogRepositoryInterface
{
	public function setPayroll($request);

	public function generatePayroll();

	public function getRegularDays();

	public function getApprovedLeaves();

	public function getTotalOverTime();

	public function getNightDifferentials();

	public function getSpecialDayOrSunday();

	public function getHolidays();

	public function getLate();

	public function getLoans();

	public function nightDifference($start_work, $end_work);

	public function getWithholdingTax($taxableIncome, $category_id);

	public function saveOtherIncome($request);

	public function updatePayroll($request, $total_other_income);
}