<?php

namespace App\ViewModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use App\ViewModels\EmployeeProfile;
use App\Models\Requestor;

class ApproverRequestor extends Model
{
    use SoftDeletes;
     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id', 
        'requisition_type', 
        'requisition_level', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'approvers';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'approver_details',
        'requestors',
    ];

    public function getRequestors()
    {
        return $this->hasMany('App\Models\Requestor', 'approver_id', 'employee_id');
    }

    public function getApproverDetailsAttribute() 
    {
        return EmployeeProfile::find($this->employee_id);
    }

    public function getRequestorsAttribute() 
    {   
        $requestor = Requestor::where('approver_id', $this->employee_id)
        ->where('employee.active', 1)
        ->whereNull('employee.deleted_at')
        ->join('employee', 'requestors.requestor_id', '=', 'employee.id')
        ->get();

        if($requestor)
            return $requestor;
        return null;
    }    
}
