<?php

namespace App\ViewModels;

use Illuminate\Database\Eloquent\Model;

class AttendanceLogDetails extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'attendance_logs';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'name',
    ];

    public function getEmployeeDetails()
    {
        return $this->hasOne('App\Models\Employee', 'id', 'employee_id')
    }

    public function getNameAttribute() 
    {
        return $this->getEmployeeDetails()->pluck('name')->first();
    }   

}
