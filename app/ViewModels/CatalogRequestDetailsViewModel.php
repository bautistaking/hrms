<?php

namespace App\ViewModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use App\Models\InboundDetails;

class CatalogRequestDetailsViewModel extends Model
{
    use SoftDeletes;
     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'catalog_request_details';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'item_name',
        'item_details',
        'total_price_formated',
        'total_inbound_qty',
    ];

    public function getItemDetails()
    {
        return $this->hasOne('App\ViewModels\LogisticMaster', 'id', 'logistic_master_id');
    }

    /****************************************
    *           ATTRIBUTES PARTS            *
    ****************************************/
    public function getItemNameAttribute() 
    {
        $item_name = $this->getItemDetails()->pluck('item_name')->first();
        if(!$item_name)
        	return 'Pay Amount';
        return $item_name;
    }

    public function getItemDetailsAttribute() 
    {
        return $this->getItemDetails()->first();
    }

    public function getTotalPriceFormatedAttribute() 
    {
        return number_format($this->total_price, 2);
    }

    public function getTotalInboundQtyAttribute() 
    {
        return InboundDetails::where('catalog_request_details_id', $this->id)->sum('quantity_receive');
    }
}
