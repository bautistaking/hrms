<?php

namespace App\ViewModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\JobOrderDetails;
use App\Models\RequestStatus;
use App\Models\CatalogRequestPurchaseOrder;
use Carbon\Carbon;

class CatalogRequestViewModel extends Model
{
    use SoftDeletes;
     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'catalog_requests';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:M d, Y h:i',
        'updated_at' => 'datetime:M d, Y h:i',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'requestor',
        'status_name',
        'catalog_details',
        'total_price_formated',
        'supplier_name',
        'supplier_details',
        'po_number',
        'payment_status',
        'attachments',
    ];

    public function getClient()
    {
        return $this->hasOne('App\Models\Client', 'id', 'client_id');
    }

    public function getJobOrder()
    {
        return $this->hasOne('App\Models\JobOrder', 'id', 'job_order_id');
    }

    public function getRequestorDetails()
    {
        return $this->hasOne('App\Models\Employee', 'id', 'employee_id');
    }

    public function getSupplierDetails()
    {
        return $this->hasOne('App\Models\Supplier', 'id', 'supplier_id');
    }

    public function getCatalogDetails()
    {
        return $this->hasMany('App\ViewModels\CatalogRequestDetailsViewModel', 'catalog_request_id', 'id');
    }

    public function getAttachments()
    {
        return $this->hasMany('App\Models\CatalogRequestAttachments', 'catalog_request_id', 'id');
    }

    public function getPaymentRequestDetails()
    {
        return $this->hasOne('App\Models\PaymentRequest', 'catalog_request_id', 'id');
    }

    /****************************************
    *           ATTRIBUTES PARTS            *
    ****************************************/
    public function getRequestorAttribute() 
    {
        $requestor = $this->getRequestorDetails()->first();
        return $requestor->name;
    }  

    public function getStatusNameAttribute() 
    {
        $status = RequestStatus::find($this->status_id);
        return $status->status;
    }

    public function getCatalogDetailsAttribute() 
    {
        return $this->getCatalogDetails()->get();
    }

    public function getTotalPriceFormatedAttribute() 
    {
        return number_format($this->total_price, 2);
    }

    public function getSupplierNameAttribute() 
    {
        $name = $this->getSupplierDetails()->pluck('name')->first();
        if(!$name)
            return '';
        return $name;
    }

    public function getSupplierDetailsAttribute() 
    {
        return $this->getSupplierDetails()->first();
    }    

    public function getPoNumberAttribute() 
    {
        $purchase_order = CatalogRequestPurchaseOrder::where('catalog_request_id', $this->id)->first();
        if($purchase_order)
            return $purchase_order->po_number;

        return null;
    } 

    public function getPaymentStatusAttribute() 
    {
        $payment_status = $this->getPaymentRequestDetails()->first();
        if($payment_status)
            return $payment_status->status_id;

        return 0;
    }

    public function getAttachmentsAttribute() 
    {
        $attachments = $this->getAttachments()->get();
        if($attachments)
            return $attachments;

        return null;
    }

}
