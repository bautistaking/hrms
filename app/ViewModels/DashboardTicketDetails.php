<?php

namespace App\ViewModels;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\Ticket;
use App\Models\PlansReference;
use App\Models\TicketDetailsRemark;

class DashboardTicketDetails extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'deleted_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'ticket_details';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'site_name',
        'ticket_name',
        'ticket_references',
        'start_date_formated',
        'end_date_formated',
        'end_date_comparison',
        'status_name',
        'comments',
        'region_name',
        'province_name',
        'city_municipality_name',
    ];

    public function getTicket()
    {
        return Ticket::find($this->ticket_id);
    }

	public function getSiteNameAttribute()
    {
        $ticket = $this->getTicket();
        if($ticket)
            return $ticket->site_details->site_name;

        return null;
    }

    public function getTicketNameAttribute()
    {
        $reference = PlansReference::find($this->plans_reference_id);
        if($reference)
            return $reference->name;

        return null;
    }

    public function getTicketReferencesAttribute()
    {
        $reference = PlansReference::find($this->plans_reference_id);
        if($reference)
            return $reference->references;

        return null;
    }

    public function getStartDateFormatedAttribute()
    {
        if($this->start_date)
            return Carbon::createFromFormat('Y-m-d', $this->start_date)->format('M d, Y'); 
        return null;
    }

    public function getEndDateFormatedAttribute()
    {
        if($this->end_date)
            return Carbon::createFromFormat('Y-m-d', $this->end_date)->format('M d, Y'); 
        return null;
    }

    public function getEndDateComparisonAttribute()
    {
        if($this->end_date)
            return Carbon::createFromFormat('Y-m-d', $this->end_date)->format('m-d-Y'); 
        return null;
    }

    public function getStatusNameAttribute()
    {
        switch ($this->status) {
            case 1:
                return 'To Start';
            break;
            case 2:
                return 'On-Going';
            break;
            case 3:
                return 'Completed';
            break;
            case 4:
                return 'Critical';
            break;
            case 5:
                return 'Delayed';
            break;
        }
    }

    public function getCommentsAttribute()
    {
        return TicketDetailsRemark::where('ticket_details_id', $this->id)
                                  ->latest()
                                  ->get();
    }

    public function getRegionNameAttribute()
    {
        $ticket = $this->getTicket();
        if($ticket)
        	return ($ticket->site_details->region_details) ? $ticket->site_details->region_details->name : null;

        return null;
    }

    public function getProvinceNameAttribute()
    {
        $ticket = $this->getTicket();
        if($ticket)
        	return ($ticket->site_details->province_details) ? $ticket->site_details->province_details->province_name : null;

        return null;
    }

    public function getCityMunicipalityNameAttribute()
    {
        $ticket = $this->getTicket();
        if($ticket)
        	return ($ticket->site_details->city_details) ? $ticket->site_details->city_details->city_municipality_name : null;

        return null;
    }

}
