<?php

namespace App\ViewModels;

use Illuminate\Database\Eloquent\Model;

use App\Models\PayrollOtherIncome;
use App\Models\PayPeriod;
use Carbon\Carbon;

class EmployeePaySlip extends Model
{
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:M d, Y',
        'total_basic_salary' => 'decimal:2',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'payrolls';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'employee_details',
        'allowances',
        'adjustments',
        'other_deductions',
        'period_covered',
        '13_month_pay',
        'tax_refund',
    ];

    public function getEmployeeDetails()
    {
        return EmployeePayroll::find($this->employee_id);
    }

    public function getAllowance()
    {
		return PayrollOtherIncome::where('employee_id', $this->employee_id)
        ->where('pay_period', $this->pay_period)
        ->whereIn('type', ['allowance', 'incentive'])
        ->get();
    }

    public function getAdjustment()
    {
        return PayrollOtherIncome::where('employee_id', $this->employee_id)
        ->where('pay_period', $this->pay_period)
        ->whereIn('type', ['adjustment', 'less-adjustment'])
        ->get();
    }

    public function getOtherDeductions()
    {
        return PayrollOtherIncome::where('employee_id', $this->employee_id)
        ->where('pay_period', $this->pay_period)
        ->where('type', 'deduction')
        ->get();
    }

    public function getPayPeriod()
    {
    	$pay_period = PayPeriod::find($this->pay_period);
    	$date_from = Carbon::parse($pay_period->date_from);
        $date_to = Carbon::parse($pay_period->date_to);
        return $date_from->format('M d, Y'). ' to '.$date_to->format('M d, Y');
    }

    public function getEmployeeDetailsAttribute() 
    {
        return $this->getEmployeeDetails();
    }

    public function getAllowancesAttribute() 
    {
        return $this->getAllowance();
    }

    public function getAdjustmentsAttribute() 
    {
        return $this->getAdjustment();
    }

    public function getOtherDeductionsAttribute() 
    {
        return $this->getOtherDeductions();
    }

    public function getPeriodCoveredAttribute() 
    {
        return $this->getPayPeriod();
    }

    public function get13MonthPayAttribute() 
    {
        return PayrollOtherIncome::where('employee_id', $this->employee_id)
        ->where('pay_period', $this->pay_period)
        ->whereIn('type', ['13th-mon'])
        ->get();
    }

    public function getTaxRefundAttribute() 
    {
        return PayrollOtherIncome::where('employee_id', $this->employee_id)
        ->where('pay_period', $this->pay_period)
        ->whereIn('type', ['tax-refund'])
        ->get();
    }

}
