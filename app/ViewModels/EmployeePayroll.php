<?php

namespace App\ViewModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use App\Models\EmployeeMeta;

class EmployeePayroll extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'salt',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'employee';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
	public $appends = [
        'monthly_rate',
        'daily_wage_wo_cola',
        'cola',
        'sss_prem',
        'sss_mpf',
        'philhealth_prem',
        'pagibig_prem',
        'vacation_leave',
        'sick_leave',
        'emergency_leave',
        'maternity_leave',
        'paternity_leave',
        'bereavement_leave',
        'birthday_leave',
        'work_from_home'
    ];

    public function getEmployeeDetails()
    {   
        return $this->hasMany('App\Models\EmployeeMeta', 'employee_id', 'id');
    }

    /****************************************
    *           ATTRIBUTES PARTS            *
    ****************************************/
    public function getMonthlyRateAttribute() 
    {
        $monthly_rate = $this->getEmployeeDetails()
        					 ->where('meta_key', 'basic_salary')
        					 ->pluck('meta_value')->first();
        return $monthly_rate;
    }

    public function getDailyWageWoColaAttribute() 
    {
        if($this->category_id == 3){
            return $this->getEmployeeDetails()
            ->where('meta_key', 'daily_wage')
            ->pluck('meta_value')->first();
            //return round($this->monthly_rate/30,2);
        }
        else if($this->category_id == 11) {
            return $this->getEmployeeDetails()
            ->where('meta_key', 'daily_wage')
            ->pluck('meta_value')->first();
        }
        else {
            $employee_cola = $this->getEmployeeDetails()
            ->where('meta_key', 'cola')
            ->pluck('meta_value')->first();
            
            return round($this->monthly_rate/26-$employee_cola,2);
        }
    } 

	public function getColaAttribute() 
    {
        $cola = $this->getEmployeeDetails()
        					 ->where('meta_key', 'cola')
        					 ->pluck('meta_value')->first();
        return $cola;
    }

    public function getSssPremAttribute()
    {
        return $this->getEmployeeDetails()
        ->where('meta_key', 'sss_prem')
        ->pluck('meta_value')->first();
    }

    public function getSssMpfAttribute()
    {
        return $this->getEmployeeDetails()
        ->where('meta_key', 'sss_mpf')
        ->pluck('meta_value')->first();
    }

    public function getPhilhealthPremAttribute()
    {
        return $this->getEmployeeDetails()
        ->where('meta_key', 'philhealth_prem')
        ->pluck('meta_value')->first();
    }

    public function getPagibigPremAttribute()
    {
        return $this->getEmployeeDetails()
        ->where('meta_key', 'pagibig_prem')
        ->pluck('meta_value')->first();
    }

    public function getVacationLeaveAttribute()
    {
        return $this->getEmployeeDetails()
        ->where('meta_key', 'vacation_leave')
        ->pluck('meta_value')->first();
    }

    public function getSickLeaveAttribute()
    {
        return $this->getEmployeeDetails()
        ->where('meta_key', 'sick_leave')
        ->pluck('meta_value')->first();
    }

    public function getEmergencyLeaveAttribute()
    {
        return $this->getEmployeeDetails()
        ->where('meta_key', 'emergency_leave')
        ->pluck('meta_value')->first();
    }

    public function getMaternityLeaveAttribute()
    {
        return $this->getEmployeeDetails()
        ->where('meta_key', 'maternity_leave')
        ->pluck('meta_value')->first();
    }

    public function getPaternityLeaveAttribute()
    {
        return $this->getEmployeeDetails()
        ->where('meta_key', 'paternity_leave')
        ->pluck('meta_value')->first();
    }

    public function getBereavementLeaveAttribute()
    {
        return $this->getEmployeeDetails()
        ->where('meta_key', 'bereavement_leave')
        ->pluck('meta_value')->first();
    }

    public function getBirthdayLeaveAttribute()
    {
        return $this->getEmployeeDetails()
        ->where('meta_key', 'birthday_leave')
        ->pluck('meta_value')->first();
    }

    public function getWorkFromHomeAttribute()
    {
        return $this->getEmployeeDetails()
        ->where('meta_key', 'work_from_home')
        ->pluck('meta_value')->first();
    }

}
