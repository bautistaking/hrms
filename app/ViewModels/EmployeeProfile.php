<?php

namespace App\ViewModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use App\Models\EmployeeMeta;
use App\Models\EmployeeRequest;

class EmployeeProfile extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'salt'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'deleted_at' => 'datetime:Y-m-d H:i:s'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'employee';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
	public $appends = [
        'category',
        'designation',
        'birth_date',
        'date_hired',
        'date_regularization',
        'duration',
        'duration_reg',
        'details',
        'is_purchasing',
        'is_iocc',
        'is_finance',
        'is_iocc_edit',
        'vacation_leave',
        'sick_leave',
        'emergency_leave',
        'maternity_leave',
        'paternity_leave',
        'bereavement_leave',
        'birthday_leave',
        'department',
        'sign_path',
    ];

    public function getEmployeeDetails()
    {   
        return $this->hasMany('App\Models\EmployeeMeta', 'employee_id', 'id');
    }

    public function getCategory()
    {
        return $this->hasOne('App\Models\Category', 'id', 'category_id');
    }

    public function getDesignation()
    {
        return $this->hasOne('App\Models\Designation', 'id', 'designation_id');
    }

    public function getDepartment()
    {
        return $this->hasOne('App\Models\Department', 'id', 'department_id');
    }

    public function getRequest()
    {
        return $this->hasMany('App\Models\EmployeeRequest', 'employee_id', 'id');
    }

    public function getCatalog()
    {
        return $this->hasMany('App\Models\CatalogRequest', 'employee_id', 'id');
    }

    public function getTotalLeave($type)
    {
        return EmployeeRequest::where('employee_id', $this->id)
                                ->where('request_type', $type)
                                ->whereYear('date_from', Carbon::now()->year)
                                ->where('approved', 1)
                                ->selectRaw('(number_of_hours/8) AS days')
                                ->get()
                                ->sum('days');
    }

    /****************************************
    *           ATTRIBUTES PARTS            *
    ****************************************/
    public function getDetailsAttribute() 
    {
        return $this->getEmployeeDetails()->pluck('meta_value','meta_key')->toArray();
    }

    public function getCategoryAttribute() 
    {
        return $this->getCategory()->pluck('name')->first();
    }

    public function getDesignationAttribute() 
    {
        return $this->getDesignation()->pluck('name')->first();
    }

    public function getDepartmentAttribute() 
    {
        return $this->getDepartment()->pluck('name')->first();
    }

    public function getBirthDateAttribute() 
    {
        $birthDate =  $this->getEmployeeDetails()->where('meta_key', 'birth_date')->pluck('meta_value')->first();
        $birthDate = Carbon::parse($birthDate);
        return $birthDate->format('M d, Y');
    }

    public function getDateHiredAttribute() 
    {
        $dateHired =  $this->getEmployeeDetails()->where('meta_key', 'date_hired')->pluck('meta_value')->first();
        $dateHired = Carbon::parse($dateHired);
        return $dateHired->format('M d, Y');
    }

    public function getDateRegularizationAttribute() 
    {
        $dateRegular =  $this->getEmployeeDetails()->where('meta_key', 'date_regularization')->pluck('meta_value')->first();
        $dateRegular = Carbon::parse($dateRegular);
        return $dateRegular->format('M d, Y');
    }

    public function getDurationAttribute() 
    {
        $now  = Carbon::now();
        $date = $this->getEmployeeDetails()->where('meta_key', 'date_hired')->pluck('meta_value')->first();
        // show difference in days between now and end dates
        $dateHired = Carbon::parse($date);
        //$inYears = $now->diffInYears($dateHired);
        $inMonth = $now->diffInMonths($dateHired);

        return floor($inMonth/12) .' Years, '. $inMonth%12 .' Months';
    }

    public function getDurationRegAttribute() 
    {
        $now  = Carbon::now();
        $date = $this->getEmployeeDetails()->where('meta_key', 'date_regularization')->pluck('meta_value')->first();
        // show difference in days between now and end dates
        $dateHired = Carbon::parse($date);
        //$inYears = $now->diffInYears($dateHired);
        $inMonth = $now->diffInMonths($dateHired);

        return floor($inMonth/12) .' Years, '. $inMonth%12 .' Months';
    }

    public function getIsPurchasingAttribute() 
    {
        $user_purchasing = array('80097', '80201', '80199', '80226', '80168', '80204', '80207', '80073', '80221', '80210', '80027', '80198', '80222', '80119', '80219', '80224', '80220', '80209', '80215', '80223', '80132', '80148', '80039', '80233', '80241', '80223', '80244', '80236', '80243', '80260', '80259', '80147', '80271', '80228');
        return in_array($this->id_number, $user_purchasing);
    }

    public function getIsFinanceAttribute()
    {
        $finance_users = array('80016', '80230', '80039', '80231', '80223', '80233', '80241', '80223', '80244', '80243', '80147');
        return in_array($this->id_number, $finance_users);
    }

    public function getIsIoccAttribute()
    {
        $iocc_users = array('80012', '80147', '80097', '80148', '80132', '80213', '80208', '80028', '80048','80001', '80004','80005','80007','80008','80009','80013','80014','80016','80018','80020','80223','80022','80025','80027','80029','80032','80033','80034','80035','80036','80037','80039','80041','80042','80044','80047','80049','80050','80051','80057','80058','80059','80061','80063','80064','80065','80066','80067','80068','80069','80071','80072','80075','80079','80081','80084','80089','80093','80098','80108','80119','80124','80140','80159','80160','80170','80191','80198','80199','80200','80201','80202','80204','80207','80209','80210','80215','80054', '80226', '80232', '80233', '80223', '80224', '80244', '80236', '80243', '80260', '80259');

        return in_array($this->id_number, $iocc_users);
    }

    public function getIsIoccEditAttribute() 
    {
        $iocc_users = array('80028', '80048', '80012');
        return in_array($this->id_number, $iocc_users);
    }

    public function getVacationLeaveAttribute()
    {
        $employee_details = $this->getEmployeeDetails()->pluck('meta_value','meta_key');
        if(isset($employee_details['vacation_leave']))
            return ($employee_details['vacation_leave']-$this->getTotalLeave('VL'));
        return 0;
    }

    public function getSickLeaveAttribute()
    {
        $employee_details = $this->getEmployeeDetails()->pluck('meta_value','meta_key');
        if(isset($employee_details['sick_leave']))
            return ($employee_details['sick_leave']-$this->getTotalLeave('SL'));
        return 0;
    }

    public function getEmergencyLeaveAttribute()
    {
        $employee_details = $this->getEmployeeDetails()->pluck('meta_value','meta_key');
        if(isset($employee_details['emergency_leave']))
            return ($employee_details['emergency_leave']-$this->getTotalLeave('EL'));
        return 0;
    }

    public function getMaternityLeaveAttribute()
    {
        $employee_details = $this->getEmployeeDetails()->pluck('meta_value','meta_key');
        if(isset($employee_details['maternity_leave']))
            return ($employee_details['maternity_leave']-$this->getTotalLeave('ML'));
        return 0;
    }

    public function getPaternityLeaveAttribute()
    {
        $employee_details = $this->getEmployeeDetails()->pluck('meta_value','meta_key');
        if(isset($employee_details['paternity_leave']))
            return ($employee_details['paternity_leave']-$this->getTotalLeave('PL'));
        return 0;
    }

    public function getBereavementLeaveAttribute()
    {
        $employee_details = $this->getEmployeeDetails()->pluck('meta_value','meta_key');
        if(isset($employee_details['bereavement_leave']))
            return ($employee_details['bereavement_leave']-$this->getTotalLeave('SPL'));
        return 0;
    }

    public function getBirthdayLeaveAttribute()
    {
        $employee_details = $this->getEmployeeDetails()->pluck('meta_value','meta_key');
        if(isset($employee_details['birthday_leave']))
            return ($employee_details['birthday_leave']-$this->getTotalLeave('BL'));
        return 0;
    }

    public function getSignPathAttribute()
    {
        $employee_details = $this->getEmployeeDetails()->pluck('meta_value','meta_key');
        if(isset($employee_details['sign_path']))            
            return asset($employee_details['sign_path']);
        return null;
    }

}
