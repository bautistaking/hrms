<?php

namespace App\ViewModels;

use Illuminate\Database\Eloquent\Model;
use App\Models\Ticket;
use App\Models\JobOrderDetails;

class RegionViewModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'regions';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'total_done',
        'total_on_going',
    ];

    public function getTotalDoneAttribute()
    {
        $job_order_details = JobOrderDetails::where('job_order_details.region_id', $this->id)
        ->where('tickets.status', 2)
        ->join('tickets', 'job_order_details.id', 'tickets.job_order_details_id')
        ->get()
        ->count();

        return $job_order_details;
    }

    public function getTotalOnGoingAttribute()
    {
        $job_order_details = JobOrderDetails::where('job_order_details.region_id', $this->id)
        ->where('tickets.status', 1)
        ->join('tickets', 'job_order_details.id', 'tickets.job_order_details_id')
        ->get()
        ->count();

        return $job_order_details;
    }
}
