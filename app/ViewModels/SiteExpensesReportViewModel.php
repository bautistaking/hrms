<?php

namespace App\ViewModels;

use Illuminate\Database\Eloquent\Model;

use App\Models\JobOrderDetailsTag;

class SiteExpensesReportViewModel extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:M, d Y H:i:s',
        'deleted_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'job_order_details';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'running_expenses',
        'sites_expenses',
    ];    

    public function getExpenses()
    {
        return JobOrderDetailsTag::where('job_order_detail_id', $this->id)
                                 ->whereIn('reference_name', ['payment_request','outbound_request'])
                                 ->get();
    }

    public function getRunningExpensesAttribute() 
    {
    	$expenses = $this->getExpenses()->sum('total');
        return ((double)$this->expenses+$expenses);
    }

    public function getSitesExpensesAttribute() 
    {
        $expenses = $this->getExpenses();
        if($expenses)
            return $expenses;
        return null;
    }


}
