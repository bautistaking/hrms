<?php

namespace App\ViewModels;

use Illuminate\Database\Eloquent\Model;
use App\Models\Role;
use App\Models\RolePermission;
use App\Models\UserMeta;

class UserProfile extends Model
{
	/**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token', 
        'activation_token', 
        'salt'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

	/**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'users';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

	/**
     * Append additiona info to the return data
     *
     * @var string
     */
	public $appends = [
        'details',
        'roles',
        'permissions'
    ];

    public function getUserRoles()
    {   
        return $this->hasMany('App\Models\UserRole', 'user_id');
    }

    public function getUserDetails()
    {   
        return $this->hasMany('App\Models\UserMeta', 'user_id', 'id');
    }
    
    /****************************************
    *           ATTRIBUTES PARTS            *
    ****************************************/
    public function getDetailsAttribute() 
    {
        return $this->getUserDetails()->pluck('meta_value','meta_key')->toArray();
    }

    public function getRolesAttribute()
    {   
    	$roles = $this->getUserRoles()->pluck('role_id')->toArray();
        return Role::whereIn('id', $roles)->get();
    }

    public function getPermissionsAttribute() 
    {
        $roles = $this->getUserRoles()->pluck('role_id')->toArray();
        return RolePermission::whereIn('role_id',$roles)->get();
    }
}
