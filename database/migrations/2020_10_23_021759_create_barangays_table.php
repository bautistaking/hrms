<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBarangaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barangays', function (Blueprint $table) {
            $table->engine = "InnoDB";
            
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('barangay_name');
            $table->string('region_id', 10)->index();
            $table->string('province_id', 10)->index();
            $table->string('city_id', 10)->index();

            $table->index(
                ['province_id', 'region_id'], 
                'barangay_idx_1'
            );
            $table->index(
                ['city_id', 'province_id', 'region_id'], 
                'barangay_idx_2'
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barangays');
    }
}
