<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->engine = "InnoDB";
            
            $table->bigIncrements('id');
            $table->bigInteger('parent_id')->nullable()->unsigned()->index();
            $table->string('name');
            $table->string('slug_name');
            $table->string('pay_period');
            $table->string('working_hours');
            $table->mediumText('description')->nullable();
            $table->mediumText('image_url')->nullable();
            $table->integer('sort_order')->nullable();
            $table->string('module')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
