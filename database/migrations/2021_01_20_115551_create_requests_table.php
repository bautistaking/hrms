<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->bigInteger('employee_id')->nullable()->unsigned();
            $table->enum('request_type', ['OT', 'OB', 'VL', 'SL', 'EL', 'ML', 'PL', 'WFH', 'SPL']);
            $table->date('date_from')->nullable();
            $table->date('date_to')->nullable();
            $table->time('time_start')->nullable();
            $table->time('time_end')->nullable();
            $table->integer('number_of_hours')->default(0);
            $table->mediumText('description')->nullable();
            $table->mediumText('attachment')->nullable();
            $table->boolean('approved')->default(false);
            $table->bigInteger('approver_id')->nullable()->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('employee_id')->references('id')->on('employee');                        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
