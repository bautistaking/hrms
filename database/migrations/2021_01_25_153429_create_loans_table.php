<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->bigInteger('employee_id')->nullable()->unsigned();
            $table->bigInteger('loan_type_id')->nullable()->unsigned();
            $table->decimal('total_loan_amount', 10, 2);
            $table->decimal('monthly_amortization', 10, 2);
            $table->decimal('total_paid_amount', 10, 2);
            $table->decimal('remaining_balance', 10, 2);
            $table->integer('deduction_count')->default(0);
            $table->integer('total_deductions')->default(0);
            $table->mediumText('remarks')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('employee_id')->references('id')->on('employee');
            $table->foreign('loan_type_id')->references('id')->on('loan_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
