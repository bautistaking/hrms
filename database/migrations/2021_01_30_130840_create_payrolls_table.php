<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payrolls', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->bigInteger('pay_period')->unsigned();
            $table->bigInteger('employee_id')->unsigned();
            $table->integer('number_of_days')->default(0);
            $table->integer('regular_holidays')->default(0);
            $table->integer('approved_leaves')->default(0);            
            $table->decimal('total_cola', 10, 2)->default(0.00);
            $table->decimal('other_income', 10, 2)->default(0.00);
            $table->decimal('total_basic_salary', 10, 2)->default(0.00);
            $table->integer('over_time_no_of_hrs')->default(0);
            $table->decimal('over_time_rate_per_hrs', 10, 2)->default(0.00);
            $table->decimal('over_time_total_amount', 10, 2)->default(0.00);
            $table->integer('night_differential_no_of_hrs')->default(0);
            $table->decimal('night_differential_rate_per_hrs', 10, 2)->default(0.00);
            $table->decimal('night_differential_total_amount', 10, 2)->default(0.00);
            $table->integer('special_day_no_of_hrs')->default(0);
            $table->decimal('special_day_rate_per_hrs', 10, 2)->default(0.00);
            $table->decimal('special_day_total_amount', 10, 2)->default(0.00);
            $table->integer('special_day_over_time_no_of_hrs')->default(0);
            $table->decimal('special_day_over_time_rate_per_hrs', 10, 2)->default(0.00);
            $table->decimal('special_day_over_time_total_amount', 10, 2)->default(0.00);
            $table->integer('holiday_day_no_of_hrs')->default(0);
            $table->decimal('holiday_day_rate_per_hrs', 10, 2)->default(0.00);
            $table->decimal('holiday_day_total_amount', 10, 2)->default(0.00);
            $table->integer('holiday_day_over_time_no_of_hrs')->default(0);
            $table->decimal('holiday_day_over_time_rate_per_hrs', 10, 2)->default(0.00);
            $table->decimal('holiday_day_over_time_total_amount', 10, 2)->default(0.00);
            $table->decimal('total_over_time', 10, 2)->default(0.00);
            $table->integer('late_no_of_minutes')->default(0);
            $table->decimal('late_rate_per_hrs', 10, 2)->default(0.00);
            $table->decimal('late_total_amount', 10, 2)->default(0.00);
            $table->decimal('total_gross_salary', 10, 2)->default(0.00);
            $table->decimal('withholding_tax', 10, 2)->default(0.00);
            $table->decimal('sss_prem', 10, 2)->default(0.00);
            $table->decimal('sss_mpf', 10, 2)->default(0.00);
            $table->decimal('philhealth_prem', 10, 2)->default(0.00);
            $table->decimal('pagibig_prem', 10, 2)->default(0.00);
            $table->mediumText('loans')->nullable();            
            $table->decimal('total_loans', 10, 2)->default(0.00);
            $table->decimal('other_deductions', 10, 2)->default(0.00);
            $table->decimal('total_deductions', 10, 2)->default(0.00);
            $table->decimal('net_salary', 10, 2)->default(0.00);
            $table->decimal('taxable_income', 10, 2)->default(0.00);
            $table->boolean('is_process')->default(false);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('pay_period')->references('id')->on('pay_periods');            
            $table->foreign('employee_id')->references('id')->on('employee');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payrolls');
    }
}
