<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayrollOtherIncomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_other_incomes', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->bigInteger('pay_period')->nullable()->unsigned();
            $table->bigInteger('employee_id')->nullable()->unsigned();
            $table->enum('type', ['allowance', 'incentive', 'adjustment', 'deduction']);
            $table->decimal('amount', 10, 2);
            $table->string('remarks')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('pay_period')->references('id')->on('pay_periods')->onDelete('cascade');        
            $table->foreign('employee_id')->references('id')->on('employee')->onDelete('cascade');        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_other_incomes');
    }
}
