<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogisticMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logistic_masters', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->string('sku')->nullable();
            $table->string('item_name')->nullable();
            $table->string('unit_of_measurement')->nullable();
            $table->bigInteger('category_id')->nullable()->unsigned();
            $table->integer('in_stock')->nullable()->unsigned();
            $table->decimal('unit_price',12,2)->default(0.00);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logistic_masters');
    }
}
