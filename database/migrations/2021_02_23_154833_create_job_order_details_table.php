<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_order_details', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->bigInteger('job_order_id')->unsigned();
            $table->string('bcf_name')->nullable();
            $table->string('site_name')->nullable();
            $table->string('site_details')->nullable();
            $table->decimal('bill_of_quantities', 12, 2)->default(0.00);
            $table->decimal('variation_amount', 12, 2)->default(0.00);
            $table->decimal('updated_bill_of_quantities', 12, 2)->default(0.00);
            $table->decimal('badget', 12, 2)->default(0.00);
            $table->decimal('balance_amount', 12, 2)->default(0.00);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('job_order_id')->references('id')->on('job_orders');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_order_details');
    }
}
