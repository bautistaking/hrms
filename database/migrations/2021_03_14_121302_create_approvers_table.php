<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApproversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approvers', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->bigInteger('employee_id')->unsigned();
            $table->integer('requisition_type')->nullable();
            $table->integer('requisition_level')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('employee_id')->references('id')->on('employee');            
        });

        Schema::create('requestors', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigInteger('approver_id')->unsigned();
            $table->bigInteger('requestor_id')->unsigned();

            $table->foreign('approver_id')->references('id')->on('employee');
            $table->foreign('requestor_id')->references('id')->on('employee');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requestors');
        Schema::dropIfExists('approvers');
    }
}
