<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatalogRequestsTable extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalog_requests', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->string('request_number')->nullable();
            $table->bigInteger('employee_id')->unsigned();
            $table->bigInteger('approver_id')->unsigned();
            $table->bigInteger('status_id')->unsigned();
            $table->bigInteger('supplier_id')->unsigned()->nullable();
            $table->decimal('total_price', 12, 2)->default(0.00);
            $table->enum('catalog_type', ['catalog', 'non-catalog']);
            $table->mediumText('remarks')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('employee_id')->references('id')->on('employee');
            $table->foreign('status_id')->references('id')->on('request_statuses');
        });

        Schema::create('catalog_request_details', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->bigInteger('catalog_request_id')->unsigned();
            $table->bigInteger('logistic_master_id')->unsigned();
            $table->integer('quantity')->unsigned();
            $table->string('uom')->nullable();
            $table->decimal('unit_price', 12, 2)->default(0.00);
            $table->decimal('total_price', 12, 2)->default(0.00);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('catalog_request_id')->references('id')->on('catalog_requests');
        });  

        Schema::create('catalog_request_purchase_orders', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->bigInteger('catalog_request_id')->unsigned();
            $table->bigInteger('supplier_id')->unsigned();
            $table->boolean('vatable')->default(false);
            $table->string('po_number')->nullable();
            $table->decimal('gross_amount', 12, 2)->default(0.00);
            $table->decimal('net_amount', 12, 2)->default(0.00);
            $table->decimal('vat_amount', 12, 2)->default(0.00);
            $table->decimal('ewt_amount', 12, 2)->default(0.00);
            $table->decimal('ewt_percent', 8, 2)->default(0.00);
            $table->string('ewt_type')->nullable();
            $table->decimal('total_amount_due', 12, 2)->default(0.00);
            $table->string('deliver_to')->nullable();
            $table->date('required_date')->nullable();
            $table->string('prepared_by')->nullable();
            $table->timestamps();

            $table->foreign('catalog_request_id')->references('id')->on('catalog_requests');
            $table->foreign('supplier_id')->references('id')->on('suppliers');
        });      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalog_request_purchase_orders');
        Schema::dropIfExists('catalog_request_details');
        Schema::dropIfExists('catalog_requests');
        
    }

}
