<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoanHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_histories', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->bigInteger('employee_id')->unsigned();
            $table->bigInteger('pay_period')->unsigned();
            $table->bigInteger('loan_id')->unsigned();
            $table->decimal('total_paid_amount_before', 10, 2);
            $table->decimal('total_paid_amount_after', 10, 2);
            $table->decimal('remaining_balance_before', 10, 2);
            $table->decimal('remaining_balance_after', 10, 2);
            $table->integer('deduction_count_before');
            $table->integer('deduction_count_after');
            $table->decimal('paid_amount', 10, 2);
            $table->timestamps();

            $table->foreign('employee_id')->references('id')->on('employee');
            $table->foreign('pay_period')->references('id')->on('pay_periods');
            $table->foreign('loan_id')->references('id')->on('loans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_histories');
    }
}
