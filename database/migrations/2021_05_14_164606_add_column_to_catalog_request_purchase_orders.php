<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToCatalogRequestPurchaseOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('catalog_request_purchase_orders', function (Blueprint $table) {
            $table->string('deliver_to')->after('total_amount_due')->nullable();
            $table->date('required_date')->after('deliver_to')->nullable();
            $table->string('prepared_by')->after('required_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('catalog_request_purchase_orders', function (Blueprint $table) {
            $table->dropColumn(['prepared_by']);
            $table->dropColumn(['required_date']);
            $table->dropColumn(['deliver_to']);
        });
    }   
    
}