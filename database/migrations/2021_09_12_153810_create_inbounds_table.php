<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInboundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inbounds', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->bigInteger('employee_id')->unsigned()->index();
            $table->string('reference_number')->nullable();
            $table->string('po_number')->nullable();
            $table->string('transaction_type')->nullable();            
            $table->string('remarks')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('inbounds_details', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->bigInteger('inbound_id')->unsigned();
            $table->bigInteger('catalog_request_details_id')->unsigned();
            $table->bigInteger('logistic_master_id')->unsigned();
            $table->integer('quantity_receive')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inbounds_details');
        Schema::dropIfExists('inbounds');
    }
}
