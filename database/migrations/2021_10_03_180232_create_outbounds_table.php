<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutboundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outbounds', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->bigInteger('employee_id')->unsigned()->index();
            $table->bigInteger('client_id')->unsigned()->index();
            $table->bigInteger('job_order_id')->unsigned()->index();
            $table->bigInteger('job_order_details_id')->unsigned()->index();
            $table->string('request_number')->nullable();
            $table->decimal('total_price', 12, 2)->default(0.00);
            $table->string('pull_out_from')->nullable();
            $table->string('deliver_to')->nullable();
            $table->date('required_date')->nullable();
            $table->mediumText('remarks')->nullable();
            $table->integer('status_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('outbounds_details', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->bigInteger('outbound_id')->unsigned()->index();
            $table->bigInteger('logistic_master_id')->unsigned()->index();
            $table->integer('quantity')->unsigned();
            $table->string('uom')->nullable();
            $table->decimal('unit_price', 12, 2)->default(0.00);
            $table->decimal('total_price', 12, 2)->default(0.00);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outbounds_details');
        Schema::dropIfExists('outbounds');
    }
    
}
