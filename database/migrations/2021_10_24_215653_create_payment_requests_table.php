<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_requests', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->string('request_number')->nullable();
            $table->bigInteger('employee_id')->unsigned()->index();
            $table->bigInteger('client_id')->unsigned()->index();
            $table->bigInteger('job_order_id')->unsigned()->index();
            $table->bigInteger('job_order_details_id')->unsigned()->index()->nullable();
            $table->bigInteger('catalog_request_id')->unsigned()->index()->nullable();
            $table->bigInteger('request_id')->unsigned()->index();
            $table->string('request_type')->nullable();
            $table->date('required_date')->nullable();
            $table->mediumText('pay_to')->nullable();
            $table->decimal('gross_amount', 12, 2)->default(0.00);
            $table->decimal('net_amount', 12, 2)->default(0.00);
            $table->decimal('vat_amount', 12, 2)->default(0.00);
            $table->decimal('ewt_amount', 12, 2)->default(0.00);
            $table->decimal('ewt_percent', 8, 2)->default(0.00);
            $table->string('ewt_type')->nullable();
            $table->decimal('total_amount_due', 12, 2)->default(0.00);
            $table->mediumText('remarks')->nullable();
            $table->integer('status_id')->unsigned();
            $table->integer('approver_id')->unsigned();            
            $table->string('payment_type')->nullable();
            $table->string('release_type')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_requests');
    }
}
