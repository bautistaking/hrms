<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('type')->nullable();
            $table->string('address')->nullable();
            $table->string('team_lead')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('email')->unique()->index();
            $table->string('password');
            $table->string('api_token')->nullable();
            $table->string('salt')->nullable();
            $table->integer('login_attempt')->default(0);
            $table->integer('is_blocked')->default(0);
            $table->boolean('active')->default(false);
            $table->string('activation_token')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('team_members', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->bigInteger('team_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('designation')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('email')->unique()->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('team_id')->references('id')->on('teams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_members');
        Schema::dropIfExists('teams');
    }
}
