<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans_references', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->enum('solution', ['wifi', 'ibs']);
            $table->integer('simple')->nullable();
            $table->integer('simple_skip')->nullable();
            $table->integer('medium')->nullable();
            $table->integer('medium_skip')->nullable();
            $table->integer('complex')->nullable();
            $table->integer('complex_skip')->nullable();
            $table->mediumText('references')->nullable();
            $table->timestamps();
            $table->softDeletes();       
                 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans_references');
    }
}
