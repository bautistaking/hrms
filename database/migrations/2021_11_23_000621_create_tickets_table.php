<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->string('ticket_number')->nullable()->index();
            $table->bigInteger('client_id')->unsigned()->index();
            $table->bigInteger('job_order_id')->unsigned()->index();
            $table->bigInteger('job_order_details_id')->unsigned()->index();
            $table->string('solution_type')->nullable()->index();
            $table->string('service_type')->nullable()->index();
            $table->mediumText('remarks')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->boolean('is_priority')->default(false);
            $table->tinyInteger('track_code')->default(1);
            $table->tinyInteger('status')->default(0);            
            $table->bigInteger('created_by')->unsigned()->index()->nullable();
            $table->bigInteger('updated_by')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('ticket_details', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->bigInteger('ticket_id')->unsigned()->index();
            $table->bigInteger('plans_reference_id')->unsigned()->index();
            $table->bigInteger('team_id')->unsigned()->index();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->tinyInteger('status')->default(0);            
            $table->mediumText('remarks')->nullable();
            $table->bigInteger('created_by')->unsigned()->index()->nullable();
            $table->bigInteger('updated_by')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('ticket_id')->references('id')->on('tickets');
            $table->foreign('plans_reference_id')->references('id')->on('plans_references');
            $table->foreign('team_id')->references('id')->on('teams');

        });

        Schema::create('ticket_details_remarks', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->bigInteger('ticket_details_id')->unsigned()->index();
            $table->bigInteger('remarks_by')->unsigned()->index();
            $table->mediumText('remarks')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('ticket_attachments', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->bigInteger('ticket_id')->unsigned()->index();
            $table->bigInteger('ticket_details_id')->unsigned()->index();
            $table->bigInteger('ticket_details_remark_id')->unsigned()->index();
            $table->mediumText('attachment_link')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_attachments');
        Schema::dropIfExists('ticket_details_remarks');
        Schema::dropIfExists('ticket_details');
        Schema::dropIfExists('tickets');
    }

}
