<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLocationJobOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_order_details', function (Blueprint $table) {
            $table->bigInteger('region_id')->after('site_details')->nullable();
            $table->bigInteger('province_id')->after('region_id')->nullable();
            $table->bigInteger('city_id')->after('province_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_order_details', function (Blueprint $table) {
            $table->dropColumn(['city_id']);
            $table->dropColumn(['province_id']);
            $table->dropColumn(['region_id']);
        });
    }
}
