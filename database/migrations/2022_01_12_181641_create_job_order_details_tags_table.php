<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobOrderDetailsTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_order_details_tags', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->bigInteger('job_order_detail_id')->unsigned();
            $table->bigInteger('reference_id')->unsigned()->index();
            $table->string('reference_name')->nullable()->index();
            $table->decimal('total', 12, 2)->default(0.00);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('job_order_detail_id')->references('id')->on('job_order_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_order_details_tags');
    }
}
