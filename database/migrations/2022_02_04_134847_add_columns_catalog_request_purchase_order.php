<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsCatalogRequestPurchaseOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('catalog_request_purchase_orders', function (Blueprint $table) {
            $table->bigInteger('client_id')->after('supplier_id')->unsigned()->index();
            $table->bigInteger('job_order_id')->after('client_id')->unsigned()->index();
            $table->bigInteger('job_order_details_id')->after('job_order_id')->unsigned()->index();
            $table->string('currency')->after('pf_number')->nullable();
            $table->decimal('discount_rate', 8, 2)->after('ewt_percent')->default(0.00);
            $table->mediumText('remarks')->after('prepared_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('catalog_request_purchase_orders', function (Blueprint $table) {
            $table->dropColumn(['remarks']);
            $table->dropColumn(['discount_rate']);
            $table->dropColumn(['currency']);
            $table->dropColumn(['job_order_details_id']);
            $table->dropColumn(['job_order_id']);
            $table->dropColumn(['client_id']);
        });
    }
}
