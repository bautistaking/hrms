<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collections', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->bigInteger('client_id')->unsigned()->index();
            $table->bigInteger('job_order_id')->unsigned()->index();
            $table->bigInteger('job_order_details_id')->unsigned()->index();
            $table->decimal('billing_progress', 12, 2)->default(0.00);
            $table->mediumText('remarks')->nullable();
            $table->date('invoice_date')->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('or_number')->nullable();
            $table->decimal('invoice_amount', 12, 2)->default(0.00);
            $table->decimal('amount_wo_vat', 12, 2)->default(0.00);
            $table->decimal('vat_amount', 12, 2)->default(0.00);
            $table->decimal('ewt_amount', 12, 2)->default(0.00);
            $table->decimal('net_amount', 12, 2)->default(0.00);
            $table->date('payment_due')->nullable();
            $table->string('policy_number')->nullable();
            $table->date('coverage_due')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collections');
    }
}
