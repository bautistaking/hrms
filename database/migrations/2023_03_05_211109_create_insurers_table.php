<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsurersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurers', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->mediumText('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('insurance_policies', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->mediumText('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('collections', function (Blueprint $table) {
            $table->bigInteger('insurer_id')->after('remarks')->unsigned()->index();
            $table->bigInteger('insurance_policy_id')->after('insurer_id')->unsigned()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collections', function (Blueprint $table) {
            $table->dropColumn(['insurer_id']);
            $table->dropColumn(['insurance_policy_id']);
        });
        Schema::dropIfExists('insurance_policies');
        Schema::dropIfExists('insurers');
    }
}
