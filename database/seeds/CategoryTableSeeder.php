<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $input['name'] = 'Regulars';
        $input['slug_name'] = 'regulars';
	    $input['description'] = 'Regulars employees';
        $input['module'] = 'employee';
		Category::create($input);

        $input['name'] = 'Contractual';
        $input['slug_name'] = 'contractual';
	    $input['description'] = 'Contractual employees';
        $input['module'] = 'employee';
		Category::create($input);

        $input['name'] = 'Manager';
        $input['slug_name'] = 'manager';
        $input['description'] = 'Manager employees';
        $input['module'] = 'employee';
        Category::create($input);

        $input['name'] = 'CIVIL';
        $input['slug_name'] = 'civil';
        $input['description'] = 'ITEMS RELATED TO CIVIL WORKS';
        $input['module'] = 'requisition';
        Category::create($input);

        $input['name'] = 'ELECTRICAL';
        $input['slug_name'] = 'electrical';
        $input['description'] = 'ITEMS RELATED TO ELECTRICAL WORKS';
        $input['module'] = 'requisition';
        Category::create($input);

        $input['name'] = 'PPE';
        $input['slug_name'] = 'ppe';
        $input['description'] = 'SAFETY AND SECURITY RELATED ITEMS';
        $input['module'] = 'requisition';
        Category::create($input);

        $input['name'] = 'MISCELLANEOUS';
        $input['slug_name'] = 'miscellaneous';
        $input['description'] = 'OTHERS';
        $input['module'] = 'requisition';
        Category::create($input);

        $input['name'] = 'TOOLS & EQUIPMENT';
        $input['slug_name'] = 'tools_&_equipment';
        $input['description'] = 'CAPEX REQUESTS FOR MOBILIZATION RELATED ITEMS';
        $input['module'] = 'requisition';
        Category::create($input);

        $input['name'] = 'MECHANICAL';
        $input['slug_name'] = 'mechanical';
        $input['description'] = 'ITEMS RELATED TO MECHANICAL WORKS';
        $input['module'] = 'requisition';
        Category::create($input);

        $input['name'] = 'DATA';
        $input['slug_name'] = 'data';
        $input['description'] = 'FOC,UTP, PATCH CORD';
        $input['module'] = 'requisition';
        Category::create($input);
    }
}
