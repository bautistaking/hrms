<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(ModuleTableSeeder::class);
        $this->call(RolePermissionTableSeeder::class);
        $this->call(UserRolesTableSeeder::class);        
        $this->call(CategoryTableSeeder::class);        
        $this->call(DesignationTableSeeder::class);        
         // Manual Call This
        // $this->call(RegionsSeeder::class);
        // $this->call(ProvincesSeeder::class);
        // $this->call(BarangaysSeeder::class);
        // $this->call(CitiesSeeder::class); 
    }
}
