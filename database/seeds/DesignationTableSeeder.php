<?php

use Illuminate\Database\Seeder;
use App\Models\Designation;

class DesignationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $input['name'] = 'Driver';
        $input['description'] = 'Driver';

		Designation::create($input);

        $input['name'] = 'RF Engineer';
        $input['description'] = 'RF Engineer';

		Designation::create($input);

        $input['name'] = 'Electrical Engineer';
        $input['description'] = 'Electrical Engineer';

		Designation::create($input);

        $input['name'] = 'Project Engineer';
        $input['description'] = 'Project Engineer';

		Designation::create($input);

        $input['name'] = 'Auto Cad Officer';
        $input['description'] = 'Auto Cad Officer';

		Designation::create($input);

        $input['name'] = 'Warehouse Custodian';
        $input['description'] = 'Warehouse Custodian';

		Designation::create($input);

        $input['name'] = 'Liason Officer';
        $input['description'] = 'Liason Officer';

		Designation::create($input);

        $input['name'] = 'Integrator';
        $input['description'] = 'Integrator';

		Designation::create($input);

        $input['name'] = 'Account Services Officer';
        $input['description'] = 'Account Services Officer';

		Designation::create($input);

        $input['name'] = 'Treasury';
        $input['description'] = 'Treasury';

		Designation::create($input);

        $input['name'] = 'Team Leader';
        $input['description'] = 'Team Leader';

		Designation::create($input);

        $input['name'] = 'Agile Lead';
        $input['description'] = 'Agile Lead';

		Designation::create($input);

        $input['name'] = 'Project SuperVisor';
        $input['description'] = 'Project SuperVisor';

		Designation::create($input);

        $input['name'] = 'Accounting Staff';
        $input['description'] = 'Accounting Staff';

		Designation::create($input);

        $input['name'] = 'Secretary';
        $input['description'] = 'Secretary';

		Designation::create($input);

        $input['name'] = 'Billing Officer';
        $input['description'] = 'Billing Officer';

		Designation::create($input);

        $input['name'] = 'Account Services Manager';
        $input['description'] = 'Account Services Manager';

		Designation::create($input);

        $input['name'] = 'Business Unit Manager';
        $input['description'] = 'Business Unit Manager';

		Designation::create($input);

        $input['name'] = 'Planning & Design Manager';
        $input['description'] = 'Planning & Design Manager';

		Designation::create($input);

        $input['name'] = 'Financial Services Manager';
        $input['description'] = 'Financial Services Manager';

		Designation::create($input);

        $input['name'] = 'Project Manager';
        $input['description'] = 'Project Manager';

		Designation::create($input);

        $input['name'] = 'Chief Commercial Advicer';
        $input['description'] = 'Chief Commercial Advicer';

		Designation::create($input);

        $input['name'] = 'Installer';
        $input['description'] = 'Installer';

		Designation::create($input);

        $input['name'] = 'Electrician';
        $input['description'] = 'Electrician';

		Designation::create($input);

        $input['name'] = 'Helper';
        $input['description'] = 'Helper';

		Designation::create($input);

        $input['name'] = 'Account Officer';
        $input['description'] = 'Account Officer';

		Designation::create($input);

        $input['name'] = 'Auto-CAD Officer';
        $input['description'] = 'Auto-CAD Officer';

		Designation::create($input);

        $input['name'] = 'Treasury Assistant';
        $input['description'] = 'Treasury Assistant';

		Designation::create($input);

        $input['name'] = 'Rigger';
        $input['description'] = 'Rigger';

		Designation::create($input);

        $input['name'] = 'Agile Lead for Radio Frequency Engineer';
        $input['description'] = 'Agile Lead for Radio Frequency Engineer';

        Designation::create($input);

        $input['name'] = 'Corporate Logistics Supervisor';
        $input['description'] = 'Corporate Logistics Supervisor';

        Designation::create($input);

        $input['name'] = 'Lead Project Engineer';
        $input['description'] = 'Lead Project Engineer';

        Designation::create($input);

        $input['name'] = 'Agile Lead for Site Acquition';
        $input['description'] = 'Agile Lead for Site Acquition';

        Designation::create($input);

        $input['name'] = 'Safety Officer';
        $input['description'] = 'Safety Officer';

        Designation::create($input);

    }
}
