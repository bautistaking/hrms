<?php

use Illuminate\Database\Seeder;
use App\Models\Module;

class ModuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Module::create([
            'user_id' => 1,
		    'name' => 'User Management',
		    'description' => 'User Management'
		]);

        Module::create([
            'user_id' => 1,
            'name' => 'Employees Management',
            'description' => 'Employees Management'
        ]);

        Module::create([
            'user_id' => 1,
            'name' => 'Payroll Management',
            'description' => 'Payroll Management'
        ]);

        Module::create([
            'user_id' => 1,
            'name' => 'Reports',
            'description' => 'Generate Reports'
        ]);

        Module::create([
            'user_id' => 1,
            'name' => 'Approvers',
            'description' => 'Approvers'
        ]);
    }
}
