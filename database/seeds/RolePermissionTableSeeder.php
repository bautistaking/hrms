<?php

use Illuminate\Database\Seeder;
use App\Models\RolePermission;
use App\Models\Module;

class RolePermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modules = Module::get();
        
        foreach ($modules as $key => $module) {
			RolePermission::create([
			    'role_id' => 1,
			    'module_id' => $module->id,
                'status' => 1
			]);
        }
    }
}
