<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Role::create([
		    'user_id' => 1,
		    'name' => 'Administrator',
		    'description' => 'System Administrator',
		    'active' => 1
		]);

		Role::create([
		    'user_id' => 1,
		    'name' => 'Admin',
		    'description' => 'Admin User',
		    'active' => 1
		]);

		Role::create([
		    'user_id' => 1,
		    'name' => 'Human Resource',
		    'description' => 'Human Resource Admin User',
		    'active' => 1
		]);

		Role::create([
		    'user_id' => 1,
		    'name' => 'Employee',
		    'description' => 'Employee User',
		    'active' => 1
		]);

		Role::create([
		    'user_id' => 1,
		    'name' => 'Guest',
		    'description' => 'Guest User',
		    'active' => 1
		]);

    }
}
