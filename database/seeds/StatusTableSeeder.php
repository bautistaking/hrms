<?php

use Illuminate\Database\Seeder;
use App\Models\RequestStatus;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $input['status'] = 'Draft';
        $input['status_title'] = 'Draft to Pending';
        $input['status_message'] = 'Your initial request';

        RequestStatus::create($input);

        $input['status'] = 'For Canvassing';
        $input['status_title'] = 'For Canvassing to For Verification';
        $input['status_message'] = 'Your request has been approved for canvassing.';

		RequestStatus::create($input);

        $input['status'] = 'For Verification';
        $input['status_title'] = 'For Verification to Verified';
        $input['status_message'] = 'Your request has been received, please wait to verify.';

        RequestStatus::create($input);

        $input['status'] = 'Verified';
        $input['status_title'] = 'Verified by your immediate supervisor';
        $input['status_message'] = 'Your request has been Verified.';

        RequestStatus::create($input);

        $input['status'] = 'For Endorsement';
        $input['status_title'] = 'Endorsed by your immediate supervisor';
        $input['status_message'] = 'Your request has been received for Endorsement.';

        RequestStatus::create($input);

        $input['status'] = 'Endorsed';
        $input['status_title'] = 'Endorsed by your immediate supervisor';
        $input['status_message'] = 'Your request has been Endorsed.';

        RequestStatus::create($input);

        $input['status'] = 'Approved';
        $input['status_title'] = 'Approved by your immediate supervisor';
        $input['status_message'] = 'Your request has been Approved.';

		RequestStatus::create($input);

        $input['status'] = 'Decline';
        $input['status_title'] = 'Decline by your immediate supervisor';
        $input['status_message'] = 'Your request has been Decline.';

        RequestStatus::create($input);

    }
}