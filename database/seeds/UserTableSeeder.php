<?php

use Illuminate\Database\Seeder;
use App\Services\Helpers\PasswordHelper;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $salt = bcrypt(PasswordHelper::generateSalt());

        $input['name'] = 'Administrator';
        $input['email'] = 'bautistael23@gmail.com';
        $input['password'] = 'sup3r@dm1nus3r2022';
        $input['salt'] = $salt;
        $input['password'] = PasswordHelper::generate($salt, $input['password']);
        $input['active'] = 1;

		User::create($input);

        $input['name'] = 'Admin';
        $input['email'] = 'administrator@gmail.com';
        $input['password'] = '@dm1n1str@tor';
        $input['salt'] = $salt;
        $input['password'] = PasswordHelper::generate($salt, $input['password']);
        $input['active'] = 1;

        User::create($input);

        $input['name'] = 'Human Resource';
        $input['email'] = 'hr@gmail.com';
        $input['password'] = 'hr@dm1n';
        $input['salt'] = $salt;
        $input['password'] = PasswordHelper::generate($salt, $input['password']);
        $input['active'] = 1;

        User::create($input);
    }
}
