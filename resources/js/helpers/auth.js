import { setAuthorization } from "./general";

export function login(credentials) {
    return new Promise((res, rej) => {        
        if(credentials.is_employee == true) {
            axios.post('/api/v1/employee-login', credentials)
                .then((response) => {
                    setAuthorization(response.data.data.token);
                    res(response.data.data);
                })
        }
        else if(credentials.is_team == true) {
            axios.post('/api/v1/team-login', credentials)
                .then((response) => {
                    setAuthorization(response.data.data.token);
                    res(response.data.data);
                })
        }
        else {
            axios.post('/api/v1/login', credentials)
                .then((response) => {
                    setAuthorization(response.data.data.token);
                    res(response.data.data);
                })                    
        }
    })
}

export function getLocalUser() {
    const userStr = localStorage.getItem("user");
    if(!userStr) {
        return null;
    }

    return JSON.parse(userStr);
}