export function initialize(store, router) {
    router.beforeEach((to, from, next) => {
        const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
        const currentUser = store.state.currentUser;
        const dashboard = store.state.dashboard;

        if(requiresAuth && !currentUser) {
            if(to.path == '/employee/portal') {
                next('/employee/login');
            }
            else {
                next('/'+dashboard+'/login');
            }
        } 
        else if(to.path == '/'+dashboard+'/login' && currentUser) {
            next('/'+dashboard+'/dashboard');
        } else {
            next();
        }
    });

    if (store.getters.currentUser) {
        setAuthorization(store.getters.currentUser.token);
    }
}

export function setAuthorization(token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`
}