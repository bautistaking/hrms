import web from './views/WebLayout/TheMaster.vue';
import attendance from './views/Web/Index.vue';
import employeeLogin from './views/Web/Auth/Login.vue';
import employeeForgotPassword from './views/Web/Auth/ForgotPassword.vue';
import portal from './views/Web/Employee/AttendanceHistory.vue';
import profile from './views/Web/Employee/Settings.vue';
import overTime from './views/Web/Employee/OverTimeRequest.vue';
import officialBussiness from './views/Web/Employee/OfficialBusinessRequest.vue';
import vacationLeave from './views/Web/Employee/VacationLeaveRequest.vue';
import sickLeave from './views/Web/Employee/SickLeaveRequest.vue';
import otherLeave from './views/Web/Employee/OtherLeave.vue';
import loans from './views/Web/Employee/Loans.vue';
import payslip from './views/Web/Employee/Payslip.vue';
import catalogRequest from './views/Web/Employee/CatalogRequest.vue';
import catalogDetails from './views/Web/Employee/CatalogDetails.vue';
import nonCatalogRequest from './views/Web/Employee/NonCatalogRequest.vue';
import notifications from './views/Web/Employee/Notification.vue';
import canvassing from './views/Web/Employee/Canvassing.vue';
import canvassing_details from './views/Web/Employee/CanvassingDetails.vue';

import qrcode from './views/Admin/QrCode/Index.vue';
// auth
import login from './views/Admin/Auth/Login.vue';
import forgotPassword from './views/Admin/Auth/ForgotPassword.vue';
// cms components pages
import admin from './views/AdminLayout/TheMaster.vue';
import dashboard from './views/Admin/Dashboard/Index.vue';
import accountSettings from './views/Admin/MyAccount/Index.vue';
import comingSoon from './views/ComingSoon/Index.vue';
// end cms components pages

//admin
import users from './views/Admin/User/Index.vue';
import roles from './views/Admin/Role/Index.vue';
import grantPermissions from './views/Admin/Role/Permissions.vue';
import categories from './views/Admin/Categories/Index.vue';
import designation from './views/Admin/Designation/Index.vue';
import department from './views/Admin/Department/Index.vue';
import employee from './views/Admin/Employee/Index.vue';
import loanTypes from './views/Admin/Loan/Types.vue';
import employeeLoans from './views/Admin/Loan/Index.vue';
import holidays from './views/Admin/Holiday/Index.vue';
import timeKeeping from './views/Admin/TimeKeeping/Index.vue';
import overTimeRequest from './views/Admin/Request/OverTime.vue';
import officialBussinessRequest from './views/Admin/Request/OfficialBusiness.vue';
import leavesRequest from './views/Admin/Request/LeavesRequest.vue';
import payPeriod from './views/Admin/Payroll/Index.vue';
import payrollProcess from './views/Admin/Payroll/payroll.vue';
import requisitionCategories from './views/Admin/Requisition/Categories.vue';
import requisitionLogistics from './views/Admin/Requisition/LogisticsMaster.vue';
import requisitionSupplier from './views/Admin/Requisition/Supplier.vue';
import requisitionClient from './views/Admin/Requisition/Client.vue';
import requisitionJobOrder from './views/Admin/Requisition/JobOrder.vue';
import requisitionJobOrderDetails from './views/Admin/Requisition/JobOrderDetails.vue';
import approver from './views/Admin/Approver/Index.vue';
import requests from './views/Admin/Requisition/Requests.vue';
import requestDetails from './views/Admin/Requisition/RequestsDetails.vue';
import requestList from './views/Admin/Requisition/RequestsList.vue';
import teams from './views/Admin/Teams/Index.vue';
import plansReferences from './views/Admin/PlansReferences/Index.vue';
import tickets from './views/Admin/Ticket/Index.vue';
import nonCatalogRequestAdmin from './views/Admin/Requisition/Non-Catalog.vue';
import nonCatalogRequestDetailsAdmin from './views/Admin/Requisition/Non-CatalogDetails.vue';

import purchasing from './views/Web/Purchasing/Index.vue';
import purchasing_details from './views/Web/Purchasing/Details.vue';

import Inventory from './views/Web/Inventory/Index.vue';
import Inventory_categories from './views/Web/Inventory/Category.vue';
import Suppliers from './views/Web/Inventory/Suppliers.vue';
import Inbound from './views/Web/Inventory/Inbound.vue';
import Outbound from './views/Web/Inventory/Outbound.vue';

import iocc_dashboard from './views/Web/Iocc/Dashboard.vue';
import iocc from './views/Web/Iocc/Index.vue';
import iocc_team from './views/Web/Iocc/Team.vue';

import request_catalog from './views/Web/ApproveRequest/Catalog.vue';
import request_catalog_details from './views/Web/ApproveRequest/Catalog-Details.vue';
import request_payment from './views/Web/ApproveRequest/Non-Catalog.vue';
import request_payment_details from './views/Web/ApproveRequest/Non-Catalog-Details.vue';

import team_login from './views/Team/Auth/Login.vue';
import team_admin from './views/TeamLayout/TheMaster.vue';
import team_dashboard from './views/Team/Dashboard/Index.vue';

import collections from './views/Web/Billing/Index.vue';

import manualTimein from './views/Web/Employee/ManualTimeIn.vue';

import employeeTimeKeeping from './views/Web/TimeKeeping/Index.vue';

import Insurer from './views/Admin/Insurer/Index.vue';
import Policy from './views/Admin/Policy/Index.vue';

import warehouse from './views/WarehouseLayout/TheMaster.vue';
import warehouse_dashboard from './views/Warehouse/Dashboard/Index.vue';
import warehouse_login from './views/Warehouse/Auth/Login.vue';
import warehouse_inventory from './views/Web/Inventory/Index.vue';
import inbound_request from './views/Warehouse/Inbound.vue';
import outbound_request from './views/Warehouse/Outbound.vue';
import warehouse_purchasing from './views/Warehouse/Purchasing.vue';

export const routes = [
    {

        path: '/',
        component: web,
        children: [
            {
                path: '/',
                component: attendance,
                name: 'employee-home'
            },
            {
                path: '/employee/manual/time-in',
                component: manualTimein,
                name: 'employee-manual-time-in'
            },
            {
                path: '/employee/login',
                component: employeeLogin,
                name: 'employee-login'
            },
            {
                path: '/employee/forgot-password',
                component: employeeForgotPassword,
                name: 'employee-forgot-password'
            },
            {
                path: '/employee/portal',
                component: portal,
                name: 'portal',
                meta: {
                    requiresAuth: true
                },
            },
            {
                path: '/employee/profile',
                component: profile,
                name: 'profile',
                meta: {
                    requiresAuth: true
                },
            },        
            {
                path: '/employee/over-time',
                component: overTime,
                name: 'overTime',
                meta: {
                    requiresAuth: true
                },
            },        
            {
                path: '/employee/official-bussiness',
                component: officialBussiness,
                name: 'officialBussiness',
                meta: {
                    requiresAuth: true
                },
            },        
            {
                path: '/employee/vacation-leave',
                component: vacationLeave,
                name: 'vacationLeave',
                meta: {
                    requiresAuth: true
                },
            },        
            {
                path: '/employee/sick-leave',
                component: sickLeave,
                name: 'sickLeave',
                meta: {
                    requiresAuth: true
                },
            },        
            {
                path: '/employee/other-leave',
                component: otherLeave,
                name: 'otherLeave',
                meta: {
                    requiresAuth: true
                },
            },        
            {
                path: '/employee/loans',
                component: loans,
                name: 'loans',
                meta: {
                    requiresAuth: true
                },
            },        
            {
                path: '/employee/payslip',
                component: payslip,
                name: 'payslip',
                meta: {
                    requiresAuth: true
                },
            },                
            {
                path: '/employee/non-catalog',
                component: nonCatalogRequest,
                name: 'non-catalog-request',
                meta: {
                    requiresAuth: true
                },
            },        
            {
                path: '/employee/catalog-details/:id',
                component: catalogDetails,
                name: 'catalog-details',
                meta: {
                    requiresAuth: true
                },
            }, 
            {
                path: '/employee/canvassing',
                component: canvassing,
                name: 'canvassing',
                meta: {
                    requiresAuth: true
                },
            },
            {
                path: '/employee/canvassing-details/:id',
                component: canvassing_details,
                name: 'canvassing-details',
                meta: {
                    requiresAuth: true
                },
            },                                   
            {
                path: '/employee/notifications',
                component: notifications,
                name: 'notifications',
                meta: {
                    requiresAuth: true
                },
            },             
            {
                path: '/employee/time-keeping',
                component: employeeTimeKeeping,
                name: 'time-keeping',
                meta: {
                    requiresAuth: true
                },
            },             
            {
                path: '/purchasing',
                component: purchasing,
                name: 'purchasing',
                meta: {
                    requiresAuth: true
                },
            },
            {
                path: '/purchasing/:id',
                component: purchasing_details,
                name: 'purchasing-details',
                meta: {
                    requiresAuth: true
                },
            },  
            {
                path: '/inventory/logistics',
                component: Inventory,
                name: 'inventory',
                meta: {
                    requiresAuth: true
                },
            },
            {
                path: '/inventory/categories',
                component: Inventory_categories,
                name: 'inv-categories',
                meta: {
                    requiresAuth: true
                },
            },
            {
                path: '/inventory/suppliers',
                component: Suppliers,
                name: 'suplliers',
                meta: {
                    requiresAuth: true
                },
            },
            {
                path: '/inventory/inbound',
                component: Inbound,
                name: 'inbound',
                meta: {
                    requiresAuth: true
                },
            },
            {
                path: '/inventory/outbound',
                component: Outbound,
                name: 'outbound',
                meta: {
                    requiresAuth: true
                },
            },
            {
                path: '/iocc/dashboard',
                component: iocc_dashboard,
                name: 'iocc-dashboard',
                meta: {
                    requiresAuth: true
                },
            },
            {
                path: '/iocc/tickets',
                component: iocc,
                name: 'iocc',
                meta: {
                    requiresAuth: true
                },
            },
            {
                path: '/iocc/teams',
                component: iocc_team,
                name: 'iocc-teams',
                meta: {
                    requiresAuth: true
                },
            },
            {
                path: '/request/catalog',
                component: request_catalog,
                name: 'request-catalog',
                meta: {
                    requiresAuth: true
                },
            },
            {
                path: '/request/catalog/:id',
                component: request_catalog_details,
                name: 'request-catalog-details'
            },
            {
                path: '/request/non-catalog',
                component: request_payment,
                name: 'request-payment',
                meta: {
                    requiresAuth: true
                },
            },
            {
                path: '/request/non-catalog/:id',
                component: request_payment_details,
                name: 'requests-payment-details'
            },        
            {
                path: '/request/collections',
                component: collections,
                name: 'collections'
            },       
        ]
    },
    {
        path: '/admin/login',
        component: login,
        name: 'login'
    },
    {
        path: '/admin/forgot-password',
        component: forgotPassword,
        name: 'forgot-password'
    },
    {
        path: '/admin/dashboard',
        component: admin,
        meta: {
            requiresAuth: true
        },
        children: [          
            {
                path: '/',
                component: dashboard,
                name: 'home'
            },            
            {
                path: '/admin/comingSoon',
                component: comingSoon,
                name: 'comming-soon'
            },  
            {
                path: '/admin/users',
                component: users,
                name: 'users'
            },            
            {
                path: '/admin/roles',
                component: roles,
                name: 'roles'
            },            
            {
                path: '/admin/role/permissions/:id',
                component: grantPermissions,
                name: 'grant.permissions'
            },            
            {
                path: '/admin/employee/categories',
                component: categories,
                name: 'categories'
            },            
            {
                path: '/admin/employee/designation',
                component: designation,
                name: 'designation'
            },            
            {
                path: '/admin/employee/department',
                component: department,
                name: 'department'
            },                        
            {
                path: '/admin/employee/list',
                component: employee,
                name: 'employee'
            },            
            {
                path: '/admin/qr-code',
                component: qrcode,
                name: 'qrcode'
            },                   
            {
                path: '/admin/loan/types',
                component: loanTypes,
                name: 'loanTypes'
            },                  
            {
                path: '/admin/loan/list',
                component: employeeLoans,
                name: 'employeeLoans'
            },                  
            {
                path: '/admin/payroll/holidays',
                component: holidays,
                name: 'holidays'
            },                  
            {
                path: '/admin/payroll/time-keeping',
                component: timeKeeping,
                name: 'timeKeeping'
            },                  
            {
                path: '/admin/payroll/over-time',
                component: overTimeRequest,
                name: 'over-time'
            },                  
            {
                path: '/admin/payroll/official-bussiness',
                component: officialBussinessRequest,
                name: 'official-bussiness-request'
            },                  
            {
                path: '/admin/payroll/leaves-request',
                component: leavesRequest,
                name: 'leaves-request'
            },                  
            {
                path: '/admin/payroll/pay-period',
                component: payPeriod,
                name: 'payroll'
            },
            {
                path: '/admin/payroll/pay-period/:id',
                component: payrollProcess,
                name: 'payroll-process'
            },            
            {
                path: '/admin/requisition/categories',
                component: requisitionCategories,
                name: 'requisition-categories'
            }, 
            {
                path: '/admin/requisition/policy',
                component: Policy,
                name: 'requisition-Policy'
            },   
            {
                path: '/admin/requisition/insurer',
                component: Insurer,
                name: 'requisition-Insurer'
            },
            {
                path: '/admin/requisition/logistics',
                component: requisitionLogistics,
                name: 'requisition-logistics'
            },   
            {
                path: '/admin/requisition/supplier',
                component: requisitionSupplier,
                name: 'requisition-supplier'
            },   
            {
                path: '/admin/requisition/client',
                component: requisitionClient,
                name: 'requisition-client'
            },   
            {
                path: '/admin/requisition/job-order',
                component: requisitionJobOrder,
                name: 'requisition-job-order'
            },   
            {
                path: '/admin/requisition/job-order/:id',
                component: requisitionJobOrderDetails,
                name: 'requisition-job-order-details'
            },   
            {
                path: '/admin/requisition/catalog',
                component: requests,
                name: 'requests'
            },              
            {
                path: '/admin/requisition/requests/:id',
                component: requestDetails,
                name: 'requests-details'
            },  
            {
                path: '/admin/requisition/non-catalog',
                component: nonCatalogRequestAdmin,
                name: 'requests-non-catalog'
            },  
            {
                path: '/admin/requisition/non-catalog/:id',
                component: nonCatalogRequestDetailsAdmin,
                name: 'requests-non-catalog-details'
            },              
            {
                path: '/admin/account-settings',
                component: accountSettings,
                name: 'account'
            },              
            {
                path: '/admin/approver',
                component: approver,
                name: 'approver'
            },  
            {
                path: '/admin/request-list',
                component: requestList,
                name: 'request-list'
            },  
            {
                path: '/admin/teams',
                component: teams,
                name: 'teams'
            },  
            {
                path: '/admin/plans-and-references',
                component: plansReferences,
                name: 'plans-referenses'
            },
            {
                path: '/admin/tickets',
                component: tickets,
                name: 'tickets'
            },

            
        ]
    },
    {
        path: '/team/login',
        component: team_login,
        name: 'team-login'
    },
    {
        path: '/team/dashboard',
        component: team_admin,
        meta: {
            requiresAuth: true
        },
        children: [          
            {
                path: '/',
                component: team_dashboard,
                name: 'team-home'
            },  
        ]
    },
    {
        path: '/warehouse/login',
        component: warehouse_login,
        name: 'warehouse-login'
    },
    {
        path: '/warehouse',
        component: warehouse,
        meta: {
            requiresAuth: true
        },
        children: [          
            {
                path: '/warehouse/dashboard',
                component: warehouse_dashboard,
                name: 'warehouse-dashboard'
            },
            {
                path: '/warehouse/inventories',
                component: warehouse_inventory,
                name: 'warehouse-inventory'
            },
            {
                path: '/warehouse/inbound-request',
                component: inbound_request,
                name: 'inbound-request'
            },  
            {
                path: '/warehouse/outbound-request',
                component: outbound_request,
                name: 'outbound-request'
            },  
            {
                path: '/warehouse/purchase-orders',
                component: warehouse_purchasing,
                name: 'purchase-orders'
            }, 
            {
                path: '/warehouse/portal',
                component: portal,
                name: 'warehouse-portal',
            },
            {
                path: '/warehouse/profile',
                component: profile,
                name: 'warehouse-profile',
            },        
            {
                path: '/warehouse/over-time',
                component: overTime,
                name: 'warehouse-overTime',
            },        
            {
                path: '/warehouse/official-bussiness',
                component: officialBussiness,
                name: 'warehouse-officialBussiness',
            },        
            {
                path: '/warehouse/vacation-leave',
                component: vacationLeave,
                name: 'warehouse-vacationLeave',
            },        
            {
                path: '/warehouse/sick-leave',
                component: sickLeave,
                name: 'warehouse-sickLeave',
            },        
            {
                path: '/warehouse/other-leave',
                component: otherLeave,
                name: 'warehouse-otherLeave',
            },        
            {
                path: '/warehouse/loans',
                component: loans,
                name: 'warehouse-loans',
            },        
            {
                path: '/warehouse/payslip',
                component: payslip,
                name: 'warehouse-payslip',
            },             
        ]
    }


];