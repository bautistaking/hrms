@component('mail::message')

@component('mail::panel')
# Outbound Details :
@component('mail::table')
	|                     																	|    
	| -------------------------------------------------------------------------------------	|
	| Request Number:  			| {{ $request_number }}           								
	| Requestor:  				| {{ $requestor }}           								
	| Job Order / PO Number: 		| {{ $po_number }}            							
	| Supplier Name:    			| {{ $supplier_name }}         									
	| remarks:    				| {{ $remarks }}         									
	| status:    				| {{ $status_name }}         									
	| Total Price:    			| {{ $total_price }}         									
	| Particulars:    					       									
		| Item name    			| Qty 					       									
	@foreach($catalog_details as $index => $particular)
	| {{ $particular->item_name }}	| {{ $particular->quantity }}								
	@endforeach
@endcomponent
@endcomponent