@component('mail::message')

@component('mail::panel')
# Employee Request :
@component('mail::table')
	|                     																	|    
	| -------------------------------------------------------------------------------------	|
	| Date:  			| {{ $date }}           								
	| Number of hours: 		| {{ $number_of_hours }}            							
	| Description:    		| {{ $description }}         									
@endcomponent
@endcomponent