@component('mail::message')

@component('mail::panel')
# Non-Catalog Request Details :
@component('mail::table')
	|                     																	|    
	| -------------------------------------------------------------------------------------	|
	| PAY TO:  					| {{ $pay_to }}           								
	| PROJECT / SITE NAME: 				| {{ $site_name }}            							
	| DATE REQUESTED:    				| {{ $date_requested }}         									
	| DATE NEEDED:    				| {{ $date_needed }}
	| PF NUMBER:    				| {{ $pf_number }}
	| NAME OF REQUESTOR:	   			| {{ $requestor }}
	| PARTICULARS:    				| {{ $particulars }}
	| AMOUNT:    					| {{ $amount }}
	| STATUS:    					| {{ $status }}

@endcomponent
@endcomponent