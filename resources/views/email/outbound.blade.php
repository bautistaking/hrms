@component('mail::message')

@component('mail::panel')
# Outbound Details :
@component('mail::table')
	|                     																	|    
	| -------------------------------------------------------------------------------------	|
	| Request Number:  			| {{ $request_number }}           								
	| Client Name:  			| {{ $client_name }}           								
	| Job Order / PO Number: 		| {{ $po_number }}            							
	| Site Name:    			| {{ $site_name }}         									
	| Required Date:    			| {{ $required_date }}         									
	| Pull Out From:    			| {{ $pull_out_from }}         									
	| Deliver To:    			| {{ $deliver_to }}         									
	| remarks:    				| {{ $remarks }}         									
	| status:    				| {{ $status }}         									
	| Particulars:    					       									
		| Item name    			| Qty 					       									
	@foreach($outbound_details as $index => $particular)
	{{ $particular->item_details->item_name }}	| {{ $particular->quantity }}								
	@endforeach
@endcomponent
@endcomponent