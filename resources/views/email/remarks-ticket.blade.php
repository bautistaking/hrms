@component('mail::message')

@component('mail::panel')
# Assign Ticket Details :
@component('mail::table')
	|                     																	|    
	| -------------------------------------------------------------------------------------	|
	| Client Name:  			| {{ $client_name }}           								
	| Job Order / PO Number: 		| {{ $job_order }}            							
	| Site Name:    			| {{ $site_name }}         									
	| Ticket:    				| {{ $ticket }}
	| Remarks:    				| {{ $remarks }}

@endcomponent
@endcomponent