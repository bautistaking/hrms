@component('mail::message')

@component('mail::panel')
# Hi Admin, <br>

@component('mail::panel')
# Ticket Details :
@component('mail::table')
	|                     																	|    
	| -------------------------------------------------------------------------------------	|
	| Ticket Number:  			| {{ $ticket_number }}										|
	| Ticket:    				| {{ $ticket_name }}       									|
	| Status:    				| {{ $status }}       										|
	| Client Name:  			| {{ $client_name }}           								|
	| Job Order / PO Number: 		| {{ $job_order }}            								|
	| Site Name:    			| {{ $site_name }}         									|
	| Team Name:    			| {{ $team_name }}         									|
	| Remarks:	    			| {{ $remarks }}         									|
@endcomponent

@endcomponent