@component('mail::message')

@component('mail::panel')
# Hi {{ $name }}, <br>

We received your order **#{{ $reference_number }}** on {{ $date_placed }} and you’ll be paying for this via **{{ $payment_type }}**. We’re getting your order ready and will let you know once it’s on the way. We wish you enjoy shopping with us and hope to see you again real soon!
@endcomponent

@component('mail::panel')
# Delivery Details :
@component('mail::table')
	|                     |    
	| ------------------- |
	| Name:     | {{ $name }}     |
	| Address:              | {{ $address }}               |
	| Phone:              |  {{ $phone }}               |
	| Email:              | {{ $email }}             |
@endcomponent

@endcomponent

@component('mail::panel')
# Order List :

@component('mail::table')
	| Quantity                 | Product Name                 | Brand Name                   | Amount                 |
	| :-------------: |:----------------------------:|:----------------------------:|-----------------------:|
	@foreach($orders as $order)
	| x{{ $order["quantity"] }} | {{ $order["product_name"] }} |    | ₱ {{ $order["amount"] }} |
	@endforeach	
@endcomponent
@component('mail::table')
	|                     |                              |                        		|						 |
	| ------------------- |:----------------------------:|:----------------------------:| ----------------------:|
	| Delivery Charge     |                  			 |      						| ₱ {{ $delivery_charge }} |
	| Total               |                  			 |      						|	   **₱ {{ $total }}**      |
@endcomponent
@component('mail::table')
	|                     |                              |                        		|						 |
	| ------------------- |:----------------------------:|:----------------------------:| ----------------------:|
	| Shipping Option:     |                  			 |      						| Standard |
	| Paid By:            |                  			 |      						|	   {{ $payment_type }}    |
@endcomponent

@endcomponent
