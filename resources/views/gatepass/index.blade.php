<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<body>
	<div class="container">
		<br/><br/>
		<table style="width: 100%;" class="col-border">		
			<tr>
				<td class="text-center" colspan="4"><img src="{{ asset('img/fabriquem-logo-small.png') }}"></td>
			</tr>
			<tr>
				<td class="text-center" colspan="4"><span style="font-size:9px;">Linking families & communities<br/>to the world of possibilities</span></td>
			</tr>
			<tr>
				<td class="text-center" colspan="4"><h2> PULL-OUT FORM </h2></td>
			</tr>
			<tr>
				<td style="width: 25%;" class="text-right col-label">DATE REQUESTED:</td>
				<td style="width: 25%;" class="text-center col-border-bottom"><strong>{{ $outbound->created_date_formated }}</strong></td>
				<td style="width: 25%;" class="text-right col-label">PO NUMBER:</td>
				<td style="width: 25%;" class="text-center col-border-bottom"><strong>{{ $outbound->job_order_details->po_number }}</strong></td>
			</tr>
			<tr>
				<td style="width: 25%;" class="text-right col-label">DATE NEEDED:</td>
				<td style="width: 25%;" class="text-center col-border-bottom"><strong>{{ $outbound->required_date_formated }}</strong></td>
				<td style="width: 25%;" class="text-right col-label">SITE NAME:</td>
				<td style="width: 25%;" class="text-center col-border-bottom"><strong>{{ $outbound->site_details->site_name }}</strong></td>
			</tr>
			<tr>
				<td style="width: 25%;" class="text-right col-label">DELIVER FROM:</td>
				<td style="width: 25%;" class="text-center col-border-bottom"><strong>STA. ANA WAREHOUSE</strong></td>
				<td style="width: 25%;" class="text-right col-label">PULL OUT NUMBER:</td>
				<td style="width: 25%;" class="text-center col-border-bottom"><strong>{{ $outbound->request_number }}</strong></td>
			</tr>
			<tr>
				<td style="width: 25%;" class="text-right col-label">ACTIVITY:</td>
				<td style="width: 25%;" class="text-center col-border-bottom"></td>
				<td style="width: 25%;" class="text-right col-label">DEPARTMENT:</td>
				<td style="width: 25%;" class="text-center col-border-bottom"><strong>{{ $outbound->employee_department }}</strong></td>
			</tr>
			<tr>
				<td class="text-center" colspan="4"></td>
			</tr>
		</table>
		<hr>
		<table style="width: 100%;">		
			<tr>
				<td class="col-border text-center"><strong> ITEM # </strong></td>
				<td class="col-border text-center"><strong> DESCRIPTION </strong></td>
				<td class="col-border text-center"><strong> CATEGORY </strong></td>
				<td class="col-border text-center"><strong> UOM </strong></td>
				<td class="col-border text-center"><strong> QUANTITY </strong></td>
				<td class="col-border text-center"><strong> REMARKS </strong></td>
			</tr>
			@php ($i = 1)
			@foreach ($outbound->outbound_details as $item)
			<tr>
				<td class="col-border text-center">{{ $i }}</td>
				<td class="col-border text-center">{{ $item->item_details['item_name'] }}</td>
				<td class="col-border text-center">{{ $item->item_details['category_name'] }}</td>
				<td class="col-border text-center">{{ $item->uom }}</td>
				<td class="col-border text-center">{{ $item->quantity }}</td>
				<td class="col-border text-center"></td>
			</tr>
			@php ($i++)
			@endforeach
			<tr>
				<td colspan="6" class="col-side-border text-left"><strong>REMARKS</strong></td>
			</tr>
			<tr>
				<td colspan="6" class="col-side-border text-left"></td>
			</tr>
			<tr>
				<td colspan="6" class="col-side-border text-left"></td>
			</tr>
			<tr>
				<td colspan="6" class="col-border-wo-top text-left"></td>
			</tr>
		</table>
		<hr>
		<table style="width: 100%;">		
			<tr>
				<td style="width: 25%;" class="col-side-border text-left">REQUESTED BY</td>
				<td class="text-left" colspan="3">REVIEW BY</td>
				<td style="width: 25%;" class="col-side-border text-left">APPROVED BY</td>
			</tr>
			<tr>
				<td style="width: 25%;" class="col-side-border text-left"><img src="{{ $outbound->employee_signature }}" width="100%"></td>
				<td style="width: 15%;" class="text-left"><img src="{{ $outbound->approver_signature }}" width="100%"></td>
				<td style="width: 15%;" class="text-left"><img src="{{ asset('img/ASUNCION_ABIGAIL_C.png') }}" width="100%"></td>
				<td style="width: 15%;" class="text-left"></td>
				<td style="width: 25%;" class="col-side-border text-left"><img src="{{ asset('img/20230912_215413.jpg') }}" width="50%"></td>
			</tr>
			<tr>
				<td style="width: 25%;" class="col-side-border text-left">{{ $outbound->employee_name }}</td>
				<td class="text-left">{{ $outbound->approver_name }}</td>
				<td class="text-left"></td>
				<td class="text-left"></td>
				<!-- <td style="width: 25%;" class="col-side-border text-left">MACALINAO, BENNETH C.</td> -->
				<td style="width: 25%;" class="col-side-border text-left"></td>
			</tr>
			<tr>
				<td style="width: 25%;" class="col-side-border text-left">NAME & SIGNATURE</td>
				<td class="text-left">IMMEDIATE MANAGER</td>
				<td class="text-left">P&L DIVISION</td>
				<td class="text-left">ACCOUNTS DIVISION</td>
				<td style="width: 25%;" class="col-side-border text-left">FINANCE DIVISION</td>
			</tr>
			<tr>
				<td style="width: 25%;" class="col-side-border col-border-bottom text-left">DATE:</td>
				<td class="col-border-bottom text-left">DATE:</td>
				<td class="col-border-bottom text-left">DATE:</td>
				<td class="col-border-bottom text-left">DATE:</td>
				<td style="width: 25%;" class="col-side-border col-border-bottom text-left">DATE:</td>
			</tr>
		</table>
		<hr>
		<table style="width: 100%;">		
			<tr>
				<td colspan="6" class="col-side-border text-left"><strong style="color:red;">TO BE FILLED IN BY PROCUREMENT<br/>AND LOGISTICS ONLY:</strong></td>
			</tr>
			<tr>
				<td style="width: 25%;" class="col-left-border text-left"><strong>GATE PASS NUMBER:</strong></td>
				<td style="width: 25%;" class="col-border-bottom text-left"><strong>{{ $outbound->request_number }}</strong></td>
				<td colspan="4" class="col-right-border"></td>
			</tr>
			<tr>
				<td class="col-left-border text-left"><strong>RELEASED DATE:</strong></td>
				<td class="col-border-bottom text-left"></td>
				<td colspan="4" class="col-right-border"></td>
			</tr>
			<tr>
				<td colspan="6" class="col-side-border text-left"></td>
			</tr>
		</table>
		<hr>
		<table style="width: 100%;">		
			<tr>
				<td style="width: 25%;" class="col-left-border text-left">RELEASED BY:</td>
				<td style="width: 25%;"></td>
				<td style="width: 25%;" class="text-left">CHECKED BY:</td>
				<td style="width: 25%;" class="col-right-border"></td>
			</tr>
			<tr>
				<td style="width: 25%;" class="col-left-border text-left">NAME & SIGNATURE</td>
				<td style="width: 25%;">DATE</td>
				<td style="width: 25%;" class="text-left">NAME & SIGNATURE</td>
				<td style="width: 25%;" class="col-right-border">DATE</td>
			</tr>
			<tr>
				<td colspan="4" class="col-border-wo-top"></td>
			</tr>
		</table>
		<br/><br/>
		
		<!-- <table style="width: 100%;">		
			<tr>
				<td class="text-center"><h2> GATEPASS </h2></td>
			</tr>
		</table>		
		<table style="width: 100%;">
			<tr>
				<td rowspan="3" width="25%" class="text-left"><img src="{{ asset('img/fabriquem-logo-small.png') }}"></td>
				<td class="col-label">ORF #: <strong>{{ $outbound->request_number }} </strong></td>
				<td class="text-left">&nbsp;</td> 
				<td class="col-label" style="white-space: nowrap;">DATE ISSUED: <strong>{{ $outbound->required_date }}</strong></td>
				<td class="text-left"></td>
			</tr>		
			<tr>
				<td class="col-label">ORF TITLE:</td>
				<td class="text-left"></td> 
				<td class="text-left"></td> 
				<td class="text-left"></td> 
			</tr>			
			<tr>
				<td class="col-label" style="white-space: nowrap;">IN-HOUSE/SUBCONTRACTOR:</td>
				<td class="text-left"></td> 
				<td class="text-left"></td> 
				<td class="text-left"></td> 
			</tr>			
		</table>
		<table style="width: 100%;">		
			<tr>
				<td class="text-left"><strong>AIRSPEED WAREHOUSE ADDRESS</strong> : DUTY FREE WAREHOUSE CBW 84, KAINGIN ROAD, PARANAQUE CITY</td>
			</tr>
			<tr>
				<td class="text-left"><strong>STA. ANA WAREHOUSE ADDRESS</strong> : 346 STA. CLARA STREET CORNER SYQUIA STREET, SANTA ANA, MANILA 1009 METRO MANILA</td>
			</tr>
		</table>
		<table style="width: 100%;">		
			<tr>
				<td class="col-border text-center"><strong> ITEM # </strong></td>
				<td class="col-border text-center"><strong> ITEM DESCRIPTION </strong></td>
				<td class="col-border text-center"><strong> STORE ROOM </strong></td>
				<td class="col-border text-center"><strong> QUANTITY </strong></td>
				<td class="col-border text-center"><strong> REMARKS </strong></td>
			</tr>
			@php ($i = 1)
			@foreach ($outbound->outbound_details as $item)
			<tr>
				<td class="col-border text-center">{{ $i }}</td>
				<td class="col-border text-center">{{ $item->item_details['item_name'] }}</td>
				<td class="col-border text-center"></td>
				<td class="col-border text-center">{{ $item->quantity }}</td>
				<td class="col-border text-center"></td>
			</tr>
			@php ($i++)
			@endforeach
		</table>
		<hr>
		<table style="width: 100%;">
			<tr>
				<td class="col-border text-left">Checked/Approved by:</td>
				<td class="col-border text-left">Delivered by:</td>
				<td class="col-border text-left">Received by:</td>
			</tr>			
			<tr>
				<td class="col-side-border" width="33%"><img src="{{ asset('img/MACALINAO_BENNETH.jpg') }}" width="50%"></td>
				<td class="col-side-border">&nbsp;</td>
				<td class="col-side-border">&nbsp;</td>
			</tr>					
			<tr>
				<td class="col-border-wo-top">&nbsp;</td>
				<td class="col-border-wo-top">&nbsp;</td>
				<td class="col-border-wo-top">&nbsp;</td>
			</tr>
			<tr>
				<td class="col-border">MACALINAO, BENNETH C.</td>
				<td class="col-side-border">&nbsp;</td>
				<td class="col-side-border">&nbsp;</td>
			</tr>					
			<tr>
				<td class="col-border">Corporate Logistics Head</td>
				<td class="col-border">PLATE#:</td>
				<td class="col-border"></td>
			</tr>				
		</table>
		<br/><br/>
		<table style="width: 100%;">		
			<tr>
				<td class="text-center"><h2> GATEPASS </h2></td>
			</tr>
		</table>
		<table style="width: 100%;">
			<tr>
				<td rowspan="3" width="25%" class="text-left"><img src="{{ asset('img/fabriquem-logo-small.png') }}"></td>
				<td class="col-label">ORF #: <strong>{{ $outbound->request_number }} </strong></td>
				<td class="text-left">&nbsp;</td> 
				<td class="col-label" style="white-space: nowrap;">DATE ISSUED: <strong>{{ $outbound->required_date }}</strong></td>
				<td class="text-left"></td>
			</tr>		
			<tr>
				<td class="col-label">ORF TITLE:</td>
				<td class="text-left"></td> 
				<td class="text-left"></td> 
				<td class="text-left"></td> 
			</tr>			
			<tr>
				<td class="col-label" style="white-space: nowrap;">IN-HOUSE/SUBCONTRACTOR:</td>
				<td class="text-left"></td> 
				<td class="text-left"></td> 
				<td class="text-left"></td> 
			</tr>			
		</table>
		<table style="width: 100%;">		
			<tr>
				<td class="text-left"><strong>AIRSPEED WAREHOUSE ADDRESS</strong> : DUTY FREE WAREHOUSE CBW 84, KAINGIN ROAD, PARANAQUE CITY</td>
			</tr>
			<tr>
				<td class="text-left"><strong>STA. ANA WAREHOUSE ADDRESS</strong> : 346 STA. CLARA STREET CORNER SYQUIA STREET, SANTA ANA, MANILA 1009 METRO MANILA</td>
			</tr>
		</table>
		<table style="width: 100%;">		
			<tr>
				<td class="col-border text-center"><strong> ITEM # </strong></td>
				<td class="col-border text-center"><strong> DESCRIPTION </strong></td>
				<td class="col-border text-center"><strong> STORE ROOM </strong></td>
				<td class="col-border text-center"><strong> QUANTITY </strong></td>
				<td class="col-border text-center"><strong> REMARKS </strong></td>
			</tr>
			@php ($i = 1)
			@foreach ($outbound->outbound_details as $item)
			<tr>
				<td class="col-border text-center">{{ $i }}</td>
				<td class="col-border text-center">{{ $item->item_details['item_name'] }}</td>
				<td class="col-border text-center"></td>
				<td class="col-border text-center">{{ $item->quantity }}</td>
				<td class="col-border text-center"></td>
			</tr>
			@php ($i++)
			@endforeach
		</table>
		<hr>
		<table style="width: 100%;">
			<tr>
				<td class="col-border text-left">Checked/Approved by:</td>
				<td class="col-border text-left">Delivered by:</td>
				<td class="col-border text-left">Received by:</td>
			</tr>			
			<tr>
				<td class="col-side-border" width="33%"><img src="{{ asset('img/MACALINAO_BENNETH.jpg') }}" width="50%"></td>
				<td class="col-side-border">&nbsp;</td>
				<td class="col-side-border">&nbsp;</td>
			</tr>					
			<tr>
				<td class="col-border-wo-top">&nbsp;</td>
				<td class="col-border-wo-top">&nbsp;</td>
				<td class="col-border-wo-top">&nbsp;</td>
			</tr>
			<tr>
				<td class="col-border">MACALINAO, BENNETH C.</td>
				<td class="col-side-border">&nbsp;</td>
				<td class="col-side-border">&nbsp;</td>
			</tr>					
			<tr>
				<td class="col-border">Corporate Logistics Head</td>
				<td class="col-border">PLATE#:</td>
				<td class="col-border"></td>
			</tr>				
		</table> -->
	</div>
</body>

<style>
    table th{
        font-size: 14px;
        font-weight: bold;
        text-align:center;
    }
    table td{
        font-size: 16px;
        padding: 5px;
    }

    table td p{
        margin: 0;
        padding: 0;
    }

    .col-label {
    	width: 15%;
    	font-size: 16px;
    	font-weight: 700;
    }

    .col-content {
    	width: 35%;
    	font-size: 18px;
    	font-weight: 500;
    	padding: 16px 5px;
    }

    .col-border {
    	border: solid 1px; 
    }

	.col-left-border {
    	border-left: solid 1px;
    }

	.col-right-border {
    	border-right: solid 1px;
    }

    .col-side-border {
    	border-left: solid 1px;
    	border-right: solid 1px;
    }

	.col-border-bottom {
    	border-bottom: solid 2px;
    }

    .col-border-wo-top {
    	border-left: solid 1px;
    	border-right: solid 1px;
    	border-bottom: solid 1px;
    }

    .col-even-width {
    	width: 33.33%;
    }

    .col-border-right {
    	border-right: solid 1px;
    }

    hr {
    	border:solid 5px;
    	width: 99.1%;
    	margin: 0;
    }

    h4 {
    	color: red;
    }

    input[type=checkbox] {
		-ms-transform: scale(1.5); /* IE */
		-moz-transform: scale(1.5); /* FF */
		-webkit-transform: scale(1.5); /* Safari and Chrome */
		-o-transform: scale(1.5); /* Opera */
		transform: scale(1.5);	
		margin-left: 5px;
    }
</style>