<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<body>
	<div class="container">
		<table style="width: 100%;">
	        <tr>
	          <td width="25%"><img src="{{ asset('img/fabriquem-logo-small.png') }}"></td>
	          <td style="text-align: center;"><h1>PAYMENT FORM</h1></td>
	          <td width="25%" style="text-align: center; color: red;"><strong>Note: <br/>PLEASE PRINT IN A LONG BOND PAPER</strong></td>
	        </tr>
	      </table>
	      <table style="width: 100%;">
	        <tr>
	          <td class="col-border">PAY TO</td>
	          <td class="col-border col-label text-center" style="width: 25%"><strong>{{ $request_details->pay_to }}</strong></td> 
	          <td class="col-border">DATE REQUESTED</td>
	          <td class="col-border col-label text-center" style="width: 15%"><strong>{{ $request_details->updated_at }}</strong></td> 
	          <td class="col-border">PF NUMBER:</td>
	          <td class="col-border col-label text-center" style="width: 15%"><strong><span style="color:red; font-size: 1.5rem;">{{ $request_details->request_number }}</span></strong></td> 
	        </tr> 
	        <tr>
	          <td class="col-border">PROJECT / SITE NAME:</td>
	          <td class="col-border col-label text-center">{{ $request_details->client_name }} / {{ $request_details->site_name }}</td> 
	          <td class="col-border">DATE NEEDED:</td>
	          <td class="col-border col-label text-center">{{ $request_details->required_date }}</td> 
	          <td class="col-border">NAME OF REQUESTOR:</td>
	          <td class="col-border col-label text-center">{{ $request_details->requestor_name }}</td> 
	        </tr>
	      </table>
	      <table style="width: 100%;">
	        <tr style="text-align: center;">
	          <td class="col-border">AVAILABLE BALANCE</td>
	          <td class="col-border">PARTICULARS</td>
	          <td class="col-border">AMOUNT</td>
	        </tr>
	        <tr style="text-align: center; height: 2rem;">
	          <td class="col-label col-border" rowspan="7"><strong><span style="color:red; font-size: 1.5rem;">{{ $request_details->site_details->balance_amount_formated }}</span></strong></td>
	          <td class="col-label col-border"> {{ $request_details->request_name }} </td>
	          <td class="col-label col-border text-right">PHP {{ $request_details->amount_formated }}</td>
	        </tr>
	        <tr style="text-align: center; height: 2rem;">
	          <td class="col-label col-border"> {{ $request_details->remarks }} 
	          @if ($request_details->po_number)
	          	<br/> FOR P.O. # {{ $request_details->po_number }}
	          @endif
	          </td>
	          <td class="col-label col-border"></td>
	        </tr>
	        <tr style="text-align: center; height: 2rem;">
	          <td class="col-label col-border"></td>
	          <td class="col-label col-border"></td>
	        </tr>
	        <tr style="text-align: center; height: 2rem;">
	          <td class="col-label col-border"></td>
	          <td class="col-label col-border"></td>
	        </tr>
	        <tr style="text-align: center; height: 2rem;">
	          <td class="col-label col-border"></td>
	          <td class="col-label col-border"></td>
	        </tr>
	        <tr style="text-align: center; height: 2rem;">
	          <td class="col-label col-border"></td>
	          <td class="col-label col-border"></td>
	        </tr>
	        <tr style="height: 2rem;">
	          <td class="col-label col-border" style="text-align: right;">NET VAT</td>
	          <td class="col-label col-border text-right">PHP {{ $request_details->net_amount }}</td>
	        </tr>
	        <tr style="text-align: left; height: 2rem;">
	          <td class="col-label col-border"><span style="color: red;">{{ $request_details->payment_type }}</span></td>
	          <td class="col-label col-border" style="text-align: right;">VAT INPUT</td>
	          <td class="col-label col-border text-right">PHP {{ $request_details->vat_amount }}</td>
	        </tr>
	        <tr style="text-align: left; height: 2rem;">
	          <td class="col-label col-border"><span style="color: red;">{{ $request_details->release_type }}</span></td>
	          <td class="col-label col-border" style="text-align: right;"><span style="color: red;">LESS EWT {{ $request_details->ewt_percent }}</span></td>
	          <td class="col-label col-border text-right"><span style="color: red;">PHP {{ $request_details->ewt_amount }}</span></td>
	        </tr>
	        <tr style="text-align: left; height: 2rem;">
	          <td class="col-label col-border"></td>
	          <td class="col-label col-border" style="text-align: right;">TOTAL AMOUNT:</td>
	          <td class="col-label col-border text-right">PHP {{ $request_details->total_amount_due }}</td>
	        </tr>
	      </table>
	      <table style="width: 100%;">
          <tr>
            <td style="width: 25%;" class="col-side-border text-left">VERIFIED BY:</td>
            <td style="width: 25%;" class="col-side-border text-left">VALIDATED BY:</td>
            <td style="width: 25%;" class="col-side-border text-left">ENDORSED BY:</td>
            <td style="width: 25%;" class="col-side-border text-left">APPROVED BY:</td>
          </tr>
	        <tr style="text-align: left; height: 2rem;">
	          <td class="col-side-border col-label" style="width: 25%; vertical-align:bottom; padding-bottom: 15px;">
            <img src="{{ $request_details->approver_signature }}" width="50%"><br>
            {{ $request_details->approver_name }}</td>
	          <td class="col-side-border col-label" style="width: 25%; vertical-align:bottom; padding-bottom: 15px;">
	          	<img src="{{ asset('img/ORTEGA_RUTH.jpg') }}" width="50%"><br>
	          	<strong>ORTEGA, RUTH A.</strong>
	      	  </td>
	          <td class="col-side-border col-label" style="width: 25%; vertical-align:bottom; padding-bottom: 15px;">
	          	<img src="{{ asset('img/abelardo_ednacot.jpg') }}" width="50%"><br>
	          	<strong>EDNACOT, ABELARDO V.</strong>
	          </td>
	          <td class="col-side-border col-label" style="width: 25%; vertical-align:bottom; padding-bottom: 15px;">
	          	<img src="{{ asset('img/MACALINAO_BENNETH.jpg') }}" width="50%"><br>
	            <strong>MACALINAO, BENNETH C.</strong> 
	          </td>
	        </tr>
	      </table>
	      <table style="width: 100%;">
            <tr style="text-align: center; height: 2rem;">
              <td class="col-label col-border"><span>CASH</span></td>
              <td class="col-label col-border"><span>CHEQUE</span></td>
              <td class="col-label col-border" colspan="2"><span>BANK</span></td>
            </tr>
            <tr style="text-align: center; height: 2rem;">
              <td class="col-label col-border">
                <table style="width: 100%;">
                  <tr>
                    <td>Release By:</td>
                    <td>____________</td>
                    <td>RF #</td>
                    <td>____________</td>
                  </tr>
                  <tr>
                    <td>Receive By:</td>
                    <td>____________</td>
                    <td>Date:</td>
                    <td>____________</td>
                  </tr>
                </table>
              </td>
              <td class="col-label col-border">
                <table style="width: 100%;">
                  <tr>
                    <td>CV #</td>
                    <td>____________</td>
                    <td>Check #</td>
                    <td>____________</td>
                  </tr>
                  <tr>
                    <td>Date:</td>
                    <td>____________</td>
                    <td>Date:</td>
                    <td>____________</td>
                  </tr>
                </table>
              </td>
              <td class="col-label col-border" colspan="2">
                <table style="width: 100%;">
                  <tr>
                    <td>MBTC</td>
                    <td class="text-center">___________________________</td>
                  </tr>
                  <tr>
                    <td>SB</td>
                    <td class="text-center">___________________________</td>
                  </tr>
                  <tr>
                    <td>PNB</td>
                    <td class="text-center">___________________________</td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr style="text-align: center; height: 2rem;">
              <td class="col-label col-border"><span>BANK TRANSFER</span></td>
              <td class="col-label col-border"><span>CREDIT CARD</span></td>
              <td class="col-label col-border"><span>PF NUMBER</span></td>
            </tr>
            <tr style="text-align: center; height: 2rem;">
              <td class="col-label col-border">
                <table style="width: 100%;">
                  <tr>
                    <td>Reference #:</td>
                    <td>____________</td>
                  </tr>
                  <tr>
                    <td>Date :</td>
                    <td>____________</td>
                  </tr>
                </table>
              </td>
              <td class="col-label col-border">
                <table style="width: 100%;">
                  <tr>
                    <td>
                      <table style="width: 100%;">
                        <tr>
                          <td>Reference #:</td>
                          <td>____________</td>
                        </tr>
                        <tr>
                          <td>Date :</td>
                          <td>____________</td>
                        </tr>
                      </table>
                    </td>
                    <td>
                      <table style="width: 100%;">
                        <tr>
                          <td>CL</td>
                          <td>____________</td>
                        </tr>
                        <tr>
                          <td>ADMIN</td>
                          <td>____________</td>
                        </tr>
                        <tr>
                          <td>CE</td>
                          <td>____________</td>
                        </tr>
                        <tr>
                          <td>CEO</td>
                          <td>____________</td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
              <td class="col-label col-border"><strong><span style="color:red; font-size: 1.5rem;">{{ $request_details->request_number }}</span></strong></td>
            </tr>
          </table>
          <table>
          	<tr>
          		<td class="col-label col-border">The requestor acknowledges that the company has the rights to deduct the amount provided above in his/her salary upon failure to liquidate in a span of 15 days</td>
          	</tr>
          	<tr>
          		<td class="col-label col-border"></td>
          	</tr>
          </table>

          <table style="width: 100%;">
	        <tr>
	          <td width="25%"><img src="{{ asset('img/fabriquem-logo-small.png') }}"></td>
	          <td style="text-align: center;"><h1>PAYMENT FORM</h1></td>
	          <td width="25%" style="text-align: center; color: red;"><strong>DUPLICATE</strong></td>
	        </tr>
	      </table>
	      <table style="width: 100%;">
	        <tr>
	          <td class="col-border">PAY TO</td>
	          <td class="col-border col-label text-center" style="width: 25%"><strong>{{ $request_details->pay_to }}</strong></td> 
	          <td class="col-border">DATE REQUESTED</td>
	          <td class="col-border col-label text-center" style="width: 15%"><strong>{{ $request_details->updated_at }}</strong></td> 
	          <td class="col-border">PF NUMBER:</td>
	          <td class="col-border col-label text-center" style="width: 15%"><strong><span style="color:red; font-size: 1.5rem;">{{ $request_details->request_number }}</span></strong></td> 
	        </tr> 
	        <tr>
	          <td class="col-border">PROJECT / SITE NAME:</td>
	          <td class="col-border col-label text-center">{{ $request_details->client_name }} / {{ $request_details->site_name }}</td> 
	          <td class="col-border">DATE NEEDED:</td>
	          <td class="col-border col-label text-center">{{ $request_details->required_date }}</td> 
	          <td class="col-border">NAME OF REQUESTOR:</td>
	          <td class="col-border col-label text-center">{{ $request_details->requestor_name }}</td> 
	        </tr>
	      </table>
	      <table style="width: 100%;">
	        <tr style="text-align: center;">
	          <td class="col-border">AVAILABLE BALANCE</td>
	          <td class="col-border">PARTICULARS</td>
	          <td class="col-border">AMOUNT</td>
	        </tr>
	        <tr style="text-align: center; height: 2rem;">
	          <td class="col-label col-border" rowspan="7"><strong><span style="color:red; font-size: 1.5rem;">{{ $request_details->site_details->balance_amount_formated }}</span></strong></td>
	          <td class="col-label col-border"> {{ $request_details->request_name }} </td>
	          <td class="col-label col-border text-right">PHP {{ $request_details->amount_formated }}</td>
	        </tr>
	        <tr style="text-align: center; height: 2rem;">
	          <td class="col-label col-border"> {{ $request_details->remarks }} 
	          @if ($request_details->po_number)
	          	<br/> FOR P.O. # {{ $request_details->po_number }}
	          @endif
	          </td>
	          <td class="col-label col-border"></td>
	        </tr>
	        <tr style="text-align: center; height: 2rem;">
	          <td class="col-label col-border"></td>
	          <td class="col-label col-border"></td>
	        </tr>
	        <tr style="text-align: center; height: 2rem;">
	          <td class="col-label col-border"></td>
	          <td class="col-label col-border"></td>
	        </tr>
	        <tr style="text-align: center; height: 2rem;">
	          <td class="col-label col-border"></td>
	          <td class="col-label col-border"></td>
	        </tr>
	        <tr style="text-align: center; height: 2rem;">
	          <td class="col-label col-border"></td>
	          <td class="col-label col-border"></td>
	        </tr>
	        <tr style="height: 2rem;">
	          <td class="col-label col-border" style="text-align: right;">NET VAT</td>
	          <td class="col-label col-border text-right">PHP {{ $request_details->net_amount }}</td>
	        </tr>
	        <tr style="text-align: left; height: 2rem;">
	          <td class="col-label col-border"><span style="color: red;">{{ $request_details->payment_type }}</span></td>
	          <td class="col-label col-border" style="text-align: right;">VAT INPUT</td>
	          <td class="col-label col-border text-right">PHP {{ $request_details->vat_amount }}</td>
	        </tr>
	        <tr style="text-align: left; height: 2rem;">
	          <td class="col-label col-border"><span style="color: red;">{{ $request_details->release_type }}</span></td>
	          <td class="col-label col-border" style="text-align: right;"><span style="color: red;">LESS EWT {{ $request_details->ewt_percent }}</span></td>
	          <td class="col-label col-border text-right"><span style="color: red;">PHP {{ $request_details->ewt_amount }}</span></td>
	        </tr>
	        <tr style="text-align: left; height: 2rem;">
	          <td class="col-label col-border"></td>
	          <td class="col-label col-border" style="text-align: right;">TOTAL AMOUNT:</td>
	          <td class="col-label col-border text-right">PHP {{ $request_details->total_amount_due }}</td>
	        </tr>
	      </table>
	      <table style="width: 100%;">
          <tr>
            <td style="width: 25%;" class="col-side-border text-left">VERIFIED BY:</td>
            <td style="width: 25%;" class="col-side-border text-left">VALIDATED BY:</td>
            <td style="width: 25%;" class="col-side-border text-left">ENDORSED BY:</td>
            <td style="width: 25%;" class="col-side-border text-left">APPROVED BY:</td>
          </tr>
	        <tr style="text-align: left; height: 2rem;">
	          <td class="col-side-border col-label" style="width: 25%; vertical-align:bottom; padding-bottom: 15px;">
            <img src="{{ $request_details->approver_signature }}" width="50%"><br>
            {{ $request_details->approver_name }}</td>
	          <td class="col-side-border col-label" style="width: 25%; vertical-align:bottom; padding-bottom: 15px;">
	          	<img src="{{ asset('img/ORTEGA_RUTH.jpg') }}" width="50%"><br>
	          	<strong>ORTEGA, RUTH A.</strong>
	      	  </td>
	          <td class="col-side-border col-label" style="width: 25%; vertical-align:bottom; padding-bottom: 15px;">
	          	<img src="{{ asset('img/abelardo_ednacot.jpg') }}" width="50%"><br>
	          	<strong>EDNACOT, ABELARDO V.</strong>
	          </td>
	          <td class="col-side-border col-label" style="width: 25%; vertical-align:bottom; padding-bottom: 15px;">
	          	<img src="{{ asset('img/MACALINAO_BENNETH.jpg') }}" width="50%"><br>
	            <strong>MACALINAO, BENNETH C.</strong> 
	          </td>
	        </tr>
	      </table>
	      <table style="width: 100%;">
            <tr style="text-align: center; height: 2rem;">
              <td class="col-label col-border"><span>CASH</span></td>
              <td class="col-label col-border"><span>CHEQUE</span></td>
              <td class="col-label col-border" colspan="2"><span>BANK</span></td>
            </tr>
            <tr style="text-align: center; height: 2rem;">
              <td class="col-label col-border">
                <table style="width: 100%;">
                  <tr>
                    <td>Release By:</td>
                    <td>____________</td>
                    <td>RF #</td>
                    <td>____________</td>
                  </tr>
                  <tr>
                    <td>Receive By:</td>
                    <td>____________</td>
                    <td>Date:</td>
                    <td>____________</td>
                  </tr>
                </table>
              </td>
              <td class="col-label col-border">
                <table style="width: 100%;">
                  <tr>
                    <td>CV #</td>
                    <td>____________</td>
                    <td>Check #</td>
                    <td>____________</td>
                  </tr>
                  <tr>
                    <td>Date:</td>
                    <td>____________</td>
                    <td>Date:</td>
                    <td>____________</td>
                  </tr>
                </table>
              </td>
              <td class="col-label col-border" colspan="2">
                <table style="width: 100%;">
                  <tr>
                    <td>MBTC</td>
                    <td class="text-center">___________________________</td>
                  </tr>
                  <tr>
                    <td>SB</td>
                    <td class="text-center">___________________________</td>
                  </tr>
                  <tr>
                    <td>PNB</td>
                    <td class="text-center">___________________________</td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr style="text-align: center; height: 2rem;">
              <td class="col-label col-border"><span>BANK TRANSFER</span></td>
              <td class="col-label col-border"><span>CREDIT CARD</span></td>
              <td class="col-label col-border"><span>PF NUMBER</span></td>
            </tr>
            <tr style="text-align: center; height: 2rem;">
              <td class="col-label col-border">
                <table style="width: 100%;">
                  <tr>
                    <td>Reference #:</td>
                    <td>____________</td>
                  </tr>
                  <tr>
                    <td>Date :</td>
                    <td>____________</td>
                  </tr>
                </table>
              </td>
              <td class="col-label col-border">
                <table style="width: 100%;">
                  <tr>
                    <td>
                      <table style="width: 100%;">
                        <tr>
                          <td>Reference #:</td>
                          <td>____________</td>
                        </tr>
                        <tr>
                          <td>Date :</td>
                          <td>____________</td>
                        </tr>
                      </table>
                    </td>
                    <td>
                      <table style="width: 100%;">
                        <tr>
                          <td>CL</td>
                          <td>____________</td>
                        </tr>
                        <tr>
                          <td>ADMIN</td>
                          <td>____________</td>
                        </tr>
                        <tr>
                          <td>CE</td>
                          <td>____________</td>
                        </tr>
                        <tr>
                          <td>CEO</td>
                          <td>____________</td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
              <td class="col-label col-border"><strong><span style="color:red; font-size: 1.5rem;">{{ $request_details->request_number }}</span></strong></td>
            </tr>
          </table>
          <table>
          	<tr>
          		<td class="col-label col-border">The requestor acknowledges that the company has the rights to deduct the amount provided above in his/her salary upon failure to liquidate in a span of 15 days</td>
          	</tr>
          </table>
	</div>
</body>

<style>
	.container {
		max-width: 100% !important;		
	}

    table th{
        font-size: 14px;
        font-weight: bold;
        text-align:center;
    }
    table td{
        font-size: 16px;
        padding: 5px;
    }

    table td p{
        margin: 0;
        padding: 0;
    }

    .col-label {
    	width: 15%;
    	font-size: 16px;
    	font-weight: 700;
    }

    .col-content {
    	width: 35%;
    	font-size: 18px;
    	font-weight: 500;
    	padding: 16px 5px;
    }

    .col-border {
    	border: solid 1px; 
    }

    .col-side-border {
    	border-left: solid 1px;
    	border-right: solid 1px;
    }

    .col-border-wo-top {
    	border-left: solid 1px;
    	border-right: solid 1px;
    	border-bottom: solid 1px;
    }

    .col-even-width {
    	width: 33.33%;
    }

    .col-border-right {
    	border-right: solid 1px;
    }

    hr {
    	border:solid 5px;
    	width: 99.1%;
    	margin: 0;
    }

    h4 {
    	color: red;
    }

    input[type=checkbox] {
		-ms-transform: scale(1.5); /* IE */
		-moz-transform: scale(1.5); /* FF */
		-webkit-transform: scale(1.5); /* Safari and Chrome */
		-o-transform: scale(1.5); /* Opera */
		transform: scale(1.5);	
		margin-left: 5px;
    }
</style>