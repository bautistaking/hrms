<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<body>
  <div class="container">
    <div class="card-body">
      <h2>Fabriquem Integrated Resources Inc.</h2>
      <table class="table">
        <tbody>
          <tr>
            <td>Payment Date:</td>
            <td> {{ $payroll['created_at'] }} </td>
          </tr>
          <tr>
            <td>Period Covered:</td>
            <td> {{ $payroll['period_covered'] }} </td>
          </tr>
          <tr>
            <td>Employee Name:</td>
            <td> {{ $payroll['employee_details']['name'] }} </td>
          </tr>
        </tbody>
      </table>
      <h5>EARNINGS</h5>
      <table class="table table-hover table-bordered">
        <tbody>
          <tr class="table-info">
            <td class="td-width">Daily Wage:</td>
            <td> {{ $payroll['employee_details']['daily_wage_wo_cola'] }} </td>
          </tr>
          <tr>
            <td>Number of Days:</td>
            <td> {{ $payroll['number_of_days'] }} </td>
          </tr>
          <tr>
            <td>Regular/Special Holidays:</td>
            <td> {{ $payroll['regular_holidays'] }} </td>
          </tr>
          <tr>
            <td>Approved Leaves:</td>
            <td> {{ $payroll['approved_leaves'] }} </td>
          </tr>
          <tr>
            <td>COLA:</td>
            <td> {{ $payroll['total_cola'] }} </td>
          </tr>
          <tr class="table-primary">
            <td>Total Basic Salary:</td>
            <td> {{ number_format($payroll['total_basic_salary'],2) }} </td>
          </tr>
          <tr>
            <td>Over Time:</td>
            <td> {{ $payroll['over_time_total_amount'] }} </td>
          </tr>
          <tr>
            <td>Night Differentials:</td>
            <td> {{ $payroll['night_differential_total_amount'] }} </td>
          </tr>
          <tr>
            <td>Special Holiday & Sunday:</td>
            <td> {{ $payroll['special_day_total_amount'] }} </td>
          </tr>
          <tr>
            <td>Special Holiday & Sunday (OT):</td>
            <td> {{ $payroll['special_day_over_time_total_amount'] }} </td>
          </tr>
          <tr>
            <td>Regular Holiday:</td>
            <td> {{ $payroll['holiday_day_total_amount'] }} </td>
          </tr>
          <tr>
            <td>Regular Holiday (OT):</td>
            <td> {{ $payroll['holiday_day_over_time_total_amount'] }} </td>
          </tr>
          <tr class="table-primary">
            <td>Total Over Time:</td>
            <td> {{ $payroll['total_over_time'] }} </td>
          </tr>
          <tr>
            <td colspan="2"><strong>Allowance/Incentive:</strong></td>
          </tr>
          @foreach ($payroll['allowances'] as $allowance)
          <tr>
            <td>{{ $allowance['remarks'] }}</td>
            <td>{{ $allowance['amount'] }}</td>
          </tr>
          @endforeach
          <tr>
            <td colspan="2"><strong>Adjustments:</strong></td>
          </tr>
          @foreach ($payroll['adjustments'] as $adjustment)
          <tr>
            <td>{{ $adjustment['remarks'] }}</td>
            @if ($adjustment['type'] == 'less-adjustment')
            <td>-{{ $adjustment['amount'] }}</td>
            @else
            <td>{{ $adjustment['amount'] }}</td>
            @endif
          </tr>
          @endforeach
          <tr>
            <td colspan="2"><strong>13 Month Pay:</strong></td>
          </tr>
          @foreach ($payroll['13_month_pay'] as $month_pay)
          <tr>
            <td>{{ $month_pay['remarks'] }}</td>
            <td>{{ $month_pay['amount'] }}</td>
          </tr>
          @endforeach
          <tr>
            <td colspan="2"><strong>Tax Refund:</strong></td>
          </tr>
          @foreach ($payroll['tax_refund'] as $tax_refund)
          <tr>
            <td>{{ $tax_refund['remarks'] }}</td>
            <td>{{ $tax_refund['amount'] }}</td>
          </tr>
          @endforeach
          <tr>
            <td>Late:</td>
            <td> {{ $payroll['late_total_amount'] }} </td>
          </tr>
          <tr class="table-primary">
            <td>Total Gross Salary:</td>
            <td> {{ number_format($payroll['total_gross_salary'],2) }} </td>
          </tr>
        </tbody>
      </table>
      <h5>CONTRIBUTIONS</h5>
      <table class="table table-hover table-bordered">
        <tbody>
          <tr>
            <td class="td-width">Withholding Tax:</td>
            <td> {{ $payroll['withholding_tax'] }} </td>
          </tr>
          <tr>
            <td>SSS Premium:</td>
            <td> {{ $payroll['sss_prem'] }} </td>
          </tr>
          <tr>
            <td>SSS MPF:</td>
            <td> {{ $payroll['sss_mpf'] }} </td>
          </tr>
          <tr>
            <td>Philhealth:</td>
            <td> {{ $payroll['philhealth_prem'] }} </td>
          </tr>
          <tr>
            <td>Pag-ibig Fund:</td>
            <td> {{ $payroll['pagibig_prem'] }} </td>
          </tr>
        </tbody>
      </table>
      <h5>LOANS</h5>
      <span>
        {!! $payroll['loans'] !!}
      </span>
      <h5>OTHER DEDUCTION</h5>
      <table class="table table-hover table-bordered">
        <tbody>
          @foreach ($payroll['other_deductions'] as $deduction)
          <tr>
            <td class="td-width">{{ $deduction['remarks'] }}</td>
            <td>{{ $deduction['amount'] }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
      <h5>TOTAL DEDUCTIONS</h5>
      <table class="table table-hover table-bordered">
        <tbody>
          <tr class="table-danger">
            <td class="td-width">Total Deductions:</td>
            <td> {{ number_format($payroll['total_deductions'],2) }} </td>
          </tr>
        </tbody>
      </table>
      <h5>NET EARNINGS</h5>
      <table class="table table-hover table-bordered">
        <tbody>
          <tr class="table-primary">
            <td class="td-width">Net Salary:</td>
            <td> {{ number_format($payroll['net_salary'],2) }} </td>
          </tr>
        </tbody>
      </table>
    </div>

  </div>
</body>

<style>
  .td-width {
      width: 75%;
  }  
</style>

