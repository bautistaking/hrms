<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<body>
	<div class="container">
		<table>
			<tr>
				<td><img src="{{ asset('img/fabriquem-logo.png') }}"></td>
				<td style="font-weight: bold;"><h3>FABRIQUEM INTEGRATED RESOURCES, INC.</h3><br/>
					6F RICOGEN BUILDING 112 AGUIRRE STREET<br/>
					LEGASPI VILLAGE, MAKATI CITY<br/>
					TIN: 211-393-918-000<br/>
					info@fabriquem.com<br/>
				</td>
			</tr>
		</table>
		<br/>
		<table style="width: 100%;">
			<tr>
				<td style="text-align: center;"><h1>PURCHASE ORDER</h1></td>
			</tr>			
		</table>
		<table style="width: 100%;">
			<tr>
				<td class="col-label col-border">PO. No.</td>
				<td class="col-border" style="vertical-align: middle; font-weight: bold;"><span style="color: red;">{{ $po_info->po_number }}</span></td> 
				<td colspan="5"></td>
			</tr>			
			<tr>
				<td class="col-label col-border">Date:</td>
				<td class="col-border">{{ $po_info->due_date }}</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>COS&nbsp; <input type="checkbox"></td>
				<td>OPEX&nbsp; <input type="checkbox"></td>
				<td>CAPEX&nbsp; <input type="checkbox"></td>
				<td>Other&nbsp; <input type="checkbox"></td>
			</tr>			
		</table>
		<hr>
		<table style="width: 100%;">
			<tr>
				<td class="col-label col-border">TO:</td>
				<td class="col-content col-border">{{ $po_info->supplier_details['name'] }}</td>
				<td class="col-label col-border" rowspan="2">Deliver To:</td>
				<td class="col-content col-border" rowspan="2">{{ $po_info->deliver_to }}</td>
			</tr>
			<tr>
				<td class="col-label col-border">Contact Person:</td>
				<td class="col-content col-border">{{ $po_info->supplier_details['contact_person'] }}</td>
			</tr>
			<tr>
				<td class="col-label col-border">Address & TIN number:</td>
				<td class="col-content col-border">{{ $po_info->supplier_details['address'] }} / {{ $po_info->supplier_details['tin'] }}</td>
				<td class="col-label col-border">Delivery Address:</td>
				<td class="col-content col-border"></td>
			</tr>
			<tr>
				<td class="col-label col-border">Contact Number:</td>
				<td class="col-content col-border">{{ $po_info->supplier_details['contact_number'] }}</td>
				<td class="col-label col-border">Date Required:</td>
				<td class="col-content col-border">{{ $po_info->required_date }}</td>
			</tr>
			<tr>
				<td class="col-label col-border">Payment Terms:</td>
				<td class="col-content col-border">{{ $po_info->payment_terms }}</td>
				<td class="col-label col-border">Site/s:</td>
				<td class="col-content col-border">{{ $po_info->remarks }}</td>
			</tr>
		</table>
		<hr>
		<table style="width: 100%;">
			<tr>
				<td class="col-border text-center">ITEM</td>
				<td class="col-border text-center" style="width: 45%;">DESCRIPTION</td>
				<td class="col-border text-center">QUANTITY</td>
				<td class="col-border text-center">UNIT OF MEASUREMENT</td>
				<td class="col-border text-center">UNIT PRICE</td>
				<td class="col-border text-center">TOTAL PRICE</td>
			</tr>
			<!-- LOOP HERE -->
			@php ($i = 1)
			@foreach ($po_info->catalogs as $catalog)
			<tr>
				<td class="col-border text-center">{{ $i }}</td>
				<td class="col-border text-left" style="width: 45%;">{{ $catalog->item_name }}</td>
				<td class="col-border text-center">{{ $catalog->quantity }}</td>
				<td class="col-border text-center">{{ $catalog->uom }}</td>
				<td class="col-border text-right">{{ $catalog->unit_price }}</td>
				<td class="col-border text-right">{{ $catalog->total_price }}</td>
			</tr>
			@php ($i++)
			@endforeach
			<!-- END LOOP HERE -->
			<tr>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-center" style="width: 45%;">&nbsp;</td>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-left">&nbsp;</td>
				<td class="col-border text-right">&nbsp;</td>
			</tr>					
			<tr>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-center" style="width: 45%;">&nbsp;</td>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-left"><strong>TOTAL</strong></td>
				<td class="col-border text-right"><strong>{{ number_format($po_info->gross_amount, 2) }}</strong></td>
			</tr>
			<tr>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-center" style="width: 45%;">&nbsp;</td>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-left">&nbsp;</td>
				<td class="col-border text-right">&nbsp;</td>
			</tr>				
			<tr>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-center" style="width: 45%;">&nbsp;</td>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-left">NET OF VAT</td>
				<td class="col-border text-right">{{ number_format($po_info->net_amount, 2) }}</td>
			</tr>
			<tr>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-center" style="width: 45%;">&nbsp;</td>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-left">VAT AMOUNT</td>
				<td class="col-border text-right">{{ number_format($po_info->vat_amount, 2) }}</td>
			</tr>
			<tr> 
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-center" style="width: 45%;">&nbsp;</td>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-left">GROSS AMOUNT</td>
				<td class="col-border text-right">{{ number_format($po_info->gross_amount,2) }}</td>
			</tr>
			<tr>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-center" style="width: 45%;">&nbsp;</td>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-left">
				@if($po_info->discount_rate > 0)         
					<span style="color: red;">Less {{ $po_info->discount_rate*100 }}% DISCOUNT</span>
				@else
					<span style="color: red;">Less {{ $po_info->ewt_percent*100 }}% EWT</span>
				@endif
				</td>
				<td class="col-border text-right">
				@if($po_info->discount_rate > 0)         
					<span style="color: red;">{{ number_format($po_info->discount_amount, 2) }}</span>
				@else
					<span style="color: red;">{{ number_format($po_info->ewt_amount, 2) }}</span>
				@endif
					
				</td>
			</tr>
			<tr>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-center" style="width: 45%;">&nbsp;</td>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-center">&nbsp;</td>
				<td class="col-border text-left"><strong>TOTAL AMOUNT DUE</strong></td>
				<td class="col-border text-right"><strong>{{ number_format($po_info->total_amount_due, 2) }}</strong></td>
			</tr>
		</table>
		<hr>
		<table style="width: 100%;">
			<tr>
				<td class="col-side-border col-even-width text-left">PREPARED BY:</td>
				<td class="col-side-border col-even-width text-left" colspan="2">APPROVED BY:</td>
			</tr>
			<tr>
				<td class="col-side-border col-even-width text-left">&nbsp;</td>
				<td class="col-even-width text-left"><img src="{{ asset('img/MACALINAO_BENNETH.jpg') }}" width="50%"></td>
				<td class="col-border-right col-even-width text-center"><img src="{{ asset('img/catherine_p.png') }}" width="50%"></td>
			</tr>			
			<tr>
				<td class="col-side-border col-even-width text-left">&nbsp;</td>
				<td class="col-even-width text-left">&nbsp;</td>
				<td class="col-border-right col-even-width text-center">&nbsp;</td>
			</tr>			
			<tr>
				<td class="col-side-border col-even-width text-left"><strong>{{ $po_info->prepared_by }}</strong></td>
				<td class="col-even-width text-left">
					<strong>MACALINAO, BENNETH C.</strong><br/>
					Corporate Logistics Head
				</td>
				<td class="col-border-right col-even-width text-left">
					<strong>MENDOZA, CATHERINE P</strong><br>
					General Manager
				</td>
			</tr>
			<tr>
				<td class="col-border text-left" colspan="2">SUPPLIER ACKNOWLEDGEMENT:</td>
				<td class="col-border col-even-width text-center"><strong>NOTE: This document is null and void without the approval from the President and CEO.</strong></td>
			</tr>
			<tr>
				<td class="col-border text-center" colspan="3"><strong style="color: red;">CONFIDENTIAL - INTENDED FOR SUPPLIER AND PURCHASING USE ONLY.</strong></td>
			</tr>				
		</table>
	</div>
</body>

<style>
    table th{
        font-size: 14px;
        font-weight: bold;
        text-align:center;
    }
    table td{
        font-size: 16px;
        padding: 5px;
    }

    table td p{
        margin: 0;
        padding: 0;
    }

    .col-label {
    	width: 15%;
    	font-size: 16px;
    	font-weight: 700;
    }

    .col-content {
    	width: 35%;
    	font-size: 18px;
    	font-weight: 500;
    	padding: 16px 5px;
    }

    .col-border {
    	border: solid 1px; 
    }

    .col-side-border {
    	border-left: solid 1px;
    	border-right: solid 1px;
    }

    .col-border-wo-top {
    	border-left: solid 1px;
    	border-right: solid 1px;
    	border-bottom: solid 1px;
    }

    .col-even-width {
    	width: 33.33%;
    }

    .col-border-right {
    	border-right: solid 1px;
    }

    hr {
    	border:solid 5px;
    	width: 99.1%;
    	margin: 0;
    }

    h4 {
    	color: red;
    }

    input[type=checkbox] {
		-ms-transform: scale(1.5); /* IE */
		-moz-transform: scale(1.5); /* FF */
		-webkit-transform: scale(1.5); /* Safari and Chrome */
		-o-transform: scale(1.5); /* Opera */
		transform: scale(1.5);	
		margin-left: 5px;
    }
</style>