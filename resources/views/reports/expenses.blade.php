<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<body>
	<div class="container-fluid">
		<table style="width: 100%;">
			<tr>
				<td width="25%"><img src="{{ asset('img/fabriquem-logo-small.png') }}"></td>
				<td style="text-align: center;"><h1>EXPENSES REPORT</h1></td>
			</tr>
		</table>
		<table style="width: 100%;">
			<tr style="text-align: center; height: 2rem;">
				<td class="col-label col-border">SITE NAME</td>
				<td class="col-label col-border">SITE DETAILS</td>
				<td class="col-label col-border">ASSIGNED PO NUMBER</td>
				<td class="col-label col-border">CATEGORY</td>
				<td class="col-label col-border">BOQ AMOUNT (VAT INC)</td>
				<td class="col-label col-border">PROJECT COST</td>
				<td class="col-label col-border">RUNNING EXPENSES</td>
				<td class="col-label col-border">RUNNING BALANCE</td>
				<td class="col-label col-border">OTHER EXPENSES</td>
				<td class="col-label col-border">EXPENSES DETAILS</td>
			</tr>
			@foreach ($report_requests as $item)
			<tr style="text-align: left;">
				<td class="col-border">{{ $item->site_name }}</td>
				<td class="col-border">{{ $item->site_details }}</td>
				<td class="col-border">{{ $item->po_number }}</td>
				<td class="col-border">{{ $item->category }}</td>
				<td class="col-border">PHP {{ number_format($item->updated_bill_of_quantities, 2) }}</td>
				<td class="col-border">PHP {{ number_format($item->badget, 2) }}</td>
				<td class="col-border">PHP {{ number_format($item->running_expenses, 2) }}</td>
				<td class="col-border">PHP {{ number_format($item->balance_amount, 2) }}</td>
				<td class="col-border">PHP {{ number_format($item->expenses, 2) }}</td>
				<td class="col-border">
					<table>
						@foreach ($item->sites_expenses as $expense)
						<tr style="border-bottom: solid 1px; ">
							<td style="border-right: solid 1px;">{{ $expense->request_name }}</td>
							<td>PHP {{ number_format($expense->total, 2) }}</td>
						</tr>
						@endforeach
					</table>
				</td>
			</tr>
			@endforeach
		</table>
	</div>
</body>

<style>
    table th{
        font-size: 14px;
        font-weight: bold;
        text-align:center;
    }
    table td{
        font-size: 16px;
        padding: 5px;
    }

    table td p{
        margin: 0;
        padding: 0;
    }

    .col-label {
    	width: 15%;
    	font-size: 16px;
    	font-weight: 700;
    }

    .col-content {
    	width: 35%;
    	font-size: 18px;
    	font-weight: 500;
    	padding: 16px 5px;
    }

    .col-border {
    	border: solid 1px; 
    	vertical-align: top;
    	white-space: nowrap;
    }

    .col-side-border {
    	border-left: solid 1px;
    	border-right: solid 1px;
    }

    .col-border-wo-top {
    	border-left: solid 1px;
    	border-right: solid 1px;
    	border-bottom: solid 1px;
    }

    .col-even-width {
    	width: 33.33%;
    }

    .col-border-right {
    	border-right: solid 1px;
    }

    hr {
    	border:solid 5px;
    	width: 99.1%;
    	margin: 0;
    }

    h4 {
    	color: red;
    }

    input[type=checkbox] {
		-ms-transform: scale(1.5); /* IE */
		-moz-transform: scale(1.5); /* FF */
		-webkit-transform: scale(1.5); /* Safari and Chrome */
		-o-transform: scale(1.5); /* Opera */
		transform: scale(1.5);	
		margin-left: 5px;
    }
</style>