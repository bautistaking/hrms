<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Fabriquem') }}</title>

        <link rel="icon" type="image/webp" href="{{ asset('img/7a5c3c_968939db3a904c9db19ed94ef7aaa15b_mv2.webp') }}"/>
        <!-- Bootstrap core CSS -->
        <link href="{{ asset('web_components/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Toastr -->
        <link rel="stylesheet" href="{{ asset('bower_components/admin-lte/plugins/toastr/toastr.min.css') }}">
        <!-- Custom fonts for this template -->
        <link href="{{ asset('web_components/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('web_components/vendor/simple-line-icons/css/simple-line-icons.css') }}">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset('bower_components/admin-lte/dist/css/adminlte.css') }}">
        <!-- Custom styles for this template -->
        <link href="{{ asset('web_components/css/new-age.css') }}" rel="stylesheet">     
    </head>

    <body id="page-top">
        <div id="app">
            <Index></Index>
        </div>

      <!-- Bootstrap core JavaScript -->
      <script src="{{ asset('web_components/vendor/jquery/jquery.min.js') }}"></script>
      <script src="{{ asset('web_components/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>      
      <!-- Toastr -->
      <script src="{{ asset('bower_components/admin-lte/plugins/toastr/toastr.min.js') }}"></script>      
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATD3gn7Jz0faihEDwA0cR2BsX-0XJIIog&libraries=places"></script>
      <!-- App script -->
      <script src="{{ asset('js/app.js') }}"></script>

    </body>
</html>
