<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'v1'], function () 
{
    /*
    |--------------------------------------------------------------------------
    | Auth Routes
    |--------------------------------------------------------------------------
    */    
    Route::post('login', 'Admin\AuthController@login')->name('api.v1.auth.login');
    Route::post('refresh', 'Admin\AuthController@refresh')->name('api.v1.auth.refresh');

    /*
    |--------------------------------------------------------------------------
    | Auth Password Reset
    |--------------------------------------------------------------------------
    */
    Route::post('password/create', 'Admin\PasswordResetController@create')->name('api.v1.password.create');
    Route::get('password/find/{token}', 'Admin\PasswordResetController@find')->name('api.v1.password.find');
    Route::post('password/reset', 'Admin\PasswordResetController@reset')->name('api.v1.password.reset');

});

Route::group(['prefix' => 'v1', 'middleware' => ['auth:api']], function () 
{
    /*
    |--------------------------------------------------------------------------
    | Dashboard Routes
    |--------------------------------------------------------------------------
    */
    Route::get('dashboard/catalog-request', 'Admin\DashboardController@getCatalogRequest')->name('api.v1.dashboard.catalog');
    Route::get('dashboard/payment-request', 'Admin\DashboardController@getPaymentRequest')->name('api.v1.dashboard.payment');

    /*
    |--------------------------------------------------------------------------
    | User Routes
    |--------------------------------------------------------------------------
    */
    Route::get('user/{id}', 'Admin\UserController@details')->where('id', '[0-9]+')->name('api.v1.user.show');
    Route::post('user/store', 'Admin\UserController@store')->name('api.v1.user.store');
    Route::put('user/update', 'Admin\UserController@update')->name('api.v1.user.update');
    Route::get('user/delete/{id}', 'Admin\UserController@delete')->where('id', '[0-9]+')->name('api.v1.user.delete');
    Route::get('user/list', 'Admin\UserController@list')->name('api.v1.user.list');
    Route::post('user/assign-roles', 'Admin\UserRoleController@setUserRoles')->name('api.v1.user.assign.roles');
    Route::get('user/roles/{id}', 'Admin\UserRoleController@getUserRoles')->where('id', '[0-9]+')->name('api.v1.user.role');

    /*
    |--------------------------------------------------------------------------
    | Roles Routes
    |--------------------------------------------------------------------------
    */
    Route::post('role/create', 'Admin\RoleController@create')->name('api.v1.role.create');
    Route::get('role/{roleId}', 'Admin\RoleController@show')->where('roleId', '[0-9]+')->name('api.v1.role.show');
    Route::put('role/update', 'Admin\RoleController@update')->name('api.v1.role.update');
    Route::get('role/delete/{roleId}', 'Admin\RoleController@delete')->where('moduleId', '[0-9]+')->name('api.v1.role.delete');
    Route::get('role/list', 'Admin\RoleController@list')->name('api.v1.role.list');
    Route::get('role/get-all', 'Admin\RoleController@all')->name('api.v1.role.all');
    Route::post('role/grant-permission', 'Admin\PermissionController@grantPermission')->name('api.v1.grant.permission');
    
    /*
    |--------------------------------------------------------------------------
    | QrCode Routes
    |--------------------------------------------------------------------------
    */    
    Route::post('qr-code/generate', 'Admin\QrCodeController@generateQrCode')->name('api.v1.qr-code.generate');

    /*
    |--------------------------------------------------------------------------
    | Module Routes
    |--------------------------------------------------------------------------
    */
    Route::post('module/create', 'Admin\ModuleController@create')->name('api.v1.module.create');
    Route::get('module/{moduleId}', 'Admin\ModuleController@show')->where('moduleId', '[0-9]+')->name('api.v1.module.show');
    Route::post('module/update', 'Admin\ModuleController@update')->name('api.v1.module.update');
    Route::get('module/delete/{moduleId}', 'Admin\ModuleController@delete')->where('moduleId', '[0-9]+')->name('api.v1.module.delete');
    Route::get('module/list', 'Admin\ModuleController@list')->name('api.v1.module.list');

    /*
    |--------------------------------------------------------------------------
    | Categories Routes
    |--------------------------------------------------------------------------
    */
    Route::get('category/{id}', 'Admin\CategoryController@getCategory')->where('id', '[0-9]+')->name('api.v1.category.show');
    Route::post('category/store', 'Admin\CategoryController@store')->name('api.v1.category.store');
    Route::put('category/update', 'Admin\CategoryController@update')->name('api.v1.category.update');
    Route::get('category/delete/{id}', 'Admin\CategoryController@delete')->where('id', '[0-9]+')->name('api.v1.category.delete');
    Route::get('category/list/{module}', 'Admin\CategoryController@list')->name('api.v1.category.list');
    Route::get('category/all/{module}', 'Admin\CategoryController@all')->name('api.v1.category.all');
    Route::post('category/batch-upload', 'Admin\CategoryController@batchUpload')->name('api.v1.category.batch-upload');    
    Route::get('category/download-csv', 'Admin\CategoryController@downloadCsv')->name('api.v1.category.download-csv');    

    /*
    |--------------------------------------------------------------------------
    | Designation Routes
    |--------------------------------------------------------------------------
    */
    Route::get('designation/{id}', 'Admin\DesignationController@getDetails')->where('id', '[0-9]+')->name('api.v1.designation.show');
    Route::post('designation/store', 'Admin\DesignationController@store')->name('api.v1.designation.store');
    Route::put('designation/update', 'Admin\DesignationController@update')->name('api.v1.designation.update');
    Route::get('designation/delete/{id}', 'Admin\DesignationController@delete')->where('id', '[0-9]+')->name('api.v1.designation.delete');
    Route::get('designation/list', 'Admin\DesignationController@list')->name('api.v1.designation.list');
    Route::get('designation/all', 'Admin\DesignationController@all')->name('api.v1.designation.all');
    Route::post('designation/batch-upload', 'Admin\DesignationController@batchUpload')->name('api.v1.designation.batch-upload');    

    /*
    |--------------------------------------------------------------------------
    | Department Routes
    |--------------------------------------------------------------------------
    */
    Route::get('department/{id}', 'Admin\DepartmentController@getDetails')->where('id', '[0-9]+')->name('api.v1.department.show');
    Route::post('department/store', 'Admin\DepartmentController@store')->name('api.v1.department.store');
    Route::put('department/update', 'Admin\DepartmentController@update')->name('api.v1.department.update');
    Route::get('department/delete/{id}', 'Admin\DepartmentController@delete')->where('id', '[0-9]+')->name('api.v1.department.delete');
    Route::get('department/list', 'Admin\DepartmentController@list')->name('api.v1.department.list');
    Route::get('department/all', 'Admin\DepartmentController@all')->name('api.v1.department.all');
    Route::post('department/batch-upload', 'Admin\DepartmentController@batchUpload')->name('api.v1.department.batch-upload');    

    /*
    |--------------------------------------------------------------------------
    | Employee Routes
    |--------------------------------------------------------------------------
    */
    Route::get('employee/{id}', 'Admin\EmployeeController@getDetails')->where('id', '[0-9]+')->name('api.v1.employee.show');
    Route::post('employee/store', 'Admin\EmployeeController@store')->name('api.v1.employee.store');
    Route::put('employee/update', 'Admin\EmployeeController@update')->name('api.v1.employee.update');
    Route::get('employee/delete/{id}', 'Admin\EmployeeController@delete')->where('id', '[0-9]+')->name('api.v1.employee.delete');
    Route::get('employee/list', 'Admin\EmployeeController@list')->name('api.v1.employee.list');
    Route::post('employee/batch-upload', 'Admin\EmployeeController@batchUpload')->name('api.v1.employee.batch-upload');    
    Route::get('employee/download-csv', 'Admin\EmployeeController@downloadCsv')->name('api.v1.employee.download-csv');    
    Route::get('employee/all', 'Admin\EmployeeController@all')->name('api.v1.employee.all');    

    /*
    |--------------------------------------------------------------------------
    | Loan Type Routes
    |--------------------------------------------------------------------------
    */
    Route::get('loan-type/{id}', 'Admin\LoanTypeController@getDetails')->where('id', '[0-9]+')->name('api.v1.loan.type.show');
    Route::post('loan-type/store', 'Admin\LoanTypeController@store')->name('api.v1.loan.type.store');
    Route::put('loan-type/update', 'Admin\LoanTypeController@update')->name('api.v1.loan.type.update');
    Route::get('loan-type/delete/{id}', 'Admin\LoanTypeController@delete')->where('id', '[0-9]+')->name('api.v1.loan.type.delete');
    Route::get('loan-type/list', 'Admin\LoanTypeController@list')->name('api.v1.loan.type.list');
    Route::get('loan-type/all', 'Admin\LoanTypeController@all')->name('api.v1.loan.type.all');

    /*
    |--------------------------------------------------------------------------
    | Loans Routes
    |--------------------------------------------------------------------------
    */
    Route::get('loan/{id}', 'Admin\LoanController@getDetails')->where('id', '[0-9]+')->name('api.v1.loan.show');
    Route::post('loan/store', 'Admin\LoanController@store')->name('api.v1.loan.store');
    Route::put('loan/update', 'Admin\LoanController@update')->name('api.v1.loan.update');
    Route::get('loan/delete/{id}', 'Admin\LoanController@delete')->where('id', '[0-9]+')->name('api.v1.loan.delete');
    Route::get('loan/list', 'Admin\LoanController@list')->name('api.v1.loan.list');

    /*
    |--------------------------------------------------------------------------
    | Loans Routes
    |--------------------------------------------------------------------------
    */
    Route::get('holiday/{id}', 'Admin\HolidayController@getDetails')->where('id', '[0-9]+')->name('api.v1.holiday.show');
    Route::post('holiday/store', 'Admin\HolidayController@store')->name('api.v1.holiday.store');
    Route::put('holiday/update', 'Admin\HolidayController@update')->name('api.v1.holiday.update');
    Route::get('holiday/delete/{id}', 'Admin\HolidayController@delete')->where('id', '[0-9]+')->name('api.v1.holiday.delete');
    Route::get('holiday/all', 'Admin\HolidayController@all')->name('api.v1.holiday.all');

    /*
    |--------------------------------------------------------------------------
    | Time Keeping Routes
    |--------------------------------------------------------------------------
    */
    Route::get('time-keeping/{id}', 'Admin\TimeKeepingController@getDetails')->where('id', '[0-9]+')->name('api.v1.time-keeping.show');
    Route::post('time-keeping/store', 'Admin\TimeKeepingController@store')->name('api.v1.time-keeping.store');
    Route::put('time-keeping/update', 'Admin\TimeKeepingController@update')->name('api.v1.time-keeping.update');
    Route::get('time-keeping/delete/{id}', 'Admin\TimeKeepingController@delete')->where('id', '[0-9]+')->name('api.v1.time-keeping.delete');
    Route::get('time-keeping/list', 'Admin\TimeKeepingController@list')->name('api.v1.time-keeping.all');
    Route::post('time-keeping/download-csv', 'Admin\TimeKeepingController@downloadCsv')->name('api.v1.time-keeping.download-csv');

    /*
    |--------------------------------------------------------------------------
    | Request Routes
    |--------------------------------------------------------------------------
    */
    Route::get('request/over-time', 'Admin\RequestController@getOverTime')->name('api.v1.admin-request.over.time');
    Route::get('request/official-bussiness', 'Admin\RequestController@getOfficialBusiness')->name('api.v1.admin-request.official.bussiness');
    Route::get('request/leave-request', 'Admin\RequestController@getLeaveRequest')->name('api.v1.admin-request.leave.request');
    Route::put('request/update', 'Admin\RequestController@update')->name('api.v1.admin-request.update');
    Route::post('request/download-csv', 'Admin\RequestController@downloadCsv')->name('api.v1.request.download-csv');
 
    /*
    |--------------------------------------------------------------------------
    | Payrol Routes
    |--------------------------------------------------------------------------
    */
    Route::get('payperiod/{id}', 'Admin\PayrollController@getDetails')->where('id', '[0-9]+')->name('api.v1.payperiod.show');
    Route::post('payperiod/store', 'Admin\PayrollController@store')->name('api.v1.payperiod.store');
    Route::put('payperiod/update', 'Admin\PayrollController@update')->name('api.v1.payperiod.update');
    Route::get('payperiod/delete/{id}', 'Admin\PayrollController@delete')->where('id', '[0-9]+')->name('api.v1.payperiod.delete');
    Route::get('payperiod/list', 'Admin\PayrollController@list')->name('api.v1.payperiod.list');
    Route::post('payperiod/process-payroll', 'Admin\PayrollController@processPayroll')->name('api.v1.payperiod.process-payroll');
    Route::put('payperiod/add-other-income', 'Admin\PayrollController@addOtherIncome')->name('api.v1.payperiod.add-other-income');
    Route::get('payperiod/other-income/delete/{id}', 'Admin\PayrollController@deleteOtherIncome')->where('id', '[0-9]+')->name('api.v1.payperiod.other-income.delete');
    Route::get('payperiod/download-csv/{id}', 'Admin\PayrollController@downloadCsv')->where('id', '[0-9]+')->name('api.v1.payperiod.download-csv');    
    Route::get('payperiod/locked/{id}', 'Admin\PayrollController@lockPayPeriod')->where('id', '[0-9]+')->name('api.v1.payperiod.locked');    

    /*
    |--------------------------------------------------------------------------
    | Logistics Routes
    |--------------------------------------------------------------------------
    */
    Route::get('logistic/{id}', 'Admin\LogisticController@getDetails')->where('id', '[0-9]+')->name('api.v1.logistic.show');
    Route::post('logistic/store', 'Admin\LogisticController@store')->name('api.v1.logistic.store');
    Route::put('logistic/update', 'Admin\LogisticController@update')->name('api.v1.logistic.update');
    Route::get('logistic/delete/{id}', 'Admin\LogisticController@delete')->where('id', '[0-9]+')->name('api.v1.logistic.delete');
    Route::get('logistic/list', 'Admin\LogisticController@list')->name('api.v1.logistic.list');
    Route::post('logistic/batch-upload', 'Admin\LogisticController@batchUpload')->name('api.v1.logistic.batch-upload');    
    Route::get('logistic/download-csv', 'Admin\LogisticController@downloadCsv')->name('api.v1.logistic.download-csv');    

    /*
    |--------------------------------------------------------------------------
    | Suppliers Routes
    |--------------------------------------------------------------------------
    */
    Route::get('supplier/{id}', 'Admin\SupplierControlller@getDetails')->where('id', '[0-9]+')->name('api.v1.supplier.show');
    Route::post('supplier/store', 'Admin\SupplierControlller@store')->name('api.v1.supplier.store');
    Route::put('supplier/update', 'Admin\SupplierControlller@update')->name('api.v1.supplier.update');
    Route::get('supplier/delete/{id}', 'Admin\SupplierControlller@delete')->where('id', '[0-9]+')->name('api.v1.supplier.delete');
    Route::get('supplier/list', 'Admin\SupplierControlller@list')->name('api.v1.supplier.list');
    Route::get('supplier/all', 'Admin\SupplierControlller@all')->name('api.v1.supplier.all');
    Route::post('supplier/batch-upload', 'Admin\SupplierControlller@batchUpload')->name('api.v1.supplier.batch-upload');    
    Route::get('supplier/download-csv', 'Admin\SupplierControlller@downloadCsv')->name('api.v1.supplier.download-csv');    

    /*
    |--------------------------------------------------------------------------
    | Clients Routes
    |--------------------------------------------------------------------------
    */
    Route::get('client/{id}', 'Admin\ClientController@getDetails')->where('id', '[0-9]+')->name('api.v1.client.show');
    Route::post('client/store', 'Admin\ClientController@store')->name('api.v1.client.store');
    Route::put('client/update', 'Admin\ClientController@update')->name('api.v1.client.update');
    Route::get('client/delete/{id}', 'Admin\ClientController@delete')->where('id', '[0-9]+')->name('api.v1.client.delete');
    Route::get('client/list', 'Admin\ClientController@list')->name('api.v1.client.list');
    Route::get('client/all', 'Admin\ClientController@all')->name('api.v1.client.all');

    /*
    |--------------------------------------------------------------------------
    | Job Orders Routes
    |--------------------------------------------------------------------------
    */
    Route::get('job-order/{id}', 'Admin\JobOrderController@getDetails')->where('id', '[0-9]+')->name('api.v1.job-order.show');
    Route::post('job-order/store', 'Admin\JobOrderController@store')->name('api.v1.job-order.store');
    Route::put('job-order/update', 'Admin\JobOrderController@update')->name('api.v1.job-order.update');
    Route::get('job-order/delete/{id}', 'Admin\JobOrderController@delete')->where('id', '[0-9]+')->name('api.v1.job-order.delete');
    Route::get('job-order/list', 'Admin\JobOrderController@list')->name('api.v1.job-order.list');
    Route::get('job-order/details/{id}', 'Admin\JobOrderController@getJobOrderDetails')->where('id', '[0-9]+')->name('api.v1.job-order-details.show');
    Route::post('job-order/details/store', 'Admin\JobOrderController@storeOrderDetails')->name('api.v1.job-order-details.store');
    Route::put('job-order/details/update', 'Admin\JobOrderController@updateOrderDetails')->name('api.v1.job-order-details.update');
    Route::get('job-order/details/delete/{id}', 'Admin\JobOrderController@deleteOrderDetails')->where('id', '[0-9]+')->name('api.v1.job-order-details.delete');
    Route::get('job-order/details/list/{id}', 'Admin\JobOrderController@listOrderDetails')->where('id', '[0-9]+')->name('api.v1.job-order-details.list');

    /*
    |--------------------------------------------------------------------------
    | Approver Routes
    |--------------------------------------------------------------------------
    */
    Route::get('approver/{id}', 'Admin\ApproverController@getDetails')->where('id', '[0-9]+')->name('api.v1.approver.show');
    Route::post('approver/store', 'Admin\ApproverController@store')->name('api.v1.approver.store');
    Route::get('approver/delete/{id}', 'Admin\ApproverController@delete')->where('id', '[0-9]+')->name('api.v1.approver.delete');
    Route::get('approver/list', 'Admin\ApproverController@list')->name('api.v1.approver.list');
    Route::put('approver/requestor/delete', 'Admin\ApproverController@deleteRequestor')->name('api.v1.approver.delete.requestor');

    /*
    |--------------------------------------------------------------------------
    | Approver Routes
    |--------------------------------------------------------------------------
    */
    Route::get('catalog/{id}', 'Admin\RequisitionController@details')->where('id', '[0-9]+')->name('api.v1.catalog.show');
    Route::get('catalog/list', 'Admin\RequisitionController@list')->name('api.v1.catalog.list');
    Route::put('catalog/update', 'Admin\RequisitionController@update')->name('api.v1.catalog.update');

    /*
    |--------------------------------------------------------------------------
    | Request List Routes
    |--------------------------------------------------------------------------
    */
    Route::get('request-list/{id}', 'Admin\RequestListController@details')->where('id', '[0-9]+')->name('api.v1.request-list.show');
    Route::get('request-list/list', 'Admin\RequestListController@list')->name('api.v1.request-list.list');
    Route::post('request-list/store', 'Admin\RequestListController@store')->name('api.v1.request-list.store');    
    Route::put('request-list/update', 'Admin\RequestListController@update')->name('api.v1.request-list.update');    
    Route::get('request-list/delete/{id}', 'Admin\RequestListController@delete')->where('id', '[0-9]+')->name('api.v1.request-list.delete');

    /*
    |--------------------------------------------------------------------------
    | Media Library Routes
    |--------------------------------------------------------------------------
    */
    Route::get('media-library/{id}', 'Admin\MediaLibraryController@getMediaDetails')->where('id', '[0-9]+')->name('api.v1.media-library.show');
    Route::post('media-library/store', 'Admin\MediaLibraryController@store')->name('api.v1.media-library.store');
    Route::put('media-library/update', 'Admin\MediaLibraryController@update')->name('api.v1.media-library.update');
    Route::get('media-library/delete/{id}', 'Admin\MediaLibraryController@delete')->where('id', '[0-9]+')->name('api.v1.media-library.delete');
    Route::get('media-library/list', 'Admin\MediaLibraryController@list')->name('api.v1.media-library.list');    

    /*
    |--------------------------------------------------------------------------
    | Teams Routes
    |--------------------------------------------------------------------------
    */
    Route::get('team/{id}', 'Admin\TeamController@getDetails')->where('id', '[0-9]+')->name('api.v1.team.show');
    Route::get('team/list', 'Admin\TeamController@list')->name('api.v1.team.list');    
    Route::post('team/store', 'Admin\TeamController@store')->name('api.v1.team.store');    
    Route::get('team/delete/{id}', 'Admin\TeamController@delete')->where('id', '[0-9]+')->name('api.v1.team.delete');

    /*
    |--------------------------------------------------------------------------
    | Plans & References Routes
    |--------------------------------------------------------------------------
    */
    Route::get('references/{id}', 'Admin\ReferencesController@getDetails')->where('id', '[0-9]+')->name('api.v1.references.show');
    Route::get('references/list', 'Admin\ReferencesController@list')->name('api.v1.references.list');    
    Route::post('references/store', 'Admin\ReferencesController@store')->name('api.v1.references.store');    
    Route::get('references/delete/{id}', 'Admin\ReferencesController@delete')->where('id', '[0-9]+')->name('api.v1.references.delete');

    /*
    |--------------------------------------------------------------------------
    | Ticket Routes
    |--------------------------------------------------------------------------
    */
    Route::get('ticket/{id}', 'Admin\TicketController@getDetails')->where('id', '[0-9]+')->name('api.v1.ticket.show');
    Route::get('ticket/list', 'Admin\TicketController@list')->name('api.v1.ticket.list');    
    Route::post('ticket/store', 'Admin\TicketController@store')->name('api.v1.ticket.store');    
    Route::get('ticket/delete/{id}', 'Admin\TicketController@delete')->where('id', '[0-9]+')->name('api.v1.ticket.delete');

    /*
    |--------------------------------------------------------------------------
    | Non-Catalog Routes
    |--------------------------------------------------------------------------
    */
    Route::get('requisition/non-catalog/{id}', 'Admin\RequisitionController@paymentDetails')->where('id', '[0-9]+')->name('api.v1.requisition.non-catalog.show');
    Route::get('requisition/non-catalog/payment-list', 'Admin\RequisitionController@paymentList')->name('api.v1.requisition.non-catalog-payment-list');       
    Route::put('requisition/non-catalog/tagging', 'Admin\RequisitionController@updateStatusTagging')->where('id', '[0-9]+')->name('api.v1.requisition.non-catalog.tagging');


    /*
    |--------------------------------------------------------------------------
    | User Profile Routes
    |--------------------------------------------------------------------------
    */
    Route::get('user-profile', 'Admin\UserProfileController@userDetails')->name('api.v1.user.profile');
    Route::put('edit-profile', 'Admin\UserProfileController@editProfile')->name('api.v1.edit.profile');

    /*
    |--------------------------------------------------------------------------
    | Insurer Routes
    |--------------------------------------------------------------------------
    */
    Route::get('insurer/{id}', 'Admin\InsurerController@getInsurer')->where('id', '[0-9]+')->name('api.v1.insurer.show');
    Route::post('insurer/store', 'Admin\InsurerController@store')->name('api.v1.insurer.store');
    Route::put('insurer/update', 'Admin\InsurerController@update')->name('api.v1.insurer.update');
    Route::get('insurer/delete/{id}', 'Admin\InsurerController@delete')->where('id', '[0-9]+')->name('api.v1.insurer.delete');
    Route::get('insurer/list', 'Admin\InsurerController@list')->name('api.v1.insurer.list');
    Route::get('insurer/all', 'Admin\InsurerController@all')->name('api.v1.insurer.all');

    /*
    |--------------------------------------------------------------------------
    | Policy Routes
    |--------------------------------------------------------------------------
    */
    Route::get('policy/{id}', 'Admin\InsurancePolicYController@getInsurancePolicy')->where('id', '[0-9]+')->name('api.v1.policy.show');
    Route::post('policy/store', 'Admin\InsurancePolicYController@store')->name('api.v1.policy.store');
    Route::put('policy/update', 'Admin\InsurancePolicYController@update')->name('api.v1.policy.update');
    Route::get('policy/delete/{id}', 'Admin\InsurancePolicYController@delete')->where('id', '[0-9]+')->name('api.v1.policy.delete');
    Route::get('policy/list', 'Admin\InsurancePolicYController@list')->name('api.v1.policy.list');
    Route::get('policy/all', 'Admin\InsurancePolicYController@all')->name('api.v1.policy.all');

    /*
    |--------------------------------------------------------------------------
    | Auth Routes
    |--------------------------------------------------------------------------
    */
    Route::get('logout', 'Admin\AuthController@logout')->name('api.v1.auth.logout');    
});
