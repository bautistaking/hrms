<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'], function () 
{
    /*
    |--------------------------------------------------------------------------
    | Employee Routes
    |--------------------------------------------------------------------------
    */    
    Route::post('employee-login', 'App\AuthController@login')->name('api.v1.employee.login');
    Route::post('employee/time-in', 'App\EmployeeController@timeIn')->name('api.v1.employee.timeIn');
    Route::post('employee/manual/time-in', 'App\EmployeeController@manualTimeInOut')->name('api.v1.employee.manual.timeIn');
    Route::put('employee/time-out', 'App\EmployeeController@timeOut')->name('api.v1.employee.timeOut');

    /*
    |--------------------------------------------------------------------------
    | Look-Ups
    |--------------------------------------------------------------------------
    */
    Route::get('lookup/getCountries', 'App\LookUpController@getCountries')->name('api.v1.lookup.getCountries');
    Route::get('lookup/getRegions', 'App\LookUpController@getRegions')->name('api.v1.lookup.getRegions');
    Route::post('lookup/getProvinces', 'App\LookUpController@getProvinces')->name('api.v1.lookup.getProvinces');
    Route::post('lookup/getCities', 'App\LookUpController@getCities')->name('api.v1.lookup.getCities');
    Route::post('lookup/getBarangays', 'App\LookUpController@getBarangays')->name('api.v1.lookup.getBarangays');
    Route::post('lookup/checkZipCode', 'App\LookUpController@checkZipCode')->name('api.v1.lookup.checkZipCode');
    Route::get('lookup/clients', 'App\LookUpController@getClient')->name('api.v1.lookup.getClient');
    Route::put('lookup/job-orders', 'App\LookUpController@getJobOrder')->name('api.v1.lookup.job-orders');
    Route::put('lookup/site-list', 'App\LookUpController@getSiteList')->name('api.v1.lookup.site-list');
    Route::get('lookup/logistics', 'App\LookUpController@getLogistics')->name('api.v1.lookup.logistics');
    Route::get('lookup/suppliers', 'App\LookUpController@getSuppliers')->name('api.v1.lookup.suppliers');
    Route::get('lookup/request-list', 'App\LookUpController@getRequestList')->name('api.v1.lookup.request-list');
    Route::get('lookup/team-list', 'App\LookUpController@getTeamList')->name('api.v1.lookup.team-list');
    Route::get('lookup/service-tickets', 'App\LookUpController@serviceTickets')->name('api.v1.lookup.service-tickets');
    Route::get('lookup/category/{module}', 'App\LookUpController@getCategory')->name('api.v1.lookup.category.all');
    Route::get('lookup/insurers', 'App\LookUpController@getInsurers')->name('api.v1.lookup.insurers');
    Route::get('lookup/policies', 'App\LookUpController@getPolicies')->name('api.v1.lookup.policies');
    Route::get('lookup/statuses', 'App\LookUpController@requestStatuses')->name('api.v1.lookup.statuses');

    /*
    |--------------------------------------------------------------------------
    | Team Routes
    |--------------------------------------------------------------------------
    */    
    Route::post('team-login', 'Iocc\AuthController@login')->name('api.v1.team.login');

});

Route::group(['prefix' => 'v1', 'middleware' => ['auth:api_employee']], function () 
{
    /*
    |--------------------------------------------------------------------------
    | Auth Routes
    |--------------------------------------------------------------------------
    */
    Route::get('employee-logout', 'App\AuthController@logout')->name('api.v1.employee.logout');   

    /*
    |--------------------------------------------------------------------------
    | Employee Routes
    |--------------------------------------------------------------------------
    */
    Route::get('employee/details', 'App\EmployeeController@details')->name('api.v1.employee.details');
    Route::get('employee/attendance-logs', 'App\EmployeeController@attendanceLog')->name('api.v1.employee.attendance.logs');
    Route::post('employee/update-profile', 'App\EmployeeController@updateProfile')->name('api.v1.employee.update.profile');
    Route::get('employee/loans', 'App\EmployeeController@getLoans')->name('api.v1.employee.loans');
    Route::get('employee/payslip', 'App\EmployeeController@getPayslip')->name('api.v1.employee.payslip');

    /*
    |--------------------------------------------------------------------------
    | Request Routes
    |--------------------------------------------------------------------------
    */
    Route::get('employee/list/over-time', 'App\RequestController@getOverTime')->name('api.v1.request.list.over.time');
    Route::get('employee/list/official-bussiness', 'App\RequestController@getOfficialBusiness')->name('api.v1.request.list.official.bussiness');
    Route::get('employee/list/vacation-leave', 'App\RequestController@getVacationLeave')->name('api.v1.request.list.vacation.leave');
    Route::get('employee/list/sick-leave', 'App\RequestController@getSickLeave')->name('api.v1.request.list.sick.leave');
    Route::get('employee/list/other-leave', 'App\RequestController@getOtherLeave')->name('api.v1.request.list.other.leave');
    Route::get('employee/request/{id}', 'App\RequestController@getDetails')->where('id', '[0-9]+')->name('api.v1.request.details');
    Route::post('employee/request/add', 'App\RequestController@store')->name('api.v1.request.store');
    Route::post('employee/request/update', 'App\RequestController@update')->name('api.v1.request.update');
    Route::get('employee/request/delete/{id}', 'App\RequestController@delete')->where('id', '[0-9]+')->name('api.v1.request.delete');

    /*
    |--------------------------------------------------------------------------
    | Requisition Routes
    |--------------------------------------------------------------------------
    */
    Route::get('requisition/catalog/list', 'App\RequisitionController@list')->name('api.v1.requisition.list');
    Route::post('requisition/catalog/add', 'App\RequisitionController@store')->name('api.v1.requisition.add');
    Route::get('requisition/catalog/{id}', 'App\RequisitionController@getDetails')->where('id', '[0-9]+')->name('api.v1.requisition.details');
    Route::put('requisition/catalog/update', 'App\RequisitionController@update')->name('api.v1.requisition.update');
    Route::put('requisition/catalog/update-status', 'App\RequisitionController@updateStatus')->name('api.v1.requisition.update-status');
    Route::get('requisition/catalog-request/list', 'App\RequisitionController@catalogRequests')->name('api.v1.requisition.catalog-request-list');
    Route::put('requisition/catalog-request/update', 'App\RequisitionController@updateCatalogRequest')->name('api.v1.requisition.catalog-request-update');
    Route::get('requisition/catalog/details/delete/{id}', 'App\RequisitionController@deleteDetails')->name('api.v1.requisition.delete-details');
    Route::get('requisition/catalog/details/delete-attachment/{id}', 'App\RequisitionController@deleteAttachment')->name('api.v1.requisition.delete-attachment');
    Route::get('requisition/catalog/delete/{id}', 'App\RequisitionController@delete')->name('api.v1.requisition.delete');
    Route::post('requisition/purchase-order/create', 'App\RequisitionController@createPoNumber')->name('api.v1.requisition.purchase-order.create');
    Route::put('requisition/catalog/request-payment', 'App\RequisitionController@requestPayment')->name('api.v1.requisition.catalog-request-payment');
    Route::get('requisition/non-catalog-request/list', 'App\RequisitionController@nonCatalogRequests')->name('api.v1.requisition.non-catalog-request-list');
    Route::get('requisition/non-catalog-request/{id}', 'Admin\RequisitionController@paymentDetails')->where('id', '[0-9]+')->name('api.v1.requisition.non-catalog-request.show');
    Route::put('requisition/non-catalog-request/tagging', 'Admin\RequisitionController@updateStatusTagging')->where('id', '[0-9]+')->name('api.v1.requisition.non-catalog-request.tagging');
    Route::post('requisition/download-csv', 'App\RequisitionController@downloadReport')->name('api.v1.requisition.download-csv');

    /*
    |--------------------------------------------------------------------------
    | Logistics Routes
    |--------------------------------------------------------------------------
    */
    // Route::get('requisition/logistic/list', 'App\LogisticController@list')->name('api.v1.requisition.logistic.list');
    Route::get('requisition/logistic/list', 'App\LogisticController@list')->name('api.v1.requisition.logistic.list');
    Route::get('requisition/logistic/{id}', 'App\LogisticController@getDetails')->where('id', '[0-9]+')->name('api.v1.requisition.logistic.show');
    Route::post('requisition/logistic/store', 'App\LogisticController@store')->name('api.v1.requisition.logistic.store');
    Route::put('requisition/logistic/update', 'App\LogisticController@update')->name('api.v1.requisition.logistic.update');
    Route::get('requisition/logistic/delete/{id}', 'App\LogisticController@delete')->where('id', '[0-9]+')->name('api.v1.requisition.logistic.delete');
    Route::post('requisition/logistic/batch-upload', 'App\LogisticController@batchUpload')->name('api.v1.requisition.logistic.batch-upload');    
    Route::get('requisition/logistic/download-csv', 'App\LogisticController@downloadCsv')->name('api.v1.requisition.logistic.download-csv');    
    /*
    |--------------------------------------------------------------------------
    | Inbound Routes
    |--------------------------------------------------------------------------
    */
    Route::get('requisition/inbound/list', 'App\LogisticController@inboundList')->name('api.v1.requisition.inbound.list');
    Route::post('requisition/inbound/search-po', 'App\LogisticController@getPoDetails')->name('api.v1.requisition.inbound.search-po');
    Route::post('requisition/inbound/store', 'App\LogisticController@storeInbound')->name('api.v1.requisition.inbound.store');
    Route::post('requisition/inbound/validate', 'App\LogisticController@validateInbound')->name('api.v1.requisition.inbound.validate');
    
    /*
    |--------------------------------------------------------------------------
    | Outbound Routes
    |--------------------------------------------------------------------------
    */
    Route::get('requisition/outbound/list', 'App\LogisticController@outboundList')->name('api.v1.requisition.outbound-list');
    Route::post('requisition/outbound/store', 'App\LogisticController@storeOutbound')->name('api.v1.requisition.outbound-store');
    Route::put('requisition/outbound/update', 'App\LogisticController@updateOutbound')->name('api.v1.requisition.outbound-update');
    Route::put('requisition/outbound/update-status', 'App\LogisticController@updateStatus')->name('api.v1.requisition.outbound-update-status');
    Route::get('requisition/outbound/delete/{id}', 'App\LogisticController@deleteOutbound')->where('id', '[0-9]+')->name('api.v1.requisition.outbound.delete');
    Route::get('requisition/outbound/delete-details/{id}', 'App\LogisticController@deleteOutboundDetail')->where('id', '[0-9]+')->name('api.v1.requisition.outbound.delete-detail');

    /*
    |--------------------------------------------------------------------------
    | Suppliers Routes
    |--------------------------------------------------------------------------
    */
    Route::get('requisition/supplier/list', 'App\SupplierControlller@list')->name('api.v1.requisition.supplier.list');
    Route::get('requisition/supplier/{id}', 'App\SupplierControlller@getDetails')->where('id', '[0-9]+')->name('api.v1.requisition.supplier.show');
    Route::post('requisition/supplier/store', 'App\SupplierControlller@store')->name('api.v1.requisition.supplier.store');
    Route::put('requisition/supplier/update', 'App\SupplierControlller@update')->name('api.v1.requisition.supplier.update');
    Route::get('requisition/supplier/delete/{id}', 'App\SupplierControlller@delete')->where('id', '[0-9]+')->name('api.v1.requisition.supplier.delete');
    Route::post('requisition/supplier/batch-upload', 'App\SupplierControlller@batchUpload')->name('api.v1.requisition.supplier.batch-upload');    
    Route::get('requisition/supplier/download-csv', 'App\SupplierControlller@downloadCsv')->name('api.v1.requisition.supplier.download-csv');    

    Route::get('requisition/request/list', 'App\RequisitionController@requestsList')->name('api.v1.requisition.request-list');
    Route::put('requisition/request/update', 'App\RequisitionController@updateRequest')->name('api.v1.requisition.request-update');

    Route::get('requisition/non-catalog/list', 'App\RequisitionController@listNonCatalog')->name('api.v1.requisition.non-catalog-list');       
    Route::post('requisition/non-catalog/add', 'App\RequisitionController@storeNonCatalog')->name('api.v1.requisition.non-catalog-add');
    Route::put('requisition/non-catalog/update', 'App\RequisitionController@updateNonCatalog')->name('api.v1.requisition.non-catalog-update');
    Route::get('requisition/non-catalog/delete/{id}', 'App\RequisitionController@deleteNonCatalog')->name('api.v1.requisition.non-catalog-delete');
    Route::put('requisition/non-catalog-request/update', 'App\RequisitionController@updateNonCatalogRequest')->name('api.v1.requisition.non-catalog-request.update');
    Route::put('requisition/non-catalog-request/liquidate', 'App\RequisitionController@liquidateNonCatalogRequest')->name('api.v1.requisition.non-catalog-request.liquidate');

    /*
    |--------------------------------------------------------------------------
    | Team Routes
    |--------------------------------------------------------------------------
    */
    Route::get('tickets/{id}', 'Iocc\TicketController@getDetails')->where('id', '[0-9]+')->name('api.v1.tickets.show');
    Route::get('tickets/list', 'Iocc\TicketController@list')->name('api.v1.tickets.list');
    Route::post('tickets/update', 'Iocc\TicketController@update')->name('api.v1.tickets.update');
    Route::post('tickets/update-status', 'Iocc\TicketController@updateStatusByDate')->name('api.v1.tickets.update.status');
    Route::post('tickets/site/update', 'Iocc\TicketController@updateSiteStatus')->name('api.v1.tickets.site.update');
    Route::post('tickets/create', 'Iocc\TicketController@create')->name('api.v1.tickets.create');
    Route::get('tickets/delete/{id}', 'Iocc\TicketController@delete')->name('api.v1.tickets.delete');
    Route::post('tickets/remarks/save', 'Iocc\TicketController@saveRemarks')->name('api.v1.tickets.remarks.save');
    Route::get('tickets/remark/delete/{id}', 'Iocc\TicketController@deleteRemarks')->name('api.v1.tickets.remark.delete');
    Route::get('tickets/references', 'Iocc\TicketController@getReferences')->name('api.v1.tickets.references');

    /*
    |--------------------------------------------------------------------------
    | Teams Routes
    |--------------------------------------------------------------------------
    */
    Route::get('iocc/team/{id}', 'Admin\TeamController@getDetails')->where('id', '[0-9]+')->name('api.v1.iocc-team.show');
    Route::get('iocc/team/list', 'Admin\TeamController@list')->name('api.v1.iocc-team.list');    
    Route::post('iocc/team/store', 'Admin\TeamController@store')->name('api.v1.iocc-team.store');    
    Route::get('iocc/team/delete/{id}', 'Admin\TeamController@delete')->where('id', '[0-9]+')->name('api.v1.iocc-team.delete');

    /*
    |--------------------------------------------------------------------------
    | IOCC Dashboard Routes
    |--------------------------------------------------------------------------
    */
    Route::get('iocc/dashboard/site-list', 'Iocc\DashboardController@siteList')->name('api.v1.iocc.dashboard.site-list');
    Route::get('iocc/dashboard/quick-issues', 'Iocc\DashboardController@quickIssues')->name('api.v1.iocc.dashboard.quick-issues');
    Route::get('iocc/dashboard/regional-sites', 'Iocc\DashboardController@regionalSites')->name('api.v1.iocc.dashboard.regional-sites');
    Route::get('iocc/dashboard/total-sites', 'Iocc\DashboardController@totalSites')->name('api.v1.iocc.dashboard.total-sites');
    Route::get('iocc/dashboard/fishbone-funnel', 'Iocc\DashboardController@fishboneFunnel')->name('api.v1.iocc.dashboard.fishbone-funnel');
    Route::get('iocc/dashboard/service-status', 'Iocc\DashboardController@servicestatus')->name('api.v1.iocc.dashboard.service-status');

    /*
    |--------------------------------------------------------------------------
    | Categories Routes
    |--------------------------------------------------------------------------
    */
    Route::get('inventory/category/{id}', 'App\CategoryController@getCategory')->where('id', '[0-9]+')->name('api.v1.inventory.category.show');
    Route::post('inventory/category/store', 'App\CategoryController@store')->name('api.v1.inventory.category.store');
    Route::put('inventory/category/update', 'App\CategoryController@update')->name('api.v1.inventory.category.update');
    Route::get('inventory/category/delete/{id}', 'App\CategoryController@delete')->where('id', '[0-9]+')->name('api.v1.inventory.category.delete');
    Route::get('inventory/category/list/{module}', 'App\CategoryController@list')->name('api.v1.inventory.category.list');
    
    /*
    |--------------------------------------------------------------------------
    | Approver Non-Catalog Routes
    |--------------------------------------------------------------------------
    */
    Route::get('request/non-catalog/payment-list', 'Admin\RequisitionController@paymentList')->name('api.v1.request.non-catalog-payment-list'); 
    Route::get('request/non-catalog/{id}', 'Admin\RequisitionController@paymentDetails')->where('id', '[0-9]+')->name('api.v1.request.non-catalog.show');
    Route::put('request/non-catalog/tagging', 'Admin\RequisitionController@updateStatusTagging')->where('id', '[0-9]+')->name('api.v1.request.non-catalog.tagging');
    Route::put('request/non-catalog/bulk-update', 'Admin\RequisitionController@paymentBulkUpdate')->where('id', '[0-9]+')->name('api.v1.request.non-catalog.bulk-update');
    
    /*
    |--------------------------------------------------------------------------
    | Approver Catalog Routes
    |--------------------------------------------------------------------------
    */
    Route::get('request/catalog/{id}', 'Admin\RequisitionController@details')->where('id', '[0-9]+')->name('api.v1.request.catalog.show');
    Route::get('request/catalog/list', 'Admin\RequisitionController@list')->name('api.v1.request.catalog.list');
    Route::put('request/catalog/update', 'Admin\RequisitionController@update')->name('api.v1.request.catalog.update');
    Route::put('request/catalog/bulk-update', 'Admin\RequisitionController@bulkUpdate')->name('api.v1.request.catalog.bulk-update');

    /*
    |--------------------------------------------------------------------------
    | Collections Routes
    |--------------------------------------------------------------------------
    */
    Route::get('request/collection/{id}', 'App\CollectionController@details')->where('id', '[0-9]+')->name('api.v1.request.collection.show');
    Route::get('request/collection/list', 'App\CollectionController@list')->name('api.v1.request.collection.list');
    Route::post('request/collection/store', 'App\CollectionController@store')->name('api.v1.inventory.collection.store');
    Route::put('request/collection/update', 'App\CollectionController@update')->name('api.v1.request.collection.update');
    Route::post('request/collection/download-csv', 'App\CollectionController@downloadCsv')->name('api.v1.request.collection.download-csv');

    /*
    |--------------------------------------------------------------------------
    | Employee Time Keeping
    |--------------------------------------------------------------------------
    */
    Route::get('employee-time-keeping/list', 'App\EmployeeController@attendanceLogs')->name('api.v1.employee-time-keeping.list');
    Route::get('employee-time-keeping/all', 'App\EmployeeController@attendanceLogs')->name('api.v1.employee-time-keeping.all');
    Route::post('employee-time-keeping/download-csv', 'Admin\TimeKeepingController@downloadCsv')->name('api.v1.employee-time-keeping.download-csv');

    /*
    |--------------------------------------------------------------------------
    | Warehouse Routes
    |--------------------------------------------------------------------------
    */
    Route::get('warehouse/get-purchase-orders', 'App\WarehouseController@getPurchaseOrders')->name('api.v1.warehouse.get-purchase-orders');
    Route::get('warehouse/get-outbound-request', 'App\WarehouseController@getOutboundRequest')->name('api.v1.warehouse.get-outbound-request');
    Route::get('warehouse/get-inbound-request', 'App\WarehouseController@getInboundRequest')->name('api.v1.warehouse.get-inbound-request');
    Route::get('warehouse/get-inventories', 'App\WarehouseController@getInventory')->name('api.v1.warehouse.get-inventories');
    Route::get('warehouse/get-totals', 'App\WarehouseController@getTotalRequest')->name('api.v1.warehouse.get-totals');
    Route::post('warehouse/inbound/verified', 'App\WarehouseController@verifiedInbound')->name('api.v1.warehouse.inbound.verified');
    Route::post('warehouse/outbound/verified', 'App\WarehouseController@verifiedOutbound')->name('api.v1.warehouse.outbound.verified');
    
});

Route::group(['prefix' => 'v1', 'middleware' => ['auth:api_team']], function () 
{
    /*
    |--------------------------------------------------------------------------
    | Auth Routes
    |--------------------------------------------------------------------------
    */
    Route::get('team-logout', 'Iocc\AuthController@logout')->name('api.v1.team.logout');   
    /*
    |--------------------------------------------------------------------------
    | Team Routes
    |--------------------------------------------------------------------------
    */
    Route::get('team/details', 'Iocc\TeamController@getDetails')->name('api.v1.team.details');
    Route::get('team/tickets', 'Iocc\TeamController@getTickets')->name('api.v1.team.tickets');
    Route::post('team/ticket/request-update', 'Iocc\TeamController@requestUpdate')->name('api.v1.team.request-update');
    Route::post('team/update-members', 'Iocc\TeamController@updateMembers')->name('api.v1.team.update-members');

});