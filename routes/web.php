<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/purchase-order/{po_number}', 'Web\PrintController@purchaseOrder');
Route::get('/payment-form/{po_number}', 'Web\PrintController@paymentForm');
Route::get('/gatepass/{request_number}', 'Web\PrintController@gatepass');
Route::get('/download/payslip/{payperiod}/{id}', 'Web\DownloadPdfController@downloadPayslip');

Route::get('/non-catalog-payment/{id}', 'Web\PrintController@printNonCatalog');
Route::get('/report-expenses/{client_id}/{order_id?}/{details_id?}', 'Web\PrintController@printExpensesReport');
Route::get('/download/report-expenses/{client_id}', 'Web\DownloadPdfController@downloadExpensesReport');

Route::get('/admin/{any}', 'SpaController@index')->where('any', '.*');
Route::get('/{any}', 'SpaController@webApp')->where('any', '.*');

